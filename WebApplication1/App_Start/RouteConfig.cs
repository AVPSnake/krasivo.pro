﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication1
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Post",
                "Blog/{year}/{month}/{title}",
                new { controller = "Blog", action = "Post" }
            );

            routes.MapRoute(
                name: "Cart",
                url: "cart",
                defaults: new { controller = "Basket", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Catalog",
                url: "catalog",
                defaults: new { controller = "InventTable", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Item",
                url: "catalog/{id}",
                defaults: new { controller = "InventTable", action = "Details", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Brand",
                url: "brand",
                defaults: new { controller = "Brand", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "BrandItem",
                url: "brand/{id}",
                defaults: new { controller = "Brand", action = "Details", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "SaleAction",
                url: "saleaction",
                defaults: new { controller = "SaleAction", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "SaleActionItem",
                url: "SaleAction/{id}",
                defaults: new { controller = "SaleAction", action = "Details", id = UrlParameter.Optional }
            );


            routes.MapRoute(
                name: "Payment",
                url: "payment",
                defaults: new { controller = "Home", action = "PaymentTerms", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Delivery",
                url: "delivery",
                defaults: new { controller = "Home", action = "Delivery", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Contacts",
                url: "contacts",
                defaults: new { controller = "Home", action = "Contacts", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Shops",
                url: "shops",
                defaults: new { controller = "Home", action = "Shops", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "License",
                url: "license",
                defaults: new { controller = "Home", action = "License", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            
        }
    }
}
