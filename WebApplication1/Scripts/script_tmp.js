﻿$(document).ready(function () {
    basketUpdate();

    $(".check").parent().addClass("show");

    $(".tree").on("click", ".group", function () {
        $(this).siblings("ul").toggleClass("show");
    });

    $(".dis").css("display", "none");

    getCurrCount("-9999px", "0");
    

    //тыкаем в спан рядом с чекбоксом
    $(".check_name").on("click", function () {
        $(this).siblings(".filter").checked('toggle');
        getCurrCount($(this).position().left + $(this).width() + 5, $(this).position().top - 6);
    });

    //тыкаем в чекбокс фильтра
    $(".filter").change(function () {
        getCurrCount($(this).siblings(".check_name").position().left + $(this).siblings(".check_name").width() + 5, $(this).position().top - 6);
    });

    //////////
    //show btn
    //////////
    $(".do_filt").click(function () {

        

        var fs = getFiltStr();

        $(".product_list").load($(".tmpFilter").attr("data-products-url") + '?' + fs, function () {
            
            if ($(".count_text").children("span").text().trim() > $("body").find(".product_item").length) {
                $(".more_block").addClass("vis");
            }
            $(".count_block").removeClass("act").css("left", "-9999px");
            currFilter(fs); //актуальный фильтр  

            setLocation($(".tmpFilter").attr("data-index-url") + "/?" + fs);

            
        });       
    });


    //crear btn
    $(".clear_filt").click(function () {
        //$.cookie('currentFilter', null);
        document.location.href = $(".tmpFilter").attr("data-index-url");
        
    });

    //show_more btn
    $("body").on("click", ".show_more", function (e) {
        e.preventDefault();
        
        var currPos = $(".item_num").last().text().toString();

        $("body #loading").addClass("act");

        var fs = getFiltStr();
        setTimeout(function () {
            
        //console.log(currPos.trim());
            $(".more_block").load($(".tmpFilter").attr("data-products-url") + '?' + fs + '&num2start=' + currPos.trim(), function () {

            
                    $(".more_block").replaceWith($(".more_block").html());

                    if ($(".count_text").children("span").text().trim() > $("body").find(".product_item").length) {
                        $(".more_block").addClass("vis");
                    }
                   
                    $('#loading').removeClass('act');

            });
        }, 1000);
        
    });

    /*add to basket*/
    $("body").on("click", ".add2basket", function (e) {
        e.preventDefault();
        var currId = $(this).parent("td").siblings(".itemId").text().trim();
        var currPrice = $(this).parent("td").siblings(".itemPrice").text().trim();
        add2basket(currId, 1, currPrice, 0);
    });

    $("body").on("click", ".change", function (e) {
        e.preventDefault();
        var currQty = parseInt($(this).siblings(".item_qty").text().trim());
        var tmpQty = 0;
        var currProduct = $(this).parent("td").siblings(".item_id").text().trim();
        var currPrice = $(this).parent("td").siblings(".item_price").text().trim();
        
        if ($(this).hasClass("minus") && currQty > 1) {
            currQty--;
            tmpQty = -1;
            $(this).siblings(".item_qty").text(currQty.toString());
        }
        if ($(this).hasClass("plus") && currQty < 100) {
            currQty++;
            tmpQty = 1;
            $(this).siblings(".item_qty").text(currQty.toString());
        }
        add2basket(currProduct, tmpQty, currPrice, 0);
    });

    $("body").on("click", ".del", function (e) {
        e.preventDefault();
        var currQty = parseInt($(this).parent("td").siblings(".qty").children(".item_qty").text().trim());
        var currProduct = $(this).parent("td").siblings(".item_id").text().trim();
        var currPrice = $(this).parent("td").siblings(".item_price").text().trim();

        var currBtn = $(this);
        add2basket(currProduct, 0, currPrice, 1)
        currBtn.parent("td").parent("tr").remove();

        if ($(".product_item").length == 0) {
            $(".cart_summ").remove();
            $(".order_row").remove();
            $(".table").before("<p> Корзина пуста </p>");
        }
    });

});

function add2basket(_itemId, _count, _price, _overwriteQty) {
    if (_itemId != "" && _price != "") {

        
        //отправляем запрос       
        var url = $(this).attr('action');
        
        var action = '/Krasivo.local/Basket/AddToBasket';

        $.ajax({
            url: action,
            data: ({ itemId: _itemId, qty: _count, price: _price, overwriteQty: _overwriteQty }),
            global: false,
            type: "POST",
            dataType: "text",
            complete: function(data) {
                console.log("Запрос отправлен" + " Ответ:");
                console.log(data);
            },
            success: function(data) {
                //пересчет малой корзины
                basketUpdate();
                $('#loading').removeClass('act');
            },
            fail: function (data) {
                
            }
        });
    }
}

function basketUpdate() {

    

    var url = $(this).attr('action');

    var action = '/Krasivo.local/Basket/GetBasketParams';

    $.ajax({
        url: action,
        global: false,
        type: "POST",
        dataType: "text",
        complete: function (data) {
            //console.log("Запрос отправлен" + " Ответ:");
            console.log(data);
        },
        success: function (data) {
            //пересчет малой корзины
            var currValues = data.split(";");
            if (currValues[0] > 0) {
                $(".basket_content").html('<a href="/Krasivo.local/cart">Товаров: ' + currValues[0].toString() + ' на сумму: ' + currValues[1].toString() + "</a>");
                $(".cart_summ").html("Итого: " + currValues[1].toString() + "руб.");
            }
            else $(".basket_content").html("пока пусто");

            
        },
        fail: function (data) {
            
        }
    });
}

function currFilter(_currentFilterString) {
    

    $(".tmpFilter").load($(".tmpFilter").attr("data-filter-items") + '?' + _currentFilterString, function() {
        var tmpData = $(".tmpFilter").text().split(',');
        
        $.each($(".group"), function (index, value) {
            var currGroup = $(this);
            var tmpCount = $(currGroup).siblings("ul").children(".item").length;
            //alert(tmpCount.toString());

            $.each($(currGroup).siblings("ul").children(".item").children(".filter"), function (index, value) {
                var filtItem = $(this);
                var tmpStr = filtItem.attr("name") + "=" + filtItem.attr("value");

                if ($.inArray(tmpStr, tmpData) == -1) {
                    filtItem.parent(".item").addClass("dis");
                }
                else {
                    filtItem.parent(".item").removeClass("dis").css("display", "block");
                }
            });

            if ($(currGroup).siblings("ul").children(".dis").length == tmpCount) {
                $(this).addClass("dis");
            }
            else {
                $(this).removeClass("dis").css("display", "block");
            }
        });
            
        $(".dis").css("display", "none");

        
        
    });
}

function getCurrCount(_left, _top) {

    

    var fs = getFiltStr();
    
    $(".count_text").children("span").load($(".tmpFilter").attr("data-filter-url") + '?' + fs, function () {
        if ($(".count_text").children("span").text().trim() > $("body").find(".product_item").length) {       
            $(".more_block").addClass("vis");
        }
        
    });
    $(".count_block").addClass("act");
    $(".count_block.act").css("left", _left);
    $(".count_block.act").css("top", _top);
}

(function ($) {

    $.fn.checked = function (value) {

        if (value === true || value === false) {
            // Set the value of the checkbox
            $(this).each(function () { this.checked = value; });
        } else if (value === undefined || value === 'toggle') {
            // Toggle the checkbox
            $(this).each(function () { this.checked = !this.checked; });
        }
    };

})(jQuery);


function setLocation(curLoc) {
    try {
        history.pushState(null, null, curLoc);
        return;
    } catch (e) { }
    location.hash = '#' + curLoc;
}

function getFiltStr() {

    var fstr = "";
    
    //тут надо парсить фильтр и передавать его в контроллер
    $('.filter').each(function (i, elem) {
        if ($(this).prop('checked')) {

            if (fstr == "")
                fstr += $(this).attr("name") + "=" + $(this).attr("id");
            else
                fstr += "&" + $(this).attr("name") + "=" + $(this).attr("id");

        }
    });

    return encodeURI(fstr);
}

