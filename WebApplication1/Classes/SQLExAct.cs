﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Web;
using System.Data;

using System.Data.SqlClient;
using WebApplication1.Models.ViewModels;
using WebApplication1.Models;
using System.Net;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Security.Cryptography;
using System.Xml;
using System.Globalization;
using System.Text.RegularExpressions;

namespace WebApplication1.Classes
{
    #region Const

    public enum InventJournalType { Movement = 0, Lossprofit = 1, Reject = 2 };
    public enum GridRefreshType { AddRow = 0, UpdateRow = 1, RemoveRow = 2, Refresh = 3 };
    public enum TransferStatus { Created = 0, Shipped = 1, Posted = 2, Received = 3, All = 4 };
    public enum Sex { Male = 1, Female = 2, None = 0 };
    public enum DocumentType { None = 0, Inventory = 1, Movement = 2, Transfer = 3 }
    public enum ItemType { None = 0, Item = 1, Service = 2 };
    public enum SaleDocumentTypeId : int { None = -1, Sale = 0, Return = 1 };//, Facility = 2 };
    public enum AccountableDocumentType : int { None = 0, CashFlowOut = 1, CashFlowIn = 2 }
    public enum PurchDocumentType : int { Purch = 0, PurchRet = 1, PurchAdd = 2 }
    public enum InnerCompanyId : int { FS = 0, BI = 1 }

    public enum PaymentTypeId : int { Cash = 0, Card = 1 };
    public enum CashDepartment : int { Item = 1, Service = 2 };
    public enum SaleStatus : int
    {
        None = -2,
        Cancel = -1,
        Edit = 0,
        Aside = 1,
        Post = 2,
        Picked = 3,
        SentToDelivery = 4,
        Delivered = 5,
        Storno = 6,
        Received = 7
    };

    public enum ItemInitType : int { ItemId = 0, UrlSlug = 1 };

    public enum ItemSearchType : int { Barcode = 0, Name = 1 };

    public struct UserInfo
    {
        public int userId;
        public string name;
        public int roleId;
        public string password;
    }

    public enum InventImageType : int
    {
        None = 0,
        Catalog = 1,
        Primary = 2,
        Secondary = 3
    };
    

    #endregion

    public class StoredProcedure
    {
        //SynchronizationContext uiContext;
        string connectionString = ""; //@"Data Source=SNAKE;Initial Catalog=Aval;Integrated Security=True";

        public bool ok = true;

        public StoredProcedure()
        {
            connectionString = @"Data Source=SQL2;Initial Catalog=ISHOP;User Id = ISHOP; Password = GjkysqGbpltw19";
        }

        #region templates

        SqlConnection CreateSQLConnection()
        {
            SqlConnection con = null;
            try
            {
                con = new SqlConnection(connectionString);

                con.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Не удалось установить соединение с базой данных\n" + ex.Message);
            }
            return con;
        }

        public SqlCommand CreateTemplateSQLCommand(CommandType _commandType = CommandType.StoredProcedure)
        {
            SqlCommand command = null;

            ok = true;

            try
            {
                SqlConnection con = CreateSQLConnection();

                command = new SqlCommand();
                command.CommandType = _commandType;
                command.CommandTimeout = 1000;
                command.Connection = con;
                command.Transaction = con.BeginTransaction(IsolationLevel.Serializable);
            }
            catch (Exception ex)
            {
                ok = false;

                throw new Exception("Не удалось создать шаблон команды базы данных\n" + ex.Message);
            }

            return command;
        }

        public void CloseSQLCommand(SqlCommand _command)
        {
            try
            {
                if (_command == null)
                    return;

                if (_command.Transaction != null)
                {
                    if (ok)
                        _command.Transaction.Commit();
                    else
                        _command.Transaction.Rollback();
                }

                if (_command.Connection.State != ConnectionState.Closed)
                {
                    _command.Connection.Close();
                    _command.Connection.Dispose();
                }
            }
            catch (Exception ex)
            {
                ok = false;

                //MessageBox.Show(ex.Message);
            }
            GC.Collect();
        }

        public void ExecuteNonQuery(SqlCommand _c)
        {
            try
            {
                _c.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ok = false;
                throw new Exception("Ошибка вызова хранимой процедуры:\n" + _c.CommandText + "\n" + ex.Message);
            }
        }

        public SqlDataReader ExecuteReader(SqlCommand _c)
        {
            SqlDataReader r = null;

            try
            {
                r = _c.ExecuteReader();
            }
            catch (Exception ex)
            {
                ok = false;
                throw new Exception("Ошибка вызова хранимой процедуры:\n" + _c.CommandText + "\n" + ex.Message);
            }

            return r;
        }

        public bool OK
        {
            get { return ok; }
        }
        #endregion
    }
    #region ItemInfo
    public class ItemInfo
    {
        string itemId = "";
        string name = "";
        string article = "";
        int remainQty = 0;
        int reservQty = 0;

        ItemType itemType;
        string discCardId = "";
        double price;

        int documentQty;
        int parentQty;
        string documentId;

        //string emplInn = "";
        //string masterInn = "";

        string shortDescription = string.Empty;
        string description = string.Empty;
        string urlSlug = string.Empty;

        double oldPrice = 0;
        DateTime priceExpDate = DateTime.MinValue;

        string userId = "";

        public ItemInfo(string _itemId, string _userId = "")
        {
            this.itemId = _itemId;

            this.userId = _userId;

            Init();
        }

        public ItemInfo(string parm, ItemInitType _initType, string _userId = "")
        {
            this.userId = _userId;

            switch (_initType)
            {
                case ItemInitType.UrlSlug:
                    {
                        this.UrlSlug = parm;

                        InitByUrlSlug();
                        break;
                    }
                default:
                    {
                        this.itemId = parm;

                        Init();
                        break;
                    }
            }
        }


        public string ShortDescription
        {
            set { shortDescription = value; }
            get { return shortDescription; }
        }

        public string Description
        {
            set { description = value; }
            get { return description; }
        }

        public string UrlSlug
        {
            set { urlSlug = value; }
            get { return urlSlug; }
        }

        public string DocumentId
        {
            set { documentId = value; }
            get { return documentId; }
        }

        public string DiscCardId
        {
            set
            {
                discCardId = value;

                //Init();
            }
        }


        public double SaleLineAmount
        {
            get { return documentQty * price; }
        }


        void RefreshRemain()
        {
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;

            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "bf_GetItemRemain";

                c.Parameters.Add("itemId", SqlDbType.NVarChar).Value = itemId;

                SqlParameter remain = c.Parameters.Add("remain", SqlDbType.Int);
                remain.Direction = ParameterDirection.Output;

                SqlParameter reserv = c.Parameters.Add("reserv", SqlDbType.Int);
                reserv.Direction = ParameterDirection.Output;

                p.ExecuteNonQuery(c);

                Int32.TryParse(remain.Value.ToString(), out remainQty);
                Int32.TryParse(reserv.Value.ToString(), out reservQty);
            }
            catch (Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка обновления остатка", ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

        }

        public int DocumentQty
        {
            get { return documentQty; }
            set { documentQty = value; }
        }

        public int ParentQty
        {
            get { return parentQty; }
            set { parentQty = value; }
        }

        public double Price
        {
            get { return price; }
            set { price = value; }//для случаев года есть ценовой штрих код
        }

        public ItemType ItemType
        {
            get { return itemType; }
        }

        public string ItemId
        {
            get { return itemId; }
        }

        public string Name
        {
            get { return name; }
        }

        public string Article
        {
            get { return article; }
        }

        public int RemainQty
        {
            get { return remainQty; }
        }

        public int ReservQty
        {
            get { return reservQty; }
        }

        public int AvailQty
        {
            get { return remainQty - reservQty; }
        }

        public bool OK
        {
            get { return itemId != ""; }
        }

        public double OldPrice
        {
            get { return oldPrice; }
            set { oldPrice = value; }//для случаев года есть ценовой штрих код
        }

        public DateTime PriceExpDate
        {
            get { return priceExpDate; }
            set { priceExpDate = value; }
        }

        public string UserId
        {
            set
            {
                userId = value;

                Init();
            }
        }

        /*
        public string EmplInn
        {
            get { return emplInn; }
            set { emplInn = value; }
        }

        public string MasterInn
        {
            get { return masterInn; }
            set { masterInn = value; }
        }
        */
        void Init()
        {
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;

            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetItemInfo";

                c.Parameters.Add("itemId", SqlDbType.NVarChar).Value = itemId;
//              c.Parameters.Add("discCardId", SqlDbType.NVarChar).Value = discCardId;
                c.Parameters.Add("userId", SqlDbType.NVarChar).Value = userId;

                SqlParameter pr = c.Parameters.Add("name", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 100;

                pr = c.Parameters.Add("article", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 50;

                pr = c.Parameters.Add("urlSlug", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 20;

                pr = c.Parameters.Add("shortDescription", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 255;

                pr = c.Parameters.Add("description", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 4000;

                pr = c.Parameters.Add("price", SqlDbType.Decimal);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 18;
                pr.Scale = 2;

                pr = c.Parameters.Add("OldPrice", SqlDbType.Decimal);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 18;
                pr.Scale = 2;

                pr = c.Parameters.Add("PriceExpDate", SqlDbType.DateTime);
                pr.Direction = ParameterDirection.Output;


                c.Parameters.Add("remainQty", SqlDbType.Int).Direction = ParameterDirection.Output;
                c.Parameters.Add("reservQty", SqlDbType.Int).Direction = ParameterDirection.Output;
                c.Parameters.Add("itemType", SqlDbType.Int).Direction = ParameterDirection.Output;

                /*
                pr = c.Parameters.Add("EmplInn", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 12;

                pr = c.Parameters.Add("MasterlInn", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 12;
                */

                c.ExecuteNonQuery();

                name = c.Parameters["name"].Value.ToString();
                article = c.Parameters["article"].Value.ToString();

                shortDescription = c.Parameters["shortDescription"].Value.ToString();
                description = c.Parameters["description"].Value.ToString();
                itemId = c.Parameters["itemId"].Value.ToString();

                Double.TryParse(c.Parameters["price"].Value.ToString(), out price);

                Int32.TryParse(c.Parameters["remainQty"].Value.ToString(), out remainQty);
                Int32.TryParse(c.Parameters["reservQty"].Value.ToString(), out reservQty);

                int it = 0;

                Int32.TryParse(c.Parameters["itemType"].Value.ToString(), out it);

                itemType = (ItemType)it;

                Double.TryParse(c.Parameters["oldPrice"].Value.ToString(), out oldPrice);


                var priceExpDateStr = c.Parameters["priceExpDate"].Value.ToString();

                if (!string.IsNullOrWhiteSpace(priceExpDateStr))
                {
                    DateTime.TryParse(priceExpDateStr, out priceExpDate);
                }

                //emplInn = c.Parameters["EmplInn"].Value.ToString();
                //masterInn = c.Parameters["MasterlInn"].Value.ToString();
            }
            catch (Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка получения данных о номенклатуре " + itemId, ex.Message);
                itemId = "";
            }
            finally
            {
                p.CloseSQLCommand(c);
            }
        }

        void InitByUrlSlug()
        {
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;

            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetItemInfoByUrlSlug";

                c.Parameters.Add("urlSlug", SqlDbType.NVarChar).Value = urlSlug;
//                c.Parameters.Add("discCardId", SqlDbType.NVarChar).Value = discCardId;
                c.Parameters.Add("userId", SqlDbType.NVarChar).Value = userId;

                SqlParameter pr = c.Parameters.Add("name", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 100;

                pr = c.Parameters.Add("article", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 50;

                pr = c.Parameters.Add("itemId", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 20;

                pr = c.Parameters.Add("shortDescription", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 255;

                pr = c.Parameters.Add("description", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 4000;

                pr = c.Parameters.Add("price", SqlDbType.Decimal);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 18;
                pr.Scale = 2;

                pr = c.Parameters.Add("OldPrice", SqlDbType.Decimal);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 18;
                pr.Scale = 2;

                pr = c.Parameters.Add("PriceExpDate", SqlDbType.DateTime);
                pr.Direction = ParameterDirection.Output;


                c.Parameters.Add("remainQty", SqlDbType.Int).Direction = ParameterDirection.Output;
                c.Parameters.Add("reservQty", SqlDbType.Int).Direction = ParameterDirection.Output;
                c.Parameters.Add("itemType", SqlDbType.Int).Direction = ParameterDirection.Output;

                /*
                pr = c.Parameters.Add("EmplInn", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 12;

                pr = c.Parameters.Add("MasterlInn", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 12;
                */

                c.ExecuteNonQuery();

                itemId = c.Parameters["itemId"].Value.ToString();
                name = c.Parameters["name"].Value.ToString();
                article = c.Parameters["article"].Value.ToString();

                shortDescription = c.Parameters["shortDescription"].Value.ToString();
                description = c.Parameters["description"].Value.ToString();
                urlSlug = c.Parameters["urlSlug"].Value.ToString();

                Double.TryParse(c.Parameters["price"].Value.ToString(), out price);

                Int32.TryParse(c.Parameters["remainQty"].Value.ToString(), out remainQty);
                Int32.TryParse(c.Parameters["reservQty"].Value.ToString(), out reservQty);

                int it = 0;

                Int32.TryParse(c.Parameters["itemType"].Value.ToString(), out it);

                itemType = (ItemType)it;

                Double.TryParse(c.Parameters["oldPrice"].Value.ToString(), out oldPrice);

                var priceExpDateStr = c.Parameters["priceExpDate"].Value.ToString();

                if (!string.IsNullOrWhiteSpace(priceExpDateStr))
                {
                    DateTime.TryParse(priceExpDateStr, out priceExpDate);
                }


                //emplInn = c.Parameters["EmplInn"].Value.ToString();
                //masterInn = c.Parameters["MasterlInn"].Value.ToString();
            }
            catch (Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка получения данных о номенклатуре "+urlSlug, ex.Message);
                itemId = "";
            }
            finally
            {
                p.CloseSQLCommand(c);
            }
        }

    }
    #endregion
    #region SaleInfo
    public class SaleInfo
    {
        string custId = "";
        string salesId = "";
        string discCardId = "";
        string discCardTypeId = "";

        //PaymentType pt = (PaymentType)1;

        bool bonusAllowed = false; //разрешено ли накопление/списание бонусов
        //bool posted = false; //документ разнесен

        double bonusAccumulationPercent = 0; //процент начисления бонусов от суммы оплаты
        double bonusRejectPercent = 0; // максимальный процент от суммы покупки, который можно оплатить бонусами

        double invoiceAmount = 0; //сумма накладной

        //CashDepartment cashDepartment = CashDepartment.Item; //номер секциия на фискальнике

        PaymentTypeId paymentTypeId = PaymentTypeId.Card; //тип оплаты нал/карта
        SaleDocumentTypeId documentTypeId = SaleDocumentTypeId.Sale;//тип документа

        double amount4payment = 0;//сумма для оплаты деньгами
        double amount4paymentBonus = 0;//сумма для оплаты бонусами
        double amount4paymentCertif = 0;//сумма для оплаты сертификатом

        double allowedBonusAmount4Payment = 0; //Количество бонусов разрешенное для оплаты товара

        double amountGivenByClient = 0; //сумма к оплате

        double amountDisc = 0;//сумма скидки
        double amountBonus = 0;//сумма полученных от оплаты деньгами бонусов
        double amountBonusPublicity = 0;//сумма бонусов полученных по РА
        double amountDiscPublicity = 0;//сумма скидок полученных по РА

        double custTotalAmount = 0;//сумма покупок клиента за всю историю
        double custBonusAmount = 0;//бонусные баллы клиента
        double bonusTypeBorder = 0;//порог суммы для перехода на следующий тип дисконтной карты

        double charge = 0;//сдача

        string salesIdParent = ""; //предок

        DateTime transDate = DateTime.MinValue;
        string cashId = ""; //номера кассы у нас нет

        SaleStatus salesStatus = SaleStatus.None;

        //инн сотрудника (личные продажи)
        string emplInn = ""; //пока не используется


        //интернет-поля: имя получателя, комментарий, способ оплаты, варианты доставки etc
        string userFirstName = string.Empty;
        string userLastName = string.Empty;
        string userComment = string.Empty;
        string userPhone = string.Empty;

        string custFirstName = string.Empty;
        string custLastName = string.Empty;

        string deliveryTypeId = string.Empty;
        string deliveryTypeDescription = string.Empty;

        string selfDeliveryPointId = string.Empty;
        string selfDeliveryPointName = string.Empty;
        string selfDeliveryPointIdExt = string.Empty;
        string selfDeliveryPointDescription = string.Empty;

        double deliveryCost = 0;
        DateTime deliveryDate = DateTime.MinValue;
        int transportLeg = 0;

        //поля для сбербанка
        string paymServLink = string.Empty;
        string paymServAlias = string.Empty;
        DateTime paymLinkExpDatetime = DateTime.MinValue;
        int paymAttempt = 0;
        string paymServOrderId = string.Empty;
        // дата создания
        DateTime createdDate = DateTime.MinValue;
        //код клиента в интернет-магазине
        string aspCustId = string.Empty;
        //код asp-юзера
        string aspId = string.Empty;

        //поля для доставки
        string cityName = string.Empty;
        string cityId = string.Empty;

        string deliveryModeId = string.Empty;

        double amountWithDelivery = 0;

        string deliveryAddressStreet = string.Empty;
        string deliveryAddressBuilding = string.Empty;
        string deliveryAddressAppt = string.Empty;
        string deliveryAddressGate = string.Empty;
        string deliveryAddressIntercomCode = string.Empty;
        string deliveryAddressLevel = string.Empty;

        string trackNumber = string.Empty;

        public SaleInfo(string _salesId)
        {
            salesId = _salesId;

            Init();
        }
        public SaleInfo()
        {
        }

        public string CustId
        {
            get { return custId; }
        }


        public string PaymentTypeDescription
        {
            get
            {
                string desc = "";

                switch (PaymentTypeId)
                {
                    case (PaymentTypeId.Cash):
                        desc = "Наличные";
                        break;
                    case (PaymentTypeId.Card):
                        desc = "Карта";
                        break;
                }

                return desc;
            }
        }


        public string TypeDescription
        {
            get
            {
                string desc = "";

                switch (DocumentTypeId)
                {
                    case (SaleDocumentTypeId.Sale):
                        desc = "Продажа";
                        break;
                    case (SaleDocumentTypeId.Return):
                        desc = "Возврат";
                        break;
                }

                return desc;
            }
        }

        public string StatusDescription
        {
            get
            {
                /*
                string desc = "";

                switch (Status)
                {
                    case (SaleStatus.Edit):
                        desc = "Создан";
                        break;
                    case (SaleStatus.Aside):
                        desc = "Принят. Ожидает проверки";
                        break;
                    case (SaleStatus.Picked):
                        desc = "Собран и проверен";
                        break;
                    case (SaleStatus.SentToDelivery):
                        desc = "Передан на доставку";
                        break;
                    case (SaleStatus.Delivered):
                        desc = "Доставлен";
                        break;
                    case (SaleStatus.Cancel):
                        desc = "Отменен";
                        break;
                    case (SaleStatus.Post):
                        desc = "Оплачен. Передан на доставку";
                        break;
                    case (SaleStatus.Storno):
                        desc = "Сторнирован";
                        break;
                }

                return desc;
                */
                return DataService.SalesStatusToString(Status);
            }
        }

        public SaleStatus Status
        {
            get { return salesStatus; }
        }

        public DateTime TransDate
        {
            get { return transDate; }
        }

        public string CashId
        {
            get { return cashId; }
        }

        public double BonusTypeBorder
        {
            get { return bonusTypeBorder; }
        }

        public double CustTotalAmount
        {
            get { return custTotalAmount; }
        }

        public string DiscCardId
        {
            get { return discCardId; }
        }

        public double AmountBonus
        {
            get { return amountBonus; }
        }

        public double AmountBonusPublicity
        {
            get { return amountBonusPublicity; }
            set { amountBonusPublicity = value; }
        }

        public double AccruedBonus
        {
            get { return amountBonus + amountBonusPublicity; }
        }

        public double InvoiceAmount
        {
            get { return invoiceAmount; }
        }

        public string SalesIdParent
        {
            get { return salesIdParent; }
        }

        public SaleDocumentTypeId DocumentTypeId
        {
            get { return documentTypeId; }
        }

        public double AmountDisc
        {
            get { return amountDisc; }
        }

        public double AmountDiscPublicity
        {
            get { return amountDiscPublicity; }
            set { amountDiscPublicity = value; }
        }

        public string SalesId
        {
            get { return salesId; }
        }

        public bool Posted
        {
            get { return salesStatus == SaleStatus.Post; }
        }

        public bool BonusAllowed
        {
            get { return bonusAllowed; }
        }

        public double Amount4Payment
        {
            get { return amount4payment; }
        }

        public double Charge
        {
            get { return charge; }
        }

        public double AllowedBonusAmount4Payment
        {
            get { return allowedBonusAmount4Payment; }
        }

        public double CustBonusAmount
        {
            get { return custBonusAmount; }
        }

        public double Amount4paymentBonus
        {
            set
            {
                amount4paymentBonus = value;
                AmountGivenByClient = 0;
                Parce();
            }
            get { return amount4paymentBonus; }
        }

        public double Amount4paymentCertif
        {
            set
            {
                amount4paymentCertif = value;
                amount4paymentBonus = 0;
                //AmountGivenByClient = 0;
                Parce();
            }
            get
            {
                return amount4paymentCertif;
            }
        }

        public double AmountGivenByClient
        {
            set
            {
                amountGivenByClient = value;
                Parce();
            }
            get { return amountGivenByClient; }
        }

        public bool CheckAmounts()
        {
            if (invoiceAmount - amountDisc - amountDiscPublicity > amountGivenByClient + amount4paymentBonus + amount4paymentCertif)
            {
                return false;
            }

            return true;
        }

        public bool NeedCash()
        {
            if (invoiceAmount - amountDisc - amountDiscPublicity > amount4paymentCertif)
                return true;

            return false;
        }

        public PaymentTypeId PaymentTypeId
        {
            set
            {
                paymentTypeId = value;

                /*
                switch (paymentTypeId)
                {
                    case(PaymentTypeId.Cash):
                        cashDepartment = CashDepartment.Cash;
                        break;
                    case (PaymentTypeId.Card):
                        cashDepartment = CashDepartment.Card;
                        break;
                    default:
                        throw new Exception("Тип оплаты не поддерживается.");

                }
                 */
            }

            get { return paymentTypeId; }

        }

        public void Parce()
        {
            if (Posted)//если документ проведен, то все значения уже есть после инита
                return;

            allowedBonusAmount4Payment = 0;
            amount4payment = 0;
            charge = 0;

            //сумма к оплате деньгами
            amount4payment = invoiceAmount;
            amount4payment -= amountDisc;//общая скидка
            amount4payment -= amountDiscPublicity; //скидки по РА

            if (amount4payment <= amount4paymentCertif)
            {
                amount4paymentCertif = amount4payment;
                amount4paymentBonus = 0;
                amount4payment = 0;
                amountGivenByClient = 0;
                return;
            }

            amount4payment -= amount4paymentCertif;//вычтем сумму оплаченную сертификатом

            //сколько мы можем заплатить бонусами
            if (bonusAllowed)
            {
                allowedBonusAmount4Payment = Math.Round(amount4payment * bonusRejectPercent / 100, 2);

                if (allowedBonusAmount4Payment > custBonusAmount)
                    allowedBonusAmount4Payment = Math.Max(custBonusAmount, 0);

                if (allowedBonusAmount4Payment < amount4paymentBonus)
                    amount4paymentBonus = allowedBonusAmount4Payment;

                amount4payment -= amount4paymentBonus; //вычтем бонусные баллы

                if (amount4payment < 0)
                    amount4paymentBonus = 0;

            }

            if (PaymentTypeId == PaymentTypeId.Card)
                amountGivenByClient = amount4payment;

            charge = amountGivenByClient - amount4payment;


            if (bonusAllowed)
                amountBonus = Math.Round(amount4payment * bonusAccumulationPercent / 100, 2);

            //amountWithDelivery = invoiceAmount + deliveryCost;
        }

        public bool OK
        {
            get { return salesId != ""; }
        }

        public string EmplInn
        {
            get { return emplInn; }

            set { emplInn = value; }
        }

        public string UserFirstName
        {
            get { return userFirstName; }

            set { userFirstName = value; }
        }

        public string UserLastName
        {
            get { return userLastName; }

            set { userLastName = value; }
        }


        public string UserComment
        {
            get { return userComment; }

            set { userComment = value; }
        }

        public string CustFirstName
        {
            get { return custFirstName; }
            set { custFirstName = value; }
        }

        public string CustLastName
        {
            get { return custLastName; }
            set { custLastName = value; }
        }

        public string Phone
        {
            get { return userPhone; }
            set { userPhone = value; }
        }

        public string DeliveryTypeId
        {
            get { return deliveryTypeId; }
            set { deliveryTypeId = value; }
        }

        public string DeliveryTypeDescription
        {
            get { return deliveryTypeDescription; }
            set { deliveryTypeDescription = value; }
        }

        public string SelfDeliveryPointId
        {
            get { return selfDeliveryPointId; }
            set { selfDeliveryPointId = value; }
        }

        public string SelfDeliveryPointIdExt
        {
            get { return selfDeliveryPointIdExt; }
            set { selfDeliveryPointIdExt = value; }
        }

        public string SelfDeliveryPointName
        {
            get { return selfDeliveryPointName; }
            set { selfDeliveryPointName = value; }
        }

        public string SelfDeliveryPointDescription
        {
            get { return selfDeliveryPointDescription; }
            set { selfDeliveryPointDescription = value; }
        }

        public double DeliveryCost
        {
            get { return deliveryCost; }
            set { deliveryCost = value; }
        }

        public DateTime DeliveryDate
        {
            get { return deliveryDate; }
            set { deliveryDate = value; }
        }

        public int TransportLeg
        {
            get { return transportLeg; }
            set { transportLeg = value; }
        }

        public double AmountWithDelivery
        {
            get { return amountWithDelivery; }
            set { amountWithDelivery = value; }
        }

        public string PaymServLink
        {
            get { return paymServLink; }
            set { paymServLink = value; }
        }

        public string PaymServAlias
        {
            get { return paymServAlias; }
            set { paymServAlias = value; }
        }

        public DateTime PaymLinkExpDatetime
        {
            get { return paymLinkExpDatetime; }
            set { paymLinkExpDatetime = value; }
        }

        public int PaymAttempt
        {
            get { return paymAttempt; }
            set { paymAttempt = value; }
        }

        public string PaymServOrderId
        {
            get { return paymServOrderId; }
            set { paymServOrderId = value; }
        }


        public DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        public string AspCustId
        {
            get { return aspCustId; }
            set { aspCustId = value; }
        }

        public string AspId
        {
            get { return aspId; }
            set { aspId = value; }
        }

        public string CityName
        {
            get { return cityName; }
            set { cityName = value; }
        }

        public string CityId
        {
            get { return cityId; }
            set { cityId = value; }
        }

        public string DeliveryModeId
        {
            get { return deliveryModeId; }
            set { deliveryModeId = value; }
        }

        public string DeliveryAddressStreet
        {
            get {return deliveryAddressStreet; }
            set { deliveryAddressStreet = value; }
        }
        
        public string DeliveryAddressBuilding
        {
            get { return deliveryAddressBuilding; }
            set { deliveryAddressBuilding = value; }
        }
        
        public string DeliveryAddressAppt
        {
            get { return deliveryAddressAppt; }
            set { deliveryAddressAppt = value; }
        }

        public string DeliveryAddressGate
        {
            get { return deliveryAddressGate; }
            set { deliveryAddressGate = value; }
        }

        public string DeliveryAddressIntercomCode
        {
            get { return deliveryAddressIntercomCode; }
            set { deliveryAddressIntercomCode = value; }
        }


        public string DeliveryAddressLevel
        {
            get { return deliveryAddressLevel; }
            set { deliveryAddressLevel = value; }
        }

        public string TrackNumber
        {
            get { return trackNumber; }
            set { trackNumber = value; }
        }



        public void Init()
        {
            StoredProcedure sp = new StoredProcedure();
            SqlCommand c = null;

            try
            {

                c = sp.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetSaleInfo";

                c.Parameters.Add("SalesId", SqlDbType.NVarChar).Value = salesId;

                SqlParameter p;

                p = c.Parameters.Add("CustId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("SalesIdParent", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("DiscCardId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                //SqlDbType.Xml

                p = c.Parameters.Add("DiscCardTypeId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 10;


                c.Parameters.Add("DocumentTypeId", SqlDbType.Int).Direction = ParameterDirection.Output;
                c.Parameters.Add("PaymentTypeId", SqlDbType.Int).Direction = ParameterDirection.Output;
                //             command.Parameters.Add("CashDepartment", SqlDbType.Int).Direction = ParameterDirection.Output;


                /*
                bool bonusAllowed = false; //разрешено ли накопление/списание бонусов
                double bonusAccumulationPercent = 0; //процент начисления бонусов от суммы оплаты
                double bonusRejectPercent = 0; // максимальный процент от суммы покупки, который можно оплатить бонусами
                 */

                p = c.Parameters.Add("BonusAllowed", SqlDbType.Int);
                p.Direction = ParameterDirection.Output;

                p = c.Parameters.Add("status", SqlDbType.Int);
                p.Direction = ParameterDirection.Output;

                p = c.Parameters.Add("BonusAccumulationPercent", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("BonusRejectPercent", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой


                p = c.Parameters.Add("InvoiceAmount", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                /*
                double custTotalAmount = 0;//сумма покупок клиента за всю историю
                double bonusTypeBorder = 0;//порог суммы для перехода на следующий тип дисконтной карты
                */
                p = c.Parameters.Add("CustTotalAmount", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("CustBonusAmount", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("BonusTypeBorder", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                /*
                double amount4payment = 0;//сумма для оплаты деньгами
                double amount4paymentBonus = 0;//сумма для оплаты бонусами
                double amount4paymentCertif = 0;//сумма для оплаты сертификатом
                 */

                p = c.Parameters.Add("Amount4payment", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("Amount4paymentBonus", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("Amount4paymentCertif", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                /*
                double amountDisc = 0;//сумма скидки
                double amountBonus = 0;//сумма полученных от оплаты деньгами бонусов
                double amountBonusPublicity = 0;//сумма бонусов полученных по РА
                */

                p = c.Parameters.Add("AmountDisc", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("AmountBonus", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("AmountBonusPublicity", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("AmountDiscPublicity", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                c.Parameters.Add("transDate", SqlDbType.DateTime).Direction = ParameterDirection.Output;

                p = c.Parameters.Add("userFirstName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 256;

                p = c.Parameters.Add("userLastName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 256;

                p = c.Parameters.Add("userComment", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 1024;

                p = c.Parameters.Add("custFirstName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 256;

                p = c.Parameters.Add("custLastName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 256;

                p = c.Parameters.Add("deliveryTypeId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("deliveryTypeDescription", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                p = c.Parameters.Add("SelfDeliveryPointId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("SelfDeliveryPointName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 255;

                p = c.Parameters.Add("SelfDeliveryPointDescription", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 1024;


                c.Parameters.Add("deliveryDate", SqlDbType.DateTime).Direction = ParameterDirection.Output;

                p = c.Parameters.Add("deliveryCost", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("transportLeg", SqlDbType.Int);
                p.Direction = ParameterDirection.Output;

                p = c.Parameters.Add("PhoneNumber", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                //поля для сбербанка
                p = c.Parameters.Add("PaymServLink", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 1000;

                p = c.Parameters.Add("PaymServAlias", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 30;

                c.Parameters.Add("PaymLinkExpDatetime", SqlDbType.DateTime).Direction = ParameterDirection.Output;

                p = c.Parameters.Add("PaymAttempt", SqlDbType.Int);
                p.Direction = ParameterDirection.Output;

                p = c.Parameters.Add("PaymServOrderId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                c.Parameters.Add("CreatedDate", SqlDbType.DateTime).Direction = ParameterDirection.Output;

                p = c.Parameters.Add("amountWithDelivery", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("AspCustId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("AspId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 128;

                p = c.Parameters.Add("cityName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                p = c.Parameters.Add("cityId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 128;

                p = c.Parameters.Add("DeliveryModeId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("DeliveryAddressStreet", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                p = c.Parameters.Add("DeliveryAddressBuilding", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                p = c.Parameters.Add("DeliveryAddressAppt", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                p = c.Parameters.Add("DeliveryAddressGate", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;


                p = c.Parameters.Add("DeliveryAddressLevel", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;


                p = c.Parameters.Add("DeliveryAddressIntercomCode", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                p = c.Parameters.Add("ExtDeliveryPointId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("TrackNumber", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 50;

                /*
                p = c.Parameters.Add("cashId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("emplInn", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 12;
                */


                sp.ExecuteNonQuery(c);

                //информация о клиенте
                custId = c.Parameters["CustId"].Value.ToString();
                salesIdParent = c.Parameters["SalesIdParent"].Value.ToString();
                discCardId = c.Parameters["DiscCardId"].Value.ToString();
                discCardTypeId = c.Parameters["DiscCardTypeId"].Value.ToString();

                if (c.Parameters["BonusAllowed"].Value.ToString() == "1")
                    bonusAllowed = true;

                salesStatus = (SaleStatus)Int32.Parse(c.Parameters["status"].Value.ToString());

                bonusAccumulationPercent = Double.Parse(c.Parameters["BonusAccumulationPercent"].Value.ToString());
                bonusRejectPercent = Double.Parse(c.Parameters["BonusRejectPercent"].Value.ToString());

                Double.TryParse(c.Parameters["CustTotalAmount"].Value.ToString(), out custTotalAmount);
                Double.TryParse(c.Parameters["CustBonusAmount"].Value.ToString(), out custBonusAmount);
                bonusTypeBorder = Double.Parse(c.Parameters["BonusTypeBorder"].Value.ToString());

                //инормация по суммам накладной
                //       cashDepartment = (CashDepartment)Int32.Parse(command.Parameters["CashDepartment"].Value.ToString());
                documentTypeId = (SaleDocumentTypeId)Int32.Parse(c.Parameters["DocumentTypeId"].Value.ToString());
                PaymentTypeId = (PaymentTypeId)Int32.Parse(c.Parameters["PaymentTypeId"].Value.ToString());

                invoiceAmount = Double.Parse(c.Parameters["InvoiceAmount"].Value.ToString());

                amount4payment = Double.Parse(c.Parameters["Amount4payment"].Value.ToString());
                amount4paymentBonus = Double.Parse(c.Parameters["Amount4paymentBonus"].Value.ToString());
                amount4paymentCertif = Double.Parse(c.Parameters["Amount4paymentCertif"].Value.ToString());

                amountDisc = Double.Parse(c.Parameters["AmountDisc"].Value.ToString());
                amountBonus = Double.Parse(c.Parameters["AmountBonus"].Value.ToString());
                amountBonusPublicity = Double.Parse(c.Parameters["AmountBonusPublicity"].Value.ToString());
                amountDiscPublicity = Double.Parse(c.Parameters["AmountDiscPublicity"].Value.ToString());

                DateTime.TryParse(c.Parameters["transDate"].Value.ToString(), out transDate);

                /*
                cashId = c.Parameters["cashId"].Value.ToString();

                emplInn = c.Parameters["emplInn"].Value.ToString();
                */

                userFirstName = c.Parameters["userFirstName"].Value.ToString();
                userLastName = c.Parameters["userLastName"].Value.ToString();
                userComment = c.Parameters["userComment"].Value.ToString();

                custFirstName = c.Parameters["custFirstName"].Value.ToString();
                custLastName = c.Parameters["custLastName"].Value.ToString();

                deliveryTypeId = c.Parameters["deliveryTypeId"].Value.ToString();
                deliveryTypeDescription = c.Parameters["deliveryTypeDescription"].Value.ToString();

                selfDeliveryPointId = c.Parameters["selfDeliveryPointId"].Value.ToString();
                selfDeliveryPointDescription = c.Parameters["selfDeliveryPointDescription"].Value.ToString();
                selfDeliveryPointName = c.Parameters["selfDeliveryPointName"].Value.ToString();
                DateTime.TryParse(c.Parameters["deliveryDate"].Value.ToString(), out deliveryDate);
                deliveryCost = Double.Parse(c.Parameters["deliveryCost"].Value.ToString());

                userPhone = c.Parameters["PhoneNumber"].Value.ToString();

                paymServLink = c.Parameters["PaymServLink"].Value.ToString();
                paymServAlias = c.Parameters["PaymServAlias"].Value.ToString();
                DateTime.TryParse(c.Parameters["PaymLinkExpDatetime"].Value.ToString(), out paymLinkExpDatetime);

                paymServOrderId = c.Parameters["PaymServOrderId"].Value.ToString();

                DateTime.TryParse(c.Parameters["CreatedDate"].Value.ToString(), out createdDate);
                int.TryParse(c.Parameters["PaymAttempt"].Value.ToString(), out paymAttempt);

                int.TryParse(c.Parameters["transportLeg"].Value.ToString(), out transportLeg);

                Double.TryParse(c.Parameters["amountWithDelivery"].Value.ToString(), out amountWithDelivery);

                aspCustId = c.Parameters["AspCustId"].Value.ToString();

                aspId = c.Parameters["AspId"].Value.ToString();

                cityName = c.Parameters["CityName"].Value.ToString();

                cityId = c.Parameters["CityId"].Value.ToString();

                deliveryModeId = c.Parameters["DeliveryModeId"].Value.ToString();

                deliveryAddressStreet = c.Parameters["DeliveryAddressStreet"].Value.ToString();
                deliveryAddressBuilding = c.Parameters["DeliveryAddressBuilding"].Value.ToString();
                deliveryAddressAppt = c.Parameters["DeliveryAddressAppt"].Value.ToString();
                deliveryAddressGate = c.Parameters["DeliveryAddressGate"].Value.ToString();
                deliveryAddressLevel = c.Parameters["DeliveryAddressLevel"].Value.ToString();
                deliveryAddressIntercomCode = c.Parameters["DeliveryAddressIntercomCode"].Value.ToString();

                selfDeliveryPointIdExt = c.Parameters["extDeliveryPointId"].Value.ToString();

                trackNumber = c.Parameters["TrackNumber"].Value.ToString();

                if (salesStatus == SaleStatus.Edit || salesStatus == SaleStatus.Aside || salesStatus == SaleStatus.Picked)
                    Parce();
            }
            catch (Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка получения данных о заказе на продажу " + salesId, ex.Message);
                salesId = "";
            }
            finally
            {
                sp.CloseSQLCommand(c);
            }
        }

        public static SaleInfo InitByPaymAlias(string _paymAlias)
        {
            StoredProcedure sp = new StoredProcedure();
            SqlCommand c = null;
            SaleInfo ret = new SaleInfo();
            try
            {

                c = sp.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetSaleInfoByAlias";

                c.Parameters.Add("PaymServAlias", SqlDbType.NVarChar).Value = _paymAlias;

                SqlParameter p;

                p = c.Parameters.Add("SalesId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;


                p = c.Parameters.Add("CustId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("SalesIdParent", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("DiscCardId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                //SqlDbType.Xml

                p = c.Parameters.Add("DiscCardTypeId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 10;


                c.Parameters.Add("DocumentTypeId", SqlDbType.Int).Direction = ParameterDirection.Output;
                c.Parameters.Add("PaymentTypeId", SqlDbType.Int).Direction = ParameterDirection.Output;
                //             command.Parameters.Add("CashDepartment", SqlDbType.Int).Direction = ParameterDirection.Output;


                /*
                bool bonusAllowed = false; //разрешено ли накопление/списание бонусов
                double bonusAccumulationPercent = 0; //процент начисления бонусов от суммы оплаты
                double bonusRejectPercent = 0; // максимальный процент от суммы покупки, который можно оплатить бонусами
                 */

                p = c.Parameters.Add("BonusAllowed", SqlDbType.Int);
                p.Direction = ParameterDirection.Output;

                p = c.Parameters.Add("status", SqlDbType.Int);
                p.Direction = ParameterDirection.Output;

                p = c.Parameters.Add("BonusAccumulationPercent", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("BonusRejectPercent", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой


                p = c.Parameters.Add("InvoiceAmount", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                /*
                double custTotalAmount = 0;//сумма покупок клиента за всю историю
                double bonusTypeBorder = 0;//порог суммы для перехода на следующий тип дисконтной карты
                */
                p = c.Parameters.Add("CustTotalAmount", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("CustBonusAmount", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("BonusTypeBorder", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                /*
                double amount4payment = 0;//сумма для оплаты деньгами
                double amount4paymentBonus = 0;//сумма для оплаты бонусами
                double amount4paymentCertif = 0;//сумма для оплаты сертификатом
                 */

                p = c.Parameters.Add("Amount4payment", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("Amount4paymentBonus", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("Amount4paymentCertif", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                /*
                double amountDisc = 0;//сумма скидки
                double amountBonus = 0;//сумма полученных от оплаты деньгами бонусов
                double amountBonusPublicity = 0;//сумма бонусов полученных по РА
                */

                p = c.Parameters.Add("AmountDisc", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("AmountBonus", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("AmountBonusPublicity", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("AmountDiscPublicity", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                c.Parameters.Add("transDate", SqlDbType.DateTime).Direction = ParameterDirection.Output;

                p = c.Parameters.Add("userFirstName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 256;

                p = c.Parameters.Add("userLastName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 256;

                p = c.Parameters.Add("userComment", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 1024;

                p = c.Parameters.Add("custFirstName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 256;

                p = c.Parameters.Add("custLastName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 256;

                p = c.Parameters.Add("deliveryTypeId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("deliveryTypeDescription", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                p = c.Parameters.Add("SelfDeliveryPointId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("SelfDeliveryPointName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 255;

                p = c.Parameters.Add("SelfDeliveryPointDescription", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 1024;

                c.Parameters.Add("deliveryDate", SqlDbType.DateTime).Direction = ParameterDirection.Output;

                p = c.Parameters.Add("deliveryCost", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("transportLeg", SqlDbType.Int);
                p.Direction = ParameterDirection.Output;

                p = c.Parameters.Add("PhoneNumber", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                //поля для сбербанка
                p = c.Parameters.Add("PaymServLink", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 1000;


                c.Parameters.Add("PaymLinkExpDatetime", SqlDbType.DateTime).Direction = ParameterDirection.Output;

                p = c.Parameters.Add("PaymAttempt", SqlDbType.Int);
                p.Direction = ParameterDirection.Output;

                p = c.Parameters.Add("PaymServOrderId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                c.Parameters.Add("CreatedDate", SqlDbType.DateTime).Direction = ParameterDirection.Output;

                p = c.Parameters.Add("AmountWithDelivery", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("AspCustId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("AspId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 128;

                p = c.Parameters.Add("cityName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                p = c.Parameters.Add("cityId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 128;

                p = c.Parameters.Add("DeliveryModeId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("DeliveryAddressStreet", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                p = c.Parameters.Add("DeliveryAddressBuilding", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                p = c.Parameters.Add("DeliveryAddressAppt", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                p = c.Parameters.Add("DeliveryAddressGate", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;


                p = c.Parameters.Add("DeliveryAddressLevel", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;


                p = c.Parameters.Add("DeliveryAddressIntercomCode", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                p = c.Parameters.Add("ExtDeliveryPointId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("TrackNumber", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 50;


                /*
                p = c.Parameters.Add("cashId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("emplInn", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 12;
                */


                sp.ExecuteNonQuery(c);

                ret.salesId = c.Parameters["SalesId"].Value.ToString();
                //информация о клиенте
                ret.custId = c.Parameters["CustId"].Value.ToString();
                ret.salesIdParent = c.Parameters["SalesIdParent"].Value.ToString();
                ret.discCardId = c.Parameters["DiscCardId"].Value.ToString();
                ret.discCardTypeId = c.Parameters["DiscCardTypeId"].Value.ToString();

                if (c.Parameters["BonusAllowed"].Value.ToString() == "1")
                    ret.bonusAllowed = true;

                ret.salesStatus = (SaleStatus)Int32.Parse(c.Parameters["status"].Value.ToString());

                ret.bonusAccumulationPercent = Double.Parse(c.Parameters["BonusAccumulationPercent"].Value.ToString());
                ret.bonusRejectPercent = Double.Parse(c.Parameters["BonusRejectPercent"].Value.ToString());

                Double.TryParse(c.Parameters["CustTotalAmount"].Value.ToString(), out ret.custTotalAmount);
                Double.TryParse(c.Parameters["CustBonusAmount"].Value.ToString(), out ret.custBonusAmount);
                ret.bonusTypeBorder = Double.Parse(c.Parameters["BonusTypeBorder"].Value.ToString());

                //инормация по суммам накладной
                //       cashDepartment = (CashDepartment)Int32.Parse(command.Parameters["CashDepartment"].Value.ToString());
                ret.documentTypeId = (SaleDocumentTypeId)Int32.Parse(c.Parameters["DocumentTypeId"].Value.ToString());
                ret.PaymentTypeId = (PaymentTypeId)Int32.Parse(c.Parameters["PaymentTypeId"].Value.ToString());

                ret.invoiceAmount = Double.Parse(c.Parameters["InvoiceAmount"].Value.ToString());

                ret.amount4payment = Double.Parse(c.Parameters["Amount4payment"].Value.ToString());
                ret.amount4paymentBonus = Double.Parse(c.Parameters["Amount4paymentBonus"].Value.ToString());
                ret.amount4paymentCertif = Double.Parse(c.Parameters["Amount4paymentCertif"].Value.ToString());

                ret.amountDisc = Double.Parse(c.Parameters["AmountDisc"].Value.ToString());
                ret.amountBonus = Double.Parse(c.Parameters["AmountBonus"].Value.ToString());
                ret.amountBonusPublicity = Double.Parse(c.Parameters["AmountBonusPublicity"].Value.ToString());
                ret.amountDiscPublicity = Double.Parse(c.Parameters["AmountDiscPublicity"].Value.ToString());

                DateTime.TryParse(c.Parameters["transDate"].Value.ToString(), out ret.transDate);

                /*
                cashId = c.Parameters["cashId"].Value.ToString();

                emplInn = c.Parameters["emplInn"].Value.ToString();
                */

                ret.userFirstName = c.Parameters["userFirstName"].Value.ToString();
                ret.userLastName = c.Parameters["userLastName"].Value.ToString();
                ret.userComment = c.Parameters["userComment"].Value.ToString();

                ret.custFirstName = c.Parameters["custFirstName"].Value.ToString();
                ret.custLastName = c.Parameters["custLastName"].Value.ToString();

                ret.deliveryTypeId = c.Parameters["deliveryTypeId"].Value.ToString();
                ret.deliveryTypeDescription = c.Parameters["deliveryTypeDescription"].Value.ToString();

                ret.selfDeliveryPointId = c.Parameters["selfDeliveryPointId"].Value.ToString();
                ret.selfDeliveryPointDescription = c.Parameters["selfDeliveryPointDescription"].Value.ToString();
                ret.selfDeliveryPointName = c.Parameters["selfDeliveryPointName"].Value.ToString();
                DateTime.TryParse(c.Parameters["deliveryDate"].Value.ToString(), out ret.deliveryDate);
                ret.deliveryCost = Double.Parse(c.Parameters["deliveryCost"].Value.ToString());

                ret.userPhone = c.Parameters["PhoneNumber"].Value.ToString();

                ret.paymServLink = c.Parameters["PaymServLink"].Value.ToString();
                ret.paymServAlias = c.Parameters["PaymServAlias"].Value.ToString();
                ret.paymServOrderId = c.Parameters["PaymServOrderId"].Value.ToString();
                var s = c.Parameters["PaymLinkExpDatetime"].Value.ToString();
                DateTime.TryParse(c.Parameters["PaymLinkExpDatetime"].Value.ToString(), out ret.paymLinkExpDatetime);
                int.TryParse(c.Parameters["PaymAttempt"].Value.ToString(), out ret.paymAttempt);

                DateTime.TryParse(c.Parameters["CreatedDate"].Value.ToString(), out ret.createdDate);

                int.TryParse(c.Parameters["transportLeg"].Value.ToString(), out ret.transportLeg);

                Double.TryParse(c.Parameters["amountWithDelivery"].Value.ToString(), out ret.amountWithDelivery);

                ret.aspCustId = c.Parameters["AspCustId"].Value.ToString();

                ret.aspId = c.Parameters["AspId"].Value.ToString();

                ret.cityName = c.Parameters["CityName"].Value.ToString();

                ret.cityId = c.Parameters["CityId"].Value.ToString();

                ret.deliveryModeId = c.Parameters["DeliveryModeId"].Value.ToString();

                ret.deliveryAddressStreet = c.Parameters["DeliveryAddressStreet"].Value.ToString();
                ret.deliveryAddressBuilding = c.Parameters["DeliveryAddressBuilding"].Value.ToString();
                ret.deliveryAddressAppt = c.Parameters["DeliveryAddressAppt"].Value.ToString();
                ret.deliveryAddressGate = c.Parameters["DeliveryAddressGate"].Value.ToString();
                ret.deliveryAddressLevel = c.Parameters["DeliveryAddressLevel"].Value.ToString();
                ret.deliveryAddressIntercomCode = c.Parameters["DeliveryAddressIntercomCode"].Value.ToString();

                ret.selfDeliveryPointIdExt = c.Parameters["extDeliveryPointId"].Value.ToString();

                ret.trackNumber = c.Parameters["TrackNumber"].Value.ToString();

                if (ret.salesStatus == SaleStatus.Edit || ret.salesStatus == SaleStatus.Aside || ret.salesStatus == SaleStatus.Picked)
                    ret.Parce();
            }
            catch (Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка инициализации заказа по алиасу " + _paymAlias, ex.Message);
                ret.salesId = "";

            }
            finally
            {
                sp.CloseSQLCommand(c);
            }
            return ret;
        }

        public static SaleInfo InitInOpenStatusByUserId(string _userId)
        {
            StoredProcedure sp = new StoredProcedure();
            SqlCommand c = null;
            SaleInfo ret = new SaleInfo();
            try
            {

                c = sp.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetSaleInfoOpenStatusByUserId";

                c.Parameters.Add("aspId", SqlDbType.NVarChar).Value = _userId;

                SqlParameter p;

                p = c.Parameters.Add("SalesId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;


                p = c.Parameters.Add("CustId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("SalesIdParent", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("DiscCardId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                //SqlDbType.Xml

                p = c.Parameters.Add("DiscCardTypeId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 10;


                c.Parameters.Add("DocumentTypeId", SqlDbType.Int).Direction = ParameterDirection.Output;
                c.Parameters.Add("PaymentTypeId", SqlDbType.Int).Direction = ParameterDirection.Output;
                //             command.Parameters.Add("CashDepartment", SqlDbType.Int).Direction = ParameterDirection.Output;


                /*
                bool bonusAllowed = false; //разрешено ли накопление/списание бонусов
                double bonusAccumulationPercent = 0; //процент начисления бонусов от суммы оплаты
                double bonusRejectPercent = 0; // максимальный процент от суммы покупки, который можно оплатить бонусами
                 */

                p = c.Parameters.Add("BonusAllowed", SqlDbType.Int);
                p.Direction = ParameterDirection.Output;

                p = c.Parameters.Add("status", SqlDbType.Int);
                p.Direction = ParameterDirection.Output;

                p = c.Parameters.Add("BonusAccumulationPercent", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("BonusRejectPercent", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой


                p = c.Parameters.Add("InvoiceAmount", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                /*
                double custTotalAmount = 0;//сумма покупок клиента за всю историю
                double bonusTypeBorder = 0;//порог суммы для перехода на следующий тип дисконтной карты
                */
                p = c.Parameters.Add("CustTotalAmount", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("CustBonusAmount", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("BonusTypeBorder", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                /*
                double amount4payment = 0;//сумма для оплаты деньгами
                double amount4paymentBonus = 0;//сумма для оплаты бонусами
                double amount4paymentCertif = 0;//сумма для оплаты сертификатом
                 */

                p = c.Parameters.Add("Amount4payment", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("Amount4paymentBonus", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("Amount4paymentCertif", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                /*
                double amountDisc = 0;//сумма скидки
                double amountBonus = 0;//сумма полученных от оплаты деньгами бонусов
                double amountBonusPublicity = 0;//сумма бонусов полученных по РА
                */

                p = c.Parameters.Add("AmountDisc", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("AmountBonus", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("AmountBonusPublicity", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("AmountDiscPublicity", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                c.Parameters.Add("transDate", SqlDbType.DateTime).Direction = ParameterDirection.Output;

                p = c.Parameters.Add("userFirstName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 256;

                p = c.Parameters.Add("userLastName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 256;

                p = c.Parameters.Add("userComment", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 1024;

                p = c.Parameters.Add("custFirstName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 256;

                p = c.Parameters.Add("custLastName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 256;

                p = c.Parameters.Add("deliveryTypeId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("deliveryTypeDescription", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                p = c.Parameters.Add("SelfDeliveryPointId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("SelfDeliveryPointName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 255;

                p = c.Parameters.Add("SelfDeliveryPointDescription", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 1024;

                c.Parameters.Add("deliveryDate", SqlDbType.DateTime).Direction = ParameterDirection.Output;

                p = c.Parameters.Add("deliveryCost", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("transportLeg", SqlDbType.Int);
                p.Direction = ParameterDirection.Output;

                p = c.Parameters.Add("PhoneNumber", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                //поля для сбербанка
                p = c.Parameters.Add("PaymServLink", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 1000;


                c.Parameters.Add("PaymLinkExpDatetime", SqlDbType.DateTime).Direction = ParameterDirection.Output;

                p = c.Parameters.Add("PaymAttempt", SqlDbType.Int);
                p.Direction = ParameterDirection.Output;

                p = c.Parameters.Add("PaymServAlias", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 30;

                p = c.Parameters.Add("PaymServOrderId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                c.Parameters.Add("CreatedDate", SqlDbType.DateTime).Direction = ParameterDirection.Output;

                p = c.Parameters.Add("AmountWithDelivery", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("AspCustId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("cityName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                p = c.Parameters.Add("cityId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 128;

                p = c.Parameters.Add("DeliveryModeId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("DeliveryAddressStreet", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                p = c.Parameters.Add("DeliveryAddressBuilding", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                p = c.Parameters.Add("DeliveryAddressAppt", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                p = c.Parameters.Add("DeliveryAddressGate", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;


                p = c.Parameters.Add("DeliveryAddressLevel", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;


                p = c.Parameters.Add("DeliveryAddressIntercomCode", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                p = c.Parameters.Add("ExtDeliveryPointId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("TrackNumber", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 50;



                /*
                p = c.Parameters.Add("cashId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("emplInn", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 12;
                */


                sp.ExecuteNonQuery(c);

                ret.salesId = c.Parameters["SalesId"].Value.ToString();
                //информация о клиенте
                ret.custId = c.Parameters["CustId"].Value.ToString();
                ret.salesIdParent = c.Parameters["SalesIdParent"].Value.ToString();
                ret.discCardId = c.Parameters["DiscCardId"].Value.ToString();
                ret.discCardTypeId = c.Parameters["DiscCardTypeId"].Value.ToString();

                if (c.Parameters["BonusAllowed"].Value.ToString() == "1")
                    ret.bonusAllowed = true;

                ret.salesStatus = (SaleStatus)Int32.Parse(c.Parameters["status"].Value.ToString());

                ret.bonusAccumulationPercent = Double.Parse(c.Parameters["BonusAccumulationPercent"].Value.ToString());
                ret.bonusRejectPercent = Double.Parse(c.Parameters["BonusRejectPercent"].Value.ToString());

                Double.TryParse(c.Parameters["CustTotalAmount"].Value.ToString(), out ret.custTotalAmount);
                Double.TryParse(c.Parameters["CustBonusAmount"].Value.ToString(), out ret.custBonusAmount);
                ret.bonusTypeBorder = Double.Parse(c.Parameters["BonusTypeBorder"].Value.ToString());

                //инормация по суммам накладной
                //       cashDepartment = (CashDepartment)Int32.Parse(command.Parameters["CashDepartment"].Value.ToString());
                ret.documentTypeId = (SaleDocumentTypeId)Int32.Parse(c.Parameters["DocumentTypeId"].Value.ToString());
                ret.PaymentTypeId = (PaymentTypeId)Int32.Parse(c.Parameters["PaymentTypeId"].Value.ToString());

                ret.invoiceAmount = Double.Parse(c.Parameters["InvoiceAmount"].Value.ToString());

                ret.amount4payment = Double.Parse(c.Parameters["Amount4payment"].Value.ToString());
                ret.amount4paymentBonus = Double.Parse(c.Parameters["Amount4paymentBonus"].Value.ToString());
                ret.amount4paymentCertif = Double.Parse(c.Parameters["Amount4paymentCertif"].Value.ToString());

                ret.amountDisc = Double.Parse(c.Parameters["AmountDisc"].Value.ToString());
                ret.amountBonus = Double.Parse(c.Parameters["AmountBonus"].Value.ToString());
                ret.amountBonusPublicity = Double.Parse(c.Parameters["AmountBonusPublicity"].Value.ToString());
                ret.amountDiscPublicity = Double.Parse(c.Parameters["AmountDiscPublicity"].Value.ToString());

                DateTime.TryParse(c.Parameters["transDate"].Value.ToString(), out ret.transDate);

                /*
                cashId = c.Parameters["cashId"].Value.ToString();

                emplInn = c.Parameters["emplInn"].Value.ToString();
                */

                ret.userFirstName = c.Parameters["userFirstName"].Value.ToString();
                ret.userLastName = c.Parameters["userLastName"].Value.ToString();
                ret.userComment = c.Parameters["userComment"].Value.ToString();

                ret.custFirstName = c.Parameters["custFirstName"].Value.ToString();
                ret.custLastName = c.Parameters["custLastName"].Value.ToString();

                ret.deliveryTypeId = c.Parameters["deliveryTypeId"].Value.ToString();
                ret.deliveryTypeDescription = c.Parameters["deliveryTypeDescription"].Value.ToString();

                ret.selfDeliveryPointId = c.Parameters["selfDeliveryPointId"].Value.ToString();
                ret.selfDeliveryPointDescription = c.Parameters["selfDeliveryPointDescription"].Value.ToString();
                ret.selfDeliveryPointName = c.Parameters["selfDeliveryPointName"].Value.ToString();
                DateTime.TryParse(c.Parameters["deliveryDate"].Value.ToString(), out ret.deliveryDate);
                ret.deliveryCost = Double.Parse(c.Parameters["deliveryCost"].Value.ToString());

                ret.userPhone = c.Parameters["PhoneNumber"].Value.ToString();

                ret.paymServLink = c.Parameters["PaymServLink"].Value.ToString();
                ret.paymServAlias = c.Parameters["PaymServAlias"].Value.ToString();
                ret.paymServOrderId = c.Parameters["PaymServOrderId"].Value.ToString();
                var s = c.Parameters["PaymLinkExpDatetime"].Value.ToString();
                DateTime.TryParse(c.Parameters["PaymLinkExpDatetime"].Value.ToString(), out ret.paymLinkExpDatetime);
                int.TryParse(c.Parameters["PaymAttempt"].Value.ToString(), out ret.paymAttempt);

                DateTime.TryParse(c.Parameters["CreatedDate"].Value.ToString(), out ret.createdDate);

                int.TryParse(c.Parameters["transportLeg"].Value.ToString(), out ret.transportLeg);

                Double.TryParse(c.Parameters["amountWithDelivery"].Value.ToString(), out ret.amountWithDelivery);

                ret.aspCustId = c.Parameters["AspCustId"].Value.ToString();

                ret.aspId = c.Parameters["AspId"].Value.ToString();

                ret.cityName = c.Parameters["CityName"].Value.ToString();

                ret.cityId = c.Parameters["CityId"].Value.ToString();

                ret.deliveryModeId = c.Parameters["DeliveryModeId"].Value.ToString();

                ret.deliveryAddressStreet = c.Parameters["DeliveryAddressStreet"].Value.ToString();
                ret.deliveryAddressBuilding = c.Parameters["DeliveryAddressBuilding"].Value.ToString();
                ret.deliveryAddressAppt = c.Parameters["DeliveryAddressAppt"].Value.ToString();
                ret.deliveryAddressGate = c.Parameters["DeliveryAddressGate"].Value.ToString();
                ret.deliveryAddressLevel = c.Parameters["DeliveryAddressLevel"].Value.ToString();
                ret.deliveryAddressIntercomCode = c.Parameters["DeliveryAddressIntercomCode"].Value.ToString();

                ret.selfDeliveryPointIdExt = c.Parameters["extDeliveryPointId"].Value.ToString();

                ret.TrackNumber = c.Parameters["TrackNumber"].Value.ToString();

                if (ret.salesStatus == SaleStatus.Edit || ret.salesStatus == SaleStatus.Aside || ret.salesStatus == SaleStatus.Picked)
                    ret.Parce();
            }
            catch (Exception ex)
            {            
                ret.salesId = "";

            }
            finally
            {
                sp.CloseSQLCommand(c);
            }
            return ret;
        }

        public OrderViewModel ConvertToOrderViewModel()
        {
            var order = new OrderViewModel();
            order.OrderId = SalesId;
            order.CustId = CustId;
            order.CustFirstName = CustFirstName;
            order.CustLastName = CustLastName;
            order.TransDate = TransDate;
            order.Status = Status;
            order.StatusString = StatusDescription;
            order.InvoiceAmount = InvoiceAmount;
            order.ReceiverFirstName = UserFirstName;
            order.ReceiverLastName = UserLastName;
            order.Comment = UserComment;            

            /*
            //вытащили дефолтные
            var deliveryType = DeliveryTypeStatic.GetDeliveryTypes();
            //вытащили итем

            var deliveryItem = deliveryType.TypeItems.FirstOrDefault(x => x.DeliveryTypeId == DeliveryTypeId);

            var deliveryItemTypeIndex = deliveryType.TypeItems.IndexOf(deliveryItem);

            deliveryType.SelectedDeliveryTypeId = deliveryItem.DeliveryTypeId;
            deliveryType.SelectedDeliveryTypeDescription = deliveryItem.Name;


            deliveryItem.SelectedCityId = CityId;
            deliveryItem.SelectedCityName = CityName;

            deliveryItem.SelectedDeliveryModeId = DeliveryModeId;
            deliveryItem.SelectedDeliveryModeDescription = DeliveryTypeStatic.GetDeliveryModeName(DeliveryModeId);

            DeliveryModeRadioItem deliveryModeItem = new DeliveryModeRadioItem();

            int deliveryItemModeIndex = -1;

            if (DeliveryModeId == DeliveryTypeStatic.SelfDeliveryModeId)
            {
                deliveryModeItem = deliveryItem.ModeItems.FirstOrDefault(x => x.DeliveryModeId == DeliveryModeId);
                //убираем этот item
                deliveryItemModeIndex = deliveryItem.ModeItems.IndexOf(deliveryModeItem);
            }
            else
            {
                deliveryModeItem = DeliveryTypeStatic.InitDeliveryModeItem(DeliveryModeId);
            }

            if (deliveryModeItem.DeliveryModeId == DeliveryTypeStatic.CDEKPVZ10ModeId
                || deliveryModeItem.DeliveryModeId == DeliveryTypeStatic.CDEKPVZ136ModeId
                || deliveryModeItem.DeliveryModeId == DeliveryTypeStatic.SelfDeliveryModeId)
            {
                if (deliveryModeItem.DeliveryModeId == DeliveryTypeStatic.SelfDeliveryModeId)
                    deliveryModeItem.SelectedSelfDeliveryPointId = SelfDeliveryPointId;
                else
                    deliveryModeItem.SelectedSelfDeliveryPointId = selfDeliveryPointIdExt;

                deliveryModeItem.SelectedSelfDeliveryPointName = SelfDeliveryPointName;
                deliveryModeItem.SelectedSelfDeliveryPointDescription = SelfDeliveryPointDescription;
                //проставляем ID ПВЗ
            }

            if (deliveryModeItem.DeliveryModeId == DeliveryTypeStatic.CDEKCourier11ModeId
                || deliveryModeItem.DeliveryModeId == DeliveryTypeStatic.CDEKCourier137ModeId)
            {
                deliveryModeItem.DeliveryAddressStreet = DeliveryAddressStreet;
                deliveryModeItem.DeliveryAddressBuilding = DeliveryAddressBuilding;
                deliveryModeItem.DeliveryAddressAppt = DeliveryAddressAppt;
                deliveryModeItem.DeliveryAddressGate = DeliveryAddressGate;
                deliveryModeItem.DeliveryAddressLevel = DeliveryAddressLevel;
                deliveryModeItem.DeliveryAddressIntercomCode = DeliveryAddressIntercomCode;
                //проставляем только адрес
            }

            deliveryModeItem.MessageAboutCostAndPeriod = "Срок доставки: " + transportLeg.ToString() + " рабочих дн. с момента оплаты";

            //заменяем/добавляем итемы в листе на обновлённые
            if (deliveryItemModeIndex != -1)
                deliveryItem.ModeItems[deliveryItemModeIndex] = deliveryModeItem;
            else
                deliveryItem.ModeItems.Add(deliveryModeItem);

            if (deliveryItemTypeIndex != -1)
                deliveryType.TypeItems[deliveryItemTypeIndex] = deliveryItem;
            else
            {
                DataService.ReportAboutCrash("Ошибка вставки типа доставки при конвертации заказа" + salesId, "");
                deliveryType.TypeItems.Add(deliveryItem);
            }

            for (var i = 0; i < deliveryType.TypeItems.Count; i++)
            {
                deliveryType.TypeItems[i].ParentOrderId = order.OrderId;
            }
            */
            order.DeliveryType = DeliveryTypeStatic.InitDeliveryTypeViewModelByParams(SalesId, DeliveryTypeId, 
                DeliveryModeId, CityId, CityName, SelfDeliveryPointId, SelfDeliveryPointIdExt,
                SelfDeliveryPointName, SelfDeliveryPointDescription, DeliveryAddressStreet, 
                DeliveryAddressBuilding, DeliveryAddressAppt, DeliveryAddressGate, DeliveryAddressLevel,
                DeliveryAddressIntercomCode, TransportLeg);

            order.DeliveryCost = DeliveryCost;
            order.TransportLeg = TransportLeg;
            order.TotalAmount = AmountWithDelivery;
            order.ReceiverPhone = Phone;
            order.PaymServLink = PaymServLink;
            order.PaymServAlias = PaymServAlias;
            order.PaymLinkExpDatetime = PaymLinkExpDatetime;
            order.PaymServOrderId = PaymServOrderId;
            order.SubmitDate = CreatedDate;
            order.TrackingNumber = TrackNumber;

            for (var i = 0; i < order.DeliveryType.TypeItems.Count; i++)
            {
                order.DeliveryType.TypeItems[i].ParentOrderId = order.OrderId;
            }

            //            order.CityName = CityName;
            return order;
        }

        public bool ValidateDeliveryParams()
        {
            var ret = true;

            if (!OK) //на всякий пожарный
                return false;

            if (string.IsNullOrWhiteSpace(UserFirstName))
                return false;

            if (string.IsNullOrWhiteSpace(UserLastName))
                return false;

            if (string.IsNullOrWhiteSpace(Phone))
                return false;

            if (string.IsNullOrWhiteSpace(DeliveryTypeId))
                return false;

            if (string.IsNullOrWhiteSpace(DeliveryModeId))
                return false;

            if (DeliveryModeId == DeliveryTypeStatic.SelfDeliveryModeId && string.IsNullOrWhiteSpace(SelfDeliveryPointId))
                return false;

            if (DeliveryModeId == DeliveryTypeStatic.CDEKPVZ10ModeId && string.IsNullOrWhiteSpace(SelfDeliveryPointId))
                return false;

            if (DeliveryModeId == DeliveryTypeStatic.CDEKPVZ136ModeId && string.IsNullOrWhiteSpace(SelfDeliveryPointId))
                return false;

            /// пока хватит
            /// 
            return ret;
        }
    }
    #endregion
    #region QualityGoodItem
    public class QualityGoodItem
    {
        int valueId;
        public string Id { get; set; }
        public int Value
        {
            get { return valueId; }
            set { valueId = value; }
        }
        public string ValueStr
        {
            get { return valueId.ToString(); }
            set
            {
                int v;

                Int32.TryParse(value, out v);

                Value = v;
            }
        }
        public override bool Equals(object obj)
        {
            var item = obj as QualityGoodItem;

            if (item == null)
            {
                return false;
            }

            var res = this.Id.Equals(item.Id) && this.Value.Equals(item.Value);

            return res;
        }

        public override int GetHashCode()
        {
            try
            {
                return String.Format("{0}={1}", this.Id.ToString(), this.ValueStr).GetHashCode();
            }
            catch
            { 
                return base.GetHashCode();
            }
        }
    }
    #endregion QualityGoodItem
    #region ItemProperty
    public class ItemProperty
    {
        public string PropertyName { get; set; }
        public string PropertyValue { get; set; }
    }
    #endregion ItemProperty
    #region SberbankParameters
    public class SberbankParameters
    {
        string userName;
        string passwd;
        string registerPageUrl;
        string checkStatusUrl;
        string checkStatusExtUrl;
        string cancelPaymentUrl;
        string returnPaymentUrl;
        string returnUrl;

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public string Passwd
        {
            get { return passwd; }
            set { passwd = value; }
        }

        public string RegisterPageUrl
        {
            get { return registerPageUrl; }
            set { registerPageUrl = value; }
        }

        public string CancelPaymentUrl
        {
            get { return cancelPaymentUrl; }
            set { cancelPaymentUrl = value; }
        }

        public string ReturnPaymentUrl
        {
            get { return returnPaymentUrl; }
            set { returnPaymentUrl = value; }
        }

        public string CheckStatusUrl
        {
            get { return checkStatusUrl; }
            set { checkStatusUrl = value; }
        }

        public string CheckStatusExtUrl
        {
            get { return checkStatusExtUrl; }
            set { checkStatusExtUrl = value; }
        }


        public string ReturnUrl
        {
            get { return returnUrl; }
            set { returnUrl = value; }
        }
    }
    #endregion SberbankParameters
    #region sberbankRegisterAnswer
    [DataContract]
    public class SberbankAnswer
    {
        int? _errorCode;
        string _errorMessage;
        string _formUrl;
        int? _orderStatus;
        string _orderNumber;
        string _orderId;
        string _pan;
        int? _expiration;
        string _cardholderName;
        int? _amount;
        int? _currency;
        string _approvalCode;
        int? _authCode;
        string _ip;

        [DataMember]
        public int? errorCode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }
        [DataMember]
        public string errorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }
        //да, именно так - потому что сбербанк иногда присылает с большой буквы, иногда с маленькой!!!!!
        [DataMember]
        public int? ErrorCode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }
        [DataMember]
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }

        [DataMember]
        public string formUrl
        {
            get { return _formUrl; }
            set { _formUrl = value; }
        }
        [DataMember]
        public int? OrderStatus
        {
            get { return _orderStatus; }
            set { _orderStatus = value; }
        }
        [DataMember]
        public string OrderNumber
        {
            get { return _orderNumber; }
            set { _orderNumber = value; }
        }
        [DataMember]
        public string orderId
        {
            get { return _orderId; }
            set { _orderId = value; }
        }
        [DataMember]
        public string Pan
        {
            get { return _pan; }
            set { _pan = value; }
        }
        [DataMember]
        public int? Expiration
        {
            get { return _expiration; }
            set { _expiration = value; }
        }
        [DataMember]
        public string CardHolderName
        {
            get { return _cardholderName; }
            set { _cardholderName = value; }
        }
        [DataMember]
        public int? Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        [DataMember]
        public int? Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }
        [DataMember]
        public string ApprovalCode
        {
            get { return _approvalCode; }
            set { _approvalCode = value; }
        }
        [DataMember]
        public int? AuthCode
        {
            get { return _authCode; }
            set { _authCode = value; }
        }
        [DataMember]
        public string Ip
        {
            get { return _ip; }
            set { _ip = value; }
        }

    }
    #endregion sberbankRegisterAnswer
    #region CDEKAnswer
    [DataContract]
    public class CDEKSuccess
    {
        CDEKAnswer _result;
        [DataMember]
        public CDEKAnswer result
        {
            get { return _result; }
            set { _result = value; }
        }

    }
    [DataContract]
    public class CDEKError
    {
        CDEKAnswer[] _error;
        [DataMember]
        public CDEKAnswer[] error
        {
            get { return _error; }
            set { _error = value; }
        }

    }
    [DataContract]
    public class CDEKAnswer
    {
        int? _code;
        string _text;
        double? _price;
        int? _deliveryPeriodMin;
        int? _deliveryPeriodMax;
        string _deliveryDateMin;
        string _deliveryDateMax;
        int? _tariffId;
        float? _cashOnDelivery;

        [DataMember]
        public int? code
        {
            get { return _code; }
            set { _code = value; }
        }

        [DataMember]
        public string text
        {
            get { return _text; }
            set { _text = value; }
        }

        [DataMember]
        public double? price
        {
            get { return _price; }
            set { _price = value; }
        }

        [DataMember]
        public int? deliveryPeriodMin
        {
            get { return _deliveryPeriodMin; }
            set { _deliveryPeriodMin = value; }
        }

        [DataMember]
        public int? deliveryPeriodMax
        {
            get { return _deliveryPeriodMax; }
            set { _deliveryPeriodMax = value; }
        }

        [DataMember]
        public string deliveryDateMin
        {
            get { return _deliveryDateMin; }
            set { _deliveryDateMin = value; }
        }

        [DataMember]
        public string deliveryDateMax
        {
            get { return _deliveryDateMax; }
            set { _deliveryDateMax = value; }
        }

        [DataMember]
        public int? tariffId
        {
            get { return _tariffId; }
            set { _tariffId = value; }
        }

        [DataMember]
        public float? cashOnDelivery
        {
            get { return _cashOnDelivery; }
            set { _cashOnDelivery = value; }
        }
    }
    #endregion CDEKAnswer
    #region MailParms
    public class MailRobotParams
    {
        string server;
        string name;
        string passwd;
        string responsibleForCustFeeback;
        string support;
        string fromAddr;
        int port;
        bool needSsl;
        string pickup;
        string adminConsole;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Passwd
        {
            get { return passwd; }
            set { passwd = value; }
        }

        public string Server
        {
            get { return server; }
            set { server = value; }
        }

        public string ResponsibleForCustFeeback
        {
            get { return responsibleForCustFeeback; }
            set { responsibleForCustFeeback = value; }
        }

        public string Support
        {
            get { return support; }
            set { support = value; }
        }

        public string FromAddr
        {
            get { return fromAddr; }
            set { fromAddr = value; }
        }

        public int Port
        {
            get { return port; }
            set { port = value; }
        }

        public bool NeedSsl
        {
            get { return needSsl; }
            set { needSsl = value; }
        }

        public string Pickup
        {
            get { return pickup; }
            set { pickup = value; }
        }

        public string AdminConsole
        {
            get { return adminConsole; }
            set { adminConsole = value; }
        }
    }
    #endregion MailParms
    #region SMSSenderParams
    public class SMSSenderParams
    {
        string server;
        string name;
        string passwd;
        string sender;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Passwd
        {
            get { return passwd; }
            set { passwd = value; }
        }

        public string Server
        {
            get { return server; }
            set { server = value; }
        }

        public string Sender
        {
            get { return sender; }
            set { sender = value; }
        }
    }
    #endregion SMSSenderParams
    #region Mail
    public class eMailMessage
    {
        string subject;
        string body;

        public string Subject
        {
            get { return subject; }
            set { subject = value; }
        }

        public string Body
        {
            get { return body; }
            set { body = value; }
        }
    }
    #endregion Mail
    #region CDEKParams
    public class CDEKParams
    {
        string _login;
        string _passwd;
        int _citySenderId;

        public string Login
        {
            get { return _login; }
            set { _login = value; }
        }

        public string Passwd
        {
            get { return _passwd; }
            set { _passwd = value; }
        }

        public int CitySenderId
        {
            get { return _citySenderId; }
            set { _citySenderId = value; }
        }

        public void Init()
        {
            //пока хард-код
            _login = "9cbf50ceb3c92e411131d58dae057a3f";
            _passwd = "b400475f3400cd3bc677359555486426";
            _citySenderId = 438;
        }

        public CDEKParams()
        {
            Init();
        }
    }
    #endregion CDEKParams
    #region DeliveryStatic
    public static class DeliveryTypeStatic
    {
        public const string SelfDeliveryTypeId = "SelfDelivery";
        public const string SelfDeliveryTypeName = "В пункт выдачи \"Индустрия красоты \"";

        public const string SelfDeliveryModeId = "SelfDeliveryIK";
        public const string SelfDeliveryModeName = "Самовывоз";

        public const string CDEKDeliveryTypeId = "CDEK";
        public const string CDEKDeliveryTypeName = "СДЭК";

        public const string CDEKPVZ10ModeId = "CDEKPVZ10";
        public const string CDEKPVZ136ModeId = "CDEKPVZ136";

        public const string CDEKPVZModeName = "Самовывоз из пункта выдачи СДЭК";

        public const string CDEKCourier137ModeId = "CDEKCourier137";
        public const string CDEKCourier11ModeId = "CDEKCourier11";

        public const string CDEKCourierModeName = "Доставка курьером";


        public static Dictionary<string, DeliveryTypeRadioItem> Items = new Dictionary<string, DeliveryTypeRadioItem>();

        static DeliveryTypeStatic()
        {
            /*
            Items.Add("SelfDelivery", new DeliveryTypeRadioItem() { DeliveryTypeId = "SelfDelivery", Name = "Самовывоз в пунктах выдачи \"Индустрия красоты\"", IsDefault = true });
            Items.Add("CDEKPVZ", new DeliveryTypeRadioItem() { DeliveryTypeId = "CDEKPVZ", Name = "Доставка в пункт выдачи заказов СДЭК", SdekTariffId = 136, IsDefault = false });
            Items.Add("CDEKCourier", new DeliveryTypeRadioItem() { DeliveryTypeId = "CDEKCourier", Name = "Курьерская доставка", SdekTariffId = 137, IsDefault = false });
            */
        }

        public static string CDEKDeliveryModeToTariff(string deliveryModeId)
        {
            if (deliveryModeId == CDEKPVZ10ModeId)
                return "10";

            if (deliveryModeId == CDEKPVZ136ModeId)
                return "136";

            if (deliveryModeId == CDEKCourier11ModeId)
                return "11";

            if (deliveryModeId == CDEKCourier137ModeId)
                return "137";

            DataService.ReportAboutCrash("Ошибка получения кода тарифа по способу доставки", deliveryModeId);
            return string.Empty;
        }

        public static string GetDeliveryTypeName(string deliveryTypeId)
        {
            if (deliveryTypeId == SelfDeliveryTypeId)
                return SelfDeliveryTypeName;

            if (deliveryTypeId == CDEKDeliveryTypeId)
                return CDEKDeliveryTypeName;

            DataService.ReportAboutCrash("Ошибка получения названия типа доставки", deliveryTypeId);
            return string.Empty;
        }

        public static string GetDeliveryModeName(string deliveryModeId)
        {
            if (deliveryModeId == SelfDeliveryModeId)
                return SelfDeliveryModeName;

            if (deliveryModeId == CDEKPVZ10ModeId || deliveryModeId == CDEKPVZ136ModeId)
                return CDEKPVZModeName;

            if (deliveryModeId == CDEKCourier11ModeId || deliveryModeId == CDEKCourier137ModeId)
                return CDEKCourierModeName;

            DataService.ReportAboutCrash("Ошибка получения названия способа доставки", deliveryModeId);
            return string.Empty;
        }


        //делаем вьюмодел для вывода на вьюху
        public static DeliveryTypeViewModel GetDeliveryTypes()
        {
            DeliveryTypeViewModel ret = new DeliveryTypeViewModel();

            #region СамовывозИК
            //собственные
            var DeliveryTypeRadioItemIK = new DeliveryTypeRadioItem();

            //список городов
            DeliveryTypeRadioItemIK.CityItems = DataService.GetSelfDeliveryCitys();
            if (DeliveryTypeRadioItemIK.CityItems.Count > 0)
            {
                var DeliveryTypeRadioItemDefault = DeliveryTypeRadioItemIK.CityItems.FirstOrDefault(x => x.IsDefault == true);

                if (DeliveryTypeRadioItemDefault.CityId == "")
                    DeliveryTypeRadioItemDefault = DeliveryTypeRadioItemIK.CityItems[0];

                DeliveryTypeRadioItemIK.SelectedCityId = DeliveryTypeRadioItemDefault.CityId;
                DeliveryTypeRadioItemIK.SelectedCityName = DeliveryTypeRadioItemDefault.CityName;
            }

            DeliveryTypeRadioItemIK.DeliveryTypeId = SelfDeliveryTypeId;
            DeliveryTypeRadioItemIK.Name = "Доставка в пункт выдачи \"Индустрия красоты \"";
            DeliveryTypeRadioItemIK.IsDefault = true;

            //список  типов доставки
            var modeIK = new DeliveryModeRadioItem();
            modeIK.DeliveryModeId = SelfDeliveryModeId;

            modeIK.Name = SelfDeliveryModeName;
            modeIK.IsDefault = true;

            modeIK.SelfDeliveryItems = DataService.GetSelfDeliveryPointsByCityId(DeliveryTypeRadioItemIK.SelectedCityId);
            if (modeIK.SelfDeliveryItems.Count > 0)
            {
                var SelfDeliveryItemDefault = modeIK.SelfDeliveryItems.FirstOrDefault(x => x.IsDefault == true);

                if (SelfDeliveryItemDefault.SelfDeliveryPointId == "")
                    SelfDeliveryItemDefault = modeIK.SelfDeliveryItems[0];

                modeIK.SelectedSelfDeliveryPointId = SelfDeliveryItemDefault.SelfDeliveryPointId;
                modeIK.SelectedSelfDeliveryPointName = SelfDeliveryItemDefault.SelfDeliveryPointName;
                modeIK.SelectedSelfDeliveryPointDescription = SelfDeliveryItemDefault.SelfDeliveryPointName;
                //вот тут считаем доставку:
                //код заказа пустой, потому что для самовывоза нам не нужны параметры конкретного заказа для расчёта доставки
                var deliveryCalculator = DeliveryCalculator.Init("", DeliveryTypeRadioItemIK.DeliveryTypeId,
                    DeliveryTypeRadioItemIK.SelectedCityId, modeIK.DeliveryModeId,
                    modeIK.SelectedSelfDeliveryPointId);

                var deliveryParams = deliveryCalculator.GetDeliveryPrice();
                var messageAboutCostAndPeriod = string.Empty;
                if (deliveryParams.DeliveryCost!=-1)
                {
                    messageAboutCostAndPeriod = deliveryParams.Message;
                    //var cost = deliveryParams.DeliveryCost == 0 ? "Бесплатно" : string.Format("Стоимость {0} руб.", deliveryParams.DeliveryCost.ToString("N2"));
                    //messageAboutCostAndPeriod = string.Format("{0}, срок доставки {1} дн. после оплаты заказа", cost, deliveryParams.TransportLeg.ToString());
                }
                else
                {
                    DataService.ReportAboutCrash("Ошибка расчёта стоимости доставки для пункта самовывоза", modeIK.SelectedSelfDeliveryPointId);
                }
                modeIK.MessageAboutCostAndPeriod = messageAboutCostAndPeriod;
            }
            DeliveryTypeRadioItemIK.SelectedDeliveryModeId = modeIK.DeliveryModeId;
            DeliveryTypeRadioItemIK.SelectedDeliveryModeDescription = modeIK.Name;
            DeliveryTypeRadioItemIK.ModeItems.Add(modeIK);


            //проставляе дефолтные значения:
            ret.SelectedDeliveryTypeId = DeliveryTypeRadioItemIK.DeliveryTypeId;
            ret.SelectedDeliveryTypeDescription = DeliveryTypeRadioItemIK.Name;

            ret.TypeItems.Add(DeliveryTypeRadioItemIK);
            #endregion СамовывозИК

            #region СДЭК
            //здесь мы только создаём пустой тип доставки
            var DeliveryTypeRadioItemCDEK = new DeliveryTypeRadioItem();
            DeliveryTypeRadioItemCDEK.DeliveryTypeId = CDEKDeliveryTypeId;
            DeliveryTypeRadioItemCDEK.Name = CDEKDeliveryTypeName;
            /*
            var modeCDEKSelf = new DeliveryModeRadioItem();
            modeCDEKSelf.DeliveryModeId = CDEKPVZModeId;
            modeCDEKSelf.Name = CDEKPVZModeName;

            DeliveryTypeRadioItemCDEK.ModeItems.Add(modeCDEKSelf);

            var modeCDEKCourier = new DeliveryModeRadioItem();
            modeCDEKCourier.DeliveryModeId = CDEKCourierModeId;
            modeCDEKCourier.Name = CDEKCourierModeName;

            DeliveryTypeRadioItemCDEK.ModeItems.Add(modeCDEKCourier);
            */
            ret.TypeItems.Add(DeliveryTypeRadioItemCDEK);
            #endregion СДЭК
            return ret;
        }

        public static DeliveryModeRadioItem InitDeliveryModeItem(string modeId)
        {
            var ret = new DeliveryModeRadioItem();
            ret.DeliveryModeId = modeId;
            if (modeId == SelfDeliveryModeId)
            {
                ret.Name = SelfDeliveryModeName;
            }
            if (modeId == CDEKCourier11ModeId || modeId == CDEKCourier137ModeId)
            {
                ret.Name = CDEKCourierModeName;
            }
            if (modeId == CDEKPVZ10ModeId || modeId == CDEKPVZ136ModeId)
            {
                ret.Name = CDEKPVZModeName;
            }
            return ret;
        }
        ///начинаем формировать снизу вверх
        //SelfDeliveryItems для самовывоза

        public static DeliveryTypeRadioItem GetCDEKDeliveryTypeItemByParams(string salesId, string CDEKCityId, string CityName)
        {
            int intCityId = 0;
            int.TryParse(CDEKCityId, out intCityId);

            var deliveryTypeItem = new DeliveryTypeRadioItem();
            deliveryTypeItem.DeliveryTypeId = DeliveryTypeStatic.CDEKDeliveryTypeId;
            deliveryTypeItem.Name = DeliveryTypeStatic.CDEKDeliveryTypeName;
            deliveryTypeItem.ParentOrderId = salesId;
            deliveryTypeItem.SelectedCityId = CDEKCityId;
            deliveryTypeItem.SelectedCityName = CityName;
            //запрашиваем у СДЭК стоимость доставки по каждому способу
            var deliveryModeId = DeliveryTypeStatic.CDEKPVZ136ModeId;

            var deliveryCalculator = DeliveryCalculator.Init(salesId, deliveryTypeItem.DeliveryTypeId,
                deliveryTypeItem.SelectedCityId, deliveryModeId,
                "");

            var deliveryParams = deliveryCalculator.GetDeliveryPrice();

            //второй вариант
            if (deliveryParams.DeliveryCost == -1)
            {
                deliveryModeId = DeliveryTypeStatic.CDEKPVZ10ModeId;

                deliveryCalculator = DeliveryCalculator.Init(salesId, deliveryTypeItem.DeliveryTypeId,
                    deliveryTypeItem.SelectedCityId, deliveryModeId,
                    "");

                deliveryParams = deliveryCalculator.GetDeliveryPrice();
            }

            if (deliveryParams.DeliveryCost != -1)
            {
                //какой-то есть
                var deliveryModeItem = new DeliveryModeRadioItem();

                var pointsList = DataService.GetCDEKDeliveryPointsInCity(salesId, intCityId);
                deliveryModeItem.DeliveryModeId = deliveryModeId;

                deliveryModeItem.Name = DeliveryTypeStatic.CDEKPVZModeName;
                deliveryModeItem.SelfDeliveryItems = pointsList;

                deliveryModeItem.MessageAboutCostAndPeriod = deliveryParams.Message;
                //var cost = deliveryParams.DeliveryCost == 0 ? "Бесплатно" : string.Format("Стоимость {0} руб. Внимание! До 1 мая для заказов на сумму более 3 000 рублей доставка в подарок!", deliveryParams.DeliveryCost.ToString("N2"));
                //deliveryModeItem.MessageAboutCostAndPeriod = string.Format("{0}, срок доставки {1} дн. после оплаты заказа", cost, deliveryParams.TransportLeg.ToString());
                //deliveryModeItem.MessageAboutCostAndPeriod = string.Format("Стоимость {0} руб., срок доставки {1} дн. после оплаты заказа", deliveryParams.DeliveryCost.ToString("N2"), deliveryParams.TransportLeg.ToString());


                if (deliveryModeItem.SelfDeliveryItems.Count > 0)
                {
                    deliveryModeItem.SelectedSelfDeliveryPointId = deliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointId;
                    deliveryModeItem.SelectedSelfDeliveryPointName = deliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointName;
                    deliveryModeItem.SelectedSelfDeliveryPointDescription = deliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointName;
                }

                deliveryTypeItem.ModeItems.Add(deliveryModeItem);
            }
            //теперь курьерская
            deliveryModeId = DeliveryTypeStatic.CDEKCourier137ModeId;

            deliveryCalculator = DeliveryCalculator.Init(salesId, deliveryTypeItem.DeliveryTypeId,
                deliveryTypeItem.SelectedCityId, deliveryModeId,
                "");

            deliveryParams = deliveryCalculator.GetDeliveryPrice();

            //второй вариант
            if (deliveryParams.DeliveryCost == -1)
            {
                deliveryModeId = DeliveryTypeStatic.CDEKCourier11ModeId;

                deliveryCalculator = DeliveryCalculator.Init(salesId, deliveryTypeItem.DeliveryTypeId,
                    deliveryTypeItem.SelectedCityId, deliveryModeId,
                    "");

                deliveryParams = deliveryCalculator.GetDeliveryPrice();
            }

            if (deliveryParams.DeliveryCost != -1)
            {
                //какой-то есть
                var deliveryModeItem = new DeliveryModeRadioItem();

                deliveryModeItem.DeliveryModeId = deliveryModeId;

                deliveryModeItem.Name = DeliveryTypeStatic.CDEKCourierModeName;

                deliveryModeItem.MessageAboutCostAndPeriod = deliveryParams.Message;
                //var cost = deliveryParams.DeliveryCost == 0 ? "Бесплатно" : string.Format("Стоимость {0} руб. Внимание! До 1 мая для заказов на сумму более 3 000 рублей доставка в подарок!", deliveryParams.DeliveryCost.ToString("N2"));
                //deliveryModeItem.MessageAboutCostAndPeriod = string.Format("{0}, срок доставки {1} дн. после оплаты заказа", cost, deliveryParams.TransportLeg.ToString());

                //deliveryModeItem.MessageAboutCostAndPeriod = string.Format("Стоимость {0} руб., срок доставки {1} дн. после оплаты заказа", deliveryParams.DeliveryCost.ToString("N2"), deliveryParams.TransportLeg.ToString());
                deliveryTypeItem.ModeItems.Add(deliveryModeItem);
            }

            if (deliveryTypeItem.ModeItems.Count == 0)
                deliveryTypeItem.Message = "К сожалению, по выбранным условиям доставка не возможна :-(";
            else
            {
                deliveryTypeItem.SelectedDeliveryModeId = deliveryTypeItem.ModeItems[0].DeliveryModeId;
            }
            return deliveryTypeItem;
        }

        public static DeliveryTypeRadioItem InitCDEKDeliveryTypeItemByExItem(string salesId, DeliveryTypeRadioItem currCDEKDeliveryType)
        {
            var newCDEKDeliveryType = DeliveryTypeStatic.GetCDEKDeliveryTypeItemByParams(salesId, currCDEKDeliveryType.SelectedCityId, currCDEKDeliveryType.SelectedCityName);
            //ищем способ, выбранный в исходном заказе
            var newDeliveryModeItem = newCDEKDeliveryType.ModeItems.FirstOrDefault(x => x.DeliveryModeId == currCDEKDeliveryType.SelectedDeliveryModeId);
            var currDeliveryModeItem = currCDEKDeliveryType.ModeItems.FirstOrDefault(x => x.DeliveryModeId == currCDEKDeliveryType.SelectedDeliveryModeId);

            if (newDeliveryModeItem == null)
            {
                //не найден такой деливери моуд
                //значит, надо делать выбранным тип доставки "Самовывоз"
                return null;
            }
            else
            {
                var newDeliveryModeItemIndex = newCDEKDeliveryType.ModeItems.IndexOf(newDeliveryModeItem);
                //проставляем выбранный способ доставки
                newCDEKDeliveryType.SelectedDeliveryModeId = currCDEKDeliveryType.SelectedDeliveryModeId;
                newCDEKDeliveryType.SelectedDeliveryModeDescription = currCDEKDeliveryType.SelectedDeliveryModeDescription;
                //теперь в зависимости от выбранного
                if (newDeliveryModeItem.DeliveryModeId == DeliveryTypeStatic.CDEKPVZ10ModeId || newDeliveryModeItem.DeliveryModeId == DeliveryTypeStatic.CDEKPVZ136ModeId)
                {
                    var currSelfDeliveryPointItem = currDeliveryModeItem.SelfDeliveryItems.FirstOrDefault(x => x.SelfDeliveryPointId == currDeliveryModeItem.SelectedSelfDeliveryPointId);
                    var newSelfDeliveryPointItem = newDeliveryModeItem.SelfDeliveryItems.FirstOrDefault(x => x.SelfDeliveryPointId == currDeliveryModeItem.SelectedSelfDeliveryPointId);

                    if (newSelfDeliveryPointItem == null)
                    {
                        //нет такого
                        if (newDeliveryModeItem.SelfDeliveryItems.Count > 0)
                        {
                            newDeliveryModeItem.SelectedSelfDeliveryPointId = newDeliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointId;
                            newDeliveryModeItem.SelectedSelfDeliveryPointName = newDeliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointName;
                            newDeliveryModeItem.SelectedSelfDeliveryPointDescription = newDeliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointDescription;
                        }
                        else
                        {
                            newDeliveryModeItem.SelectedSelfDeliveryPointId = string.Empty;
                            newDeliveryModeItem.SelectedSelfDeliveryPointName = string.Empty;
                            newDeliveryModeItem.SelectedSelfDeliveryPointDescription = string.Empty;
                        }
                        //если ещё и в списке пусто, не делаем ничего
                    }
                    else
                    {
                        newDeliveryModeItem.SelectedSelfDeliveryPointId = newSelfDeliveryPointItem.SelfDeliveryPointId;
                        newDeliveryModeItem.SelectedSelfDeliveryPointName = newSelfDeliveryPointItem.SelfDeliveryPointName;
                        newDeliveryModeItem.SelectedSelfDeliveryPointDescription = newSelfDeliveryPointItem.SelfDeliveryPointDescription;
                    }
                }
                if (newDeliveryModeItem.DeliveryModeId == DeliveryTypeStatic.CDEKCourier11ModeId || newDeliveryModeItem.DeliveryModeId == DeliveryTypeStatic.CDEKCourier137ModeId)
                {
                    //зануляем выбранные значения для ПВЗ
                    newDeliveryModeItem.SelectedSelfDeliveryPointId = string.Empty;
                    newDeliveryModeItem.SelectedSelfDeliveryPointName = string.Empty;
                    newDeliveryModeItem.SelectedSelfDeliveryPointDescription = string.Empty;
                    //
                    newDeliveryModeItem.DeliveryAddressStreet = currDeliveryModeItem.DeliveryAddressStreet;
                    newDeliveryModeItem.DeliveryAddressBuilding = currDeliveryModeItem.DeliveryAddressBuilding;
                    newDeliveryModeItem.DeliveryAddressAppt = currDeliveryModeItem.DeliveryAddressAppt;
                    newDeliveryModeItem.DeliveryAddressGate = currDeliveryModeItem.DeliveryAddressGate;
                    newDeliveryModeItem.DeliveryAddressLevel = currDeliveryModeItem.DeliveryAddressLevel;
                    newDeliveryModeItem.DeliveryAddressIntercomCode = currDeliveryModeItem.DeliveryAddressIntercomCode;
                }
                newCDEKDeliveryType.ModeItems[newDeliveryModeItemIndex] = newDeliveryModeItem;
            }
            return newCDEKDeliveryType;
        }

        public static DeliveryTypeViewModel InitDeliveryTypeViewModelByParams(string salesId, string deliveryTypeId,
                string deliveryModeId, string cityId, string cityName, string selfDeliveryPointId, string selfDeliveryPointIdExt, 
                string selfDeliveryPointName, string selfDeliveryPointDescription, string deliveryAddressStreet,
                string deliveryAddressBuilding, string deliveryAddressAppt, string deliveryAddressGate, string deliveryAddressLevel,
                string deliveryAddressIntercomCode, int transportLeg)
        {
            //создадим дефолтный вьмюодел
            
            var deliveryType = DeliveryTypeStatic.GetDeliveryTypes();
            try
            { 
                //вытащили итем

                var deliveryItem = deliveryType.TypeItems.FirstOrDefault(x => x.DeliveryTypeId == deliveryTypeId);

                var deliveryItemTypeIndex = deliveryType.TypeItems.IndexOf(deliveryItem);

                deliveryType.SelectedDeliveryTypeId = deliveryItem.DeliveryTypeId;
                deliveryType.SelectedDeliveryTypeDescription = deliveryItem.Name;


                deliveryItem.SelectedCityId = cityId;
                deliveryItem.SelectedCityName = cityName;

                deliveryItem.SelectedDeliveryModeId = deliveryModeId;
                deliveryItem.SelectedDeliveryModeDescription = DeliveryTypeStatic.GetDeliveryModeName(deliveryModeId);

                DeliveryModeRadioItem deliveryModeItem = new DeliveryModeRadioItem();

                int deliveryItemModeIndex = -1;

                if (deliveryModeId == DeliveryTypeStatic.SelfDeliveryModeId)
                {
                    deliveryModeItem = deliveryItem.ModeItems.FirstOrDefault(x => x.DeliveryModeId == deliveryModeId);
                    //убираем этот item
                    deliveryItemModeIndex = deliveryItem.ModeItems.IndexOf(deliveryModeItem);
                }
                else
                {
                    deliveryModeItem = DeliveryTypeStatic.InitDeliveryModeItem(deliveryModeId);
                }

                if (deliveryModeItem.DeliveryModeId == DeliveryTypeStatic.CDEKPVZ10ModeId
                    || deliveryModeItem.DeliveryModeId == DeliveryTypeStatic.CDEKPVZ136ModeId
                    || deliveryModeItem.DeliveryModeId == DeliveryTypeStatic.SelfDeliveryModeId)
                {
                    if (deliveryModeItem.DeliveryModeId == DeliveryTypeStatic.SelfDeliveryModeId)
                        deliveryModeItem.SelectedSelfDeliveryPointId = selfDeliveryPointId;
                    else
                        deliveryModeItem.SelectedSelfDeliveryPointId = selfDeliveryPointIdExt;

                    deliveryModeItem.SelectedSelfDeliveryPointName = selfDeliveryPointName;
                    deliveryModeItem.SelectedSelfDeliveryPointDescription = selfDeliveryPointDescription;
                    //проставляем ID ПВЗ
                }

                if (deliveryModeItem.DeliveryModeId == DeliveryTypeStatic.CDEKCourier11ModeId
                    || deliveryModeItem.DeliveryModeId == DeliveryTypeStatic.CDEKCourier137ModeId)
                {
                    deliveryModeItem.DeliveryAddressStreet = deliveryAddressStreet;
                    deliveryModeItem.DeliveryAddressBuilding = deliveryAddressBuilding;
                    deliveryModeItem.DeliveryAddressAppt = deliveryAddressAppt;
                    deliveryModeItem.DeliveryAddressGate = deliveryAddressGate;
                    deliveryModeItem.DeliveryAddressLevel = deliveryAddressLevel;
                    deliveryModeItem.DeliveryAddressIntercomCode = deliveryAddressIntercomCode;
                    //проставляем только адрес
                }

                deliveryModeItem.MessageAboutCostAndPeriod = "Срок доставки: " + transportLeg.ToString() + " рабочих дн. с момента оплаты";

                //заменяем/добавляем итемы в листе на обновлённые
                if (deliveryItemModeIndex != -1)
                    deliveryItem.ModeItems[deliveryItemModeIndex] = deliveryModeItem;
                else
                    deliveryItem.ModeItems.Add(deliveryModeItem);

                if (deliveryItemTypeIndex != -1)
                    deliveryType.TypeItems[deliveryItemTypeIndex] = deliveryItem;
                else
                {
                    DataService.ReportAboutCrash("Ошибка вставки типа доставки при конвертации заказа" + salesId, "");
                    deliveryType.TypeItems.Add(deliveryItem);
                }

                for (var i = 0; i < deliveryType.TypeItems.Count; i++)
                {
                    deliveryType.TypeItems[i].ParentOrderId = salesId;
                }
            }
            catch(Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка инициализации вьюмодела Order по параметрам " + salesId, ex.Message);
                deliveryType = DeliveryTypeStatic.GetDeliveryTypes();
            }
            return deliveryType;
        }

        public static DeliveryTypeViewModel InitDeliveryTypeViewModelByExOrder(string salesId, OrderViewModel exOrder)
        {

            var deliveryType = GetDeliveryTypes();

            if (string.IsNullOrWhiteSpace(exOrder.OrderId))
                return deliveryType;

            try
            {
                //вот тут мы инициализируемся
                if (exOrder.DeliveryType.SelectedDeliveryTypeId == DeliveryTypeStatic.SelfDeliveryTypeId)
                {
                    //здесь всё просто
                    //нужно подставить выбранный город (если жив) и выбранный ПВЗ (если жив)
                    deliveryType.SelectedDeliveryTypeId = exOrder.DeliveryType.SelectedDeliveryTypeId;
                    deliveryType.SelectedDeliveryTypeDescription = DeliveryTypeStatic.GetDeliveryTypeName(deliveryType.SelectedDeliveryTypeId);
                    //вытащили текущий тип
                    var currDeliveryType = deliveryType.TypeItems.FirstOrDefault(x => x.DeliveryTypeId == DeliveryTypeStatic.SelfDeliveryTypeId);
                    var currDeliveryTypeIndex = deliveryType.TypeItems.IndexOf(currDeliveryType);
                    //вытащили
                    var exDeliveryType = exOrder.DeliveryType.TypeItems.FirstOrDefault(x => x.DeliveryTypeId == DeliveryTypeStatic.SelfDeliveryTypeId);
                    //если город из старого заказа жив
                    var currCityItem = currDeliveryType.CityItems.FirstOrDefault(x => x.CityId == exDeliveryType.SelectedCityId);
                    if (currCityItem != null)
                    {
                        //если город жив
                        currDeliveryType.SelectedCityId = currCityItem.CityId;
                        currDeliveryType.SelectedCityName = currCityItem.CityName;
                        //теперь способы доставки
                        var currDeliveryModeItem = currDeliveryType.ModeItems.FirstOrDefault(x => x.DeliveryModeId == exDeliveryType.SelectedDeliveryModeId);

                        if (currDeliveryModeItem != null)
                        {
                            //существует наш способ доставки
                            currDeliveryType.SelectedDeliveryModeId = currDeliveryModeItem.DeliveryModeId;
                            currDeliveryType.SelectedDeliveryModeDescription = currDeliveryModeItem.Name;
                            //запоминаем индекс нашего способа доставки
                            var currDeliveryModeItemIndex = currDeliveryType.ModeItems.IndexOf(currDeliveryModeItem);

                            currDeliveryModeItem.SelfDeliveryItems = DataService.GetSelfDeliveryPointsByCityId(currDeliveryType.SelectedCityId);

                            var exSelfDeliveryModeId = exDeliveryType.ModeItems.FirstOrDefault(x => x.DeliveryModeId == exDeliveryType.SelectedDeliveryModeId);

                            var currSelfDeliveryPointItem = currDeliveryModeItem.SelfDeliveryItems.FirstOrDefault(x => x.SelfDeliveryPointId == exSelfDeliveryModeId.SelectedSelfDeliveryPointId);

                            if (currSelfDeliveryPointItem != null)
                            {
                                //существует наш пункт св
                                currDeliveryModeItem.SelectedSelfDeliveryPointId = currSelfDeliveryPointItem.SelfDeliveryPointId;
                                currSelfDeliveryPointItem.SelfDeliveryPointName = currSelfDeliveryPointItem.SelfDeliveryPointName;
                                currSelfDeliveryPointItem.SelfDeliveryPointDescription = currSelfDeliveryPointItem.SelfDeliveryPointDescription;
                                //сохраняем
                                currDeliveryType.ModeItems[currDeliveryModeItemIndex] = currDeliveryModeItem;
                            }
                            else
                            {
                                if (currDeliveryModeItem.SelfDeliveryItems.Count > 0)
                                {
                                    currDeliveryModeItem.SelectedSelfDeliveryPointId = currDeliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointId;
                                    currDeliveryModeItem.SelectedSelfDeliveryPointName = currDeliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointName;
                                    currDeliveryModeItem.SelectedSelfDeliveryPointDescription = currDeliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointDescription;
                                    //сохраняем
                                    currDeliveryType.ModeItems[currDeliveryModeItemIndex] = currDeliveryModeItem;
                                }
                            }
                        }
                        deliveryType.TypeItems[currDeliveryTypeIndex] = currDeliveryType;
                    }
                }
                if (exOrder.DeliveryType.SelectedDeliveryTypeId == DeliveryTypeStatic.CDEKDeliveryTypeId)
                {
                    //подсовываем
                    deliveryType.SelectedDeliveryTypeId = exOrder.DeliveryType.SelectedDeliveryTypeId;
                    //берём тип доставки из старого
                    var exCDEKDeliveryTypeItem = exOrder.DeliveryType.TypeItems.FirstOrDefault(x => x.DeliveryTypeId == exOrder.DeliveryType.SelectedDeliveryTypeId);
                    //берём тип доставки из текущего
                    var currCDEKDeliveryTypeItem = deliveryType.TypeItems.FirstOrDefault(x => x.DeliveryTypeId == exOrder.DeliveryType.SelectedDeliveryTypeId);
                    //сохраняем индекс текущего
                    var currIndex = deliveryType.TypeItems.IndexOf(currCDEKDeliveryTypeItem);

                    if (currCDEKDeliveryTypeItem != null)
                    {
                        //если в текущем есть такой тип
                        var newCDEKDeliveryTypeItem = DeliveryTypeStatic.InitCDEKDeliveryTypeItemByExItem(salesId, exCDEKDeliveryTypeItem);

                        if (newCDEKDeliveryTypeItem != null)
                        {
                            deliveryType.TypeItems[currIndex] = newCDEKDeliveryTypeItem;
                        }
                    }

                }
            }
            catch(Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка инициализации параметров доставки по существующему заказу " + exOrder.OrderId, ex.Message);
                deliveryType = GetDeliveryTypes();
            }

            return deliveryType;
        }
    }

    /// <summary>
    /// НАХУЯ НАМ ЭТИ КЛАССЫ????????
    /// </summary>
    public class CDEKPVZ
    {
        string iShopDeliveryPointId;
        string cdekDeliveryPointId;
        string cdekDeliveryPointDescription;
        string cdekCityName;

        public string IShopDeliveryPointId
        {
            get { return iShopDeliveryPointId; }
            set { iShopDeliveryPointId = value; }
        }

        public string CDEKDeliveryPointId
        {
            get { return cdekDeliveryPointId; }
            set { cdekDeliveryPointId = value; }
        }

        public string CDEKDeliveryPointDescription
        {
            get { return cdekDeliveryPointDescription; }
            set { cdekDeliveryPointDescription = value; }
        }

        public string CDEKCityName
        {
            get { return cdekCityName; }
            set { cdekCityName = value; }
        }

        public CDEKPVZ(string _iShopDeliveryPointId)
        {
            IShopDeliveryPointId = _iShopDeliveryPointId;

            Init();
        }

        public CDEKPVZ()
        {
        }

        public void Init()
        {

            StoredProcedure sp = null;
            SqlCommand c = null;
            try
            {
                c = sp.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GETCDEKPVZParams";

                c.Parameters.Add("iShopDeliveryPointId", SqlDbType.NVarChar).Value = iShopDeliveryPointId;
                SqlParameter p;

                p = c.Parameters.Add("CDEKDeliveryPointId", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 20;

                p = c.Parameters.Add("CDEKDeliveryPointDescription", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 1024;

                p = c.Parameters.Add("CDEKCityName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                c.ExecuteNonQuery();

                cdekDeliveryPointDescription = c.Parameters["CDEKDeliveryPointDescription"].Value.ToString();
                cdekDeliveryPointId = c.Parameters["CDEKDeliveryPointId"].Value.ToString();
                cdekCityName = c.Parameters["CDEKCityName"].Value.ToString();

            }
            catch (Exception ex)
            {
                string error = ex.Message;
                DataService.ReportAboutCrash("Ошибка получения данных о ПВЗ Сдэк" + iShopDeliveryPointId, ex.Message);
                IShopDeliveryPointId = "";
            }
            finally
            {
                sp.CloseSQLCommand(c);
            }
        }
        public bool OK
        {
            get { return iShopDeliveryPointId != ""; }
        }

    }

    public class CDEKCourier
    {
        string iShopAddressId;
        string cdekAddress;
        string cdekCityName;

        public string IShopAddressId
        {
            get { return iShopAddressId; }
            set { iShopAddressId = value; }
        }

        public string CDEKAddress
        {
            get { return cdekAddress; }
            set { cdekAddress = value; }
        }

        public string CDEKCityName
        {
            get { return cdekCityName; }
            set { cdekCityName = value; }
        }

        public CDEKCourier(string _iShopAddressId)
        {
            iShopAddressId = _iShopAddressId;

            Init();
        }

        public CDEKCourier()
        {
        }

        public void Init()
        {

            StoredProcedure sp = null;
            SqlCommand c = null;
            try
            {
                c = sp.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GETCDEKAddressParams";

                c.Parameters.Add("IShopAddressId", SqlDbType.NVarChar).Value = iShopAddressId;
                SqlParameter p;

                p = c.Parameters.Add("CDEKAddress", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 1024;

                p = c.Parameters.Add("CDEKCityName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 250;

                c.ExecuteNonQuery();

                cdekAddress = c.Parameters["CDEKDeliveryPointDescription"].Value.ToString();
                cdekCityName = c.Parameters["CDEKCityName"].Value.ToString();

            }
            catch (Exception ex)
            {
                string error = ex.Message;
                DataService.ReportAboutCrash("Ошибка получения данных об адресе Сдэк" + iShopAddressId, ex.Message);
                IShopAddressId = "";
            }
            finally
            {
                sp.CloseSQLCommand(c);
            }
        }
        public bool OK
        {
            get { return iShopAddressId != ""; }
        }

    }
    #endregion DeliveryStatic
    #region DeliveryCalculator
    public class DeliveryParams
    {
        int transportLeg;
        double deliveryCost;
        string message;

        public int TransportLeg
        {
            get { return transportLeg; }
            set { transportLeg = value; }
        }

        public double DeliveryCost
        {
            get { return deliveryCost; }
            set { deliveryCost = value; }
        }

        public string Message
        {
            get { return message; }
            set { message = value; }
        }

    }

    public interface IDeliveryCalculator
    {
        DeliveryParams GetDeliveryPrice();
    }

    public static class DeliveryCalculator
    {
        public static IDeliveryCalculator Init(string salesId, string typeId, string cityId, string modeId, string selfDeliveryPointId)
        {
            if (typeId == DeliveryTypeStatic.CDEKDeliveryTypeId)
                return new DeliveryCDEKCalculator(salesId, typeId, cityId, modeId, selfDeliveryPointId);

            //иначе возвращаем самовывозный
            return new DeliverySelfCalculator(salesId, typeId, cityId, modeId, selfDeliveryPointId);
        }
    }

    public class DeliverySelfCalculator : IDeliveryCalculator
    {
        string salesId;
        string typeId;
        string cityId;
        string modeId;
        string selfDeliveryPointId;

        public DeliveryParams GetDeliveryPrice()
        {
            //вот здесь рулим:
            //вытаскиваем transDate из saleInfo

            //вытаскиваем transportLeg и deliverycost по деливерипойнту

            //говорим, сколько займёт доставка

            var deliveryParams = new DeliveryParams();

            var selfDeliveryParams = DataService.GetSelfDeliveryParams(selfDeliveryPointId);

            deliveryParams.DeliveryCost = selfDeliveryParams.DeliveryCost;

            deliveryParams.TransportLeg = selfDeliveryParams.TransportLeg;

            var cost = deliveryParams.DeliveryCost == 0 ? "Бесплатно" : string.Format("Стоимость {0} руб.", deliveryParams.DeliveryCost.ToString("N2"));
            deliveryParams.Message = string.Format("{0}, срок доставки {1} дн. после оплаты заказа. ", cost, deliveryParams.TransportLeg.ToString());

            if (deliveryParams.DeliveryCost>0)
                deliveryParams.Message = deliveryParams.Message  + DataService.GetDeliveryPromoMessage(salesId);

            return deliveryParams;
        }

        public DeliverySelfCalculator(string _salesId, string _typeId, string _cityId, string _modeId, string _selfDeliveryPointId)
        {
            salesId = _salesId;
            typeId = _typeId;
            cityId = _cityId;
            modeId = _modeId;
            selfDeliveryPointId = _selfDeliveryPointId;
        }
    }
    public class DeliveryCDEKCalculator : IDeliveryCalculator
    {
        string salesId;
        string typeId;
        string cityId;
        string modeId;
        string selfDeliveryPointId;

        public DeliveryParams GetDeliveryPrice()
        {
            //тут вытаскиваем из saleInfo transDate, объём и вес

            //запрашиваем стоимость доставки, указывая следующий рабочий день после transDate
            

            var deliveryParams = new DeliveryParams();
            try
            {
                int cityIdInt;

                int.TryParse(cityId, out cityIdInt);

                deliveryParams = DataService.CalculateSDEK(salesId, modeId, cityIdInt);

                var isFreeDelivery = DataService.CheckIsFreeDelivery(salesId);

                if (isFreeDelivery && deliveryParams.DeliveryCost!=-1)
                {
                    deliveryParams.DeliveryCost = 0;
                }

                var cost = deliveryParams.DeliveryCost == 0 ? "Бесплатно" : string.Format("Стоимость {0} руб.", deliveryParams.DeliveryCost.ToString("N2"));
                deliveryParams.Message = string.Format("{0}, срок доставки {1} дн. после оплаты заказа. ", cost, deliveryParams.TransportLeg.ToString());

                if (deliveryParams.DeliveryCost > 0)
                    deliveryParams.Message = deliveryParams.Message + DataService.GetDeliveryPromoMessage(salesId);
            }
            catch (Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка работы калькулятора по заказу " + salesId, ex.Message);
                deliveryParams.DeliveryCost = - 1;
                deliveryParams.TransportLeg = -1;
            }


            return deliveryParams;
        }

        public DeliveryCDEKCalculator(string _salesId, string _typeId, string _cityId, string _modeId, string _selfDeliveryPointId)
        {
            salesId = _salesId;
            typeId = _typeId;
            cityId = _cityId;
            modeId = _modeId;
            selfDeliveryPointId = _selfDeliveryPointId;
        }
    }
    #endregion DeliveryCalculator
    public class DataService
    {
        #region constants
        const string HTML_TAG_PATTERN = "<.*?>";
        #endregion constants

        #region InventTable
        public static List<string> GetRelatedItems(string itemId, string strUrl, int itemCnt2Display)
        {
            var ret = new List<string>();
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;

            try
            {

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("itemId", SqlDbType.NVarChar).Value = itemId;
                c.Parameters.Add("@cnt", SqlDbType.Int).Value = itemCnt2Display;
                c.CommandText = "is_GetItemRelations";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    var id = r[0].ToString();
                    //var itemName = r[1].ToString().Replace("<", "(").Replace(">", ")");
                    //double price = 0;
                    //Double.TryParse(r[2].ToString(), out price);
                    //int orderNumber = 0;
                    //Int32.TryParse(r[3].ToString(), out orderNumber);
                    //var thumbnailPath = strUrl + r[4].ToString();
                    //var imagePath = strUrl + r[5].ToString();
                    //var urlSlug = r[6].ToString();
                    //int availQty = 0;
                    //Int32.TryParse(r[7].ToString(), out availQty);

                    //int hitInt = 0;
                    //Int32.TryParse(r[8].ToString(), out hitInt);

                    //var hit = false; //hitInt == 1 ? true : false;

                    //InventTableModel item = new InventTableModel() { ItemId = itemId, ItemName = itemName, Price = price, OrderNumber = orderNumber, MainImagePath = new string[] { thumbnailPath, imagePath }, UrlSlug = urlSlug, AvailQty = availQty, Hit = hit };
                    ret.Add(id);
                }
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения списка сопутствующих товаров", ex.Message);
                ret = new List<string>();
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }
        public static void GetItemImages(InventTableModel item, string strUrl)
        {
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@itemId", SqlDbType.NVarChar).Value = item.ItemId;
                c.CommandText = "is_GetItemImages";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    var thumbnailPath = strUrl + r[0].ToString();
                    var imagePath = strUrl + r[1].ToString();
                    var type = 0;
                    Int32.TryParse(r[2].ToString(), out type);
                    InventImageType imageType = (InventImageType)type;
                    switch (imageType)
                    {
                        case InventImageType.Primary:
                            {
                                item.MainImagePath = new string[] { thumbnailPath, imagePath };
                                break;
                            }
                        default:
                            {
                                item.AllImages.Add(new string[] { thumbnailPath, imagePath });
                                break;
                            }
                    }
                }
                item.AllImages.Add(item.MainImagePath);
                item.AllImages.Reverse();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
        }

        public static List<ItemProperty> GetItemProperties(string itemId)
        {
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;
            var ret = new List<ItemProperty>();

            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@itemId", SqlDbType.NVarChar).Value = itemId;
                c.CommandText = "is_GetItemQG";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    var typeName = r[0].ToString();
                    var propName = r[1].ToString();
                    ret.Add(new ItemProperty() { PropertyName = typeName, PropertyValue = propName });
                }
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения списка свойств товара " + itemId, ex.Message);
                ret = new List<ItemProperty>();
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }

            return ret;
        }

        public static List<FilterTreeModel> GetFilterList(List<QualityGoodItem> selectedFilters)
        {
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;

            List<FilterTreeModel> list = new List<FilterTreeModel>();
            FilterTreeModel m = null;
            List<QualityGoodItem> allowedList = new List<QualityGoodItem>();

            bool skipAllowedCheck = false;

            try
            {
                allowedList = GetAllowedFilters(selectedFilters);

                if (!selectedFilters.Any())
                {
                    skipAllowedCheck = true;
                }

                sp = new StoredProcedure();
                c = sp.CreateTemplateSQLCommand(CommandType.Text);
                c.CommandText = "select t.TYPEID, t.NAME, v.INTEGER, v.NAME from QUALITYGOODSTYPE as t join QUALITYGOODSVALUE as v on v.TYPEID = t.TYPEID order by t.SORTORDER, t.Name, v.NAME";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    string typeId = r[0].ToString();
                    string typeName = r[1].ToString();

                    int valueId = 0;

                    Int32.TryParse(r[2].ToString(), out valueId);

                    string valueName = r[3].ToString();

                    bool isCheck = false;
                    bool isEnabled = true;

                    if (m == null || m.Id != typeId)
                    {
                        if (m != null)
                            list.Add(m);

                        m = new FilterTreeModel();
                        m.Id = typeId;
                        m.Name = typeName;
                        m.Enabled = false;
                    }

                    if (selectedFilters.Exists(x => x.Id == typeId && x.Value == valueId))
                        isCheck = true;

                    if (!skipAllowedCheck && !allowedList.Exists(x => x.Id == typeId && x.Value == valueId))
                        isEnabled = false;

                    m.TreeNodeModel.Add(new TreeNodeModel() { Id = valueId, Title = valueName, Checked = isCheck, Enabled = isEnabled });
                }

                if (m != null)
                    list.Add(m);

                foreach (var group in list)
                {
                    if (group.TreeNodeModel.Any(x => x.Enabled == true))
                        group.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                ReportAboutCrash("Ошибка получения списка фильтров", ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return list;
        }

        public static List<QualityGoodItem> GetAllowedFilters(List<QualityGoodItem> selectedFilters)
        {
            var ret = new List<QualityGoodItem>();
            FilterTreeModel m = null;
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;

            try
            {
                var dtXmlTable = new DataTable();
                DataColumn dc = new DataColumn();
                dc.DataType = typeof(string);
                dc.ColumnName = "TypeId";

                dtXmlTable.Columns.Add(dc);

                dc = new DataColumn();
                dc.DataType = typeof(int);
                dc.ColumnName = "ValueId";

                dtXmlTable.Columns.Add(dc);

                foreach (var filter in selectedFilters)
                {
                    DataRow dr = dtXmlTable.NewRow();
                    dr["TypeId"] = filter.Id;
                    dr["ValueId"] = filter.Value;
                    dtXmlTable.Rows.Add(dr);
                }

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                SqlParameter tvparam = c.Parameters.AddWithValue("@List", dtXmlTable);
                tvparam.SqlDbType = SqlDbType.Structured;
                c.CommandText = "is_GetAllowedQG";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    QualityGoodItem i = new QualityGoodItem();

                    i.Id = r[0].ToString();
                    i.ValueStr = r[1].ToString();

                    ret.Add(i);
                }

            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения разрешённых фильтров", ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }

        public static List<string> GetItemsList(List<QualityGoodItem> filters)
        {
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;
            var ret = new List<string>();
            try
            {
                var dtXmlTable = new DataTable();
                DataColumn dc = new DataColumn();
                dc.DataType = typeof(string);
                dc.ColumnName = "TypeId";

                dtXmlTable.Columns.Add(dc);

                dc = new DataColumn();
                dc.DataType = typeof(int);
                dc.ColumnName = "ValueId";

                dtXmlTable.Columns.Add(dc);

                foreach (var filter in filters)
                {
                    DataRow dr = dtXmlTable.NewRow();
                    dr["TypeId"] = filter.Id;
                    dr["ValueId"] = filter.Value;
                    dtXmlTable.Rows.Add(dr);
                }

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                SqlParameter tvparam = c.Parameters.AddWithValue("@List", dtXmlTable);
                tvparam.SqlDbType = SqlDbType.Structured;
                c.CommandText = "is_GetItemsByQG";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    ret.Add(r[0].ToString());
                }
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения списка номенклатуры по фильтру", ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }

        public static int GetItemsCount(List<QualityGoodItem> filters)
        {
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;
            var ret = new List<string>();
            int itemsCount = 0;
            try
            {
                var dtXmlTable = new DataTable();
                DataColumn dc = new DataColumn();
                dc.DataType = typeof(string);
                dc.ColumnName = "TypeId";

                dtXmlTable.Columns.Add(dc);

                dc = new DataColumn();
                dc.DataType = typeof(int);
                dc.ColumnName = "ValueId";

                dtXmlTable.Columns.Add(dc);

                foreach (var filter in filters)
                {
                    DataRow dr = dtXmlTable.NewRow();
                    dr["TypeId"] = filter.Id;
                    dr["ValueId"] = filter.Value;
                    dtXmlTable.Rows.Add(dr);
                }

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                SqlParameter tvparam = c.Parameters.AddWithValue("@List", dtXmlTable);
                tvparam.SqlDbType = SqlDbType.Structured;
                c.CommandText = "is_GetItemsCountByQG";
                r = sp.ExecuteReader(c);

                r.Read();
                Int32.TryParse(r[0].ToString(), out itemsCount);
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения количества номенклатуры в фильтре", ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return itemsCount;
        }

        public static List<QualityGoodItem> ParseFilterString(string filterString, out int SortNum2Start, out string actionId, out string orderId)
        {
            List<QualityGoodItem> ret = new List<QualityGoodItem>();
            SortNum2Start = 0;
            actionId = string.Empty;
            orderId = string.Empty;
            try
            {
                filterString = HttpUtility.UrlDecode(filterString, System.Text.Encoding.UTF8);

                string[] filters = filterString.Split(new string[] { "&" }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string element in filters)
                {
                    string[] item = element.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);

                    if (item.Length != 2)
                        continue;

                    if (item[0] == "num2start")
                    {
                        string n = item[1] == "" ? "0" : item[1];

                        int.TryParse(n, out SortNum2Start);

                        continue;
                    }

                    if (item[0] == "actionId")
                    {
                        actionId = item[1];

                        continue;
                    }

                    if (item[0] == "orderId")
                    {
                        orderId = item[1];

                        continue;
                    }

                    int value = 0;

                    Int32.TryParse(item[1], out value);

                    if (value == 0)
                        continue;

                    ret.Add(new QualityGoodItem() { Id = item[0], Value = value });
                }
            }
            catch (Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка парсинга строки фильтров "+filterString, ex.Message);
                //TODO Redirect
            }
            return ret;
        }

        public static List<InventTableModel> GetCatalogByItemList(List<string> itemList, bool forceSelectedItems, string strUrl, int itemCnt2Display, bool onlyAvailable, string userId, int number2StartFrom = 0, bool randomOrder = false, bool noHits = false)
        {
            var ret = new List<InventTableModel>();
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;

            try
            {
                var dtXmlTable = new DataTable();
                DataColumn dc = new DataColumn();
                dc.DataType = typeof(string);
                dc.ColumnName = "ItemId";

                dtXmlTable.Columns.Add(dc);

                foreach (var element in itemList)
                {
                    DataRow dr = dtXmlTable.NewRow();
                    dr["ItemId"] = element;
                    dtXmlTable.Rows.Add(dr);
                }

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                SqlParameter tvparam = c.Parameters.AddWithValue("@List", dtXmlTable);
                tvparam.SqlDbType = SqlDbType.Structured;
                c.Parameters.Add("@num", SqlDbType.Int).Value = number2StartFrom;
                c.Parameters.Add("@cnt2Display", SqlDbType.Int).Value = itemCnt2Display;
                c.Parameters.Add("@forceSelectedItems", SqlDbType.Int).Value = forceSelectedItems;
                c.Parameters.Add("@onlyAvailable", SqlDbType.Int).Value = onlyAvailable;
                c.Parameters.Add("@userId", SqlDbType.NVarChar).Value = userId;

                if (randomOrder)
                    c.Parameters.Add("@randomOrder", SqlDbType.Int).Value = randomOrder;

                c.CommandText = "is_GetCatalog";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    var itemId = r[0].ToString();
                    var itemName = r[1].ToString().Replace("<", "(").Replace(">", ")");
                    double priceList = 0;
                    Double.TryParse(r[2].ToString(), out priceList);
                    int orderNumber = 0;
                    Int32.TryParse(r[3].ToString(), out orderNumber);
                    var thumbnailPath = strUrl + r[4].ToString();
                    var imagePath = strUrl + r[5].ToString();
                    var urlSlug = r[6].ToString();
                    int availQty = 0;
                    Int32.TryParse(r[7].ToString(), out availQty);
                    int hitInt = 0;
                    Int32.TryParse(r[8].ToString(), out hitInt);
                    var hit = ((noHits == false) && (hitInt == 1)) ? true : false;

                    int recomInt = 0;
                    Int32.TryParse(r[9].ToString(), out recomInt);
                    var recommended = recomInt == 0 ? false : true;

                    double defaultPrice = 0;
                    Double.TryParse(r[10].ToString(), out defaultPrice);

                    var priceDateExpStr = r[11].ToString();
                    DateTime priceDateExp = DateTime.MinValue;

                    var isActionStr = r[12].ToString();

                    var isAction = false;

                    if (!string.IsNullOrWhiteSpace(priceDateExpStr))
                    { 
                        DateTime.TryParse(priceDateExpStr, out priceDateExp);
                    }
                    
                    if ((priceList > defaultPrice) && (isActionStr == "1"))
                    {
                        isAction = true;
                    }

                    InventTableModel item = new InventTableModel() { ItemId = itemId, ItemName = itemName, Price = defaultPrice, OldPrice = priceList, PriceExpDate = priceDateExp, OrderNumber = orderNumber, MainImagePath = new string[] { thumbnailPath, imagePath }, UrlSlug = urlSlug, AvailQty = availQty, Hit = hit, Recommended = recommended, isAction = isAction };
                    ret.Add(item);
                }
            }
            catch (Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка получения каталога ", ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }


        public static List<InventTableModel> GetRecommendedItems(IEnumerable<InventTableModel> items2Exclude, string strUrl, int itemCnt2Display, string userId)
        {
            var ret = new List<InventTableModel>();
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;

            try
            {
                var dtXmlTable = new DataTable();
                DataColumn dc = new DataColumn();
                dc.DataType = typeof(string);
                dc.ColumnName = "ItemId";

                dtXmlTable.Columns.Add(dc);

                foreach (var element in items2Exclude)
                {
                    DataRow dr = dtXmlTable.NewRow();
                    dr["ItemId"] = element.ItemId;
                    dtXmlTable.Rows.Add(dr);
                }

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                SqlParameter tvparam = c.Parameters.AddWithValue("@List", dtXmlTable);
                tvparam.SqlDbType = SqlDbType.Structured;
                c.Parameters.Add("@cnt2Display", SqlDbType.Int).Value = itemCnt2Display;
                c.Parameters.Add("@userId", SqlDbType.NVarChar).Value = userId;
                c.CommandText = "is_GetRecommended";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    var itemId = r[0].ToString();
                    var itemName = r[1].ToString().Replace("<", "(").Replace(">", ")");
                    double priceList = 0;
                    Double.TryParse(r[2].ToString(), out priceList);
                    int orderNumber = 0;
                    Int32.TryParse(r[3].ToString(), out orderNumber);
                    var thumbnailPath = strUrl + r[4].ToString();
                    var imagePath = strUrl + r[5].ToString();
                    var urlSlug = r[6].ToString();
                    int availQty = 0;
                    Int32.TryParse(r[7].ToString(), out availQty);

                    double defaultPrice = 0;
                    Double.TryParse(r[8].ToString(), out defaultPrice);

                    var priceDateExpStr = r[9].ToString();
                    DateTime priceDateExp = DateTime.MinValue;

                    var isAction = false;

                    if (!string.IsNullOrWhiteSpace(priceDateExpStr))
                    {
                        DateTime.TryParse(priceDateExpStr, out priceDateExp);
                    }

                    if (priceList > defaultPrice)
                    {
                        isAction = true;
                    }


                    InventTableModel item = new InventTableModel() { ItemId = itemId, ItemName = itemName, Price = defaultPrice, OldPrice = priceList, OrderNumber = orderNumber, MainImagePath = new string[] { thumbnailPath, imagePath }, UrlSlug = urlSlug, AvailQty = availQty, isAction = isAction };
                    ret.Add(item);
                }
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения списка рекомендованных товаров ", ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }

        public static List<string> GetHitItems(string strUrl, int itemCnt2Display)
        {
            var ret = new List<string>();
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;

            try
            {

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@cnt2Display", SqlDbType.Int).Value = itemCnt2Display;
                c.CommandText = "is_GetHits";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    var itemId = r[0].ToString();
                    //var itemName = r[1].ToString().Replace("<", "(").Replace(">", ")");
                    //double price = 0;
                    //Double.TryParse(r[2].ToString(), out price);
                    //int orderNumber = 0;
                    //Int32.TryParse(r[3].ToString(), out orderNumber);
                    //var thumbnailPath = strUrl + r[4].ToString();
                    //var imagePath = strUrl + r[5].ToString();
                    //var urlSlug = r[6].ToString();
                    //int availQty = 0;
                    //Int32.TryParse(r[7].ToString(), out availQty);

                    //int hitInt = 0;
                    //Int32.TryParse(r[8].ToString(), out hitInt);

                    //var hit = false; //hitInt == 1 ? true : false;

                    //InventTableModel item = new InventTableModel() { ItemId = itemId, ItemName = itemName, Price = price, OrderNumber = orderNumber, MainImagePath = new string[] { thumbnailPath, imagePath }, UrlSlug = urlSlug, AvailQty = availQty, Hit = hit };
                    ret.Add(itemId);
                }
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения списка хитов", ex.Message);
                ret = new List<string>();
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }

        public static List<InventTableModel> GetBrandFeaturedItems(string brandId, string strUrl, int itemCnt2Display)
        {
            var ret = new List<InventTableModel>();
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;

            try
            {

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@cnt2Display", SqlDbType.Int).Value = itemCnt2Display;
                c.Parameters.Add("@brandId", SqlDbType.NVarChar).Value = brandId;
                c.CommandText = "is_BrandFeaturedItems";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    var itemId = r[0].ToString();
                    var itemName = r[1].ToString().Replace("<", "(").Replace(">", ")");
                    double price = 0;
                    Double.TryParse(r[2].ToString(), out price);
                    int orderNumber = 0;
                    Int32.TryParse(r[3].ToString(), out orderNumber);
                    var thumbnailPath = strUrl + r[4].ToString();
                    var imagePath = strUrl + r[5].ToString();
                    var urlSlug = r[6].ToString();
                    int availQty = 0;
                    Int32.TryParse(r[7].ToString(), out availQty);

                    int hitInt = 0;
                    Int32.TryParse(r[8].ToString(), out hitInt);

                    var hit = hitInt == 1 ? true : false;

                    InventTableModel item = new InventTableModel() { ItemId = itemId, ItemName = itemName, Price = price, OrderNumber = orderNumber, MainImagePath = new string[] { thumbnailPath, imagePath }, UrlSlug = urlSlug, AvailQty = availQty, Hit = hit };
                    ret.Add(item);
                }
            }
            catch (Exception ex)
            {
                ret = new List<InventTableModel>();
                ReportAboutCrash("Ошибка получения списка товаров по бренду " + brandId, ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }

        public static string ParseSearchString(string urlParmString, out int SortNum2Start)
        {
            string searchString = string.Empty;
            SortNum2Start = 0;
            try
            {
                urlParmString = HttpUtility.UrlDecode(urlParmString, Encoding.UTF8);

                string[] parms = urlParmString.Split(new string[] { "&" }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string element in parms)
                {
                    string[] item = element.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);

                    if (item.Length != 2)
                        continue;

                    if (item[0] == "num2start")
                    {
                        string n = item[1] == "" ? "0" : item[1];

                        int.TryParse(n, out SortNum2Start);

                        continue;
                    }

                    if (item[0] == "text")
                    {
                        searchString = item[1];
                        continue;
                    }
                }

                searchString = "%" + searchString.Replace(" ", "%") + "%";
            }
            catch (Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка парсинга строки поиска"+searchString, ex.Message);
                //TODO присылать письмо
            }
            return searchString;
        }

        public static int GetFoundItemsCount(string searchString)
        {

            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;
            var ret = new List<string>();
            int itemsCount = 0;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@searchString", SqlDbType.NVarChar).Value = searchString;
                c.CommandText = "is_GetItemsCountBySearch";

                r = sp.ExecuteReader(c);
                r.Read();
                Int32.TryParse(r[0].ToString(), out itemsCount);
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка подсчёта количества найденных товаров "+searchString, ex.Message);
                itemsCount = 0;
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return itemsCount;
        }

        public static List<string> GetItemsListBySearchString(string searchString)
        {
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;
            var ret = new List<string>();

            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@searchString", SqlDbType.NVarChar).Value = searchString;
                c.CommandText = "is_GetItemListBySearch";

                r = sp.ExecuteReader(c);
                while (r.Read())
                {
                    ret.Add(r[0].ToString());
                }
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка вывода найденных товаров", searchString + " " + ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }


        public static string GetFilterStringBySearch(string searchString)
        {
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;
            var ret = string.Empty;

            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@searchString", SqlDbType.NVarChar).Value = searchString;
                c.CommandText = "is_GetQGForSearch";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    var typeId = r[0].ToString();
                    var integer = r[1].ToString();
                    ret += typeId + "=" + integer + "&";
                }
                ret = ret.Substring(0, ret.Length - 1);
            }
            catch (Exception ex)
            {
                ret = string.Empty;
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }

            return ret;
        }
        #endregion

        #region WishList
        public static bool AddToWishlist(string userId, string itemId)
        {
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;
            bool ret = true;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@userId", SqlDbType.NVarChar).Value = userId;
                c.Parameters.Add("@itemId", SqlDbType.NVarChar).Value = itemId;
                c.CommandText = "is_Item2Wishlist";

                sp.ExecuteNonQuery(c);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                ReportAboutCrash("Ошибка добавления в виш-лист " + userId + " " + itemId, ex.Message);
                ret = false;
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);


            }
            return ret;
        }

        public static bool RemoveFromWishList(string userId, string itemId)
        {
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;
            bool ret = true;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@userId", SqlDbType.NVarChar).Value = userId;
                c.Parameters.Add("@itemId", SqlDbType.NVarChar).Value = itemId;

                c.CommandText = "is_RemoveItemFromWishList";

                sp.ExecuteNonQuery(c);
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка удаления из виш-листа " + userId + " " + itemId, ex.Message);
                string error = ex.Message;
                ret = false;
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);


            }
            return ret;
        }

        public static bool MoveToWishList(string userId, string itemId)
        {
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;
            bool ret = true;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@userId", SqlDbType.NVarChar).Value = userId;
                c.Parameters.Add("@itemId", SqlDbType.NVarChar).Value = itemId;

                c.CommandText = "is_ItemFromBasket2WishList";

                sp.ExecuteNonQuery(c);
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка переноса из виш-листа в корзину " + userId + " " + itemId, ex.Message);
                string error = ex.Message;
                ret = false;
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);


            }
            return ret;
        }

        public static List<WishListViewModel> GetWishListItems(string userId, string strUrl)
        {
            var ret = new List<WishListViewModel>();
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@userId", SqlDbType.NVarChar).Value = userId;
                c.CommandText = "is_GetWishListItems";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    var itemId = r[0].ToString();
                    var itemName = r[1].ToString();
                    double price = 0;
                    Double.TryParse(r[2].ToString(), out price);
                    var thumbnailPath = strUrl + r[3].ToString();
                    var imagePath = strUrl + r[4].ToString();
                    int availQty = 0;
                    Int32.TryParse(r[5].ToString(), out availQty);
                    var urlSlug = r[6].ToString();
                    double oldPrice = 0;
                    Double.TryParse(r[7].ToString(), out oldPrice);
                    DateTime priceExpDate = DateTime.MinValue;
                    DateTime.TryParse(r[8].ToString(), out priceExpDate);

                    WishListViewModel item = new WishListViewModel() { ItemId = itemId, ItemName = itemName, Price = price, UrlSlug = urlSlug, MainImagePath = new string[] { thumbnailPath, imagePath }, AvailQty = availQty, OldPrice = oldPrice, PriceExpDate = priceExpDate };
                    ret.Add(item);
                }
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка вывода виш-листа " + userId, ex.Message);
                string error = ex.Message;
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }

            return ret;
        }

        public static void ChangeAnonimousIdInWishList(string userId, string anonimousId)
        {
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@userId", SqlDbType.NVarChar).Value = userId;
                c.Parameters.Add("@anonimousId", SqlDbType.NVarChar).Value = anonimousId;
                c.CommandText = "is_TransformWishListFromAnonimous";

                sp.ExecuteNonQuery(c);
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка перевода виш-листа от анонима " + userId, ex.Message);
                string error = ex.Message;
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);


            }
        }

        #endregion WishList

        #region Basket

        public static bool AddToBasket(string userId, string itemId, int qty, int overwriteQty = 0)
        {
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;
            bool ret = true;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@userId", SqlDbType.NVarChar).Value = userId;
                c.Parameters.Add("@itemId", SqlDbType.NVarChar).Value = itemId;
                c.Parameters.Add("@qty", SqlDbType.Int).Value = qty;
                c.Parameters.Add("@overWriteQty", SqlDbType.Int).Value = overwriteQty;
                c.CommandText = "is_Item2Basket";

                sp.ExecuteNonQuery(c);
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка добавления в корзину " + userId + " " + itemId, ex.Message);
                string error = ex.Message;
                ret = false;
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);


            }
            return ret;
        }

        public static bool RemoveFromBasket(string userId, string itemId)
        {
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;
            bool ret = true;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@userId", SqlDbType.NVarChar).Value = userId;
                c.Parameters.Add("@itemId", SqlDbType.NVarChar).Value = itemId;

                c.CommandText = "is_RemoveItemFromBasket";

                sp.ExecuteNonQuery(c);
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка удаления из корзины " + userId + " " + itemId, ex.Message);
                string error = ex.Message;
                ret = false;
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);


            }
            return ret;
        }

        public static bool MoveToBasket(string userId, string itemId, int qty)
        {
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;
            bool ret = true;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@userId", SqlDbType.NVarChar).Value = userId;
                c.Parameters.Add("@itemId", SqlDbType.NVarChar).Value = itemId;
                c.Parameters.Add("@qty", SqlDbType.Int).Value = qty;

                c.CommandText = "is_ItemFromWishList2Basket";

                sp.ExecuteNonQuery(c);
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка переноса из корзины в виш-лист " + userId + " " + itemId, ex.Message);
                string error = ex.Message;
                ret = false;
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);


            }
            return ret;
        }

        public static void GetBasketSumAndQty(string userId, out int basketQty, out double basketSum)
        {

            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;
            basketQty = 0;
            basketSum = 0;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@userId", SqlDbType.NVarChar).Value = userId;
                c.CommandText = "is_GetBasketInfo";

                SqlParameter pr = c.Parameters.Add("@sum", SqlDbType.Decimal);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 18;
                pr.Scale = 2;

                c.Parameters.Add("@qty", SqlDbType.Int).Direction = ParameterDirection.Output;

                c.ExecuteNonQuery();

                Double.TryParse(c.Parameters["@sum"].Value.ToString(), out basketSum);
                Int32.TryParse(c.Parameters["@qty"].Value.ToString(), out basketQty);
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка вывода суммы по корзине " + userId, ex.Message);
                string error = ex.Message;
                basketQty = 0;

            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
        }



        public static void ChangeAnonimousIdInBasket(string userId, string anonimousId)
        {
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@userId", SqlDbType.NVarChar).Value = userId;
                c.Parameters.Add("@anonimousId", SqlDbType.NVarChar).Value = anonimousId;
                c.CommandText = "is_TransformBasketFromAnonimous";

                sp.ExecuteNonQuery(c);
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка склейки корзины " + userId, ex.Message);
                string error = ex.Message;
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);


            }
        }

        public static bool Order2Basket(string salesId, string userId)
        {
            var ret = true;
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@userId", SqlDbType.NVarChar).Value = userId;
                c.Parameters.Add("@salesId", SqlDbType.NVarChar).Value = salesId;
                c.CommandText = "is_Order2Basket";

                sp.ExecuteNonQuery(c);
            }
            catch (Exception ex)
            {
                ret = false;
                string error = ex.Message;
                ReportAboutCrash("Ошибка возврата в корзину заказа " + salesId, ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }



        public static List<BasketLineViewModel> GetBasketItems(string userId, string strUrl)
        {
            var ret = new List<BasketLineViewModel>();
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@userId", SqlDbType.NVarChar).Value = userId;
                c.CommandText = "is_GetBasketItems";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    var itemId = r[0].ToString();
                    var itemName = r[1].ToString();
                    int qty = 0;
                    Int32.TryParse(r[2].ToString(), out qty);
                    double price = 0;
                    Double.TryParse(r[3].ToString(), out price);
                    var thumbnailPath = strUrl + r[4].ToString();
                    var imagePath = strUrl + r[5].ToString();
                    int availQty = 0;
                    Int32.TryParse(r[6].ToString(), out availQty);
                    var urlSlug = r[7].ToString();
                    double oldPrice = 0;
                    Double.TryParse(r[8].ToString(), out oldPrice);

                    DateTime priceExpDate = DateTime.MinValue;
                    var priceExpDateStr = r[9].ToString();

                    if (!string.IsNullOrWhiteSpace(priceExpDateStr))
                    {
                        DateTime.TryParse(priceExpDateStr, out priceExpDate);
                    }

                    BasketLineViewModel item = new BasketLineViewModel() { ItemId = itemId, ItemName = itemName, Qty = qty, Price = price, UrlSlug = urlSlug, MainImagePath = new string[] { thumbnailPath, imagePath }, AvailQty = availQty, OldPrice = oldPrice, PriceExpDate =  priceExpDate};

                    ret.Add(item);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                ReportAboutCrash("Ошибка получения корзины", ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }

            return ret;
        }

        public static List<OrderTransViewModel> GetSalesTrans(string salesId, string strUrl)
        {
            var ret = new List<OrderTransViewModel>();
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@salesId", SqlDbType.NVarChar).Value = salesId;
                c.CommandText = "is_GetSalesTrans";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    var itemId = r[0].ToString();
                    var itemName = r[1].ToString();
                    int qty = 0;
                    Int32.TryParse(r[2].ToString(), out qty);
                    int qtyOrdered = 0;
                    Int32.TryParse(r[3].ToString(), out qtyOrdered);
                    double price = 0;
                    Double.TryParse(r[4].ToString(), out price);
                    var thumbnailPath = strUrl + r[5].ToString();
                    var imagePath = strUrl + r[6].ToString();
                    int availQty = 0;
                    Int32.TryParse(r[7].ToString(), out availQty);

                    OrderTransViewModel item = new OrderTransViewModel() { ItemId = itemId, ItemName = itemName, Qty = qty, QtyOrdered = qtyOrdered, AvailQty = availQty, Price = price, MainImagePath = new string[] { thumbnailPath, imagePath } };
                    ret.Add(item);
                }
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения строк по заказу " + salesId, ex.Message);
                string error = ex.Message;
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }

            return ret;
        }

        public static string CreateSalesOrder(string userId)
        {
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            int id = 0;
            string orderId = "";
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_CreateSalesOrder";

                c.Parameters.Add("userId", SqlDbType.NVarChar).Value = userId;

                SqlParameter sqlId = c.Parameters.Add("salesId", SqlDbType.NVarChar);
                sqlId.Direction = ParameterDirection.Output;
                sqlId.Size = 20;

                p.ExecuteNonQuery(c);

                orderId = sqlId.Value.ToString();
            }

            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка создания заказа " + userId, ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return orderId;
        }

        public static int CalcActions(string salesId)
        {

            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;

            int actQty = 0;

            try
            {

                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "fo_CreateSalesPublicity";

                c.Parameters.Add("SalesId", SqlDbType.NVarChar);
                c.Parameters["SalesId"].Value = salesId;

                c.Parameters.Add("Result", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                p.ExecuteNonQuery(c);

                actQty = Convert.ToInt32(c.Parameters["Result"].Value.ToString());

            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка создания результатов акций по заказу" + salesId, ex.Message);
                return -1;
            }
            finally
            {
                p.CloseSQLCommand(c);
            }
            return actQty;
        }

        public static bool GivePublicityPresent(string _id, string _journalId, string _itemId, int _qty)
        {
            StoredProcedure sp = new StoredProcedure();
            SqlCommand c = null;
            var ret = true;
            try
            {
                c = sp.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_SalesPublicityGivePresent";

                c.Parameters.Add("SalesId", SqlDbType.NVarChar);
                c.Parameters["SalesId"].Value = _id;
                c.Parameters.Add("JournalId", SqlDbType.NVarChar);
                c.Parameters["JournalId"].Value = _journalId;
                c.Parameters.Add("ItemId", SqlDbType.NVarChar);
                c.Parameters["ItemId"].Value = _itemId;
                c.Parameters.Add("Qty", SqlDbType.Int);
                c.Parameters["Qty"].Value = _qty;

                c.Parameters.Add("Points", SqlDbType.Int).Direction = ParameterDirection.Output;

                sp.ExecuteNonQuery(c);
                
                //Если выдача прошла успешно, хранимка возвращает количество истраченных баллов, но т.к. мы перегружаем страницу, нам это не нужно
                //достаточно просто возвращать true или false

                //int points = Int32.Parse(c.Parameters["Points"].Value.ToString());
                //int curpoints = Int32.Parse(dataGridActions.CurrentRow.Cells[Кол_во_выдано.Index].Value.ToString());

                //dataGridActions.CurrentRow.Cells[Кол_во_выдано.Index].Value = points + curpoints;
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка выдачи подарка по акции " + _journalId + " по заказу " + _id, ex.Message);
                ret = false;
            }
            finally
            {
                sp.CloseSQLCommand(c);
            }
            return ret;
        }

        public static List<QualityGoodItem> GetAllowedFilterListForItemList(List<string> selectedItems)
        {
            var ret = new List<QualityGoodItem>();
            FilterTreeModel m = null;
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;

            try
            {
                var dtXmlTable = new DataTable();
                DataColumn dc = new DataColumn();
                dc.DataType = typeof(string);
                dc.ColumnName = "ItemId";

                dtXmlTable.Columns.Add(dc);


                foreach (var item in selectedItems)
                {
                    DataRow dr = dtXmlTable.NewRow();
                    dr["ItemId"] = item;
                    dtXmlTable.Rows.Add(dr);
                }

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                SqlParameter tvparam = c.Parameters.AddWithValue("@List", dtXmlTable);
                tvparam.SqlDbType = SqlDbType.Structured;
                c.CommandText = "is_GetAllowedQGByItemList";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    QualityGoodItem i = new QualityGoodItem();

                    i.Id = r[0].ToString();
                    i.ValueStr = r[1].ToString();

                    ret.Add(i);
                }

            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения разрешённых фильтров для списка номенклатур", ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }

        public static List<FilterTreeModel> GetFilterListForItemList(List<string> selectedItems)
        {
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;

            List<FilterTreeModel> list = new List<FilterTreeModel>();
            FilterTreeModel m = null;
            List<QualityGoodItem> allowedList = new List<QualityGoodItem>();

            bool skipAllowedCheck = false;

            try
            {
                allowedList = GetAllowedFilterListForItemList(selectedItems);

                sp = new StoredProcedure();
                c = sp.CreateTemplateSQLCommand(CommandType.Text);
                c.CommandText = "select t.TYPEID, t.NAME, v.INTEGER, v.NAME from QUALITYGOODSTYPE as t join QUALITYGOODSVALUE as v on v.TYPEID = t.TYPEID order by t.SORTORDER, t.Name, v.NAME";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    string typeId = r[0].ToString();
                    string typeName = r[1].ToString();

                    int valueId = 0;

                    Int32.TryParse(r[2].ToString(), out valueId);

                    string valueName = r[3].ToString();

                    bool isCheck = false;
                    bool isEnabled = true;

                    if (m == null || m.Id != typeId)
                    {
                        if (m != null)
                            list.Add(m);

                        m = new FilterTreeModel();
                        m.Id = typeId;
                        m.Name = typeName;
                        m.Enabled = false;
                    }

                    //if (selectedFilters.Exists(x => x.Id == typeId && x.Value == valueId))
                    //    isCheck = true;

                    if (!skipAllowedCheck && !allowedList.Exists(x => x.Id == typeId && x.Value == valueId))
                        isEnabled = false;

                    m.TreeNodeModel.Add(new TreeNodeModel() { Id = valueId, Title = valueName, Checked = false, Enabled = isEnabled });
                }

                if (m != null)
                    list.Add(m);

                foreach (var group in list)
                {
                    if (group.TreeNodeModel.Any(x => x.Enabled == true))
                        group.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                ReportAboutCrash("Ошибка получения списка фильтров для списка номенклатур", ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return list;
        }
        public static List<PublicityActionViewModel> GetActionsAndItems(string salesId, string strUrl)
        {
            var ret = new List<PublicityActionViewModel>();
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.CommandText = "is_GetSalesPublicityU";

                c.Parameters.Add("salesId", SqlDbType.NVarChar).Value = salesId;

                r = sp.ExecuteReader(c);
                string prevJournalId = string.Empty;

                PublicityActionViewModel action = null; //new OrderViewModel();

                while (r.Read())
                {
                    var journalId = r[0].ToString();
                    var descr = r[1].ToString();
                    int pointsByAction = 0;
                    int.TryParse(r[2].ToString(), out pointsByAction);
                    int pointsGiven = 0;
                    int.TryParse(r[3].ToString(), out pointsGiven);
                    int qtyAvailForAction = 0;
                    var ttt = r[4].ToString();
                    int.TryParse(r[4].ToString(), out qtyAvailForAction);
                    double bonusAmount = 0;
                    Double.TryParse(r[5].ToString(), out bonusAmount);
                    double discAmount = 0;
                    Double.TryParse(r[6].ToString(), out discAmount);

                    var itemId = r[7].ToString();
                    var itemName = r[8].ToString();
                    int points = 0;
                    int.TryParse(r[9].ToString(), out points);

                    int qtyGiven = 0;
                    int.TryParse(r[10].ToString(), out qtyGiven);
                    int qtyAvail = 0;
                    int.TryParse(r[11].ToString(), out qtyAvail);

                    var thumbnailPath = strUrl + r[12].ToString();
                    var imagePath = strUrl + r[13].ToString();
                    var urlSlug = r[14].ToString();

                    if (journalId!=prevJournalId)
                    {
                        if (action != null && action.QtyAvail>0) //зачем добавлять акцию, по которой нет ни одного доступного подарка???
                        { 
                            ret.Add(action);
                        }

                        action = new PublicityActionViewModel()
                        {
                            OrderId = salesId,
                            JournalId = journalId,
                            JournalName = descr,
                            PointsEarned = pointsByAction,
                            PointsGiven = pointsGiven,
                            QtyAvail = qtyAvailForAction,
                            PublicityItems = new List<PublicityActionItemsViewModel>()
                        };
                        prevJournalId = journalId;    
                    }

                    PublicityActionItemsViewModel item = new PublicityActionItemsViewModel()
                    {
                        ItemId = itemId,
                        ItemName = itemName,
                        Price = 0,
                        PointsCost = points,
                        AvailQty = qtyAvail,
                        JournalId = journalId,
                        UrlSlug = urlSlug,
                        MainImagePath = new string[] { thumbnailPath, imagePath },
                        QtyGiven = qtyGiven
                    };
                    if (qtyAvail>0)
                    { 
                        action.PublicityItems.Add(item);
                    }
                }

                if (action != null)
                    ret.Add(action);

                //теперь надо удалить из списка акции, по которым все подарки стоят больше, чем заработано по акции

                ret.RemoveAll(x => x.PublicityItems.First(y => y.PointsCost <= x.PointsEarned) == null);

                //var notEnoughPointsList = new List<PublicityActionViewModel>();

                //foreach (var pub in ret)
                //{
                //    var enoughItem = pub.PublicityItems.First(x => x.PointsCost >= pub.PointsEarned);
                //    if (enoughItem == null)
                //    {

                //    }
                //}
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения списка акций и подарков по заказу" + salesId, ex.Message);
                ret = new List<PublicityActionViewModel>();
                string error = ex.Message;
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }

        public static PublicityActionViewModel GetAvailPresentsForAction(string salesId, string journalId, string strUrl, int itemCnt2Display, List<QualityGoodItem> filters, int num2Start = 0)
        {
            StoredProcedure sp = new StoredProcedure();
            SqlDataReader r = null;
            SqlCommand c = null;
            var ret = new PublicityActionViewModel();
            try
            {

                var dtXmlTable = new DataTable();
                DataColumn dc = new DataColumn();
                dc.DataType = typeof(string);
                dc.ColumnName = "TypeId";

                dtXmlTable.Columns.Add(dc);

                dc = new DataColumn();
                dc.DataType = typeof(int);
                dc.ColumnName = "ValueId";

                dtXmlTable.Columns.Add(dc);

                foreach (var filter in filters)
                {
                    DataRow dr = dtXmlTable.NewRow();
                    dr["TypeId"] = filter.Id;
                    dr["ValueId"] = filter.Value;
                    dtXmlTable.Rows.Add(dr);
                }

                c = sp.CreateTemplateSQLCommand();

                c.CommandText = "is_GetActionAvailPresentsU";
                c.CommandTimeout = 1000;

                c.Parameters.Add("SalesId", SqlDbType.NVarChar).Value = salesId;

                c.Parameters.Add("JournalId", SqlDbType.NVarChar).Value = journalId;

                c.Parameters.Add("cnt2Display", SqlDbType.Int).Value = itemCnt2Display;

                c.Parameters.Add("num", SqlDbType.Int).Value = num2Start;

                SqlParameter tvparam = c.Parameters.AddWithValue("@List", dtXmlTable);
                tvparam.SqlDbType = SqlDbType.Structured;


                c.Parameters.Add("xml", SqlDbType.NVarChar, -1).Direction = ParameterDirection.Output;

                c.ExecuteNonQuery();

                var xml = c.Parameters["xml"].Value.ToString();

                XmlDocument xmlDocument = new XmlDocument();
                XmlElement xmlRoot;
                XmlNodeList xmlRecordList;

                XmlNamedNodeMap xm;

                xmlDocument.LoadXml(xml);
                xmlRoot = xmlDocument.DocumentElement;

                XmlNodeList l = xmlDocument.GetElementsByTagName("s");
                foreach (XmlNode n in l)
                {
                    var currentAction = n.SelectSingleNode("a");
                    xm = currentAction.Attributes;
                    var descr = xm.GetNamedItem("journalDesc").Value;
                    var pointsByActionStr = xm.GetNamedItem("pointsByAction").Value;
                    var pointsGivenStr = xm.GetNamedItem("pointsGiven").Value;
                    var qtyAvailForActionStr = xm.GetNamedItem("qtyAvailForAction").Value;

                    int pointsByAction = 0;
                    int pointsGiven = 0;
                    int qtyAvailForAction = 0;

                    int.TryParse(pointsByActionStr, out pointsByAction);
                    int.TryParse(pointsGivenStr, out pointsGiven);
                    int.TryParse(qtyAvailForActionStr, out qtyAvailForAction);

                    ret = new PublicityActionViewModel() { OrderId = salesId, JournalId = journalId, JournalName = descr, PointsEarned = pointsByAction, PointsGiven = pointsGiven, QtyAvail = qtyAvailForAction };

                    foreach (XmlNode i in currentAction.SelectNodes("i"))
                    {
                        xm = i.Attributes;
                        var itemId = xm.GetNamedItem("itemId").Value;
                        var itemName = xm.GetNamedItem("itemName").Value;
                        var pointsStr = xm.GetNamedItem("points").Value;
                        var qtyGivenStr = xm.GetNamedItem("qtyGiven").Value;
                        var availQtyStr = xm.GetNamedItem("availQty").Value;
                        var thumbnailPath = strUrl + xm.GetNamedItem("thumbnailPath").Value;
                        var imagePath = strUrl + xm.GetNamedItem("imagePath").Value;
                        var urlSlug = xm.GetNamedItem("urlSlug").Value;
                        var orderNumberStr = xm.GetNamedItem("orderNumber").Value;

                        int points = 0;
                        int qtyGiven = 0;
                        int availQty = 0;
                        int orderNumber = 0;

                        int.TryParse(pointsStr, out points);
                        int.TryParse(qtyGivenStr, out qtyGiven);
                        int.TryParse(availQtyStr, out availQty);
                        int.TryParse(orderNumberStr, out orderNumber);

                        PublicityActionItemsViewModel item = new PublicityActionItemsViewModel()
                        {
                            ItemId = itemId,
                            ItemName = itemName,
                            Price = 0,
                            PointsCost = points,
                            AvailQty = availQty,
                            JournalId = journalId,
                            UrlSlug = urlSlug,
                            MainImagePath = new string[] { thumbnailPath, imagePath },
                            QtyGiven = qtyGiven,
                            OrderNumber = orderNumber
                        };

                        if (item.AvailQty>0)
                            ret.PublicityItems.Add(item);
                    }

                }

            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка разбора списка подарков по заказу "+salesId +" "+journalId, ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }

        public static List<OrderTransViewModel> GetGivenPresents(string salesId, string strUrl)
        {
            var ret = new List<OrderTransViewModel>();
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.Parameters.Add("@salesId", SqlDbType.NVarChar).Value = salesId;
                c.CommandText = "is_GetGivenPresents";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    var itemId = r[0].ToString();
                    var itemName = r[1].ToString();
                    int qty = 0;
                    Int32.TryParse(r[2].ToString(), out qty);
                    int qtyOrdered = 0;
                    Int32.TryParse(r[3].ToString(), out qtyOrdered);
                    double price = 0;
                    Double.TryParse(r[4].ToString(), out price);
                    var thumbnailPath = strUrl + r[5].ToString();
                    var imagePath = strUrl + r[6].ToString();
                    int availQty = 0;
                    Int32.TryParse(r[7].ToString(), out availQty);

                    OrderTransViewModel item = new OrderTransViewModel() { ItemId = itemId, ItemName = itemName, Qty = qty, QtyOrdered = qtyOrdered, AvailQty = availQty, Price = price, MainImagePath = new string[] { thumbnailPath, imagePath } };
                    ret.Add(item);
                }
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения подарков по акциям по заказу " + salesId, ex.Message);
                string error = ex.Message;
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }

            return ret;
        }

        public static int GetItemsCountByPublicity(List<QualityGoodItem> filters, string journalId)
        {
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;
            var ret = new List<string>();
            int itemsCount = 0;
            try
            {
                var dtXmlTable = new DataTable();
                DataColumn dc = new DataColumn();
                dc.DataType = typeof(string);
                dc.ColumnName = "TypeId";

                dtXmlTable.Columns.Add(dc);

                dc = new DataColumn();
                dc.DataType = typeof(int);
                dc.ColumnName = "ValueId";

                dtXmlTable.Columns.Add(dc);

                foreach (var filter in filters)
                {
                    DataRow dr = dtXmlTable.NewRow();
                    dr["TypeId"] = filter.Id;
                    dr["ValueId"] = filter.Value;
                    dtXmlTable.Rows.Add(dr);
                }

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();
                c.Parameters.Add("actionId", SqlDbType.NVarChar).Value = journalId;

                SqlParameter tvparam = c.Parameters.AddWithValue("@List", dtXmlTable);
                tvparam.SqlDbType = SqlDbType.Structured;
                c.CommandText = "is_GetItemsCountByQGByPublicity";
                r = sp.ExecuteReader(c);

                r.Read();
                Int32.TryParse(r[0].ToString(), out itemsCount);
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения количества номенклатуры в фильтре по акции "+journalId, ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return itemsCount;
        }

        public static List<string> GetItemsListForPublicity(List<QualityGoodItem> filters, string journalId)
        {
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;
            var ret = new List<string>();
            try
            {
                var dtXmlTable = new DataTable();
                DataColumn dc = new DataColumn();
                dc.DataType = typeof(string);
                dc.ColumnName = "TypeId";

                dtXmlTable.Columns.Add(dc);

                dc = new DataColumn();
                dc.DataType = typeof(int);
                dc.ColumnName = "ValueId";

                dtXmlTable.Columns.Add(dc);

                foreach (var filter in filters)
                {
                    DataRow dr = dtXmlTable.NewRow();
                    dr["TypeId"] = filter.Id;
                    dr["ValueId"] = filter.Value;
                    dtXmlTable.Rows.Add(dr);
                }

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                SqlParameter tvparam = c.Parameters.AddWithValue("@List", dtXmlTable);

                c.Parameters.Add("journalId", SqlDbType.NVarChar).Value = journalId;

                tvparam.SqlDbType = SqlDbType.Structured;
                c.CommandText = "is_GetItemsByQGByPublicity";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    ret.Add(r[0].ToString());
                }
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения списка номенклатуры по фильтру для акции "+journalId, ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }

        public static List<FilterTreeModel> GetFilterListForPublicity(List<QualityGoodItem> selectedFilters, string actionId)
        {
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;

            List<FilterTreeModel> list = new List<FilterTreeModel>();
            FilterTreeModel m = null;
            List<QualityGoodItem> allowedList = new List<QualityGoodItem>();

            bool skipAllowedCheck = false;

            try
            {
                allowedList = GetAllowedFiltersForPublicity(selectedFilters, actionId);

                //if (!selectedFilters.Any())
                //{
                //    skipAllowedCheck = true;
                //}

                sp = new StoredProcedure();
                c = sp.CreateTemplateSQLCommand(CommandType.Text);
                c.CommandText = "select t.TYPEID, t.NAME, v.INTEGER, v.NAME from QUALITYGOODSTYPE as t join QUALITYGOODSVALUE as v on v.TYPEID = t.TYPEID order by t.SORTORDER, t.Name, v.NAME";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    string typeId = r[0].ToString();
                    string typeName = r[1].ToString();

                    int valueId = 0;

                    Int32.TryParse(r[2].ToString(), out valueId);

                    string valueName = r[3].ToString();

                    bool isCheck = false;
                    bool isEnabled = true;

                    if (m == null || m.Id != typeId)
                    {
                        if (m != null)
                            list.Add(m);

                        m = new FilterTreeModel();
                        m.Id = typeId;
                        m.Name = typeName;
                        m.Enabled = false;
                    }

                    if (selectedFilters.Exists(x => x.Id == typeId && x.Value == valueId))
                        isCheck = true;

                    if (!skipAllowedCheck && !allowedList.Exists(x => x.Id == typeId && x.Value == valueId))
                        isEnabled = false;

                    m.TreeNodeModel.Add(new TreeNodeModel() { Id = valueId, Title = valueName, Checked = isCheck, Enabled = isEnabled });
                }

                if (m != null)
                    list.Add(m);

                foreach (var group in list)
                {
                    if (group.TreeNodeModel.Any(x => x.Enabled == true))
                        group.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                ReportAboutCrash("Ошибка получения списка фильтров", ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return list;
        }

        public static List<QualityGoodItem> GetAllowedFiltersForPublicity(List<QualityGoodItem> selectedFilters, string journalId)
        {
            var ret = new List<QualityGoodItem>();
            FilterTreeModel m = null;
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;

            try
            {
                var dtXmlTable = new DataTable();
                DataColumn dc = new DataColumn();
                dc.DataType = typeof(string);
                dc.ColumnName = "TypeId";

                dtXmlTable.Columns.Add(dc);

                dc = new DataColumn();
                dc.DataType = typeof(int);
                dc.ColumnName = "ValueId";

                dtXmlTable.Columns.Add(dc);

                foreach (var filter in selectedFilters)
                {
                    DataRow dr = dtXmlTable.NewRow();
                    dr["TypeId"] = filter.Id;
                    dr["ValueId"] = filter.Value;
                    dtXmlTable.Rows.Add(dr);
                }

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                SqlParameter tvparam = c.Parameters.AddWithValue("@List", dtXmlTable);
                tvparam.SqlDbType = SqlDbType.Structured;
                c.CommandText = "is_GetAllowedQGByPublicity";
                c.Parameters.Add("journalId", SqlDbType.NVarChar).Value = journalId;

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    QualityGoodItem i = new QualityGoodItem();

                    i.Id = r[0].ToString();
                    i.ValueStr = r[1].ToString();

                    ret.Add(i);
                }

            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения разрешённых фильтров для акции "+journalId, ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }


        #endregion
        #region Brand
        public static List<BrandViewModel> GetBrandsCatalog(string strUrl)
        {
            var ret = new List<BrandViewModel>();
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;

            try
            {

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.CommandText = "is_GetBrandCatalog";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    var brandId = r[0].ToString();
                    var brandName = r[1].ToString().Replace("<", "(").Replace(">", ")");
                    var brandDescription = r[2].ToString();
                    var thumbnailPath = strUrl + r[3].ToString();
                    var imagePath = strUrl + r[4].ToString();
                    var urlSlug = r[5].ToString();

                    BrandViewModel brand = new BrandViewModel() { BrandId = brandId, BrandName = brandName, BrandDescription = brandDescription, MainImagePath = new string[] { thumbnailPath, imagePath }, UrlSlug = urlSlug };
                    ret.Add(brand);
                }
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка вывода каталога брендов", ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }

        public static BrandViewModel GetBrandInfoBySlug(string urlSlug, string strUrl)
        {
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            var ret = new BrandViewModel();
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetBrandInfoBySlug";

                c.Parameters.Add("urlSlug", SqlDbType.NVarChar).Value = urlSlug;

                SqlParameter pr = c.Parameters.Add("brandId", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 20;

                pr = c.Parameters.Add("brandName", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 255;

                pr = c.Parameters.Add("brandDescription", SqlDbType.NVarChar, -1);
                pr.Direction = ParameterDirection.Output;

                pr = c.Parameters.Add("thumbnailPath", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 1024;

                pr = c.Parameters.Add("imagePath", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 1024;

                pr = c.Parameters.Add("filterString", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 255;

                pr = c.Parameters.Add("LinkToCatalog", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 255;


                c.ExecuteNonQuery();

                var brandId = c.Parameters["brandId"].Value.ToString();
                var brandName = c.Parameters["brandName"].Value.ToString();
                var brandDescription = c.Parameters["brandDescription"].Value.ToString();
                var thumbnailPath = strUrl + c.Parameters["thumbnailPath"].Value.ToString();
                var imagePath = strUrl + c.Parameters["imagePath"].Value.ToString();
                var filterString = c.Parameters["filterString"].Value.ToString();
                var linkToCatalog = c.Parameters["linkToCatalog"].Value.ToString();

                if (!string.IsNullOrEmpty(linkToCatalog))
                    linkToCatalog = strUrl + linkToCatalog;

                ret.BrandId = brandId;
                ret.BrandName = brandName;
                ret.BrandDescription = brandDescription;
                ret.FilterString = filterString;
                ret.LinkToCatalog = linkToCatalog;

                ret.MainImagePath = new string[] { thumbnailPath, imagePath };
            }
            catch (Exception ex)
            {
                ret = new BrandViewModel();
                DataService.ReportAboutCrash("Ошибка получения информации о бренде "+urlSlug, ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }
            return ret;
        }
        #endregion Brand

        #region SaleAction
        public static List<SaleActionViewModel> GetActionsCatalog4HomePage(string strUrl, int onlyActive, int cnt = 0)
        {
            var ret = new List<SaleActionViewModel>();
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;

            try
            {

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.CommandText = "is_GetActionsCatalog";
                c.Parameters.Add("onlyActive", SqlDbType.Int).Value = onlyActive;
                c.Parameters.Add("cnt", SqlDbType.Int).Value = cnt;

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    var journalId = r[0].ToString();
                    var title = r[1].ToString();
                    var shortDescription = r[2].ToString();
                    var description = r[3].ToString();
                    var meta = r[4].ToString();
                    var urlSlug = r[5].ToString();
                    DateTime dateBegin = DateTime.MinValue;
                    DateTime.TryParse(r[6].ToString(), out dateBegin);
                    DateTime dateEnd = DateTime.MinValue;
                    DateTime.TryParse(r[7].ToString(), out dateEnd);

                    var longImagePath = strUrl + r[8].ToString();
                    var medImagePath = strUrl + r[9].ToString();
                    var shortImagePath = strUrl + r[10].ToString();
                    var linkToCatalog = r[11].ToString();

                    if (!string.IsNullOrEmpty(linkToCatalog))
                        linkToCatalog = strUrl + linkToCatalog;

                    SaleActionViewModel action = new SaleActionViewModel()
                    {
                        PublicityJournalId = journalId,
                        Title = title,
                        ShortDescription = shortDescription,
                        Description = description,
                        UrlSlug = urlSlug,
                        Meta = meta,
                        DateBegin = dateBegin,
                        DateEnd = dateEnd,
                        LongMainImage = longImagePath,
                        MedMainImage = medImagePath,
                        ShortMainImage = shortImagePath,
                        LinkToCatalog = linkToCatalog
                    };

                    ret.Add(action);
                }
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения сниппета для акция ", ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }

        public static List<BannerViewModel> GetBannerCatalog4HomePage(string strUrl)
        {
            var ret = new List<BannerViewModel>();
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;

            try
            {

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.CommandText = "is_GetBannersCatalog";
                
                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    var id = r[0].ToString();
                    var description = r[1].ToString();
                    DateTime dateFrom = DateTime.MinValue;
                    DateTime.TryParse(r[2].ToString(), out dateFrom);
                    DateTime dateTo = DateTime.MinValue;
                    DateTime.TryParse(r[3].ToString(), out dateTo);

                    var imagePath = strUrl + r[4].ToString();
                    var linkTo = r[5].ToString();

                    if (!string.IsNullOrEmpty(linkTo))
                        linkTo = strUrl + linkTo;

                    BannerViewModel banner = new BannerViewModel()
                    {
                        Id = id,
                        Description = description,
                        DateFrom = dateFrom,
                        DateTo = dateTo,
                        PathToImage = imagePath,
                        LinkTo = linkTo
                    };

                    ret.Add(banner);
                }
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения сниппета для баннеров ", ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }

        public static SaleActionViewModel GetSaleActionInfoBySlug(string urlSlug, string strUrl)
        {
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            var ret = new SaleActionViewModel();
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetSaleActionInfoBySlug";

                c.Parameters.Add("urlSlug", SqlDbType.NVarChar).Value = urlSlug;

                SqlParameter pr = c.Parameters.Add("publicityJournalId", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 20;

                pr = c.Parameters.Add("title", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 500;

                pr = c.Parameters.Add("shortDescription", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 4000;

                pr = c.Parameters.Add("description", SqlDbType.NVarChar, -1);
                pr.Direction = ParameterDirection.Output;

                pr = c.Parameters.Add("meta", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 1000;

                pr = c.Parameters.Add("dateBegin", SqlDbType.Date);
                pr.Direction = ParameterDirection.Output;

                pr = c.Parameters.Add("dateEnd", SqlDbType.Date);
                pr.Direction = ParameterDirection.Output;

                pr = c.Parameters.Add("longImagePath", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 1024;

                pr = c.Parameters.Add("linkToCatalog", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 1024;


                c.ExecuteNonQuery();

                var publicityJournalId = c.Parameters["publicityJournalId"].Value.ToString();
                var title = c.Parameters["title"].Value.ToString();
                var shortDescription = c.Parameters["shortDescription"].Value.ToString();
                var description = c.Parameters["description"].Value.ToString();
                var meta = c.Parameters["meta"].Value.ToString();

                var dateBeginStr = c.Parameters["dateBegin"].Value.ToString();
                DateTime dateBegin = DateTime.MinValue;
                DateTime.TryParse(dateBeginStr, out dateBegin);

                var dateEndStr = c.Parameters["dateEnd"].Value.ToString();
                DateTime dateEnd = DateTime.MinValue;
                DateTime.TryParse(dateEndStr, out dateEnd);

                var longImagePath = strUrl + c.Parameters["longImagePath"].Value.ToString();
                var linkToCatalog = c.Parameters["longImagePath"].Value.ToString();

                if (!(string.IsNullOrEmpty(linkToCatalog)))
                    linkToCatalog = strUrl + linkToCatalog;

                ret.PublicityJournalId = publicityJournalId;
                ret.Title = title;
                ret.ShortDescription = shortDescription;
                ret.Description = description;
                ret.Meta = meta;
                ret.DateBegin = dateBegin;
                ret.DateEnd = dateEnd;
                ret.LongMainImage = longImagePath;
                ret.LinkToCatalog = linkToCatalog;

            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения деталей акции ", ex.Message);
                ret = new SaleActionViewModel();
            }
            finally
            {
                p.CloseSQLCommand(c);
            }
            return ret;
        }
        public static List<string> GetPublicityItemsList(string journalId)
        {
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;
            var ret = new List<string>();
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.CommandText = "is_GetPublicityItems";
                c.Parameters.Add("publicityJournalId", SqlDbType.NVarChar).Value = journalId;

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    ret.Add(r[0].ToString());
                }
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения списка номенклатуры по акции "+journalId, ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }
        #endregion SaleAction

        #region Magazine
        public static List<JournalByYearViewModel> GetMagazineCatalog(string strUrl)
        {
            var ret = new List<JournalByYearViewModel>();
            StoredProcedure sp = null;
            SqlDataReader r = null;
            SqlCommand c = null;

            try
            {

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.CommandText = "is_GetMagazineCatalog";

                r = sp.ExecuteReader(c);
                int prevYear = 0;
                JournalByYearViewModel journalByYear = null;

                while (r.Read())
                {
                    var yearStr         = r[0].ToString();
                    int year            = 0;
                    int.TryParse(yearStr, out year);

                    var numberStr       = r[1].ToString();
                    int number          = 0;
                    int.TryParse(numberStr, out number);

                    var descr           = r[2].ToString();
                    var pathToMagazine  = strUrl + r[3].ToString();
                    var pathToCover     = strUrl + r[4].ToString();
                    var pathToCoverTn   = strUrl + r[5].ToString();

                    if (prevYear != year)
                    {
                        if (journalByYear!=null)
                        {
                            ret.Add(journalByYear);
                        }
                        prevYear = year;
                        journalByYear = new JournalByYearViewModel { Year = year, Journals = new List<JournalViewModel>() };
                    }

                    JournalViewModel journal = new JournalViewModel() { Year = year, Number = number, Description = descr, PathToMagazineFile = pathToMagazine, PathToCoverFile = pathToCover, PathToCoverFileTn = pathToCoverTn};
                    journalByYear.Journals.Add(journal);
                }
                ret.Add(journalByYear);
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения каталога журналов ", ex.Message);
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }
        #endregion

        #region PromoCode
        public static int CheckPromoCode(string promoCode, string userId)
        {
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            int ret = 0;
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_CheckPromoCode";

                c.Parameters.Add("userId", SqlDbType.NVarChar).Value = userId;
                c.Parameters.Add("code", SqlDbType.NVarChar).Value = promoCode;

                SqlParameter pr = c.Parameters.Add("retCode", SqlDbType.Int);
                pr.Direction = ParameterDirection.Output;

                c.ExecuteNonQuery();

                var retStr = c.Parameters["retCode"].Value.ToString();
                int.TryParse(retStr, out ret);
            }
            catch (Exception ex)
            {
                ret = 0;
                DataService.ReportAboutCrash("Ошибка проверки промо-кода " + promoCode + " для пользователя "+userId, ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }
            return ret;
        }

        public static bool CheckIsUserMaster(string userId)
        {
            var ret = false;
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            SqlDataReader r = null;
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);
                c.CommandText = "is_CheckIsUserMaster";
                c.Parameters.Add("userId", SqlDbType.NVarChar).Value = userId;

                SqlParameter isMaster = c.Parameters.Add("result", SqlDbType.Int);
                isMaster.Direction = ParameterDirection.Output;

                p.ExecuteNonQuery(c);

                int intRet = 0;
                Int32.TryParse(isMaster.Value.ToString(), out intRet);

                ret = intRet == 1 ? true : false;

            }

            catch (Exception ex)
            {
                ret = false;
                ReportAboutCrash("Ошибка проверки мастер ли пользователь "+userId, ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return ret;
        }

        public static string GetCurrentPromoCodeStatusForUser(string userId)
        {
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            var ret = string.Empty;
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetCurrentPromoCodeStatusForUser";

                c.Parameters.Add("userId", SqlDbType.NVarChar).Value = userId;

                SqlParameter pr = c.Parameters.Add("promoCodeStatus", SqlDbType.NVarChar);
                pr.Size = 255;
                pr.Direction = ParameterDirection.Output;

                c.ExecuteNonQuery();

                ret = c.Parameters["promoCodeStatus"].Value.ToString();
            }
            catch (Exception ex)
            {
                ret = "Ошибка получения статуса промо-кода";
                DataService.ReportAboutCrash("Ошибка проверки промо-кода для пользователя " + userId, ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }
            return ret;
        }

        public static int CheckIsActionsOnBasket(string userId)
        {
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            int ret = 0;
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_CheckIsPublicityForUser";

                c.Parameters.Add("userId", SqlDbType.NVarChar).Value = userId;

                c.Parameters.Add("Result", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                p.ExecuteNonQuery(c);

                ret = Convert.ToInt32(c.Parameters["Result"].Value.ToString());
            }
            catch (Exception ex)
            {
                ret = 0;
                DataService.ReportAboutCrash("Ошибка проверки акций по корзине пользователя " + userId, ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }
            return ret;
        }

        public static int ApplyPromoCode(string promoCode, string userId)
        {
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            int ret = 0;
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_ApplyPromoCode";

                c.Parameters.Add("userId", SqlDbType.NVarChar).Value = userId;
                c.Parameters.Add("code", SqlDbType.NVarChar).Value = promoCode;

                SqlParameter pr = c.Parameters.Add("retCode", SqlDbType.Int);
                pr.Direction = ParameterDirection.Output;

                c.ExecuteNonQuery();

                var retStr = c.Parameters["retCode"].Value.ToString();
                int.TryParse(retStr, out ret);
            }
            catch (Exception ex)
            {
                ret = 0;
                DataService.ReportAboutCrash("Ошибка применения промо-кода " + promoCode + " для пользователя " + userId, ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }
            return ret;
        }

        public static void KillFailedPromoCodesbyUser(string userId)
        {
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_KillFailedPromoCodesStatusesForUser";

                c.Parameters.Add("userId", SqlDbType.NVarChar).Value = userId;

                p.ExecuteNonQuery(c);
            }

            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка очистки кривых промо-кодов для юзера " + userId, ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

        }
        #endregion PromoCode

        #region SaleFunctions
        public static bool SetSaleStatus(SaleInfo _si, SaleStatus _ss, string _userId)
        {

            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            var ret = true;
            try
            {
                if (!_si.OK)
                    throw new Exception("Документ не создан.");

                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_SetSalesStatus";

                c.Parameters.Add("SalesId", SqlDbType.NVarChar).Value = _si.SalesId;
                c.Parameters.Add("status", SqlDbType.Int).Value = _ss;
                c.Parameters.Add("userId", SqlDbType.NVarChar).Value = _userId;

                p.ExecuteNonQuery(c);
                ret = p.OK;
            }
            catch (Exception ex)
            {
                ret = false;
                ReportAboutCrash("Ошибка установки статуса заказа " + _si + " " + _ss.ToString(), ex.Message);                
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return ret;
        }

        public static bool SetSaleOrderParams(string salesId, string userFirstName, string userLastName, string userComment, string phone,
                                                string deliveryTypeId, string deliveryModeId, string cityId, string cityName,
                                                string selfDeliveryPointId, string selfDeliveryPointName, string selfDeliveryPointDescription,
                                                string deliveryAddressStreet, string deliveryAddressBuilding, string deliveryAddressAppt,
                                                string deliveryAddressGate, string deliveryAddressLevel, string deliveryAddressIntercomCode,
                                                double deliveryCost, int transportLeg)
        {
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            var ret = true;
            try
            {
                if (string.IsNullOrWhiteSpace(salesId))
                    throw new Exception("Документ не создан.");

                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_SetSalesOrderParamsNew"; //Mike 2017-03-03

                c.Parameters.Add("SalesId", SqlDbType.NVarChar).Value = salesId;
                c.Parameters.Add("userFirstName", SqlDbType.NVarChar).Value = userFirstName;
                c.Parameters.Add("userLastName", SqlDbType.NVarChar).Value = userLastName;
                c.Parameters.Add("userComment", SqlDbType.NVarChar).Value = userComment;
                c.Parameters.Add("deliveryTypeId", SqlDbType.NVarChar).Value = deliveryTypeId;
                c.Parameters.Add("deliveryModeId", SqlDbType.NVarChar).Value = deliveryModeId;
                c.Parameters.Add("cityId", SqlDbType.NVarChar).Value = cityId;
                c.Parameters.Add("cityName", SqlDbType.NVarChar).Value = cityName == null ? string.Empty : cityName;
                c.Parameters.Add("selfDeliveryPointId", SqlDbType.NVarChar).Value = selfDeliveryPointId;
                c.Parameters.Add("SelfDeliveryPointName", SqlDbType.NVarChar).Value = selfDeliveryPointName;
                c.Parameters.Add("selfDeliveryPointDescription", SqlDbType.NVarChar).Value = selfDeliveryPointDescription;
                c.Parameters.Add("deliveryAddressStreet", SqlDbType.NVarChar).Value = deliveryAddressStreet;
                c.Parameters.Add("deliveryAddressBuilding", SqlDbType.NVarChar).Value = deliveryAddressBuilding;
                c.Parameters.Add("deliveryAddressAppt", SqlDbType.NVarChar).Value = deliveryAddressAppt;
                c.Parameters.Add("deliveryAddressGate", SqlDbType.NVarChar).Value = deliveryAddressGate;
                c.Parameters.Add("deliveryAddressLevel", SqlDbType.NVarChar).Value = deliveryAddressLevel;
                c.Parameters.Add("deliveryAddressIntercomCode", SqlDbType.NVarChar).Value = deliveryAddressIntercomCode;
                c.Parameters.Add("deliveryCost", SqlDbType.Decimal).Value = deliveryCost;
                c.Parameters.Add("transportLeg", SqlDbType.Int).Value = transportLeg;
                //код тарифа!!!!
                if (deliveryTypeId == DeliveryTypeStatic.CDEKDeliveryTypeId)
                {
                    c.Parameters.Add("cdekTariffId", SqlDbType.NVarChar).Value = DeliveryTypeStatic.CDEKDeliveryModeToTariff(deliveryModeId);
                }

                c.Parameters.Add("phoneNumber", SqlDbType.NVarChar).Value = phone;

                p.ExecuteNonQuery(c);
                ret = p.OK;
            }
            catch (Exception ex)
            {
                ret = false;
                ReportAboutCrash("Ошибка установки параметров заказа" + salesId, ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return ret;
        }

        public static bool SetSaleOrderParamsBySaleInfo(SaleInfo _si)
        {
            var ret = SetSaleOrderParams(_si.SalesId, _si.UserFirstName, _si.UserLastName, _si.UserComment, _si.Phone,
                _si.DeliveryTypeId, _si.DeliveryModeId, _si.CityId, _si.CityName, _si.SelfDeliveryPointId,
                _si.SelfDeliveryPointName, _si.SelfDeliveryPointDescription, _si.DeliveryAddressStreet,
                _si.DeliveryAddressBuilding, _si.DeliveryAddressAppt, _si.DeliveryAddressGate, _si.DeliveryAddressLevel,
                _si.DeliveryAddressIntercomCode, _si.DeliveryCost, _si.TransportLeg) ;

            return ret;
        }

        public static bool CalcDeliveryPriceAndSaveDeliveryParams(OrderViewModel orderModel)
        {
            var ret = true;

            //мы прописали все параметры
            var deliveryTypeItem = orderModel.DeliveryType.TypeItems.First(x => x.DeliveryTypeId == orderModel.DeliveryType.SelectedDeliveryTypeId);

            if (deliveryTypeItem == null)
            {
                DataService.ReportAboutCrash(string.Format("В заказе {0} получился пустой деливеритайпитем {1}", orderModel.DeliveryType.SelectedDeliveryTypeId), "");
                return false;
            }

            var deliveryModeItem = deliveryTypeItem.ModeItems.First(x => x.DeliveryModeId == deliveryTypeItem.SelectedDeliveryModeId);

            if (deliveryModeItem == null)
            {
                DataService.ReportAboutCrash(string.Format("В заказе {0} получился пустой деливеримоудитем {1}", deliveryTypeItem.SelectedDeliveryModeId), "");
                return false;
            }

            var deliveryCalculator = DeliveryCalculator.Init(orderModel.OrderId, orderModel.DeliveryType.SelectedDeliveryTypeId,
                deliveryTypeItem.SelectedCityId, deliveryTypeItem.SelectedDeliveryModeId,
                deliveryModeItem.SelectedSelfDeliveryPointId);

            var deliveryParams = deliveryCalculator.GetDeliveryPrice();

            string MessageAboutCostAndPeriod = string.Empty;

            if (deliveryParams.DeliveryCost == -1)
            {
                //ошибка!!
                DataService.ReportAboutCrash("Ошибка расчёта стоимости доставки для заказа " + orderModel.OrderId, orderModel.DeliveryType.SelectedDeliveryTypeId + "|" + deliveryTypeItem.SelectedCityId + "|" + deliveryTypeItem.SelectedDeliveryModeId + "|" + deliveryModeItem.SelectedSelfDeliveryPointId);
                return false;
            }
            else
            {
                MessageAboutCostAndPeriod = deliveryParams.Message;
            }

            //если мы дошли сюда, всё хорошо, сохраним параметры в базе
            //
            var setParmResult = DataService.SetSaleOrderParams(orderModel.OrderId, orderModel.ReceiverFirstName, orderModel.ReceiverLastName, orderModel.Comment, orderModel.ReceiverPhone,
                orderModel.DeliveryType.SelectedDeliveryTypeId, deliveryTypeItem.SelectedDeliveryModeId, deliveryTypeItem.SelectedCityId, deliveryTypeItem.SelectedCityName,
                deliveryModeItem.SelectedSelfDeliveryPointId, deliveryModeItem.SelectedSelfDeliveryPointName, deliveryModeItem.SelectedSelfDeliveryPointDescription,
                deliveryModeItem.DeliveryAddressStreet, deliveryModeItem.DeliveryAddressBuilding, deliveryModeItem.DeliveryAddressAppt, deliveryModeItem.DeliveryAddressGate,
                deliveryModeItem.DeliveryAddressLevel, deliveryModeItem.DeliveryAddressIntercomCode, deliveryParams.DeliveryCost, deliveryParams.TransportLeg);

            if (!setParmResult)
            {
                //сообщаем, куда следует!
                DataService.ReportAboutCrash("Ошибка установки параметров заказа при создании " + orderModel.OrderId, orderModel.ReceiverFirstName + "|" + orderModel.ReceiverLastName + "|" + orderModel.Comment + "|" + orderModel.ReceiverPhone + "|" +
                    orderModel.DeliveryType.SelectedDeliveryTypeId + "|" + deliveryTypeItem.SelectedDeliveryModeId + "|" + deliveryTypeItem.SelectedCityId + "|" + deliveryTypeItem.SelectedCityName + "|" +
                    deliveryModeItem.SelectedSelfDeliveryPointId + "|" + deliveryModeItem.SelectedSelfDeliveryPointName + "|" + deliveryModeItem.SelectedSelfDeliveryPointDescription + "|" +
                    deliveryModeItem.DeliveryAddressStreet + "|" + deliveryModeItem.DeliveryAddressBuilding + "|" + deliveryModeItem.DeliveryAddressAppt + "|" + deliveryModeItem.DeliveryAddressGate + "|" +
                    deliveryModeItem.DeliveryAddressLevel + "|" + deliveryModeItem.DeliveryAddressIntercomCode + "|" + deliveryParams.DeliveryCost + "|" + deliveryParams.TransportLeg);
                return false;
            }

            //прописать в месседж
            var typeIndex = orderModel.DeliveryType.TypeItems.IndexOf(deliveryTypeItem);
            var modeIndex = deliveryTypeItem.ModeItems.IndexOf(deliveryModeItem);

            orderModel.DeliveryType.TypeItems[typeIndex].ModeItems[modeIndex].MessageAboutCostAndPeriod = MessageAboutCostAndPeriod;
            return ret;
        }

        public static bool SetSaleOrderPaymParams(string orderId, string paymServLink, DateTime paymLinkExpDatetime, string paymServAlias, string paymServOrderId)
        {

            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            var ret = true;
            try
            {
                SaleInfo _si = new SaleInfo(orderId);

                if (!_si.OK)
                    throw new Exception("Документ не создан.");

                if (_si.Status != SaleStatus.Picked)
                    throw new Exception(string.Format("Документ в статусе {0} нельзя оплатить", _si.StatusDescription));

                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_SetSalesOrderPaymParams";

                c.Parameters.Add("SalesId", SqlDbType.NVarChar).Value = orderId;
                c.Parameters.Add("paymServLink", SqlDbType.NVarChar).Value = paymServLink;
                c.Parameters.Add("paymServOrderId", SqlDbType.NVarChar).Value = paymServOrderId;
                c.Parameters.Add("paymServAlias", SqlDbType.NVarChar).Value = paymServAlias;
                c.Parameters.Add("paymLinkExpDatetime", SqlDbType.DateTime).Value = paymLinkExpDatetime;

                p.ExecuteNonQuery(c);
                ret = p.OK;
            }
            catch (Exception ex)
            {
                ret = false;
                ReportAboutCrash("Ошибка оплаты заказа " + orderId, ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return ret;
        }



        public static bool SubmitSalesOrder(SaleInfo _si, SaleStatus _ss, string _userId)
        {

            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            var ret = true;
            try
            {
                if (!_si.OK)
                    throw new Exception("Документ не создан.");

                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_SubmitSalesOrder";

                c.Parameters.Add("SalesId", SqlDbType.NVarChar).Value = _si.SalesId;
                c.Parameters.Add("status", SqlDbType.Int).Value = _ss;
                c.Parameters.Add("userId", SqlDbType.NVarChar).Value = _userId;
                c.Parameters.Add("invoiceAmount", SqlDbType.Decimal).Value = _si.InvoiceAmount;

                p.ExecuteNonQuery(c);

                ret = p.OK;
            }
            catch (Exception ex)
            {
                ret = false;
                ReportAboutCrash("Ошибка отправки заказа " + _si.SalesId, ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return ret;
        }

        public static bool PostSalesOrder(SaleInfo _si)
        {

            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            var ret = true;
            try
            {
                if (!_si.OK)
                    throw new Exception("Документ не создан.");

                if (_si.Status != SaleStatus.Picked)
                    throw new Exception("Заказ в этом статусе провести нельзя! " + _si.StatusDescription);

                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_PostSale";

                c.Parameters.Add("SalesId", SqlDbType.NVarChar).Value = _si.SalesId;
                c.Parameters.Add("paymentTypeId", SqlDbType.Int).Value = _si.PaymentTypeId;
                c.Parameters.Add("amountPaid", SqlDbType.Decimal).Value = _si.InvoiceAmount;
                c.Parameters.Add("invoiceAmount", SqlDbType.Decimal).Value = _si.InvoiceAmount;

                p.ExecuteNonQuery(c);
                ret = p.OK;

            }
            catch (Exception ex)
            {
                ret = false;
                ReportAboutCrash("Ошибка разноски заказа " + _si.SalesId, ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return ret;
        }

        [Obsolete]
        //public static bool SetDeliveryParams(SaleInfo _si, SaleStatus _ss, string _userId)
        //{

        //    StoredProcedure p = new StoredProcedure();
        //    SqlCommand c = null;
        //    var ret = true;
        //    try
        //    {
        //        if (!_si.OK)
        //            throw new Exception("Документ не создан.");

        //        c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

        //        c.CommandText = "is_SetSalesOrderParams";

        //        c.Parameters.Add("SalesId", SqlDbType.NVarChar).Value = _si.SalesId;
        //        c.Parameters.Add("userFirstName", SqlDbType.NVarChar).Value = _si.UserFirstName;
        //        c.Parameters.Add("userLastName", SqlDbType.NVarChar).Value = _si.UserLastName;
        //        c.Parameters.Add("userComment", SqlDbType.NVarChar).Value = _si.UserComment;
        //        c.Parameters.Add("deliveryTypeId", SqlDbType.NVarChar).Value = _si.DeliveryTypeId;

        //        p.ExecuteNonQuery(c);
        //        ret = p.OK;

        //    }
        //    catch (Exception ex)
        //    {
        //        ret = false;
        //        ReportAboutCrash("Ошибка установки параметров заказа " + _si.SalesId, ex.Message);
        //    }
        //    finally
        //    {
        //        p.CloseSQLCommand(c);
        //    }

        //    return ret;
        //}
        
        public static bool PickupSalesOrder(SaleInfo _si, SaleStatus _ss, DataTable trans, string _userId)
        {

            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            var ret = true;
            try
            {
                if (!_si.OK)
                    throw new Exception("Документ не создан.");

                if (_si.Status != SaleStatus.Aside)
                    throw new Exception("Собрать можно только документ в статусе Принят");

                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_PickupSalesOrder";

                SqlParameter tvparam = c.Parameters.AddWithValue("@List", trans);
                tvparam.SqlDbType = SqlDbType.Structured;

                c.Parameters.Add("SalesId", SqlDbType.NVarChar).Value = _si.SalesId;
                c.Parameters.Add("status", SqlDbType.Int).Value = _ss;
                c.Parameters.Add("userId", SqlDbType.NVarChar).Value = _userId;

                p.ExecuteNonQuery(c);
                ret = p.OK;

            }
            catch (Exception ex)
            {
                ret = false;
                ReportAboutCrash("Ошибка сборки заказа", _si.SalesId + " | " + ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return ret;
        }

        public static bool CancelSalesOrder(SaleInfo _si, string _userId)
        {

            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            var ret = true;
            try
            {

                if (!_si.OK)
                    throw new Exception("Документ не создан.");

                if (_si.Status != SaleStatus.Aside && _si.Status != SaleStatus.Picked && _si.Status != SaleStatus.Edit)
                    throw new Exception("Отменить можно только заказы в статусе Редактируется, Собран или Принят");

                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_SaleCancel";

                c.Parameters.Add("SalesId", SqlDbType.NVarChar).Value = _si.SalesId;
                c.Parameters.Add("userId", SqlDbType.NVarChar).Value = _userId;

                p.ExecuteNonQuery(c);
                ret = p.OK;

            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка отмены неоплаченного заказа " + _si.SalesId, ex.Message);
                ret = false;
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return ret;
        }

        public static bool RejectSalesOrder(SaleInfo _si, string _userId)
        {
            if (!_si.OK)
                return false;

            if (_si.Status == SaleStatus.Edit || _si.Status == SaleStatus.Aside || _si.Status == SaleStatus.Picked)
                return CancelSalesOrder(_si, _userId);

            if (_si.Status == SaleStatus.Post || _si.Status == SaleStatus.SentToDelivery) // || _si.Status == SaleStatus.Delivered)
                return ReturnSalesOrder(_si, _userId);

            ReportAboutCrash("Кто-то попытался отменить заказ, который отменять нельзя " + _si.SalesId, _userId);
            return false;
        }

        public static bool ReturnSalesOrder(SaleInfo _si, string _userId)
        {

            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            var ret = true;
            try
            {

                if (!_si.OK)
                    throw new Exception("Документ не создан.");

                if (_si.Status != SaleStatus.Post && _si.Status != SaleStatus.SentToDelivery && _si.Status != SaleStatus.Delivered)
                    throw new Exception("Делать возвраты можно только заказы в статусе Оплачен, передан на доставку или доставлен");

                var orderTrans = GetSalesTrans(_si.SalesId, ""); //на урл в данном случае можно не обращать внимания

                var dtXmlTable = new DataTable();
                DataColumn dc = new DataColumn();
                dc.DataType = typeof(string);
                dc.ColumnName = "ItemId";

                dtXmlTable.Columns.Add(dc);

                dc = new DataColumn();
                dc.DataType = typeof(int);
                dc.ColumnName = "Qty";

                dtXmlTable.Columns.Add(dc);

                foreach (var item in orderTrans)
                {
                    DataRow dr = dtXmlTable.NewRow();
                    dr["ItemId"] = item.ItemId;
                    dr["Qty"] = item.Qty;
                    dtXmlTable.Rows.Add(dr);
                }

                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_PostFullReturn";

                c.Parameters.Add("SaleId", SqlDbType.NVarChar).Value = _si.SalesId;
                SqlParameter tvparam = c.Parameters.AddWithValue("@List", dtXmlTable);
                tvparam.SqlDbType = SqlDbType.Structured;

                c.Parameters.Add("userId", SqlDbType.NVarChar).Value = _userId;

                SqlParameter pr = c.Parameters.Add("retId", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 20;

                p.ExecuteNonQuery(c);

                var returnId = c.Parameters["retId"].Value.ToString();

                if (string.IsNullOrEmpty(returnId))
                    throw new Exception("Ошибка создания возврата к заказу " + _si.SalesId);

                //теперь оплата
                SberbankAnswer answer;
                if (_si.TransDate.Date == DateTime.Now.Date)
                {
                    //вызываем reverse
                    answer = ReversePayment(_si);
                }
                else
                {
                    //вызываем refund
                    answer = RefundPayment(_si);
                }

                if (answer.errorCode != 0)
                {
                    throw new Exception("Ошибка отмены заказа сбербанком" + _si.SalesId + " " + answer.errorCode.ToString() + " " + answer.errorMessage);
                }

                ret = true;
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка отмены оплаченного заказа " + _si.SalesId, ex.Message);
                ret = false;
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return ret;
        }

        public bool rejectSaleOrder(SaleInfo _si, string _userId)
        {
            var ret = true;

            if (_si.OK)
                return false;

            if (_si.DocumentTypeId != SaleDocumentTypeId.Sale)
                return false;

            if (_si.Status == SaleStatus.Aside || _si.Status == SaleStatus.Picked || _si.Status == SaleStatus.Edit)
            {
                //возврат делать не надо, просто отменяем
                return CancelSalesOrder(_si, _userId);
            }

            if (_si.Status == SaleStatus.Post || _si.Status == SaleStatus.SentToDelivery || _si.Status == SaleStatus.Delivered)
            {
                //делаем документ возврата и деньги возвращаем
                //return CancelSalesOrder(_si, _userId);
            }

            return ret;
        }

        public static List<OrderViewModel> GetOrdersListBySaleStatus(SaleStatus ss)
        {
            var ret = new List<OrderViewModel>();
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.CommandText = "is_GetOrdersListByStatus";

                if (ss != SaleStatus.None)
                    c.Parameters.Add("status", SqlDbType.Int).Value = ss;

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    var salesId = r[0].ToString();
                    DateTime transDate;
                    DateTime.TryParse(r[1].ToString(), out transDate);
                    double invoiceAmount = 0;
                    Double.TryParse(r[2].ToString(), out invoiceAmount);
                    int status;
                    Int32.TryParse(r[3].ToString(), out status);
                    var custId = r[4].ToString();
                    var custFirstName = r[5].ToString();
                    var custLastName = r[6].ToString();
                    var receiverFirstName = r[7].ToString();
                    var receiverLastName = r[8].ToString();
                    var comment = r[9].ToString();
                    DateTime submitDate;
                    DateTime.TryParse(r[10].ToString(), out submitDate);

                    string statusString = string.Empty;

                    statusString = DataService.SalesStatusToString((SaleStatus)status);
                    //switch ((SaleStatus)Int32.Parse(r["Status"].ToString()))
                    //{
                    //    case (SaleStatus.Edit):
                    //        statusString = "Создан";
                    //        break;
                    //    case (SaleStatus.Aside):
                    //        statusString = "Ожидает проверки";
                    //        break;
                    //    case (SaleStatus.Picked):
                    //        statusString = "Собран и проверен";
                    //        break;
                    //    case (SaleStatus.SentToDelivery):
                    //        statusString = "Передан на доставку";
                    //        break;
                    //    case (SaleStatus.Delivered):
                    //        statusString = "Доставлен";
                    //        break;
                    //    case (SaleStatus.Cancel):
                    //        statusString = "Отменен";
                    //        break;
                    //    case (SaleStatus.Post):
                    //        statusString = "Оплачен. Передан на доставку";
                    //        break;
                    //    case (SaleStatus.Storno):
                    //        statusString = "Сторнирован";
                    //        break;
                    //}


                    OrderViewModel order = new OrderViewModel()
                    {
                        OrderId = salesId,
                        SubmitDate = submitDate,
                        TransDate = transDate,
                        InvoiceAmount = invoiceAmount,
                        Status = (SaleStatus)status,
                        StatusString = statusString,
                        CustId = custId,
                        CustFirstName = custFirstName,
                        CustLastName = custLastName,
                        ReceiverFirstName = receiverFirstName,
                        ReceiverLastName = receiverLastName,
                        Comment = comment,
                        Items = new List<OrderTransViewModel>()
                    };
                    ret.Add(order);
                }
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения списка заказаов по статусу" + ss.ToString(), ex.Message);
                ret = new List<OrderViewModel>();
                string error = ex.Message;
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }
        public static List<OrderViewModel> GetOrdersListByUserId(string userId, string strUrl)
        {
            var ret = new List<OrderViewModel>();
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.CommandText = "is_GetOrdersListByUserId";

                c.Parameters.Add("userId", SqlDbType.NVarChar).Value = userId;

                r = sp.ExecuteReader(c);
                string prevSalesId = string.Empty;

                OrderViewModel order = null; //new OrderViewModel();

                while (r.Read())
                {
                    var salesId = r[0].ToString();
                    DateTime transDate;
                    DateTime.TryParse(r[1].ToString(), out transDate);
                    double invoiceAmount = 0;
                    Double.TryParse(r[2].ToString(), out invoiceAmount);
                    int status;
                    Int32.TryParse(r[3].ToString(), out status);
                    var custId = r[4].ToString();
                    var custFirstName = r[5].ToString();
                    var custLastName = r[6].ToString();
                    var receiverFirstName = r[7].ToString();
                    var receiverLastName = r[8].ToString();
                    var comment = r[9].ToString();
                    var deliveryTypeId = r[10].ToString();
                    double deliveryCost = 0;
                    Double.TryParse(r[11].ToString(), out deliveryCost);

                    var itemId = r[12].ToString();
                    var itemName = r[13].ToString();
                    int qty = 0;
                    Int32.TryParse(r[14].ToString(), out qty);
                    int qtyOrdered = 0;
                    Int32.TryParse(r[15].ToString(), out qtyOrdered);
                    double price = 0;
                    Double.TryParse(r[16].ToString(), out price);
                    var thumbnailPath = strUrl + r[17].ToString();
                    var imagePath = strUrl + r[18].ToString();
                    int availQty = 0;
                    Int32.TryParse(r[19].ToString(), out availQty);
                    var urlSlug = r[20].ToString();

                    DateTime submitDate;
                    DateTime.TryParse(r[21].ToString(), out submitDate);
                    var deliveryModeId = r[22].ToString();
                    var cityId = r[23].ToString();
                    var cityName = r[24].ToString();
                    var selfDeliveryPointId = r[25].ToString();
                    var selfDeliveryPointName = r[26].ToString();
                    var selfDeliveryPointDesc = r[27].ToString();
                    var deliveryAddressStreet = r[28].ToString();
                    var deliveryAddressBuilding = r[29].ToString();
                    var deliveryAddressAppt     = r[30].ToString();
                    var deliveryAddressGate     = r[31].ToString();
                    var deliveryAddressLevel    = r[32].ToString();
                    var deliveryAddressIntercomCode    = r[33].ToString();
                    var extSelfDeliveryPointId         = r[34].ToString();
                    int transportLeg = 0;
                    Int32.TryParse(r[35].ToString(), out transportLeg);
                    var trackingNumber = r[36].ToString();

                    string statusString = string.Empty;
                    statusString = DataService.SalesStatusToString((SaleStatus)status);
                  
                    if (salesId != prevSalesId)
                    {
                        if (order != null)
                        {
                            ret.Add(order);
                        }

                        order = new OrderViewModel()
                        {
                            OrderId = salesId,
                            SubmitDate = submitDate,
                            TransDate = transDate,
                            InvoiceAmount = invoiceAmount,
                            Status = (SaleStatus)status,
                            StatusString = statusString,
                            CustId = custId,
                            CustFirstName = custFirstName,
                            CustLastName = custLastName,
                            ReceiverFirstName = receiverFirstName,
                            ReceiverLastName = receiverLastName,
                            Comment = comment,
                            DeliveryCost = deliveryCost,
                            TotalAmount = deliveryCost + invoiceAmount,
                            TransportLeg = transportLeg,
                            TrackingNumber = trackingNumber,
                            Items = new List<OrderTransViewModel>()                            
                        };
                        //инициализируем поля о доставке
                        order.DeliveryType = DeliveryTypeStatic.InitDeliveryTypeViewModelByParams(salesId, deliveryTypeId, 
                            deliveryModeId, cityId, cityName, selfDeliveryPointId, extSelfDeliveryPointId, 
                            selfDeliveryPointName, selfDeliveryPointDesc, deliveryAddressStreet, 
                            deliveryAddressBuilding, deliveryAddressAppt, deliveryAddressGate, 
                            deliveryAddressLevel, deliveryAddressIntercomCode, transportLeg);

                        prevSalesId = salesId;
                    }

                    OrderTransViewModel item = new OrderTransViewModel() { ItemId = itemId, ItemName = itemName, Qty = qty, QtyOrdered = qtyOrdered, AvailQty = availQty, Price = price, MainImagePath = new string[] { thumbnailPath, imagePath }, UrlSlug = urlSlug };


                    order.Items.Add(item);
                    order.ItemsQty += item.Qty;
                }
                if (order != null)
                    ret.Add(order);
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения списка заказов для пользователя " + userId, ex.Message);
                ret = new List<OrderViewModel>();
                string error = ex.Message;
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }
            return ret;
        }
        //public static List<OrderViewModel> GetOrdersListByUserIdOld(string userId)
        //{
        //    var ret = new List<OrderViewModel>();
        //    StoredProcedure sp = null;
        //    SqlCommand c = null;
        //    SqlDataReader r = null;
        //    try
        //    {
        //        sp = new StoredProcedure();

        //        c = sp.CreateTemplateSQLCommand();

        //        sp = new StoredProcedure();

        //        c = sp.CreateTemplateSQLCommand();

        //        c.CommandText = "is_GetOrdersListByUserId";

        //        c.Parameters.Add("userId", SqlDbType.NVarChar).Value = userId;

        //        r = sp.ExecuteReader(c);

        //        while (r.Read())
        //        {
        //            var salesId = r[0].ToString();
        //            DateTime transDate;
        //            DateTime.TryParse(r[1].ToString(), out transDate);
        //            double invoiceAmount = 0;
        //            Double.TryParse(r[2].ToString(), out invoiceAmount);
        //            int status;
        //            Int32.TryParse(r[3].ToString(), out status);
        //            var custId = r[4].ToString();
        //            var custFirstName = r[5].ToString();
        //            var custLastName = r[6].ToString();
        //            var receiverFirstName = r[7].ToString();
        //            var receiverLastName = r[8].ToString();
        //            var comment = r[9].ToString();
        //            var deliveryTypeId = r[10].ToString();
        //            double deliveryCost = 0;
        //            Double.TryParse(r[11].ToString(), out deliveryCost);


        //            string statusString = string.Empty;
        //            switch ((SaleStatus)Int32.Parse(r["Status"].ToString()))
        //            {
        //                case (SaleStatus.Edit):
        //                    statusString = "Создан";
        //                    break;
        //                case (SaleStatus.Aside):
        //                    statusString = "Принят";
        //                    break;
        //                case (SaleStatus.Picked):
        //                    statusString = "Собран";
        //                    break;
        //                case (SaleStatus.SentToDelivery):
        //                    statusString = "Передан на доставку";
        //                    break;
        //                case (SaleStatus.Delivered):
        //                    statusString = "Доставлен";
        //                    break;
        //                case (SaleStatus.Cancel):
        //                    statusString = "Отменен";
        //                    break;
        //                case (SaleStatus.Post):
        //                    statusString = "Оплачен";
        //                    break;
        //                case (SaleStatus.Storno):
        //                    statusString = "Сторнирован";
        //                    break;
        //            }


        //            OrderViewModel order = new OrderViewModel()
        //            {
        //                OrderId = salesId,
        //                TransDate = transDate,
        //                InvoiceAmount = invoiceAmount,
        //                Status = (SaleStatus)status,
        //                StatusString = statusString,
        //                CustId = custId,
        //                CustFirstName = custFirstName,
        //                CustLastName = custLastName,
        //                ReceiverFirstName = receiverFirstName,
        //                ReceiverLastName = receiverLastName,
        //                Comment = comment,
        //                DeliveryCost = deliveryCost,
        //                Items = GetSalesTrans(salesId)
        //            };
        //            ret.Add(order);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ret = new List<OrderViewModel>();
        //        string error = ex.Message;
        //    }
        //    finally
        //    {
        //        if (r != null)
        //            r.Close();

        //        sp.CloseSQLCommand(c);
        //    }
        //    return ret;
        //}

        public static OrderSummaryViewModel GetOrderSummaryForOrder(string orderId)
        {
            var ret = new OrderSummaryViewModel();
            StoredProcedure sp = new StoredProcedure();
            SqlCommand c = null;
            var cnt = 0;
            try
            {
                //получаем количество постов здесь
                //TODO: здесь также могут быть параметры: код категории, список тэгов
                c = sp.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "[is_GetOrderSummaryByOrderId]";

                c.Parameters.Add("salesId", SqlDbType.NVarChar).Value = orderId;
//                c.Parameters.Add("selectedDeliveryTypeId", SqlDbType.NVarChar).Value = string.Empty;

                SqlParameter pr = c.Parameters.Add("deliveryTypeId", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 20;

                pr = c.Parameters.Add("deliveryModeId", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 20;


                pr = c.Parameters.Add("CityName", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 250;


                pr = c.Parameters.Add("deliveryCost", SqlDbType.Decimal);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 18;
                pr.Scale = 2;

                pr = c.Parameters.Add("orderAmount", SqlDbType.Decimal);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 18;
                pr.Scale = 2;

                SqlParameter remain = c.Parameters.Add("orderQty", SqlDbType.Int);
                remain.Direction = ParameterDirection.Output;

                pr = c.Parameters.Add("orderWeight", SqlDbType.Decimal);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 18;
                pr.Scale = 3;

                pr = c.Parameters.Add("orderVolume", SqlDbType.Decimal);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 18;
                pr.Scale = 8;

                c.ExecuteNonQuery();

                var deliveryTypeId = c.Parameters["deliveryTypeId"].Value.ToString();
                var deliveryModeId = c.Parameters["deliveryModeId"].Value.ToString();

                double deliveryCost = 0;
                double orderAmount = 0;
                int orderQty = 0;

                double orderWeight = 0;
                double orderVolume = 0;

                Double.TryParse(c.Parameters["deliveryCost"].Value.ToString(), out deliveryCost);
                Double.TryParse(c.Parameters["orderAmount"].Value.ToString(), out orderAmount);
                Int32.TryParse(c.Parameters["orderQty"].Value.ToString(), out orderQty);
                Double.TryParse(c.Parameters["orderWeight"].Value.ToString(), out orderWeight);
                Double.TryParse(c.Parameters["orderVolume"].Value.ToString(), out orderVolume);

                ret.DeliveryCost = deliveryCost;
                ret.OrderAmount = orderAmount;
                ret.TotalAmount = orderAmount + deliveryCost;
                ret.ItemsQty = orderQty;
                ret.OrderWeight = orderWeight;
                ret.OrderVolume = orderVolume;

                if (deliveryTypeId!=string.Empty)
                    ret.DeliveryTypeName = DeliveryTypeStatic.GetDeliveryTypeName(deliveryTypeId);

                if (deliveryModeId != string.Empty)
                    ret.DeliveryModeName = DeliveryTypeStatic.GetDeliveryModeName(deliveryModeId);
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения суммарной информации о заказе" + orderId, ex.Message);
            }
            finally
            {
                sp.CloseSQLCommand(c);
            }

            return ret;

        }
        public static bool CheckIsUserOrder(string salesId, string orderId)
        {
            var ret = false;
            //StoredProcedure sp = new StoredProcedure();
            //SqlCommand c = null;
            //try
            //{
            //    c = sp.CreateTemplateSQLCommand(CommandType.StoredProcedure);

            //    c.CommandText = "is_CheckIs";

            //    c.Parameters.Add("salesId", SqlDbType.NVarChar).Value = orderId;
            //    c.Parameters.Add("selectedDeliveryTypeId", SqlDbType.NVarChar).Value = selectedDeliveryTypeId;

            //    SqlParameter remain = c.Parameters.Add("orderQty", SqlDbType.Int);
            //    remain.Direction = ParameterDirection.Output;

            //    c.ExecuteNonQuery();


            //}
            //catch (Exception ex)
            //{

            //}
            //finally
            //{
            //    sp.CloseSQLCommand(c);
            //}

            return ret;
        }        

        #endregion
        #region UserFunctions
        public static List<IshopUserViewModel> GetUserList()
        {
            var ret = new List<IshopUserViewModel>();
            StoredProcedure sp = null;
            SqlCommand c = null;
            SqlDataReader r = null;
            try
            {
                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                sp = new StoredProcedure();

                c = sp.CreateTemplateSQLCommand();

                c.CommandText = "is_GetUserList";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    var netId = r[0].ToString();
                    var aspId = r[1].ToString();
                    var firstName = r[2].ToString();
                    var lastName = r[3].ToString();
                    var email = r[4].ToString();
                    var phone = r[5].ToString();

                    IshopUserViewModel item = new IshopUserViewModel() { NetId = netId, AspId = aspId, FirstName = firstName, LastName = lastName, Email = email, PhoneNumber = phone };
                    ret.Add(item);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            finally
            {
                if (r != null)
                    r.Close();

                sp.CloseSQLCommand(c);
            }

            return ret;
        }
        #endregion
        #region Payment
        public static PaymentTypeViewModel GetPaymentTypes()
        {
            var ret = new PaymentTypeViewModel();
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            SqlDataReader r = null;
            string paymentTypeId = string.Empty;
            string paymentTypeName = string.Empty;

            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetPaymentTypes";

                r = p.ExecuteReader(c);

                while (r.Read())
                {
                    var id = r[0].ToString();
                    var name = r[1].ToString();
                    int isDefault = 0;
                    int.TryParse(r[2].ToString(), out isDefault);

                    if (isDefault == 1)
                    {
                        ret.SelectedPaymentTypeId = id;
                    }
                    ret.Items.Add(new PaymentTypeRadioItem() { PaymentTypeId = id, Name = name });
                }
            }

            catch (Exception ex)
            {
                ret = new PaymentTypeViewModel();
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return ret;
        }
        #endregion
        #region Delivery
        //Mike 2017-02-10 Старая ветка
        //public static DeliveryTypeViewModel GetDeliveryTypes()
        //{
        //    var ret = new DeliveryTypeViewModel();
        //    StoredProcedure p = new StoredProcedure();
        //    SqlCommand c = null;
        //    SqlDataReader r = null;
        //    string deliveryTypeId = string.Empty;
        //    string deliveryTypeName = string.Empty;

        //    try
        //    {
        //        c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

        //        c.CommandText = "is_GetDeliveryTypes";

        //        r = p.ExecuteReader(c);

        //        while (r.Read())
        //        {
        //            var id = r[0].ToString();
        //            var name = r[1].ToString();
        //            int isDefault = 0;
        //            int.TryParse(r[2].ToString(), out isDefault);

        //            if (isDefault == 1)
        //            {
        //                ret.SelectedDeliveryTypeId = id;
        //                ret.SelectedDeliveryTypeDescription = name;
        //            }
        //            ret.Items.Add(new DeliveryTypeRadioItem() { DeliveryTypeId = id, Name = name });
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        ret = new DeliveryTypeViewModel();
        //    }
        //    finally
        //    {
        //        p.CloseSQLCommand(c);
        //    }

        //    return ret;
        //}

        //Старая ветка
        public static SelfDeliveryPointViewModel GetSelfDeliveryPoints()
        {
            var ret = new SelfDeliveryPointViewModel();
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            SqlDataReader r = null;
            /*
            string selfDeliveryPointId = string.Empty;
            string selfDeliveryPointName = string.Empty;
            string selfDeliveryPointDescription = string.Empty;
            int transportLeg = 0;
            double deliveryCost = 0;
            */
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetSelfDeliveryPoints";

                r = p.ExecuteReader(c);

                while (r.Read())
                {
                    var id = r[0].ToString();
                    var name = r[1].ToString();
                    var dsc = r[2].ToString();
                    int leg = 0;
                    int.TryParse(r[3].ToString(), out leg);
                    double cost = 0;
                    double.TryParse(r[4].ToString(), out cost);
                    int isDefault = 0;
                    int.TryParse(r[5].ToString(), out isDefault);

                    if (isDefault == 1)
                    {
                        ret.SelectedSelfDeliveryPointId = id;
                        ret.SelectedSelfDeliveryPointDescription = dsc;
                        //ret.SelectedSelfDeliveryPointName = name;
                        ret.TransportLeg = leg;
                        ret.DeliveryCost = cost;
                    }

                    ret.Items.Add(new SelfDeliveryPointDropDownItem() { SelfDeliveryPointId = id, /*SelfDeliveryPointName = name,*/ SelfDeliveryPointName = dsc, TransportLeg = leg, DeliveryCost = cost });
                }
            }

            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка выбора пунктов самовывоза ", ex.Message);
                ret = new SelfDeliveryPointViewModel();
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return ret;
        }

        //список городов
        public static List<CityDropDownItem> GetSelfDeliveryCitys()
        {
            var ret = new List<CityDropDownItem>();
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            SqlDataReader r = null;
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetSelfDeliveryCitys";

                r = p.ExecuteReader(c);

                while (r.Read())
                {
                    var id = r[0].ToString();
                    var name = r[1].ToString();
                    int isDefault = 0;
                    Int32.TryParse(r[2].ToString(), out isDefault);

                    ret.Add(new CityDropDownItem() { CityId = id, CityName = name, IsDefault = (isDefault == 1)});
                }
            }

            catch (Exception ex)
            {
                ret = new List<CityDropDownItem>();
                ReportAboutCrash("Ошибка получения списка городов", ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return ret;
        }

        //список городов СДЭК
        public static List<CityDropDownItem> GetCDEKDeliveryCitys(string name_startsWith)
        {
            var ret = new List<CityDropDownItem>();
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            SqlDataReader r = null;
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);
                c.CommandText = "is_GetCDEKDeliveryCitys";
                c.Parameters.Add("name_startsWith", SqlDbType.NVarChar).Value = name_startsWith;

                r = p.ExecuteReader(c);

                while (r.Read())
                {
                    var id = r[0].ToString();
                    var name = r[1].ToString();
                    ret.Add(new CityDropDownItem() { CityId = id, CityName = name});
                }
            }

            catch (Exception ex)
            {
                ret = new List<CityDropDownItem>();
                ReportAboutCrash("Ошибка получения списка городов СДЭК", ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return ret;
        }
        //получение названия города СДЭК
        public static string GetCDEKDeliveryCityName(string cityId)
        {
            var ret = string.Format("Город не найден!");
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            SqlDataReader r = null;
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);
                c.CommandText = "is_GetCDEKDeliveryCityName";
                c.Parameters.Add("cityId", SqlDbType.NVarChar).Value = cityId;

                r = p.ExecuteReader(c);

                while (r.Read())
                {
                    ret = r[0].ToString();
                }
            }

            catch (Exception ex)
            {
                ret = string.Format("Город не найден!");
                ReportAboutCrash("Ошибка получения названия города СДЭК "+cityId, ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return ret;
        }
        //получение списка наших пунктов выдачи
        public static List<SelfDeliveryPointDropDownItem> GetSelfDeliveryPointsByCityId(string cityId)
        {
            var ret = new List<SelfDeliveryPointDropDownItem>();
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            SqlDataReader r = null;

            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetSelfDeliveryPointsByCityId";
                c.Parameters.Add("cityId", SqlDbType.NVarChar).Value = cityId;

                r = p.ExecuteReader(c);

                while (r.Read())
                {
                    var id = r[0].ToString();
                    var name = r[1].ToString();
                    var dsc = r[2].ToString();
                    int leg = 0;
                    int.TryParse(r[3].ToString(), out leg);
                    double cost = 0;
                    double.TryParse(r[4].ToString(), out cost);
                    int isDefault = 0;
                    int.TryParse(r[5].ToString(), out isDefault);

                    ret.Add(new SelfDeliveryPointDropDownItem() { SelfDeliveryPointId = id, /*SelfDeliveryPointName = name,*/ SelfDeliveryPointName = dsc, TransportLeg = leg, DeliveryCost = cost, IsDefault = (isDefault==1) });
                }
            }

            catch (Exception ex)
            {
                ret = new List<SelfDeliveryPointDropDownItem>();
                ReportAboutCrash("Ошибка получения списка пунктов самовывоза в городе "+cityId, ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return ret;
        }

        public static SelfDeliveryPointDropDownItem GetSelfDeliveryParams(string selfDeliveryPointId)
        {

            var ret = new SelfDeliveryPointDropDownItem();
            StoredProcedure sp = new StoredProcedure();
            SqlCommand c = null;
            SqlDataReader r = null;
            try
            {
                c = sp.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetSelfDeliveryPointParams";

                c.Parameters.Add("selfDeliveryPointId", SqlDbType.NVarChar).Value = selfDeliveryPointId;
                SqlParameter p;

                p = c.Parameters.Add("SelfDeliveryPointName", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 255;

                p = c.Parameters.Add("SelfDeliveryPointDescription", SqlDbType.NVarChar);
                p.Direction = ParameterDirection.Output;
                p.Size = 1024;

                p = c.Parameters.Add("deliveryCost", SqlDbType.Decimal);
                p.Direction = ParameterDirection.Output;
                p.Size = 18;
                p.Scale = 2;//два знака после запятой

                p = c.Parameters.Add("transportLeg", SqlDbType.Int);
                p.Direction = ParameterDirection.Output;

                c.ExecuteNonQuery();

                var dsc = c.Parameters["selfDeliveryPointDescription"].Value.ToString();
                var name = c.Parameters["selfDeliveryPointName"].Value.ToString();
                double cost = 0;
                double.TryParse(c.Parameters["deliveryCost"].Value.ToString(), out cost);
                int leg = 0;
                int.TryParse(c.Parameters["transportLeg"].Value.ToString(), out leg);

                ret.SelfDeliveryPointId = selfDeliveryPointId;
                ret.SelfDeliveryPointName = dsc;
                ret.SelfDeliveryPointDescription = dsc;
                //ret.SelfDeliveryPointName = name;
                ret.TransportLeg = leg;
                ret.DeliveryCost = cost;
            }

            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения информации о пункте самовывоза" + selfDeliveryPointId, ex.Message);
                ret = new SelfDeliveryPointDropDownItem();
                ret.TransportLeg = -1;
                ret.DeliveryCost = -1;
            }
            finally
            {
                sp.CloseSQLCommand(c);
            }

            return ret;
        }

        public static bool CheckIsFreeDelivery(string salesId)
        {
            var ret = false;
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            SqlDataReader r = null;
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);
                c.CommandText = "is_CheckFreeDelivery";
                c.Parameters.Add("salesId", SqlDbType.NVarChar).Value = salesId;

                r = p.ExecuteReader(c);

                r.Read();

                var strRet = r[0].ToString();
                int intRet = 0;
                Int32.TryParse(strRet, out intRet);

                ret = intRet == 1 ? true : false;

            }

            catch (Exception ex)
            {
                ret = false;
                ReportAboutCrash("Ошибка проверки бесплатной доставки", ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return ret;
        }

        public static string GetDeliveryPromoMessage(string salesId)
        {
            var ret = string.Empty;

            if (string.IsNullOrWhiteSpace(salesId))
                return ret;

            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            SqlDataReader r = null;
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);
                c.CommandText = "is_GetDeliveryPromoMessage";
                c.Parameters.Add("salesId", SqlDbType.NVarChar).Value = salesId;

                r = p.ExecuteReader(c);

                r.Read();

                ret = r[0].ToString();

            }

            catch (Exception ex)
            {
                ret = string.Empty;
                ReportAboutCrash("Ошибка получения промо-сообщения для доставки", ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return ret;
        }

        #endregion
        #region Blog
        public static List<BlogPostViewModel> GetPostList(int pageNum, int postCount, bool onlyPublished = true, string categoryId = "")
        {
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            SqlDataReader r = null;
            var ret = new List<BlogPostViewModel>();
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetBlogPosts";

                c.Parameters.Add("pageNum", SqlDbType.Int).Value = pageNum;
                c.Parameters.Add("postCount", SqlDbType.Int).Value = postCount;
                c.Parameters.Add("categoryId", SqlDbType.NVarChar).Value = categoryId;
                c.Parameters.Add("onlyPublished", SqlDbType.Int).Value = onlyPublished == true ? 1 : 0;
                r = p.ExecuteReader(c);

                string id = string.Empty;
                BlogPostViewModel post = new BlogPostViewModel();

                while (r.Read())
                {
                    var newId = r[0].ToString();

                    if (newId != id)
                    {
                        if (post.PostId != null)
                            ret.Add(post);

                        var title = r[1].ToString();
                        var shortDsc = r[2].ToString();
                        var dsc = r[3].ToString();
                        var meta = r[4].ToString();
                        var urlSlug = r[5].ToString();
                        DateTime postedOn;
                        DateTime.TryParse(r[6].ToString(), out postedOn);
                        DateTime modified;
                        DateTime.TryParse(r[7].ToString(), out modified);
                        var catId = r[8].ToString();
                        int published = 0;
                        int.TryParse(r[9].ToString(), out published);
                        var catName = r[10].ToString();
                        var catDsc = r[11].ToString();
                        var catUrl = r[12].ToString();
                        int orderNum = 0;
                        int.TryParse(r[13].ToString(), out orderNum);
                        id = newId;
                        post = new BlogPostViewModel() { PostId = id, Title = title, ShortDescription = shortDsc, Description = dsc, Meta = meta, UrlSlug = urlSlug, PostedOn = postedOn, Modified = modified, OrderNumber = orderNum, Category = new CategoryViewModel { CategoryId = catId, Name = catName, Description = catDsc, UrlSlug = catUrl }, Published = published == 1 };
                    }

                    var tagId = r[14].ToString();
                    var tagName = r[15].ToString();
                    var tagDesc = r[16].ToString();
                    var tagUrl = r[17].ToString();

                    var tag = new TagViewModel() { TagId = tagId, Name = tagName, Description = tagDesc, UrlSlug = tagUrl };

                    post.Tags.Add(tag);
                }
                if (post.PostId != null)
                    ret.Add(post);
            }

            catch (Exception ex)
            {
                ret = new List<BlogPostViewModel>();
                ReportAboutCrash("Ошибка получения списка статей для блога", ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }
            return ret;
        }

        public static int GetPostCount(bool onlyPublished, string categoryId = "")
        {
            StoredProcedure sp = new StoredProcedure();
            SqlCommand c = null;
            var cnt = 0;
            try
            {
                //получаем количество постов здесь
                //TODO: здесь также могут быть параметры: код категории, список тэгов
                c = sp.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetBlogCount";
                c.Parameters.Add("categoryId", SqlDbType.NVarChar).Value = categoryId;
                c.Parameters.Add("onlyPublished", SqlDbType.Int).Value = onlyPublished == true ? 1 : 0;

                SqlParameter sqlId = c.Parameters.Add("cnt", SqlDbType.Int);
                sqlId.Direction = ParameterDirection.Output;

                c.ExecuteNonQuery();

                int.TryParse(sqlId.Value.ToString(), out cnt);
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения количества статей по категории " + categoryId, ex.Message);
            }
            finally
            {
                sp.CloseSQLCommand(c);
            }

            return cnt;
        }

        public static CategoryViewModel GetBlogCategory(string urlSlug)
        {
            var category = new CategoryViewModel();
            StoredProcedure sp = new StoredProcedure();
            SqlCommand c = null;
            var cnt = 0;
            try
            {
                //получаем количество постов здесь
                //TODO: здесь также могут быть параметры: код категории, список тэгов
                c = sp.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetBlogCategory";
                c.Parameters.Add("urlSlug", SqlDbType.NVarChar).Value = urlSlug;

                SqlParameter pr = c.Parameters.Add("categoryName", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 100;

                pr = c.Parameters.Add("categoryId", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 100;

                pr = c.Parameters.Add("description", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 200;

                c.ExecuteNonQuery();

                var categoryName = c.Parameters["categoryName"].Value.ToString();
                var categoryId = c.Parameters["categoryId"].Value.ToString();
                var description = c.Parameters["description"].Value.ToString();

                category = new CategoryViewModel() { CategoryId = categoryId, Name = categoryName, Description = description, UrlSlug = urlSlug };
            }
            catch (Exception ex)
            {
                category = new CategoryViewModel();
            }
            finally
            {
                sp.CloseSQLCommand(c);
            }

            return category;

        }

        public static TagViewModel GetBlogTag(string urlSlug)
        {
            var tag = new TagViewModel();
            StoredProcedure sp = new StoredProcedure();
            SqlCommand c = null;
            var cnt = 0;
            try
            {
                c = sp.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetBlogTag";
                c.Parameters.Add("urlSlug", SqlDbType.NVarChar).Value = urlSlug;

                SqlParameter pr = c.Parameters.Add("tagName", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 100;

                pr = c.Parameters.Add("tagId", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 100;

                pr = c.Parameters.Add("description", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 200;

                c.ExecuteNonQuery();

                var tagName = c.Parameters["tagName"].Value.ToString();
                var tagId = c.Parameters["tagId"].Value.ToString();
                var description = c.Parameters["description"].Value.ToString();

                tag = new TagViewModel() { TagId = tagId, Name = tagName, Description = description, UrlSlug = urlSlug };
            }
            catch (Exception ex)
            {
                tag = new TagViewModel();
            }
            finally
            {
                sp.CloseSQLCommand(c);
            }

            return tag;

        }

        public static List<BlogPostViewModel> GetPostListByTag(int pageNum, int postCount, string tagId, bool onlyPublished = true)
        {
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            SqlDataReader r = null;
            var ret = new List<BlogPostViewModel>();
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetBlogPostsByTag";

                c.Parameters.Add("pageNum", SqlDbType.Int).Value = pageNum;
                c.Parameters.Add("postCount", SqlDbType.Int).Value = postCount;
                c.Parameters.Add("tagId", SqlDbType.NVarChar).Value = tagId;
                c.Parameters.Add("onlyPublished", SqlDbType.Int).Value = onlyPublished == true ? 1 : 0;
                r = p.ExecuteReader(c);

                string id = string.Empty;
                BlogPostViewModel post = new BlogPostViewModel();

                while (r.Read())
                {
                    var newId = r[0].ToString();

                    if (newId != id)
                    {
                        if (post.PostId != null)
                            ret.Add(post);

                        var title = r[1].ToString();
                        var shortDsc = r[2].ToString();
                        var dsc = r[3].ToString();
                        var meta = r[4].ToString();
                        var urlSlug = r[5].ToString();
                        DateTime postedOn;
                        DateTime.TryParse(r[6].ToString(), out postedOn);
                        DateTime modified;
                        DateTime.TryParse(r[7].ToString(), out modified);
                        int published = 0;
                        int.TryParse(r[8].ToString(), out published);
                        var catId = r[9].ToString();
                        var catName = r[10].ToString();
                        var catDsc = r[11].ToString();
                        var catUrl = r[12].ToString();
                        int orderNum = 0;
                        int.TryParse(r[13].ToString(), out orderNum);
                        id = newId;
                        post = new BlogPostViewModel() { PostId = id, Title = title, ShortDescription = shortDsc, Description = dsc, Meta = meta, UrlSlug = urlSlug, PostedOn = postedOn, Modified = modified, OrderNumber = orderNum, Category = new CategoryViewModel { CategoryId = catId, Name = catName, Description = catDsc, UrlSlug = catUrl }, Published = published == 1 };
                    }

                    var tId = r[14].ToString();
                    var tagName = r[15].ToString();
                    var tagDesc = r[16].ToString();
                    var tagUrl = r[17].ToString();

                    var tag = new TagViewModel() { TagId = tId, Name = tagName, Description = tagDesc, UrlSlug = tagUrl };

                    post.Tags.Add(tag);
                }
                if (post.PostId != null)
                    ret.Add(post);
            }

            catch (Exception ex)
            {
                ret = new List<BlogPostViewModel>();
            }
            finally
            {
                p.CloseSQLCommand(c);
            }
            return ret;
        }

        public static int GetPostCountByTag(bool onlyPublished = true, string tagId = "")
        {
            StoredProcedure sp = new StoredProcedure();
            SqlCommand c = null;
            var cnt = 0;
            try
            {
                //получаем количество постов здесь
                //TODO: здесь также могут быть параметры: код категории, список тэгов
                c = sp.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetBlogCountByTag";
                c.Parameters.Add("tagId", SqlDbType.NVarChar).Value = tagId;
                c.Parameters.Add("onlyPublished", SqlDbType.Int).Value = onlyPublished == true ? 1 : 0;

                SqlParameter sqlId = c.Parameters.Add("cnt", SqlDbType.Int);
                sqlId.Direction = ParameterDirection.Output;

                c.ExecuteNonQuery();

                int.TryParse(sqlId.Value.ToString(), out cnt);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                sp.CloseSQLCommand(c);
            }

            return cnt;
        }

        public static BlogPostViewModel GetPost(int year, int month, string urlSlug)
        {
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            SqlDataReader r = null;
            var ret = new List<BlogPostViewModel>();
            BlogPostViewModel post = new BlogPostViewModel();
            try
            {


                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetBlogPost";

                c.Parameters.Add("urlSlug", SqlDbType.NVarChar).Value = urlSlug;
                c.Parameters.Add("year", SqlDbType.Int).Value = year;
                c.Parameters.Add("month", SqlDbType.Int).Value = month;

                r = p.ExecuteReader(c);

                string id = string.Empty;

                while (r.Read())
                {
                    var newId = r[0].ToString();

                    if (post.PostId == null)
                    {
                        var title = r[1].ToString();
                        var shortDsc = r[2].ToString();
                        var dsc = r[3].ToString();
                        var meta = r[4].ToString();
                        var url = r[5].ToString();
                        DateTime postedOn;
                        DateTime.TryParse(r[6].ToString(), out postedOn);
                        DateTime modified;
                        DateTime.TryParse(r[7].ToString(), out modified);
                        int published = 0;
                        int.TryParse(r[8].ToString(), out published);
                        var catId = r[9].ToString();
                        var catName = r[10].ToString();
                        var catDsc = r[11].ToString();
                        var catUrl = r[12].ToString();
                        id = newId;
                        post = new BlogPostViewModel() { PostId = id, Title = title, ShortDescription = shortDsc, Description = dsc, Meta = meta, UrlSlug = urlSlug, PostedOn = postedOn, Modified = modified, Category = new CategoryViewModel { CategoryId = catId, Name = catName, Description = catDsc, UrlSlug = catUrl }, Published = published == 1 };
                    }

                    var tId = r[13].ToString();
                    var tagName = r[14].ToString();
                    var tagDesc = r[15].ToString();
                    var tagUrl = r[16].ToString();

                    var tag = new TagViewModel() { TagId = tId, Name = tagName, Description = tagDesc, UrlSlug = tagUrl };

                    post.Tags.Add(tag);
                }
            }

            catch (Exception ex)
            {
                ret = new List<BlogPostViewModel>();
            }
            finally
            {
                p.CloseSQLCommand(c);
            }
            return post;
        }

        public static BlogPostViewModel GetPostById(string postId)
        {
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            SqlDataReader r = null;
            var ret = new List<BlogPostViewModel>();
            BlogPostViewModel post = new BlogPostViewModel();
            try
            {


                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetBlogPostById";

                c.Parameters.Add("postId", SqlDbType.NVarChar).Value = postId;

                r = p.ExecuteReader(c);

                string id = string.Empty;
                string tagString = string.Empty;
                while (r.Read())
                {
                    var newId = r[0].ToString();

                    if (post.PostId == null)
                    {
                        var title = r[1].ToString();
                        var shortDsc = r[2].ToString();
                        var dsc = r[3].ToString();
                        var meta = r[4].ToString();
                        var url = r[5].ToString();
                        DateTime postedOn;
                        DateTime.TryParse(r[6].ToString(), out postedOn);
                        DateTime modified;
                        DateTime.TryParse(r[7].ToString(), out modified);
                        int published = 0;
                        int.TryParse(r[8].ToString(), out published);
                        var catId = r[9].ToString();
                        var catName = r[10].ToString();
                        var catDsc = r[11].ToString();
                        var catUrl = r[12].ToString();
                        id = newId;
                        post = new BlogPostViewModel() { PostId = id, Title = title, ShortDescription = shortDsc, Description = dsc, Meta = meta, UrlSlug = url, PostedOn = postedOn, Modified = modified, Category = new CategoryViewModel { CategoryId = catId, Name = catName, Description = catDsc, UrlSlug = catUrl }, Published = published == 1 };
                    }

                    var tId = r[13].ToString();
                    var tagName = r[14].ToString();
                    var tagDesc = r[15].ToString();
                    var tagUrl = r[16].ToString();

                    var tag = new TagViewModel() { TagId = tId, Name = tagName, Description = tagDesc, UrlSlug = tagUrl };

                    post.Tags.Add(tag);
                    tagString += tagName + ",";
                }
                tagString = tagString.Substring(0, tagString.Length - 1);
                post.TagString = tagString;
            }

            catch (Exception ex)
            {
                ret = new List<BlogPostViewModel>();
            }
            finally
            {
                p.CloseSQLCommand(c);
            }
            return post;
        }

        public static string AddPost(BlogPostViewModel post)
        {
            var ret = string.Empty;
            StoredProcedure sp = new StoredProcedure();
            SqlCommand c = null;
            //TODO здесь добавление поста
            try
            {
                c = sp.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_AddBlogPost";
                c.Parameters.Add("title", SqlDbType.NVarChar).Value = post.Title;
                c.Parameters.Add("ShortDescription", SqlDbType.NVarChar).Value = post.ShortDescription;
                c.Parameters.Add("Description", SqlDbType.NVarChar).Value = post.Description;
                c.Parameters.Add("Meta", SqlDbType.NVarChar).Value = post.Meta;
                if (string.IsNullOrEmpty(post.UrlSlug) || string.IsNullOrWhiteSpace(post.UrlSlug))
                    c.Parameters.Add("UrlSlug", SqlDbType.NVarChar).Value = string.Empty;
                else
                    c.Parameters.Add("UrlSlug", SqlDbType.NVarChar).Value = post.UrlSlug;
                c.Parameters.Add("Published", SqlDbType.Int).Value = post.Published == true ? 1 : 0;

                var dtXmlTable = tagStringToDataTable(post.TagString);
                SqlParameter tvparam = c.Parameters.AddWithValue("@List", dtXmlTable);
                tvparam.SqlDbType = SqlDbType.Structured;

                //TODO
                //Категории и теги
                c.Parameters.Add("CategoryId", SqlDbType.NVarChar).Value = post.Category.CategoryId;
                //теги надо передавать с помощью таблицы
                SqlParameter pr = c.Parameters.Add("postId", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 20;

                c.ExecuteNonQuery();

                ret = c.Parameters["postId"].Value.ToString();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                sp.CloseSQLCommand(c);
            }
            return ret;
        }

        public static string EditPost(BlogPostViewModel post)
        {
            StoredProcedure sp = new StoredProcedure();
            SqlCommand c = null;
            //TODO здесь добавление поста
            string ret = string.Empty;
            try
            {
                c = sp.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_EditBlogPost";
                c.Parameters.Add("postId", SqlDbType.NVarChar).Value = post.PostId;
                c.Parameters.Add("title", SqlDbType.NVarChar).Value = post.Title;
                c.Parameters.Add("ShortDescription", SqlDbType.NVarChar).Value = post.ShortDescription;
                c.Parameters.Add("Description", SqlDbType.NVarChar).Value = post.Description;
                c.Parameters.Add("Meta", SqlDbType.NVarChar).Value = post.Meta;
                c.Parameters.Add("Published", SqlDbType.Int).Value = post.Published == true ? 1 : 0;

                var dtXmlTable = tagStringToDataTable(post.TagString);
                SqlParameter tvparam = c.Parameters.AddWithValue("@List", dtXmlTable);
                tvparam.SqlDbType = SqlDbType.Structured;

                //TODO
                //Категории и теги
                c.Parameters.Add("CategoryId", SqlDbType.NVarChar).Value = post.Category.CategoryId;

                c.ExecuteNonQuery();

                ret = "OK";
            }
            catch (Exception ex)
            {
                ret += ex.Message;
            }
            finally
            {
                sp.CloseSQLCommand(c);
            }
            return ret;
        }

        public static List<CategoryDropDownItem> GetBlogCategoryList()
        {
            StoredProcedure sp = new StoredProcedure();
            SqlCommand c = null;
            List<CategoryDropDownItem> ret = new List<CategoryDropDownItem>();
            SqlDataReader r = null;
            try
            {
                c = sp.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetBlogCategoryList";

                r = sp.ExecuteReader(c);

                while (r.Read())
                {
                    var id = r[0].ToString();
                    var name = r[1].ToString();
                    var description = r[2].ToString();
                    var urlSlug = r[3].ToString();

                    ret.Add(new CategoryDropDownItem() { CategoryId = id, Name = name, Description = description, UrlSlug = urlSlug });
                }
            }
            catch (Exception ex)
            {
                ret = new List<CategoryDropDownItem>();
            }
            finally
            {
                sp.CloseSQLCommand(c);
            }

            return ret;
        }

        public static DataTable tagStringToDataTable(string tagString)
        {
            var dtXmlTable = new DataTable();
            DataColumn dc = new DataColumn();
            dc.DataType = typeof(string);
            dc.ColumnName = "TagName";

            dtXmlTable.Columns.Add(dc);

            dc = new DataColumn();
            dc.DataType = typeof(string);
            dc.ColumnName = "TagUrl";

            dtXmlTable.Columns.Add(dc);

            try
            {
                string[] p = tagString.Replace(";", ",").Split(',');

                foreach (var tag in p)
                {
                    DataRow dr = dtXmlTable.NewRow();
                    dr["TagName"] = tag.Trim();
                    dr["TagUrl"] = Transliteration.Front(tag.Trim().Replace(" ", "-"));
                    dtXmlTable.Rows.Add(dr);
                }

            }
            catch (Exception ex)
            {
                dtXmlTable = new DataTable();
                dc = new DataColumn();
                dc.DataType = typeof(string);
                dc.ColumnName = "TypeId";

                dtXmlTable.Columns.Add(dc);

                dc = new DataColumn();
                dc.DataType = typeof(int);
                dc.ColumnName = "ValueId";

                dtXmlTable.Columns.Add(dc);
            }
            return dtXmlTable;
        }
        #endregion Blog

        #region Translit
        public enum TransliterationType
        {
            Gost,
            ISO
        }
        public static class Transliteration
        {
            private static Dictionary<string, string> gost = new Dictionary<string, string>(); //ГОСТ 16876-71
            private static Dictionary<string, string> iso = new Dictionary<string, string>(); //ISO 9-95

            public static string Front(string text)
            {
                return Front(text, TransliterationType.ISO);
            }
            public static string Front(string text, TransliterationType type)
            {
                string output = text;
                Dictionary<string, string> tdict = GetDictionaryByType(type);

                foreach (KeyValuePair<string, string> key in tdict)
                {
                    output = output.Replace(key.Key, key.Value);
                }
                return output;
            }
            public static string Back(string text)
            {
                return Back(text, TransliterationType.ISO);
            }
            public static string Back(string text, TransliterationType type)
            {
                string output = text;
                Dictionary<string, string> tdict = GetDictionaryByType(type);

                foreach (KeyValuePair<string, string> key in tdict)
                {
                    output = output.Replace(key.Value, key.Key);
                }
                return output;
            }

            private static Dictionary<string, string> GetDictionaryByType(TransliterationType type)
            {
                Dictionary<string, string> tdict = iso;
                if (type == TransliterationType.Gost) tdict = gost;
                return tdict;
            }

            static Transliteration()
            {
                gost.Add("Є", "EH");
                gost.Add("І", "I");
                gost.Add("і", "i");
                gost.Add("№", "#");
                gost.Add("є", "eh");
                gost.Add("А", "A");
                gost.Add("Б", "B");
                gost.Add("В", "V");
                gost.Add("Г", "G");
                gost.Add("Д", "D");
                gost.Add("Е", "E");
                gost.Add("Ё", "JO");
                gost.Add("Ж", "ZH");
                gost.Add("З", "Z");
                gost.Add("И", "I");
                gost.Add("Й", "JJ");
                gost.Add("К", "K");
                gost.Add("Л", "L");
                gost.Add("М", "M");
                gost.Add("Н", "N");
                gost.Add("О", "O");
                gost.Add("П", "P");
                gost.Add("Р", "R");
                gost.Add("С", "S");
                gost.Add("Т", "T");
                gost.Add("У", "U");
                gost.Add("Ф", "F");
                gost.Add("Х", "KH");
                gost.Add("Ц", "C");
                gost.Add("Ч", "CH");
                gost.Add("Ш", "SH");
                gost.Add("Щ", "SHH");
                gost.Add("Ъ", "'");
                gost.Add("Ы", "Y");
                gost.Add("Ь", "");
                gost.Add("Э", "EH");
                gost.Add("Ю", "YU");
                gost.Add("Я", "YA");
                gost.Add("а", "a");
                gost.Add("б", "b");
                gost.Add("в", "v");
                gost.Add("г", "g");
                gost.Add("д", "d");
                gost.Add("е", "e");
                gost.Add("ё", "jo");
                gost.Add("ж", "zh");
                gost.Add("з", "z");
                gost.Add("и", "i");
                gost.Add("й", "jj");
                gost.Add("к", "k");
                gost.Add("л", "l");
                gost.Add("м", "m");
                gost.Add("н", "n");
                gost.Add("о", "o");
                gost.Add("п", "p");
                gost.Add("р", "r");
                gost.Add("с", "s");
                gost.Add("т", "t");
                gost.Add("у", "u");

                gost.Add("ф", "f");
                gost.Add("х", "kh");
                gost.Add("ц", "c");
                gost.Add("ч", "ch");
                gost.Add("ш", "sh");
                gost.Add("щ", "shh");
                gost.Add("ъ", "");
                gost.Add("ы", "y");
                gost.Add("ь", "");
                gost.Add("э", "eh");
                gost.Add("ю", "yu");
                gost.Add("я", "ya");
                gost.Add("«", "");
                gost.Add("»", "");
                gost.Add("—", "-");

                iso.Add("Є", "YE");
                iso.Add("І", "I");
                iso.Add("Ѓ", "G");
                iso.Add("і", "i");
                iso.Add("№", "#");
                iso.Add("є", "ye");
                iso.Add("ѓ", "g");
                iso.Add("А", "A");
                iso.Add("Б", "B");
                iso.Add("В", "V");
                iso.Add("Г", "G");
                iso.Add("Д", "D");
                iso.Add("Е", "E");
                iso.Add("Ё", "YO");
                iso.Add("Ж", "ZH");
                iso.Add("З", "Z");
                iso.Add("И", "I");
                iso.Add("Й", "J");
                iso.Add("К", "K");
                iso.Add("Л", "L");
                iso.Add("М", "M");
                iso.Add("Н", "N");
                iso.Add("О", "O");
                iso.Add("П", "P");
                iso.Add("Р", "R");
                iso.Add("С", "S");
                iso.Add("Т", "T");
                iso.Add("У", "U");
                iso.Add("Ф", "F");
                iso.Add("Х", "X");
                iso.Add("Ц", "C");
                iso.Add("Ч", "CH");
                iso.Add("Ш", "SH");
                iso.Add("Щ", "SHH");
                iso.Add("Ъ", "'");
                iso.Add("Ы", "Y");
                iso.Add("Ь", "");
                iso.Add("Э", "E");
                iso.Add("Ю", "YU");
                iso.Add("Я", "YA");
                iso.Add("а", "a");
                iso.Add("б", "b");
                iso.Add("в", "v");
                iso.Add("г", "g");
                iso.Add("д", "d");
                iso.Add("е", "e");
                iso.Add("ё", "yo");
                iso.Add("ж", "zh");
                iso.Add("з", "z");
                iso.Add("и", "i");
                iso.Add("й", "j");
                iso.Add("к", "k");
                iso.Add("л", "l");
                iso.Add("м", "m");
                iso.Add("н", "n");
                iso.Add("о", "o");
                iso.Add("п", "p");
                iso.Add("р", "r");
                iso.Add("с", "s");
                iso.Add("т", "t");
                iso.Add("у", "u");
                iso.Add("ф", "f");
                iso.Add("х", "x");
                iso.Add("ц", "c");
                iso.Add("ч", "ch");
                iso.Add("ш", "sh");
                iso.Add("щ", "shh");
                iso.Add("ъ", "");
                iso.Add("ы", "y");
                iso.Add("ь", "");
                iso.Add("э", "e");
                iso.Add("ю", "yu");
                iso.Add("я", "ya");
                iso.Add("«", "");
                iso.Add("»", "");
                iso.Add("—", "-");
            }
        }
        #endregion translit
        #region Request
        public static string GetUrl(HttpRequestBase request)
        {
            var strUrl = string.Empty;
            try
            {
                var strPathAndQuery = request.Url.PathAndQuery;
                var absolute = request.Url.AbsoluteUri;
                strUrl = absolute.Replace(strPathAndQuery, request.ApplicationPath);
                if (!strUrl.EndsWith("/"))
                    strUrl += "/";
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения strUrl ", ex.Message);
                strUrl = string.Empty;
            }
            return strUrl;
        }

        public static string ParseUrlParms(HttpRequestBase request)
        {
            string url = request.Url.ToString();

            string[] p = url.Split('?');

            if (p.Length >= 2)
            {
                var s = p[1].Replace("/",string.Empty);
                for (var i = 2; i < p.Length; i++) 
                {
                    s +="&"+p[i].Replace("/", string.Empty);
                }
                return s;
            }

            return "";
        }

        public static string DecodeUrlString(string url)
        {
            string newUrl;
            while ((newUrl = Uri.UnescapeDataString(url)) != url)
                url = newUrl;
            return newUrl;
        }

        #endregion
        #region CustomerFeedback
        public static bool CreateCustomerFeedback(int messageType, string name, string phoneNumber, string email, string message)
        {
            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            var ret = true;
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_CreateCustomerFeedback";

                c.Parameters.Add("messageType", SqlDbType.Int).Value = messageType;
                c.Parameters.Add("message", SqlDbType.NVarChar).Value = message;
                c.Parameters.Add("phoneNumber", SqlDbType.NVarChar).Value = phoneNumber;
                c.Parameters.Add("email", SqlDbType.NVarChar).Value = email;
                c.Parameters.Add("name", SqlDbType.NVarChar).Value = name;

                p.ExecuteNonQuery(c);

                //надо же сообщить человеку вне зависимости от того, работает у нас почта или нет

                //TODO: перенести поля в таблицу
                var mailerParams = GetMailRobotParams();
                /*
                string mailRobot = "bulanov@crocus.ru";
                string passwordToRobot = "loki31337";
                string responsible = "mikki.messer@gmail.com";
                */
                switch (messageType)
                {
                    case 0: //телефонный звонок
                        {
                            SendMail(mailerParams, mailerParams.ResponsibleForCustFeeback, "Заказан обратный звонок", "Здравствуйте, поступил новый обратный звонок от абонента " + phoneNumber + " " + name);
                            break;
                        }
                    case 1: //почта
                        {
                            SendMail(mailerParams, mailerParams.ResponsibleForCustFeeback, "Новое сообщение в интернет-магазине", "Здравствуйте, поступило новое сообщение от " + email + " " + phoneNumber + " " + name + "\r\n " + message);
                            break;
                        }
                    default: //почта
                        {
                            SendMail(mailerParams, mailerParams.ResponsibleForCustFeeback, "Новое сообщение в интернет-магазине", "Здравствуйте, поступило новое сообщение от " + email + " " + phoneNumber + " " + name + "\r\n " + message);
                            break;
                        }
                }

            }

            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка отправки сообщения от клиента ", ex.Message);
                ret = false;
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return ret;
        }
        #endregion CustomerFeedback

        #region BugReport
        public static void ReportAboutCrash(string caption, string message, bool sendMail = true)
        {

            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;
            var ret = true;
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_CreateErrorBase";

                c.Parameters.Add("caption", SqlDbType.NVarChar).Value = caption;
                c.Parameters.Add("message", SqlDbType.NVarChar).Value = message;

                p.ExecuteNonQuery(c);

                if (sendMail)
                {
                    var mailerParams = GetMailRobotParams();
                    DataService.SendMail(mailerParams, mailerParams.Support, caption, message);
                }
            }

            catch (Exception ex)
            {
                ret = false;
            }
            finally
            {
                p.CloseSQLCommand(c);
            }
        }
        //public static bool ReportAboutCrash(int messageType, string name, string phoneNumber, string email, string message)
        //{
        //    StoredProcedure p = new StoredProcedure();
        //    SqlCommand c = null;
        //    var ret = true;
        //    try
        //    {
        //        c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

        //        c.CommandText = "is_CreateCustomerFeedback";

        //        c.Parameters.Add("messageType", SqlDbType.Int).Value = messageType;
        //        c.Parameters.Add("message", SqlDbType.NVarChar).Value = message;
        //        c.Parameters.Add("phoneNumber", SqlDbType.NVarChar).Value = phoneNumber;
        //        c.Parameters.Add("email", SqlDbType.NVarChar).Value = email;
        //        c.Parameters.Add("name", SqlDbType.NVarChar).Value = name;

        //        p.ExecuteNonQuery(c);

        //        //надо же сообщить человеку вне зависимости от того, работает у нас почта или нет

        //        //TODO: перенести поля в таблицу
        //        string mailRobot = "bulanov@crocus.ru";
        //        string passwordToRobot = "loki31337";
        //        string responsible = "mikki.messer@gmail.com";

        //        switch (messageType)
        //        {
        //            case 0: //телефонный звонок
        //                {
        //                    SendMail("mail.crocus.ru", mailRobot, passwordToRobot, responsible, "Заказан обратный звонок", "Здравствуйте, поступил новый обратный звонок от абонента " + phoneNumber + " " + name);
        //                    break;
        //                }
        //            case 1: //почта
        //                {
        //                    SendMail("mail.crocus.ru", mailRobot, passwordToRobot, responsible, "Новое сообщение в интернет-магазине", "Здравствуйте, поступило новое сообщение от " + email + " " + phoneNumber + " " + name + "\r\n " + message);
        //                    break;
        //                }
        //            default: //почта
        //                {
        //                    SendMail("mail.crocus.ru", mailRobot, passwordToRobot, responsible, "Новое сообщение в интернет-магазине", "Здравствуйте, поступило новое сообщение от " + email + " " + phoneNumber + " " + name + "\r\n " + message);
        //                    break;
        //                }
        //        }

        //    }

        //    catch (Exception ex)
        //    {
        //        ret = false;
        //    }
        //    finally
        //    {
        //        p.CloseSQLCommand(c);
        //    }

        //    return ret;
        //}
        #endregion

        #region Mailer
        public static MailRobotParams GetMailRobotParams()
        {

            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;

            var mailerParams = new MailRobotParams();
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetMailRobotParams";

                SqlParameter pr = c.Parameters.Add("name", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 50;

                pr = c.Parameters.Add("password", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 50;

                pr = c.Parameters.Add("server", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 200;

                pr = c.Parameters.Add("responsible", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 200;

                pr = c.Parameters.Add("support", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 200;

                pr = c.Parameters.Add("fromAddr", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 50;

                pr = c.Parameters.Add("port", SqlDbType.Int);
                pr.Direction = ParameterDirection.Output;

                pr = c.Parameters.Add("needSsl", SqlDbType.Int);
                pr.Direction = ParameterDirection.Output;

                pr = c.Parameters.Add("pickup", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 200;

                pr = c.Parameters.Add("adminConsole", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 200;

                p.ExecuteNonQuery(c);

                mailerParams.Name = c.Parameters["name"].Value.ToString();
                mailerParams.Passwd = c.Parameters["password"].Value.ToString();
                mailerParams.Server = c.Parameters["server"].Value.ToString();
                mailerParams.ResponsibleForCustFeeback = c.Parameters["responsible"].Value.ToString();
                mailerParams.Support = c.Parameters["support"].Value.ToString();
                mailerParams.FromAddr = c.Parameters["fromAddr"].Value.ToString();
                mailerParams.Pickup = c.Parameters["pickup"].Value.ToString();
                mailerParams.AdminConsole = c.Parameters["adminConsole"].Value.ToString();

                int port = 0;
                int.TryParse(c.Parameters["port"].Value.ToString(), out port);
                mailerParams.Port = port;

                int needSsl = 0;
                int.TryParse(c.Parameters["needSsl"].Value.ToString(), out needSsl);

                mailerParams.NeedSsl = needSsl == 1 ? true : false;
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения параметров почты", ex.Message, false);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return mailerParams;
        }

        public static bool SendMail(MailRobotParams mailerParams, string mailto, string caption, string message, string attachFile = null, bool isBodyHtml = false)
        {
            var ret = false;
            try
            {
                //string smtpServer, string from, string password, 

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(mailerParams.FromAddr);
                var addressList = mailto.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var address in addressList)
                {
                    mail.To.Add(new MailAddress(address));
                }
                mail.Subject = caption;
                mail.Body = message;
                mail.IsBodyHtml = isBodyHtml;
                if (!string.IsNullOrEmpty(attachFile))
                    mail.Attachments.Add(new Attachment(attachFile));
                SmtpClient client = new SmtpClient();
                client.Host = mailerParams.Server;
                client.Port = mailerParams.Port;
                client.EnableSsl = mailerParams.NeedSsl;
                client.Credentials = new NetworkCredential(mailerParams.Name, mailerParams.Passwd);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(mail);
                mail.Dispose();
                ret = true;
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка отправки почты получателю " + mailto + caption, ex.Message);
                ret = false;
            }
            return ret;

        }

        public static eMailMessage GeneratePickupMail(SaleInfo saleInfo, ApplicationUser user)
        {
            var message = new eMailMessage();

            message.Subject = "Ваш заказ " + saleInfo.SalesId + " в krasivo.pro готов к оплате";
            message.Body = "Здравствуйте, " + user.FirstName + "!<br /><br />Ваш заказ " + saleInfo.SalesId + " в интернет - магазине krasivo.pro собран и готов к оплате. Вы можете оплатить его по этой <a href=\"https://krasivo.pro/Cabinet/PayOrder/" + saleInfo.SalesId + "\">ссылке</a> или в <a href=\"https://krasivo.pro/Cabinet/OrderList\">Личном кабинете</a> на сайте.<br />Спасибо, что Вы с нами! Интернет-магазин \"Индустрия красоты\"";

            return message;
        }

        public static eMailMessage GeneratePaymentMail(SaleInfo saleInfo, ApplicationUser user)
        {
            var message = new eMailMessage();

            message.Subject = "Ваш заказ " + saleInfo.SalesId + " в krasivo.pro успешно оплачен";
            message.Body = Properties.Resources.ResourceManager.GetString("PaymentMailTemplate");
            //message.Body = "Здравствуйте, " + user.FirstName + "!<br /><br />Ваш заказ " + saleInfo.SalesId + " в интернет - магазине krasivo.pro успешно оплачен. Вы можете отслеживать статусы заказа в <a href=\"https://krasivo.pro/Cabinet/OrderList\">Личном кабинете</a> на сайте.<br />Спасибо, что Вы с нами! Интернет-магазин \"Индустрия красоты\"";
            message.Body = message.Body.Replace("%%%0", user.FirstName);
            message.Body = message.Body.Replace("%%%1", saleInfo.SalesId);
            message.Body = message.Body.Replace("%%%2", saleInfo.AmountWithDelivery.ToString("N2"));

            return message;
        }

        public static eMailMessage GeneratePaymentMailSupport(SaleInfo saleInfo, ApplicationUser user, string consolePath)
        {
            var message = new eMailMessage();

            message.Subject = "Заказ " + saleInfo.SalesId + " в krasivo.pro оплачен";
            message.Body = "Тип доставки: " + saleInfo.DeliveryTypeDescription + ".<br /><br /> Ссылка для передачи на доставку: " + consolePath + "ReadyToSendToDelivery/" + saleInfo.SalesId + "."; ;

            return message;
        }

        public static eMailMessage GenerateSubmitMailSupport(SaleInfo saleInfo, string consolePath)
        {
            var message = new eMailMessage();

            message.Subject = "Поступил новый заказ " + saleInfo.SalesId + " в интернет-магазине";
            message.Body = "В интернет-магазине поступил новый заказ " + saleInfo.SalesId + ".<br /><br />Ссылка на сборку: "+consolePath+"Pickup/"+saleInfo.SalesId+".";

            return message;
        }

        public static eMailMessage GenerateCancelMailSupport(SaleInfo saleInfo)
        {
            var message = new eMailMessage();

            message.Subject = "Заказ " + saleInfo.SalesId + " отменён";
            message.Body = "Заказ " + saleInfo.SalesId + " отменён.<br /><br />Товар нужно вернуть на склад.";

            return message;
        }

        public static eMailMessage GenerateConfirmMailMessage(string userName, string callbackUrl)
        {
            var message = new eMailMessage();

            message.Subject = "Подтверждение учетной записи в интернет-магазине Krasivo.pro";
            message.Body = Properties.Resources.ResourceManager.GetString("RegisterMailTemplate");

            //message.Body = File.ReadAllText(@"~\mail\register.html");

            message.Body = message.Body.Replace("%%%0", userName);
            message.Body = message.Body.Replace("%%%1", callbackUrl);

            return message;
        }

        public static eMailMessage GenerateForgotMailMessage(string userName, string email, string callbackUrl)
        {
            var message = new eMailMessage();

            message.Subject = "Сброс пароля в интернет-магазине Krasivo.pro";
            message.Body = Properties.Resources.ResourceManager.GetString("ForgotPasswordMailTemplate");

            //message.Body = File.ReadAllText(@"~\mail\register.html");

            message.Body = message.Body.Replace("%%%0", userName);
            message.Body = message.Body.Replace("%%%1", email);
            message.Body = message.Body.Replace("%%%2", callbackUrl);

            return message;
        }

        #endregion Mailer

        #region SMSSender
        public static SMSSenderParams GetSMSRobotParams()
        {

            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;

            var smsParams = new SMSSenderParams();
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetSMSRobotParams";

                SqlParameter pr = c.Parameters.Add("name", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 50;

                pr = c.Parameters.Add("password", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 50;

                pr = c.Parameters.Add("server", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 200;

                pr = c.Parameters.Add("sender", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 200;

                p.ExecuteNonQuery(c);

                smsParams.Name = c.Parameters["name"].Value.ToString();
                smsParams.Passwd = c.Parameters["password"].Value.ToString();
                smsParams.Server = c.Parameters["server"].Value.ToString();
                smsParams.Sender = c.Parameters["sender"].Value.ToString();

            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения параметров смс", ex.Message, false);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return smsParams;
        }

        public static string GenerateSMSSendQuery(SMSSenderParams smsParams, string phone, string message)
        {
            string command = string.Empty;
            command = smsParams.Server + "?login=" + smsParams.Name
                    + "&psw=" + smsParams.Passwd
                    + "&phones=" + phone
                    + "&sender=" + smsParams.Sender
                    + "&charset=utf-8"
                    + "&mes=" + message;
            return command;
        }

        public static string CallSMSSender(string command)
        {
            var response = DataService.GetResponse(command);

            return response;
        }

        public static bool SendSingleSMS(string phone, string message)
        {
            var ret = true;
            var smsParams = GetSMSRobotParams();

            var command = GenerateSMSSendQuery(smsParams, phone, message);

            var response = CallSMSSender(command);

            if (response.IndexOf("OK") == -1)
                return false;

            return ret;
        }

        #endregion SMSSender
        #region Sberbank

        public static SberbankParameters GetSberbankParams()
        {

            StoredProcedure p = new StoredProcedure();
            SqlCommand c = null;

            var sbParams = new SberbankParameters();
            try
            {
                c = p.CreateTemplateSQLCommand(CommandType.StoredProcedure);

                c.CommandText = "is_GetSberbankParams";

                SqlParameter pr = c.Parameters.Add("userName", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 50;

                pr = c.Parameters.Add("password", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 50;

                pr = c.Parameters.Add("registerPageUrl", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 1000;

                pr = c.Parameters.Add("checkStatusUrl", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 1000;

                pr = c.Parameters.Add("checkStatusExtUrl", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 1000;

                pr = c.Parameters.Add("cancelPaymentUrl", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 1000;

                pr = c.Parameters.Add("returnPaymentUrl", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 1000;

                pr = c.Parameters.Add("returnUrl", SqlDbType.NVarChar);
                pr.Direction = ParameterDirection.Output;
                pr.Size = 1000;

                p.ExecuteNonQuery(c);

                var userName = c.Parameters["userName"].Value.ToString();
                var passwd = c.Parameters["password"].Value.ToString();
                var registerPageUrl = c.Parameters["registerPageUrl"].Value.ToString();
                var returnUrl = c.Parameters["returnUrl"].Value.ToString();
                var checkStatusUrl = c.Parameters["checkStatusUrl"].Value.ToString();
                var checkStatusExtUrl = c.Parameters["checkStatusExtUrl"].Value.ToString();
                var cancelPaymentUrl = c.Parameters["cancelPaymentUrl"].Value.ToString();
                var returnPaymentUrl = c.Parameters["returnPaymentUrl"].Value.ToString();

                sbParams.UserName = userName;
                sbParams.Passwd = passwd;
                sbParams.RegisterPageUrl = registerPageUrl;
                sbParams.ReturnUrl = returnUrl;
                sbParams.CheckStatusUrl = checkStatusUrl;
                sbParams.CheckStatusExtUrl = checkStatusExtUrl;
                sbParams.CancelPaymentUrl = cancelPaymentUrl;
                sbParams.ReturnPaymentUrl = returnPaymentUrl;
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения параметров сбера", ex.Message);
            }
            finally
            {
                p.CloseSQLCommand(c);
            }

            return sbParams;
        }

        public static string GenerateReversePaymentQuery(SberbankParameters sbParams, SaleInfo saleInfo)
        {
            string command = string.Empty;

            //вызываем отмену платежа на сервере
            command = sbParams.CancelPaymentUrl + "?orderId=" + saleInfo.PaymServOrderId
                        + "&password=" + sbParams.Passwd
                        + "&userName=" + sbParams.UserName;

            return command;
        }

        public static string GenerateRefundPaymentQuery(SberbankParameters sbParams, SaleInfo saleInfo)
        {
            string command = string.Empty;

            //вызываем регистрацию возврата на сервере
            command = sbParams.ReturnPaymentUrl + "?orderId=" + saleInfo.PaymServOrderId
                        + "&password=" + sbParams.Passwd
                        + "&userName=" + sbParams.UserName
                        + "&amount="
                        + ((saleInfo.InvoiceAmount + saleInfo.DeliveryCost) * 100).ToString();

            return command;
        }

        public static string GenerateRegisterQuery(SberbankParameters sbParams, SaleInfo saleInfo, string email = "")
        {
            string command = string.Empty;

            //вызываем регистрацию заказа на сервере
            command = sbParams.RegisterPageUrl + "?amount="
                        + ((saleInfo.InvoiceAmount + saleInfo.DeliveryCost) * 100).ToString()
                        + "&orderNumber=" + saleInfo.PaymServAlias
                        + "&password=" + sbParams.Passwd
                        + "&returnUrl=" + sbParams.ReturnUrl
                        + "&userName=" + sbParams.UserName;

            if (!string.IsNullOrEmpty(email))
            {
                command += "&jsonParams={\"email\":\"" + email + "\"}";
            }

            return command;
        }

        public static string GenerateCheckStatusQuery(SberbankParameters sbParams, string orderId)
        {
            string command = string.Empty;

            //вызываем регистрацию заказа на сервере
            command = sbParams.CheckStatusUrl + "?orderId="
                        + orderId
                        + "&password=" + sbParams.Passwd
                        + "&returnUrl=" + sbParams.ReturnUrl
                        + "&userName=" + sbParams.UserName;

            return command;
        }

        public static SberbankAnswer CallSberbank(string command)
        {

            var response = DataService.GetResponse(command);

            //парсимответ
            DataContractJsonSerializer serializer =
                new DataContractJsonSerializer(typeof(SberbankAnswer));

            MemoryStream mStrm = new MemoryStream(Encoding.UTF8.GetBytes(response));

            var answer = (SberbankAnswer)serializer.ReadObject(mStrm);

            return answer;
        }

        public static SberbankAnswer RegisterOrder(SaleInfo saleInfo, string email = "")
        {
            SberbankAnswer answer = new SberbankAnswer();
            try
            {
                var sbParams = DataService.GetSberbankParams();

                var command = DataService.GenerateRegisterQuery(sbParams, saleInfo, email);

                answer = CallSberbank(command);
            }
            catch (Exception ex)
            {
                answer = new SberbankAnswer();
                answer.errorCode = -25;
                answer.errorMessage = "Ошибка регистрации заказа " + saleInfo + " " + ex.Message;
            }
            return answer;
        }

        public static SberbankAnswer ReversePayment(SaleInfo saleInfo, string email = "")
        {
            SberbankAnswer answer = new SberbankAnswer();
            try
            {
                var sbParams = DataService.GetSberbankParams();

                var command = DataService.GenerateReversePaymentQuery(sbParams, saleInfo);

                answer = CallSberbank(command);
            }
            catch (Exception ex)
            {
                answer = new SberbankAnswer();
                answer.errorCode = -25;
                answer.errorMessage = "Ошибка отмены оплаты заказа " + saleInfo + " " + ex.Message;
            }
            return answer;
        }

        public static SberbankAnswer RefundPayment(SaleInfo saleInfo, string email = "")
        {
            SberbankAnswer answer = new SberbankAnswer();
            try
            {
                var sbParams = DataService.GetSberbankParams();

                var command = DataService.GenerateRefundPaymentQuery(sbParams, saleInfo);

                answer = CallSberbank(command);
            }
            catch (Exception ex)
            {
                answer = new SberbankAnswer();
                answer.errorCode = -25;
                answer.errorMessage = "Ошибка возврата заказа " + saleInfo + " " + ex.Message;
            }
            return answer;
        }

        public static SberbankAnswer GetOrderStatus(string sbOrderId)
        {
            SberbankAnswer answer = new SberbankAnswer();
            try
            {
                var sbParams = DataService.GetSberbankParams();

                var command = DataService.GenerateCheckStatusQuery(sbParams, sbOrderId);


                answer = CallSberbank(command);

            }
            catch (Exception ex)
            {
                answer = new SberbankAnswer();
                answer.errorCode = -25;
                answer.errorMessage = "Ошибка проверки статуса" + sbOrderId + " " + ex.Message;
            }
            return answer;
        }

        public static bool CheckIsOrderPayed(string sbOrderId, out string paymServAlias)
        {
            bool ret = true;
            paymServAlias = string.Empty;
            try
            {
                var answer = DataService.GetOrderStatus(sbOrderId);
                //если всё хорошо, тут делаем проверку
                //присылаем сообщение клиенту: спасибо говорим            
                switch (answer.OrderStatus)
                {
                    case 2:
                        {
                            paymServAlias = answer.OrderNumber;
                            break;
                        }
                    default:
                        {
                            string errMessage = string.Empty;
                            if (!string.IsNullOrEmpty(answer.errorMessage))
                                errMessage = answer.errorMessage;
                            else
                                errMessage = "Текста нет";

                            ReportAboutCrash("Сбербанк прислал странный ответ на проверку оплаты заказа", sbOrderId + " " + answer.OrderStatus + " | " + errMessage.ToString());
                            //не получилось
                            ret = false;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Сбербанк ошибка проверки статуса оплаты заказа", sbOrderId + " " + ex.Message.ToString());
                ret = false;
                paymServAlias = string.Empty;
            }
            return ret;
        }

        public static string GetResponse(string uri)
        {
            var response = string.Empty;

            try
            {

                var webrequest = System.Net.WebRequest.Create(uri);

                var httpresponse = webrequest.GetResponse();

                var stream = httpresponse.GetResponseStream();

                var streamReader = new System.IO.StreamReader(stream);

                response = streamReader.ReadToEnd();

                streamReader.Close();

                stream.Close();
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка получения респонса " +uri, ex.Message);
                response = string.Empty;
            }

            return response;
        }
        #endregion Sberbank
        #region TestCDEK
        public static string GetMD5hash(string input)
        {
            var ret = string.Empty;
            try
            { 
                // создаем объект этого класса. Отмечу, что он создается не через new, а вызовом метода Create
                MD5 md5Hasher = MD5.Create();

                // Преобразуем входную строку в массив байт и вычисляем хэш
                byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

                // Создаем новый Stringbuilder (Изменяемую строку) для набора байт
                StringBuilder sBuilder = new StringBuilder();

                // Преобразуем каждый байт хэша в шестнадцатеричную строку
                for (int i = 0; i < data.Length; i++)
                {
                    //указывает, что нужно преобразовать элемент в шестнадцатиричную строку длиной в два символа
                    sBuilder.Append(data[i].ToString("x2"));
                }

                ret = sBuilder.ToString();
            }
            catch(Exception ex)
            {
                ReportAboutCrash("Ошибка вычисления MD5 для строки "+input, ex.Message);
            }

            return ret;
        }

        public static string GenerateCDEKRequest(string salesId, string deliveryModeId, int CDEKReceiverCityId)
        {
            var ret = string.Empty;
            try
            {
                var cdekTariffId = DeliveryTypeStatic.CDEKDeliveryModeToTariff(deliveryModeId); //функцию, которая возвращает код тарифа по деливериМоудАйди
                var CDEKParams = new CDEKParams();
                var saleInfo = new SaleInfo(salesId);
                var summary = GetOrderSummaryForOrder(salesId);
                var massWeight = summary.OrderWeight;
                var orderVolume = summary.OrderVolume;
                string dateFmt = "yyyy-MM-dd";
                var dateStr = DateTime.Now.AddDays(1).ToString(dateFmt);//saleInfo.TransDate.ToString(dateFmt);
                var secureString = GetMD5hash(dateStr + "&" + CDEKParams.Passwd);

                ret = "{\"version\":\"1.0\",\"dateExecute\":\"" + dateStr + "\",\"authLogin\":\"" + CDEKParams.Login + "\",\"secure\":\"" + secureString + "\",\"senderCityId\":\"" + CDEKParams.CitySenderId + "\",\"receiverCityId\":\"" + CDEKReceiverCityId.ToString() + "\",\"tariffId\":\"" + cdekTariffId + "\",\"goods\":[{\"weight\":\"" + massWeight.ToString().Replace(",", ".") + "\",\"volume\":\"" + orderVolume.ToString().Replace(",", ".") + "\"}]}";
            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка генерации строки запроса к СДЭК " + salesId + " " + deliveryModeId + " " + CDEKReceiverCityId.ToString(), ex.Message);
            }
            return ret;
        }

        public static DeliveryParams CalculateSDEK(string salesId, string deliveryModeId, int receiverCityId)
        {
            var ret = new DeliveryParams();

            var response = string.Empty;
            string url = "http://api.cdek.ru/calculator/calculate_price_by_json.php";

            string data = GenerateCDEKRequest(salesId, deliveryModeId, receiverCityId);

            WebRequest myReq = WebRequest.Create(url);
            myReq.Method = "POST";
            myReq.ContentLength = data.Length;
            myReq.ContentType = "application/json";            

            UTF8Encoding enc = new UTF8Encoding();

            byte[] formbytes = System.Text.UnicodeEncoding.Default.GetBytes(data);

            using (Stream ds = myReq.GetRequestStream())
            {
                //ds.Write(enc.GetBytes(data), 0, data.Length);
                ds.Write(formbytes, 0, formbytes.Length);
            }


            WebResponse wr = myReq.GetResponse();
            Stream receiveStream = wr.GetResponseStream();

            StreamReader reader = new StreamReader(receiveStream, Encoding.UTF8);
            response = reader.ReadToEnd();
            //прямо тут распарсим ответ
            //парсимответ


            

            if (response.IndexOf("result") == -1)
            {
                DataContractJsonSerializer serializer =
                    new DataContractJsonSerializer(typeof(CDEKError));

                MemoryStream mStrm = new MemoryStream(Encoding.UTF8.GetBytes(response));

                var answer = (CDEKError)serializer.ReadObject(mStrm);
                ret.DeliveryCost = -1;
                ret.TransportLeg = -1;
            }
            else
            {
                DataContractJsonSerializer serializer =
                    new DataContractJsonSerializer(typeof(CDEKSuccess));

                MemoryStream mStrm = new MemoryStream(Encoding.UTF8.GetBytes(response));

                var answer = (CDEKSuccess)serializer.ReadObject(mStrm);
                ret.DeliveryCost = answer.result.price ?? -1;
                ret.TransportLeg = answer.result.deliveryPeriodMax + 1 ?? -1;
            }

            return ret;
        }
        public static List<SelfDeliveryPointDropDownItem> GetCDEKDeliveryPointsInCity(string salesId, int CDEKCityId)
        {
            var ret = new List<SelfDeliveryPointDropDownItem>();
            string url = "http://int.cdek.ru/pvzlist.php?cityid="+CDEKCityId.ToString()+"&type=ALL";

            var s = GetResponse(url);

            //теперь надо распарсить строку
            //и откинуть из списк ПВЗ, не подходящие по весу
            ret = ParseCDEKXmlList(s, salesId);

            return ret;
        }
        public static List<SelfDeliveryPointDropDownItem> ParseCDEKXmlList(string xml, string salesId)
        {
            var ret = new List<SelfDeliveryPointDropDownItem>();
            try
            {
                var summary = GetOrderSummaryForOrder(salesId);
                var massWeight = summary.OrderWeight;
                var volumeWeight = summary.OrderVolume / 5000;
                var orderWeight = massWeight > volumeWeight ? massWeight : volumeWeight;

                XmlDocument xmlDocument = new XmlDocument();
                XmlElement xmlRoot;
                XmlNodeList xmlRecordList;

                XmlNamedNodeMap xm;

                xmlDocument.LoadXml(xml);
                xmlRoot = xmlDocument.DocumentElement;
                xmlRecordList = xmlRoot.SelectNodes("//PvzList//Pvz");

                for (var i = 0; i < xmlRecordList.Count; i++)
                {
                    //взяли ПВЗ
                    var currentPVZ = xmlRecordList.Item(i);
                    //вытащили карту узлов
                    xm = currentPVZ.Attributes;
                    //далее по каждому атрибуту
                    string phone = string.Empty;
                    string address = string.Empty;
                    string worktime = string.Empty;
                    string note = string.Empty;

                    var code = xm.GetNamedItem("Code").Value;
                    var name = xm.GetNamedItem("Name").Value;
                    if (currentPVZ.Attributes["Phone"]!=null)
                        phone = xm.GetNamedItem("Phone").Value;
                    if (currentPVZ.Attributes["Address"]!=null)
                        address = xm.GetNamedItem("Address").Value;
                    if (currentPVZ.Attributes["WorkTime"]!=null)
                        worktime = xm.GetNamedItem("WorkTime").Value;
                    if (currentPVZ.Attributes["Note"] != null)
                        note = xm.GetNamedItem("Note").Value;
                    double weightMin = 0;

                    double weightMax = 0;
                    //теперь дети
                    if (currentPVZ.ChildNodes.Count!=0)
                    {
                        var childNode = currentPVZ.ChildNodes.Item(0);
                        var xmChild = childNode.Attributes;
                        if (childNode.Attributes["WeightMin"] != null)
                        {
                            var weightMinStr = xmChild.GetNamedItem("WeightMin").Value;
                            var weightMaxStr = xmChild.GetNamedItem("WeightMax").Value;

                            Double.TryParse(weightMinStr, NumberStyles.Any, CultureInfo.InvariantCulture, out weightMin);
                            Double.TryParse(weightMaxStr, NumberStyles.Any, CultureInfo.InvariantCulture, out weightMax);

                            if (orderWeight > weightMax)
                                continue;
                        }                        

                    }

                    var description = string.Format("Адрес: {1}. Режим работы: {2}. {3}. Тел: {4}",name,address, worktime, note, phone);
                    //теперь надо сформировать ПВЗ и добавить его в лист
                    var selfDeliveryItem = new SelfDeliveryPointDropDownItem() { SelfDeliveryPointId = code, SelfDeliveryPointName = name, SelfDeliveryPointDescription = description };
                    ret.Add(selfDeliveryItem);
                }

            }
            catch (Exception ex)
            {
                ReportAboutCrash("Ошибка обработки списка ПВЗ СДЭК", xml + " " + ex.Message);
            }
            return ret;
        }
        #endregion TestCDEK
        #region AuxFunctions
        public static string SalesStatusToString(SaleStatus Status)
        {
            string desc = "";

            switch (Status)
            {
                case (SaleStatus.Edit):
                    desc = "Создан";
                    break;
                case (SaleStatus.Aside):
                    desc = "Принят. Ожидает проверки";
                    break;
                case (SaleStatus.Picked):
                    desc = "Собран и проверен";
                    break;
                case (SaleStatus.SentToDelivery):
                    desc = "В пути";
                    break;
                case (SaleStatus.Delivered):
                    desc = "Доставлен";
                    break;
                case (SaleStatus.Cancel):
                    desc = "Отменен";
                    break;
                case (SaleStatus.Post):
                    desc = "Оплачен. Передан на доставку";
                    break;
                case (SaleStatus.Storno):
                    desc = "Отменен после оплаты";
                    break;
                case (SaleStatus.Received):
                    desc = "Завершён";
                    break;
            }

            return desc;
        }

        public static string StripHTML(string inputString)
        {
            return Regex.Replace
              (inputString, HTML_TAG_PATTERN, string.Empty);
        }

        public static bool ScrambledEquals<T>(IEnumerable<T> list1, IEnumerable<T> list2)
        {
            /*
             * Не работает :(
             */
            var cnt = new Dictionary<T, int>();
            foreach (T s in list1)
            {
                if (cnt.ContainsKey(s))
                {
                    cnt[s]++;
                }
                else
                {
                    cnt.Add(s, 1);
                }
            }
            foreach (T s in list2)
            {
                if (cnt.ContainsKey(s))
                {
                    cnt[s]--;
                }
                else
                {
                    return false;
                }
            }
            return cnt.Values.All(c => c == 0);
        }

        public static bool CompareLists (List<QualityGoodItem> list1, List<QualityGoodItem> list2)
        {
            if (list1 == null || list2 == null)
                return false;

            var firstNotSecond = list1.Except(list2).ToList();
            var secondNotFirst = list2.Except(list1).ToList();

            return !firstNotSecond.Any() && !secondNotFirst.Any();
        }
        #endregion AuxFunctions
    }
}

