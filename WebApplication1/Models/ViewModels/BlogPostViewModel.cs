﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebApplication1.Classes;

namespace WebApplication1.Models.ViewModels
{
    public class BlogPostViewModel
    {
        public string PostId { get; set; }
        public int OrderNumber { get; set; }
        [Display(Name = "Заголовок")]
        [Required(ErrorMessage = "Пожалуйста, введите заголовок")]
        public string Title { get; set; }
        [Display(Name = "Краткое описание")]
        [Required(ErrorMessage = "Пожалуйста, введите краткое описание")]
        [DataType(DataType.MultilineText)]
        public string ShortDescription { get; set; }
        [Display(Name = "Текст")]
        [Required(ErrorMessage = "Пожалуйста, введите текст статьи")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [Display(Name = "Ключевые слова")]
        public string Meta { get; set; }
        [Display(Name = "url-адрес")]
        public string UrlSlug { get; set; }
        [Display(Name = "Опубликовано?")]        
        public bool Published { get; set; }
        [Display(Name = "Дата публикации")]
        public DateTime PostedOn
        { get; set; }
        [Display(Name = "Дата изменения")]
        public DateTime? Modified
        { get; set; }

        public CategoryViewModel Category
        { get; set; }

        public IList<TagViewModel> Tags
        { get; set; }

        [Display(Name = "Метки поста")]
        [Required(ErrorMessage = "Пожалуйста, введите хотя бы одну метку")]
        public string TagString { get; set; }
        [Display(Name = "Ошибка")]
        public string ErrorMessage { get; set; }

        public BlogPostViewModel()
        {
            //PostId = string.Empty;
            //OrderNumber = 0;
            //Title = string.Empty;
            //ShortDescription = string.Empty;
            //Description = string.Empty;
            //Meta = string.Empty;
            //UrlSlug = string.Empty;
            //Published = false;
            //PostedOn = DateTime.MinValue;
            Category = new CategoryViewModel();
            Tags = new List<TagViewModel>();
        }
    }

    public class CategoryViewModel
    {
        public string CategoryId
        { get; set; }

        public string Name
        { get; set; }

        public string UrlSlug
        { get; set; }

        public string Description
        { get; set; }

        public List<BlogPostViewModel> Posts { get; set; }
        public List<CategoryDropDownItem> Items { get; set; }

        public CategoryViewModel()
        {
            CategoryId = string.Empty;
            Name = string.Empty;
            UrlSlug = string.Empty;
            Description = string.Empty;
            Posts = new List<BlogPostViewModel>();
            Items = DataService.GetBlogCategoryList();
        }
    }

    public class CategoryDropDownItem
    {
        public string CategoryId
        { get; set; }

        public string Name
        { get; set; }

        public string UrlSlug
        { get; set; }

        public string Description
        { get; set; }

    }

    public class TagViewModel
    {
        public string TagId
        { get; set; }

        public string Name
        { get; set; }

        public string UrlSlug
        { get; set; }

        public string Description
        { get; set; }

        public IList<BlogPostViewModel> Posts
        { get; set; }

        public TagViewModel()
        {
            //TagId = string.Empty;
            //Name = string.Empty;
            //UrlSlug = string.Empty;
            //Description = string.Empty;
            Posts = new List<BlogPostViewModel>();
        }
    }
}