﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.ViewModels
{
    public class JournalViewModel
    {
        [Display(Name = "Год")]
        public int Year { get; set; }

        [Display(Name = "Номер")]
        public int Number { get; set; }

        public string Description { get; set; }

        public string PathToMagazineFile { get; set; }

        public string PathToCoverFileTn { get; set; }
        public string PathToCoverFile { get; set; }
    }

    public class JournalByYearViewModel
    {
        [Display(Name = "Год")]
        public int Year { get; set; }
        public List<JournalViewModel> Journals;

        public JournalByYearViewModel()
        {
            Journals = new List<JournalViewModel>();
        }

    }
}