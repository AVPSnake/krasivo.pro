﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.ViewModels
{
    public class CatalogViewModel
    {
        public IEnumerable<InventTableModel> inventTable { get; set; }
        public IEnumerable<FilterTreeModel> Filter { get; set; } 

        public CatalogViewModel()
        {
            inventTable = new List<InventTableModel>();
            Filter = new List<FilterTreeModel>();
        }
    }
}