﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.ViewModels
{
    public class SaleActionViewModel
    {
        public string PublicityJournalId { get; set; }
        public int OrderNumber { get; set; }
        [Display(Name = "Заголовок")]
        [Required(ErrorMessage = "Пожалуйста, введите заголовок")]
        public string Title { get; set; }
        [Display(Name = "Краткое описание")]
        [Required(ErrorMessage = "Пожалуйста, введите краткое описание")]
        [DataType(DataType.MultilineText)]
        public string ShortDescription { get; set; }
        [Display(Name = "Описание")]
        [Required(ErrorMessage = "Пожалуйста, введите подробное описание акции")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [Display(Name = "Ключевые слова")]
        public string Meta { get; set; }
        [Display(Name = "url-адрес")]
        public string UrlSlug { get; set; }
        [Display(Name = "Опубликовано?")]
        public bool Published { get; set; }
        [Display(Name = "Дата публикации")]
        public DateTime PostedOn
        { get; set; }
        [Display(Name = "Дата начала")]
        public DateTime DateBegin
        { get; set; }
        [Display(Name = "Дата окончания")]
        public DateTime DateEnd
        { get; set; }
        public string LongMainImage { get; set; }
        public string MedMainImage { get; set; }
        public string ShortMainImage { get; set; }
        public string LinkToCatalog { get; set; }

        public List<InventTableModel> PublicityItems { get; set; }
    }
}