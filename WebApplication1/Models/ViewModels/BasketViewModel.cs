﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models.ViewModels
{

    public class BasketViewModel
    {
        public OrderSummaryViewModel Summary { get; set; }
        public List<BasketLineViewModel> BasketLines { get; set; }
        public BasketStatus Status { get; set; }
        public string PromoCode { get; set; }
        public string PromoCodeStatus { get; set; }

        public bool IsMaster { get; set; }

        public bool PossibleActions { get; set; }

        public BasketViewModel()
        {
            Summary = new OrderSummaryViewModel();
            BasketLines = new List<BasketLineViewModel>();
            Status = new BasketStatus();
            PromoCode = string.Empty;
            PromoCodeStatus = string.Empty;
            IsMaster = false;
            PossibleActions = false;
        }
    }

    public class BasketLineViewModel
    {
        [Display(Name = "Код")]
        public string ItemId { get; set; }
        [Display(Name = "Название")]
        public string ItemName { get; set; }
        [Display(Name = "Количество")]
        public int Qty { get; set; }
        [Display(Name = "Цена")]
        public double Price { get; set; }
        [Display(Name = "Доступно")]
        public double AvailQty { get; set; }
        public string[] MainImagePath { get; set; }
        public string UrlSlug { get; set; }
        [Display(Name = "Старая цена")]
        public double OldPrice { get; set; }
        public DateTime PriceExpDate { get; set; }
        public BasketStatus BasketStatus { get; set; }
    }

    public class BasketStatus
    {
        public bool isOk { get; set; }

        public string errorMessage { get; set; }

        public BasketStatus()
        {
            isOk = true;
            errorMessage = string.Empty;
        }
    }

}