﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.ViewModels
{
    public class HomeViewModel
    {
        public IEnumerable<InventTableModel> InventTable { get; set; }
        public IEnumerable<InventTableModel> RecommendedItems { get; set; }
        public IEnumerable<BrandViewModel> Brands { get; set; }
        public IEnumerable<SaleActionViewModel> Actions { get; set; }

        public IEnumerable<BannerViewModel> Banners { get; set; }

        public HomeViewModel()
        {
            InventTable = new List<InventTableModel>();
            RecommendedItems = new List<InventTableModel>();
            Brands = new List<BrandViewModel>();
            Actions = new List<SaleActionViewModel>();
            Banners = new List<BannerViewModel>();
        }
    }
}