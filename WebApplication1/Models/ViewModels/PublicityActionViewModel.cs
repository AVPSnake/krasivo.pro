﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.ViewModels
{
    public class PublicityActionViewModel
    {
        public string OrderId { get; set; }
        public string JournalId { get; set; }
        public string JournalName { get; set; }
        public int PointsEarned { get; set; }
        public int PointsGiven { get; set; }
        public int QtyAvail { get; set; }

        public List<PublicityActionItemsViewModel> PublicityItems { get; set; }
        public List<FilterTreeModel> Filter { get; set; }
        public PublicityActionViewModel()
        {
            PublicityItems = new List<PublicityActionItemsViewModel>();
            Filter = new List<FilterTreeModel>();
        }
    }

    public class PublicityActionItemsViewModel : InventTableModel
    {
        public int PointsCost { get; set; }
        public string JournalId { get; set; }
        public int QtyGiven { get; set; }
    }
}