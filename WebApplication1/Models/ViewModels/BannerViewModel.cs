﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.ViewModels
{
    public class BannerViewModel
    {
        public string Id { get; set; }
        [Display(Name = "Описание")]
        public string Description { get; set; }
        [Display(Name = "Дата начала")]
        public DateTime DateFrom
        { get; set; }
        [Display(Name = "Дата окончания")]
        public DateTime DateTo
        { get; set; }
        public string PathToImage { get; set; }
        public string LinkTo { get; set; }
    }
}