﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Classes;

namespace WebApplication1.Models.ViewModels
{
    public class BlogPostListViewModel
    {
        public IList<BlogPostViewModel> Posts { get; set; }
        public int TotalPosts { get; set; }
        public CategoryViewModel Category { get; set; }
        public TagViewModel Tag { get; set; }

        //просто модель
        public BlogPostListViewModel(int page, int postsOnPage, bool onlyPublished)
        {
                        Posts = DataService.GetPostList(page, postsOnPage, onlyPublished);
                        TotalPosts = DataService.GetPostCount(onlyPublished);
        }
        //с категорией
        public BlogPostListViewModel(int page, int postsOnPage, bool onlyPublished, CategoryViewModel category)
        {
            Posts = DataService.GetPostList(page, postsOnPage, onlyPublished, category.CategoryId);
            TotalPosts = DataService.GetPostCount(onlyPublished, category.CategoryId);
            Category = category;
        }
        //по тегу
        public BlogPostListViewModel(int page, int postsOnPage, bool onlyPublished, TagViewModel tag)
        {
            Posts = DataService.GetPostListByTag(page, postsOnPage, tag.TagId, onlyPublished);
            TotalPosts = DataService.GetPostCountByTag(onlyPublished, tag.TagId);
            Tag = tag;
        }
    }
}