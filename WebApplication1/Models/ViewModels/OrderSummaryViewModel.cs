﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebApplication1.Classes;

namespace WebApplication1.Models.ViewModels
{
    public class OrderSummaryViewModel
    {
        [Display(Name = "Итого")]
        public double TotalAmount { get; set; }
        [Display(Name = "Стоимость товаров")]
        public double OrderAmount { get; set; }
        [Display(Name = "Количество товаров")]
        public int ItemsQty { get; set; }
        [Display(Name = "Доставка")]
        public double DeliveryCost { get; set; }
        [Display(Name = "Тип доставки")]
        public string DeliveryTypeName { get; set; }
        [Display(Name = "Способ доставки")]
        public string DeliveryModeName { get; set; }
        [Display(Name = "Город")]
        public string CityName { get; set; }
        public string PaymentTypeId { get; set; }
        [Display(Name = "Способ оплаты")]
        public string PaymentTypeName { get; set; }
        [Display(Name = "Вес заказа")]
        public double OrderWeight { get; set; }
        [Display(Name = "Объём заказа")]
        public double OrderVolume { get; set; }

        public OrderSummaryViewModel()
        {
            TotalAmount = 0;
            OrderAmount = 0;
            ItemsQty = 0;
            DeliveryCost = 0;
            DeliveryTypeName = string.Empty;
            DeliveryModeName = string.Empty;
            CityName = string.Empty;
            PaymentTypeName = string.Empty;
            OrderWeight = 0;
            OrderVolume = 0;
            PaymentTypeId = PaymentTypeStatic.CardPaymentTypeId;
        }
    }
}