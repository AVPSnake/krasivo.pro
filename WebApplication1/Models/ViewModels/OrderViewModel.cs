﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebApplication1.Classes;

namespace WebApplication1.Models.ViewModels
{
    #region Payment
    public class PaymentTypeViewModel
    {
        public string SelectedPaymentTypeId { get; set; }
        public string SelectedPaymentTypeName { get; set; }
        public List<PaymentTypeRadioItem> Items { get; set; }

        public PaymentTypeViewModel()
        {
            SelectedPaymentTypeId = string.Empty;
            SelectedPaymentTypeName = string.Empty;
            Items = new List<PaymentTypeRadioItem>();
        }

        public PaymentTypeViewModel(string typeId)
        {
            var ret = PaymentTypeStatic.GetPaymentTypes();

            switch (typeId)
            {
                case (PaymentTypeStatic.CashOnDeliveryPaymentTypeId):
                    {
                        SelectedPaymentTypeId = typeId;
                        SelectedPaymentTypeName = PaymentTypeStatic.CashOnDeliveryPaymentTypeName;
                        break;
                    }
                default:
                    {
                        SelectedPaymentTypeId = PaymentTypeStatic.CardPaymentTypeId;
                        SelectedPaymentTypeName = PaymentTypeStatic.CardPaymentTypeName;
                        break;
                    }
            }

            Items = ret.Items;
        }
    }

    public class PaymentTypeRadioItem
    {
        public string PaymentTypeId { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
    }
    #endregion

    #region Delivery

    #region DeliveryType
    public class DeliveryTypeViewModel
    {
        public string SelectedDeliveryTypeId { get; set; }
        [Display(Name = "Способ доставки")]
        public string SelectedDeliveryTypeDescription { get; set; }                
        public List<DeliveryTypeRadioItem> TypeItems { get; set; }

        public DeliveryTypeViewModel()
        {
            SelectedDeliveryTypeId = string.Empty;
            SelectedDeliveryTypeDescription = string.Empty;
            TypeItems = new List<DeliveryTypeRadioItem>();
        }
    }

    public class DeliveryTypeRadioItem
    {
        public string DeliveryTypeId { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        public string Message { get; set; }
        public string ParentOrderId { get; set; }

        public string SelectedCityId { get; set; }
        [Display(Name = "Город")]
        public string SelectedCityName { get; set; }

        public List<CityDropDownItem> CityItems { get; set; }

        public string SelectedDeliveryModeId { get; set; }
        [Display(Name = "Тип доставки")]
        public string SelectedDeliveryModeDescription { get; set; }

        public List<DeliveryModeRadioItem> ModeItems { get; set; }

        public DeliveryTypeRadioItem()
        {
            Message = string.Empty;
            ModeItems = new List<DeliveryModeRadioItem>();
            CityItems = new List<CityDropDownItem>();
        }
    }
    #endregion DeliveryType

    #region SelfDeliveryPoint
    public class SelfDeliveryPointViewModel
    {
        public string SelectedSelfDeliveryPointId { get; set; }
        [Display(Name = "Название пункта самовывоза")]
        public string SelectedSelfDeliveryPointName { get; set; }
        [Display(Name = "Где забирать")]
        public string SelectedSelfDeliveryPointDescription { get; set; }
        public int TransportLeg { get; set; }
        public double DeliveryCost { get; set; }
        public List<SelfDeliveryPointDropDownItem> Items { get; set; }

        public SelfDeliveryPointViewModel()
        {
            SelectedSelfDeliveryPointId = string.Empty;
            //
            SelectedSelfDeliveryPointName = string.Empty;
            SelectedSelfDeliveryPointDescription = string.Empty;
            TransportLeg = 0;
            DeliveryCost = 0;
            Items = new List<SelfDeliveryPointDropDownItem>();
        }
    }

    public class SelfDeliveryPointDropDownItem
    {
        public string SelfDeliveryPointId { get; set; }
        public string SelfDeliveryPointName { get; set; }
        public string SelfDeliveryPointDescription { get; set; }
        public int TransportLeg { get; set; }
        public double DeliveryCost { get; set; }
        public bool IsDefault { get; set; }
    }
    #endregion SelfDeliveryPoint

    #region DeliveryCity
    public class CityDropDownItem
    {
        public string CityId { get; set; } //индекс
        public string CityName { get; set; }

        public bool IsCashOnDemandAllowed { get; set; }

        public double CashOnDemandLimit { get; set; }
        public bool IsDefault { get; set; }
    }
    public class CDEKCityArray
    {
        public List<CityDropDownItem> geonames {get; set;}

        public CDEKCityArray()
        {
            geonames = new List<CityDropDownItem>();
        }
    }
    #endregion DeliveryCity

    #region DeliveryMode

    public class DeliveryModeRadioItem
    {
        public string DeliveryModeId { get; set; }
        public string Name { get; set; }
        public string MessageAboutCostAndPeriod { get; set; }

        public bool IsDefault { get; set; }

        public string SelectedSelfDeliveryPointId { get; set; }
        public string SelectedSelfDeliveryPointName { get; set; }
        [Display(Name = "Где забирать")]        
        public string SelectedSelfDeliveryPointDescription { get; set; }

        public List<SelfDeliveryPointDropDownItem> SelfDeliveryItems { get; set; }
        [Display(Name = "Улица")]
        public string DeliveryAddressStreet { get; set; }
        [Display(Name = "Дом, корпус, строение")]
        public string DeliveryAddressBuilding { get; set; }
        [Display(Name = "Квартира, офис")]
        public string DeliveryAddressAppt { get; set; }
        [Display(Name = "Подъезд")]
        public string DeliveryAddressGate { get; set; }
        [Display(Name = "Код домофона")]
        public string DeliveryAddressIntercomCode { get; set; }
        [Display(Name = "Этаж")]
        public string DeliveryAddressLevel { get; set; }


        public DeliveryModeRadioItem()
        {
            SelfDeliveryItems = new List<SelfDeliveryPointDropDownItem>();
            DeliveryAddressStreet = string.Empty;
            DeliveryAddressBuilding = string.Empty;
            DeliveryAddressAppt = string.Empty;
            DeliveryAddressGate = string.Empty;
            DeliveryAddressIntercomCode = string.Empty;
            DeliveryAddressLevel = string.Empty;
        }
    }
    #endregion DeliveryMode
    #endregion Delivery

    #region order
    public class OrderViewModel
    {
        [Display(Name = "Код заказа")]
        public string OrderId { get; set; }
        [Display(Name = "Дата заказа")]
        public DateTime TransDate { get; set; }
        [Display(Name = "Дата создания заказа")]
        public DateTime SubmitDate { get; set; }
        [Display(Name = "Код клиента")]
        public string CustId { get; set; }
        [Display(Name = "Имя клиента")]
        public string CustFirstName { get; set; }
        [Display(Name = "Фамилия клиента")]
        public string CustLastName { get; set; }

        [Display(Name = "Сумма заказа")]
        public double InvoiceAmount { get; set; }
        [Display(Name = "Статус")]
        public string StatusString { get; set; }
        public SaleStatus Status { get; set; }
        [Required(ErrorMessage = "Пожалуйста, укажите имя получателя")]
        [StringLength(255, MinimumLength = 2)]
        [Display(Name = "Имя получателя")]
        public string ReceiverFirstName { get; set; }
        [Required(ErrorMessage = "Пожалуйста, укажите фамилию получателя")]
        [StringLength(255, MinimumLength = 2)]
        [Display(Name = "Фамилия получателя")]
        public string ReceiverLastName { get; set; }
        [Required(ErrorMessage = "Пожалуйста, укажите телефон получателя")]
        [Phone]
        [Display(Name = "Телефон получателя")]
        public string ReceiverPhone { get; set; }
        [StringLength(1024)]
        [Display(Name = "Комментарий")]
        public string Comment { get; set; }

        // [DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}", ApplyFormatInEditMode = true)]
        //[Display(Name = "Дата доставки")]
        //public string DeliveryDate { get; set; }

        [Display(Name = "Стоимость доставки")]
        public double DeliveryCost { get; set; }

        [Display(Name = "Срок доставки")]
        public double TransportLeg { get; set; }

        [Display(Name = "Сумма к оплате")]
        public double TotalAmount { get; set; }

        //поля для оплаты сбербанком
        public string PaymServLink { get; set; }
        public DateTime PaymLinkExpDatetime { get; set; }
        public string PaymServOrderId { get; set; }
        public string PaymServAlias { get; set; }
        //
        [Display(Name = "Количество товаров")]
        public int ItemsQty { get; set; }

        [Display(Name = "Трекинг-номер")]
        public string TrackingNumber { get; set; }


        public List<OrderTransViewModel> Items { get; set; }
        public PaymentTypeViewModel PaymentType { get; set; }
        public DeliveryTypeViewModel DeliveryType { get; set; }

        public DateTime PaymentDate { get; set; }
        //доставка
        //public SelfDeliveryPointViewModel SelfDeliveryPoint { get; set; }

        //public SelfDeliveryPointViewModel SelfDeliveryPointSDEKPVZ { get; set; }
        //public SelfDeliveryPointViewModel SelfDeliveryPointSDEKCourier { get; set; }

        /*
        public string CityName { get; set; }
        public string CityIdSdek { get; set; }
        public string SdekAddress { get; set; }
        public string CitySenderIdSdek { get; set; }
        */

        public OrderSummaryViewModel Summary { get; set; }

        public List<PublicityActionViewModel> Actions { get; set; }
        public List<OrderTransViewModel> PresentItems { get; set; }
        public OrderViewModel()
        {
            OrderId = string.Empty;
            TransDate = DateTime.MinValue;
            SubmitDate = DateTime.MinValue;
            CustId = string.Empty;
            CustFirstName = string.Empty;
            CustLastName = string.Empty;
            InvoiceAmount = 0;
            StatusString = string.Empty;
            Status = SaleStatus.None;
            ReceiverFirstName = string.Empty;
            ReceiverLastName = string.Empty;
            Comment = string.Empty;
            Items = new List<OrderTransViewModel>();
            PaymentType = PaymentTypeStatic.GetPaymentTypes(); //DataService.GetPaymentTypes();
            DeliveryType = DeliveryTypeStatic.GetDeliveryTypes(string.Empty); //DataService.GetDeliveryTypes();
            ReceiverPhone = string.Empty;
            Summary = new OrderSummaryViewModel();
            ItemsQty = 0;
            TrackingNumber = string.Empty; //трек-номер
            PaymentDate = DateTime.MinValue; //дата оплаты
            //акции
            Actions = new List<PublicityActionViewModel>();
            //подарки по акциям
            PresentItems = new List<OrderTransViewModel>();

            //SelfDeliveryPoint = DataService.GetSelfDeliveryPoints(); //это вьюмодел, придётся делать так
            //SelfDeliveryPointSDEKPVZ = new SelfDeliveryPointViewModel(); //это вьюмодел жи
            //SelfDeliveryPointSDEKCourier = new SelfDeliveryPointViewModel(); //это вьюмодел жи
            //SdekAddress = string.Empty;
            //DeliveryDate = DateTime.Now.AddDays(1).ToShortDateString();

            //модель для партиала с суммарной инфой
            //CitySenderIdSdek = "438"; //код Ростова в базе СДЭК
        }
    }
    #endregion
}