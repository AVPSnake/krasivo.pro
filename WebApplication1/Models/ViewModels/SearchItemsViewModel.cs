﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.ViewModels
{
    public class SearchItemsViewModel
    {
        public string QueryStr { get; set; }
        public string QueryStrDecoded { get; set; }
        public string Message { get; set;}
        public int ItemsFound { get; set;}
        public List<InventTableModel> inventTable { get; set; }

        public SearchItemsViewModel()
        {
            QueryStr = string.Empty;
            QueryStrDecoded = string.Empty;
            Message = string.Empty;
            ItemsFound = 0;
            inventTable = new List<InventTableModel>();                
        }
    }
}