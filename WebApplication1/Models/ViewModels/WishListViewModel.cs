﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.ViewModels
{
    public class WishListViewModel
    {
            [Display(Name = "Код")]
            public string ItemId { get; set; }
            [Display(Name = "Название")]
            public string ItemName { get; set; }
            [Display(Name = "Цена")]
            public double Price { get; set; }
            [Display(Name = "Доступно")]
            public double AvailQty { get; set; }
            public string[] MainImagePath { get; set; }
            public string UrlSlug { get; set; }
            [Display(Name = "Цена")]
            public double OldPrice { get; set; }
            public DateTime PriceExpDate { get; set; }
    }
}