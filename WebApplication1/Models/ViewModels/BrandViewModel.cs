﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.ViewModels
{
    public class BrandViewModel
    {
        [Display(Name = "Код")]
        public string BrandId { get; set; }
        [Display(Name = "Название")]
        public string BrandName { get; set; }
        [Display(Name = "Описание")]
        public string BrandDescription { get; set; }
        public string[] MainImagePath { get; set; }
        public string UrlSlug { get; set; }
        public string FilterString { get; set; }
        public string LinkToCatalog { get; set; }

        public List<InventTableModel> FeaturedItems { get; set; }
        public BrandViewModel()
        {
            MainImagePath = new string[] {string.Empty, string.Empty};
            FeaturedItems = new List<InventTableModel>();
        }
    }
}