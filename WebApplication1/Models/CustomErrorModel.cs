﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class CustomErrorModel
    {
        public string ErrorTitle { get; set; }
        public string ErrorDescription { get; set; }
    }
}