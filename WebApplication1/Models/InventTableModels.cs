﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using WebApplication1.Classes;

namespace WebApplication1.Models
{
    public class InventTableModel
    {
        public string ItemId { get; set; }
        public string ItemName { get; set; }
        public double Price { get; set; }
        public double OldPrice { get; set; }
        public int OrderNumber { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public int AvailQty { get; set; }
        public string[] MainImagePath { get; set; }
        public List<string[]> AllImages { get; set; }
        public string Article { get; set; }
        public string UrlSlug { get; set; }
        public List<ItemProperty> Properties { get; set; }
        public bool Hit { get; set; }
        public bool Recommended { get; set; }
        public bool isAction { get; set; }

        public DateTime PriceExpDate { get; set; }

        public List<InventTableModel> RelatedItems { get; set; }
        public InventTableModel()
        {
            MainImagePath = new string[] { "", "" };
            AllImages = new List<string[]>();
            Properties = new List<ItemProperty>();
            Hit = false;
            Recommended = false;
            isAction = false;
            PriceExpDate = DateTime.MinValue;
            RelatedItems = new List<InventTableModel>();
        }
    }
}