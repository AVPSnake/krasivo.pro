﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{

    public class BasketModel
    {
        public string ItemId { get; set; }
        public int Qty { get; set; }
        public double Price { get; set; }
        public string[] MainImagePath { get; set; }
    }
}