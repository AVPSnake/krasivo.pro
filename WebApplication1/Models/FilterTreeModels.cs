﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{

    public class TreeNodeModel
    {
        public int Id
        {
            get;
            set;
        }
        public string Title
        {
            get;
            set;
        }
        public bool Checked
        {
            get;
            set;
        }
        public bool Enabled
        {
            get;
            set;
        }
    }

    public class FilterTreeModel
    {
        public FilterTreeModel()
        {
            TreeNodeModel = new List<TreeNodeModel>();
        }

        public string Id
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }

        public bool Checked
        {
            get;
            set;
        }

        public bool Enabled
        {
            get;
            set;
        }

        public List<TreeNodeModel> TreeNodeModel
        {
            get;
            set;
        }

    }
}