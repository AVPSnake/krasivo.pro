﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Classes;
using WebApplication1.Models.ViewModels;

namespace WebApplication1.Controllers
{
    public class BlogController : Controller
    {
        const int postsOnPage = 10;
        // GET: Blog
        public ActionResult Index(int page = 1)
        {
            /*
            var posts = DataService.GetPostList(page, postsOnPage, true );

            var totalPosts = DataService.GetPostCount(true);

            var postList = new BlogPostListViewModel()
            {
                Posts = posts,
                TotalPosts = totalPosts
            };
            */
            var postList = new BlogPostListViewModel(page: page, postsOnPage: postsOnPage, onlyPublished: true);
            return View(postList);
        }
        public ActionResult Category(string category, int page = 1, bool onlyPublished = true)
        {
            // TODO: get the posts for the category and return the view.
            var Category = DataService.GetBlogCategory(category);

            if (String.IsNullOrEmpty(Category.CategoryId))
                throw new HttpException(404, "Категория не существует");

            /*
            var posts = DataService.GetPostList(page, postsOnPage, onlyPublished, category);
            var totalPosts = DataService.GetPostCount(onlyPublished, category);

            var postList = new BlogPostListViewModel()
            {
                Posts = posts,
                TotalPosts = totalPosts,
                Category = Category
            };
            */
            var postList = new BlogPostListViewModel(page, postsOnPage, onlyPublished, Category);
            ViewBag.Title = String.Format(@"Статьи в рубрике: ""{0}""",
                                    Category.Name);
            return View("Index", postList);
        }

        public ActionResult Tag(string tag, int page = 1, bool onlyPublished = true)
        {
            // TODO: get the posts for the category and return the view.
            var Tag = DataService.GetBlogTag(tag);

            if (String.IsNullOrEmpty(Tag.TagId))
                throw new HttpException(404, "Тег не существует");

            /*
            var posts = DataService.GetPostListByTag(page, postsOnPage, Tag.TagId, onlyPublished);
            var totalPosts = DataService.GetPostCount(onlyPublished, Tag.TagId);

            var postList = new BlogPostListViewModel()
            {
                Posts = posts,
                TotalPosts = totalPosts,
                Tag = Tag
            };
            */
            var postList = new BlogPostListViewModel(page, postsOnPage, onlyPublished, Tag);

            ViewBag.Title = string.Format(@"Все статьи с меткой ""{0}""",
                                    Tag.Name);
            return View("Index", postList);
        }

        public ActionResult Post(int year, int month, string title)
        {
            var post = DataService.GetPost(year, month, title);

            if (post == null)
                throw new HttpException(404, "Пост не найден");

            //TODO прямо в коде надо проверять на принадлежность к роли

            if (post.Published == false)
                throw new HttpException(404, "Пост не найден");

            return View(post);
        }


        [Authorize, Authorize(Roles = "Admin")]
        public ActionResult Manage(int page = 1)
        {
            //сделать аналог index с пагинацией
            /*
            var posts = DataService.GetPostList(page, postsOnPage, false);

            var totalPosts = DataService.GetPostCount(false);

            var postList = new BlogPostListViewModel()
            {
                Posts = posts,
                TotalPosts = totalPosts
            };
            */
            var postList = new BlogPostListViewModel(page: page, postsOnPage: postsOnPage, onlyPublished: false);

            return View(postList);
        }

        [Authorize, Authorize(Roles = "Admin")]
        public ActionResult AddPost()
        {
            BlogPostViewModel post = new BlogPostViewModel();
            return View("AddPost",post);
        }

        [HttpPost]
        [Authorize, Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult AddPost(BlogPostViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(model.UrlSlug) || string.IsNullOrWhiteSpace(model.UrlSlug))
                    model.UrlSlug = DataService.Transliteration.Front(model.Title.ToLower()).Replace(" ","-");

                if (string.IsNullOrEmpty(model.Meta) || string.IsNullOrWhiteSpace(model.Meta))
                    model.Meta = model.Title;

                var id = DataService.AddPost(model);
            }
            else
            {
                model.ErrorMessage = "Проверьте заполнение полей";
                return View("AddPost", model);
            }
            return RedirectToAction("Index");
        }

        [Authorize, Authorize(Roles = "Admin")]
        public ActionResult EditPost(string id)
        {
            BlogPostViewModel post = DataService.GetPostById(id);
            if (post.PostId == "")
                throw new HttpException(404, "Пост не найден");
            return View("EditPost", post);
        }

        [HttpPost]
        [Authorize, Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult EditPost(BlogPostViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.ErrorMessage = string.Empty;
                if (string.IsNullOrEmpty(model.Meta) || string.IsNullOrWhiteSpace(model.Meta))
                    model.Meta = model.Title;

                var answer = DataService.EditPost(model);
                if (answer!= "OK")
                {
                    model.ErrorMessage = answer; 
                    return View("EditPost", model);
                }
            }
            else
            {
                model.ErrorMessage = "Проверьте заполнение полей";
                return View("AddPost", model);
            }
            return RedirectToAction("Index");
        }

        public string Upload(HttpPostedFileBase file)
        {
            string path = String.Empty;
            string saveloc = "/img/";
            string relativeloc = "/img/";
            string filename = Path.GetFileName(file.FileName);

            //var ret = "<script>top.$('.mce-btn.mce-open').parent().find('.mce-textbox').val('" + "http://bulanov/krasivo.local/img/me.png" + "').closest('.mce-window').find('.mce-primary').click();</script>";
            //return ret;

            if (file != null && file.ContentLength > 0) //TODO: проверка на то, что файл - реально картинка
            {
                try
                {
                    var curDate = DateTime.Today;
                    var curYear = curDate.Year;
                    var curMonth = curDate.Month;


                    var path2save = Path.Combine(HttpContext.Server.MapPath(saveloc), curYear.ToString() + "\\" + curMonth.ToString() + "\\");
                    var ext = Path.GetExtension(filename);
                    var nameWOExt = Path.GetFileNameWithoutExtension(filename);

                    if (!Directory.Exists(path2save))
                        Directory.CreateDirectory(path2save);

                    path = path2save;
                    relativeloc = Path.Combine(relativeloc, curYear.ToString() + "//" + curMonth.ToString() + "//");
                    path2save = Path.Combine(path2save, filename);
                    var i = 1;
                    while (System.IO.File.Exists(path2save))
                    {
                        var fName = nameWOExt + "-" + i.ToString() +  ext;
                        path2save = Path.Combine(path, fName);
                        i++;
                    }

                    file.SaveAs(path2save);
                }
                catch (Exception e)
                {
                    return "<script>alert('Failed: " + e + "');</script>";
                }
            }
            else
            {
                return "<script>alert('Failed: Unkown Error. This form only accepts valid images.');</script>";
            }
            //т.е. здесь мы закачали
            //теперь нужен метод контроллера, который нам вернёт
            return "<script>top.$('.mce-btn.mce-open').parent().find('.mce-textbox').val('" + relativeloc + filename + "').closest('.mce-window').find('.mce-primary').click();</script>";
        }
    }
}