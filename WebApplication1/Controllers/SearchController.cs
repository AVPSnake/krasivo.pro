﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Classes;
using WebApplication1.Models;
using WebApplication1.Models.ViewModels;

namespace WebApplication1.Controllers
{
    public class SearchController : Controller
    {
        public int SortNum2Start = 0;
        public int itemCnt2Display = 90;

        // GET: Search
        public ActionResult Index(string text)
        {
            if (string.IsNullOrEmpty(text))
                text = string.Empty;

            string userId;

            if (Request.IsAuthenticated)
                userId = User.Identity.GetUserId();
            else
                userId = Request.AnonymousID;

            text = text.Trim();

            List<string> selectedItems = new List<string>();
            var model = new SearchItemsViewModel();
            var strUrl = DataService.GetUrl(Request);

            ViewBag.Title = string.Format("Результаты поиска");
            ViewBag.Description = "Интернет магазин Индустрия красоты. Krasivo.pro - профессиональная косметика известных брендов";
            ViewBag.Keywords = "Интернет-магазин Индустрия красоты. Профессиональная косметика. Интернет-магазин профессиональной косметики, купить профессиональную косметику в Ростове, купить профессиональную косметику, krasivo.pro, красиво.про, интернет-магазин Индустрия красоты Ростов, красиво про, купить Elgon в Ростове, магазин Elgon в Ростове, магазин Inebrya в Ростове, интернет-магазин Elgon, Moser, интернет-магазин Moser, купить Moser в Ростове, купить Moser, доставка, журнал Индустрия красоты, гид по профессиональной косметике, Wella, selective, barex, купить selective в Ростове-на-Дону, купить Barex в Ростове-на-Дону, купить Selective, купить Barex, красиво про";

            if (text.Length < 3)
            {
                model.QueryStr = text;
                model.Message = string.Format("Ваш запрос \"{0}\" содержит менее трёх символов. Возможно, Вас заинтересуют:", text);
                model.inventTable = DataService.GetCatalogByItemList(selectedItems, false, strUrl, 10, true, userId);
                return View(model);
            }

            model.QueryStrDecoded = text;
            model.QueryStr = Url.Encode(text);

            //Mike 2017-09-21
            //отключаем новую ветку

            //вот здесь ниже - старая ветка. Как было.

            //text = "%" + text.Replace(" ", "%") + "%";
            selectedItems = DataService.GetItemsListBySearchString(text);
            model.ItemsFound = selectedItems.Count;
            if (model.ItemsFound == 0)
            {
                //новая ветка                        
                var catalogUrl = DataService.SearchItemsByQuery(text);

                if (!string.IsNullOrEmpty(catalogUrl))
                {
                    return Redirect(strUrl + catalogUrl);
                }

                model.Message = string.Format("По запросу \"{0}\" ничего не найдено. Возможно, Вас заинтересуют:", model.QueryStrDecoded);
                //выводить хиты!!!
                model.inventTable = DataService.GetCatalogByItemList(selectedItems, false, strUrl, 10, true, userId);
                return View(model);
            }

            model.inventTable = DataService.GetCatalogByItemList(selectedItems, true, strUrl, itemCnt2Display, false, userId);
            model.Message = string.Format("По запросу \"{0}\" найдено {1} товаров", model.QueryStrDecoded, model.ItemsFound);
            return View(model);
        }

        public ActionResult Search(string search_string)
        {
            return RedirectToAction("Index", new { text = search_string });
        }

        public ActionResult GetItemsPartial(/*string searchString = ""*/)
        {

            string userId;

            if (Request.IsAuthenticated)
                userId = User.Identity.GetUserId();
            else
                userId = Request.AnonymousID;


            var strUrl = DataService.GetUrl(Request);

            var urlParm = DataService.ParseUrlParms(Request);

            var urlParmStringDecoded = HttpUtility.UrlDecode(urlParm, System.Text.Encoding.UTF8);

            string[] parms = urlParmStringDecoded.Split(new string[] { "&" }, StringSplitOptions.RemoveEmptyEntries);

            var searchString = string.Empty;



            searchString = DataService.ParseSearchString(DataService.ParseUrlParms(Request), out SortNum2Start);

            var selectedItems = DataService.GetItemsListBySearchString(searchString);
            var itemsList = DataService.GetCatalogByItemList(selectedItems, true, strUrl, itemCnt2Display, false, userId, SortNum2Start);
            return PartialView("ItemsPartial", itemsList);

        }

        public ContentResult GetSearchedItemsQty()
        {
            int i = DataService.GetFoundItemsCount(DataService.ParseSearchString(DataService.ParseUrlParms(Request), out SortNum2Start));

            return Content(i.ToString());
        }


    }
}