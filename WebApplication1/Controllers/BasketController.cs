﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Classes;
using WebApplication1.Models;
using WebApplication1.Models.ViewModels;
using System.Web.SessionState;
using Microsoft.AspNet.Identity.Owin;

namespace WebApplication1.Controllers
{
    public class BasketController : Controller
    {
        const string basketOutputDelimiter = ";";
        private ApplicationUserManager _userManager;
        public int itemCnt2Display = 30;

        public BasketController()
        {
            
        }

        public BasketController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Basket
        //отключаем кэш
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {

            string userId;
            if (Request.IsAuthenticated)
                userId = User.Identity.GetUserId();
            else
                userId = Request.AnonymousID;

            var strUrl = "https://krasivo.pro/"; //DataService.GetUrl(Request);
            var models = DataService.GetBasketItems(userId, strUrl);

            var basket = new BasketViewModel();

            double basketSum = 0;
            int basketQty = 0;

            DataService.GetBasketSumAndQty(userId, out basketQty, out basketSum);

            basket.BasketLines = models;
            basket.Summary.OrderAmount = basketSum;
            basket.Summary.ItemsQty = basketQty;

            basket.IsMaster = DataService.CheckIsUserMaster(userId);

            //проверка корзины
            foreach (var line in basket.BasketLines)
            {
                if (line.Qty > line.AvailQty)
                {
                    basket.Status.isOk = false;
                    basket.Status.errorMessage += "По товару " + line.ItemName + " не хватает остатка на складе. Можно заказать только " + line.AvailQty + "<br />";
                }
            }
            //вот здесь            
            basket.PromoCodeStatus = DataService.GetCurrentPromoCodeStatusForUser(userId);

            //Mike 2017-09-21 отключаем акциии "подарок за покупку"
            //            var actCnt = DataService.CheckIsActionsOnBasket(userId);
            var actCnt = 0;

            basket.PossibleActions = actCnt > 0;

            DataService.KillFailedPromoCodesbyUser(userId);

            ViewBag.Title = "Ваша корзина";

            return View(basket);
        }

        //Mike 2017-02-01
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Index(BasketViewModel basket)
        {
            if (!Request.IsAuthenticated)
            {
                var returnUrl = Url.Action("Index");
                return RedirectToAction("Login", "Account", new { returnUrl = returnUrl });
            }

            //проверка: у нас в корзине вообще что-нибудь есть?
            var userId = User.Identity.GetUserId();
            int inBasketQty = 0;
            double inBasketSum = 0;

            DataService.GetBasketSumAndQty(userId, out inBasketQty, out inBasketSum);

            if (inBasketQty == 0) //ничего нет, возвращаем в корзину
                return RedirectToAction("Index", "Basket");

            //смотрим, есть ли у нас живой неподтверждённый заказ от этого же пользователя
            var exOrder = new OrderViewModel();

            var exSaleInfo = SaleInfo.InitInOpenStatusByUserId(userId);

            //запомнили наш старый заказ
            if (exSaleInfo.OK && exSaleInfo.ValidateDeliveryParams())
            {
                //его ща отменят, так что запоминаем параметры
                exOrder = exSaleInfo.ConvertToOrderViewModel();
            }

            //вот здесь мы готовим модель
            var orderModel = new OrderViewModel();

            orderModel.OrderId = DataService.CreateSalesOrder(userId);

            if (orderModel.OrderId == "")
                return RedirectToAction("Index", "Basket");

            var strUrl = DataService.GetUrl(Request);

            orderModel.Items = DataService.GetSalesTrans(orderModel.OrderId, strUrl);

            int itemsQty = 0;
            double orderAmount = 0;
            foreach (var item in orderModel.Items)
            {
                itemsQty += item.Qty;
                orderAmount += item.Qty * item.Price;
            }

            orderModel.Summary.ItemsQty = itemsQty;
            orderModel.Summary.OrderAmount = orderAmount;
            orderModel.Summary.PaymentTypeName = string.Empty;


            ApplicationUser user = UserManager.FindByEmail(User.Identity.Name);

            //пошла инициализация доставки
            if (!string.IsNullOrWhiteSpace(exOrder.ReceiverFirstName))
                orderModel.ReceiverFirstName = exOrder.ReceiverFirstName;
            else
                if (!string.IsNullOrWhiteSpace(user.FirstName))
                orderModel.ReceiverFirstName = user.FirstName.ToString();

            if (!string.IsNullOrWhiteSpace(exOrder.ReceiverLastName))
                orderModel.ReceiverLastName = exOrder.ReceiverLastName;
            else
                if (!string.IsNullOrWhiteSpace(user.LastName))
                orderModel.ReceiverLastName = user.LastName.ToString();

            if (!string.IsNullOrWhiteSpace(exOrder.ReceiverPhone))
                orderModel.ReceiverPhone = exOrder.ReceiverPhone;
            else
                if (!string.IsNullOrWhiteSpace(user.PhoneNumber))
                orderModel.ReceiverPhone = user.PhoneNumber.ToString();

            if (!string.IsNullOrWhiteSpace(exOrder.Comment))
                orderModel.Comment = exOrder.Comment;

            //нам нужно: вытащить стоимость доставки

            //в будущем будем инициализировать доставку здесь по личному кабинету
            //сейчас по заказу
            if (exOrder.OrderId != "")
            {
                orderModel.DeliveryType = DeliveryTypeStatic.InitDeliveryTypeViewModelByExOrder(orderModel.OrderId, exOrder);
            }

            //заполнили всё, кроме доставки

            //проставим код заказа в DeliveryTypes;
            for (var i = 0; i < orderModel.DeliveryType.TypeItems.Count; i++)
            {
                orderModel.DeliveryType.TypeItems[i].ParentOrderId = orderModel.OrderId;
            }

            //считаем стоимость доставки и прописываем параметры

            if (!DataService.CalcDeliveryPriceAndSaveDeliveryParams(orderModel))
            {
                DataService.ReportAboutCrash("Ошибка расчёта стоимости доставки в сабмите корзины", orderModel.OrderId);
                return RedirectToAction("Index", "Basket");
            }

            //вот здесь считаем акции.

            //Mike 2017-09-21 отключаем акции подарок за покупку
            //var actCnt = DataService.CalcActions(orderModel.OrderId);
            var actCnt = 0;

            if (actCnt > 0)
            {
                //надо заполнить вьюмоделы и показать вьюмодел с акциями
                orderModel.Actions = DataService.GetActionsAndItems(orderModel.OrderId, strUrl);

                //а теперь надо сделать следующее: если по акции доступен только один подарок, сразу его и выдаём

                for (int i = 0; i < orderModel.Actions.Count; i++)
                {
                    var availPresents = orderModel.Actions[i].PublicityItems.Where(x => x.PointsCost <= orderModel.Actions[i].PointsEarned).ToList();

                    if (availPresents.Count == 1)
                    {
                        var itemId = availPresents[0].ItemId;
                        var availQty = availPresents[0].AvailQty;
                        if (availPresents[0].QtyGiven == 0) //если не равно 0, значит пользователь что-то нащёлкал, не будем ему мешать!
                        {
                            var qtyByPoints = orderModel.Actions[i].PointsEarned / availPresents[0].PointsCost;
                            var qtyToGive = Math.Min(availQty, qtyByPoints);
                            //выдаём максимально доступное количество                    

                            bool res = DataService.GivePublicityPresent(orderModel.OrderId, orderModel.Actions[i].JournalId, itemId, qtyToGive);

                            if (!res)
                            {
                                qtyToGive = 0;
                                DataService.ReportAboutCrash("Ошибка выдачи единственного подарка по заказу " + orderModel.OrderId + " по акции " + orderModel.Actions[i].JournalId, itemId + " " + qtyToGive.ToString() + " штук");
                            }
                        }
                    }
                }

                return RedirectToAction("PublicityActions");
            }


            ViewBag.Title = "Ваша корзина - способ доставки";

            //Mike 2017-10-31
            //return RedirectToAction("CreateOrder");
            return RedirectToAction("SelectPayment");

        }



        public ActionResult GetSelfDeliveryPointsPartial(string salesId, string cityId)
        {
            if (string.IsNullOrWhiteSpace(cityId))
            {
                DataService.ReportAboutCrash("Пустой код города в GetSelfDeliveryPointsPartial", "Код заказа " + salesId);
            }

            var deliveryTypeItem = new DeliveryTypeRadioItem();
            deliveryTypeItem.DeliveryTypeId = DeliveryTypeStatic.SelfDeliveryTypeId;
            deliveryTypeItem.Name = DeliveryTypeStatic.SelfDeliveryTypeName;

            deliveryTypeItem.SelectedCityId = cityId;
            deliveryTypeItem.ParentOrderId = salesId;
            var deliveryModeId = DeliveryTypeStatic.SelfDeliveryModeId;
            deliveryTypeItem.SelectedDeliveryModeId = deliveryModeId;

            var deliveryModeItem = new DeliveryModeRadioItem();
            deliveryModeItem.DeliveryModeId = deliveryModeId;

            deliveryModeItem.SelfDeliveryItems = DataService.GetSelfDeliveryPointsByCityId(salesId, cityId);

            if (deliveryModeItem.SelfDeliveryItems.Count > 0)
            {
                deliveryModeItem.SelectedSelfDeliveryPointId = deliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointId;
                deliveryModeItem.SelectedSelfDeliveryPointName = deliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointName;
                deliveryModeItem.SelectedSelfDeliveryPointDescription = deliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointDescription;
                var deliveryCalculator = DeliveryCalculator.Init(salesId, deliveryTypeItem.DeliveryTypeId,
                    deliveryTypeItem.SelectedCityId, deliveryModeId,
                    deliveryModeItem.SelectedSelfDeliveryPointId);

                var deliveryParams = deliveryCalculator.GetDeliveryPrice();

                deliveryModeItem.MessageAboutCostAndPeriod = string.Format("Срок доставки {1} раб. дн.", deliveryParams.DeliveryCost.ToString("N2"), deliveryParams.TransportLeg.ToString());
            }

            deliveryTypeItem.ModeItems.Add(deliveryModeItem);
            
            //ViewData.TemplateInfo.HtmlFieldPrefix = "DeliveryType.TypeItems[0]";
            ViewData.TemplateInfo.HtmlFieldPrefix = "DeliveryType.TypeItems[0].ModeItems[0]";
            return PartialView("~/Views/Basket/EditorTemplates/DeliveryModeRadioItem.cshtml", deliveryModeItem);
            //return PartialView("SelfDeliveryPointsPartial", deliveryTypeItem);
        }

        public ActionResult GetCDEKPartial(string salesId, string CDEKCityId, string CityName = "")
        {
            #region Устарело
            /*
            int intCityId = 0;
            int.TryParse(CDEKCityId, out intCityId);

            var deliveryTypeItem = new DeliveryTypeRadioItem();
            deliveryTypeItem.DeliveryTypeId = DeliveryTypeStatic.CDEKDeliveryTypeId;
            deliveryTypeItem.Name = DeliveryTypeStatic.CDEKDeliveryTypeName;

            deliveryTypeItem.SelectedCityId = CDEKCityId;
            deliveryTypeItem.SelectedCityName = CityName;
            //запрашиваем у СДЭК стоимость доставки по каждому способу
            var deliveryModeId = DeliveryTypeStatic.CDEKPVZ136ModeId;

            var deliveryCalculator = DeliveryCalculator.Init(salesId, deliveryTypeItem.DeliveryTypeId,
                deliveryTypeItem.SelectedCityId, deliveryModeId,
                "");

            var deliveryParams = deliveryCalculator.GetDeliveryPrice();

            //второй вариант
            if (deliveryParams.DeliveryCost == -1)
            {
                deliveryModeId = DeliveryTypeStatic.CDEKPVZ10ModeId;

                deliveryCalculator = DeliveryCalculator.Init(salesId, deliveryTypeItem.DeliveryTypeId,
                    deliveryTypeItem.SelectedCityId, deliveryModeId,
                    "");

                deliveryParams = deliveryCalculator.GetDeliveryPrice();
            }

            if (deliveryParams.DeliveryCost != -1)
            {
                //какой-то есть
                var deliveryModeItem = new DeliveryModeRadioItem();

                var pointsList = DataService.GetCDEKDeliveryPointsInCity(salesId, intCityId);
                deliveryModeItem.DeliveryModeId = deliveryModeId;

                deliveryModeItem.Name = DeliveryTypeStatic.CDEKPVZModeName;
                deliveryModeItem.SelfDeliveryItems = pointsList;
                deliveryModeItem.MessageAboutCostAndPeriod = string.Format("Стоимость {0} руб., срок доставки {1} дн. после оплаты заказа", deliveryParams.DeliveryCost.ToString("N2"), deliveryParams.TransportLeg.ToString());


                if (deliveryModeItem.SelfDeliveryItems.Count > 0)
                {
                    deliveryModeItem.SelectedSelfDeliveryPointId = deliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointId;
                    deliveryModeItem.SelectedSelfDeliveryPointDescription = deliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointName;
                }

                deliveryTypeItem.ModeItems.Add(deliveryModeItem);
            }
            //теперь курьерская
            deliveryModeId = DeliveryTypeStatic.CDEKCourier137ModeId;

            deliveryCalculator = DeliveryCalculator.Init(salesId, deliveryTypeItem.DeliveryTypeId,
                deliveryTypeItem.SelectedCityId, deliveryModeId,
                "");

            deliveryParams = deliveryCalculator.GetDeliveryPrice();

            //второй вариант
            if (deliveryParams.DeliveryCost == -1)
            {
                deliveryModeId = DeliveryTypeStatic.CDEKCourier11ModeId;

                deliveryCalculator = DeliveryCalculator.Init(salesId, deliveryTypeItem.DeliveryTypeId,
                    deliveryTypeItem.SelectedCityId, deliveryModeId,
                    "");

                deliveryParams = deliveryCalculator.GetDeliveryPrice();
            }

            if (deliveryParams.DeliveryCost != -1)
            {
                //какой-то есть
                var deliveryModeItem = new DeliveryModeRadioItem();

                deliveryModeItem.DeliveryModeId = deliveryModeId;

                deliveryModeItem.Name = DeliveryTypeStatic.CDEKCourierModeName;
                deliveryModeItem.MessageAboutCostAndPeriod = string.Format("Стоимость {0} руб., срок доставки {1} дн. после оплаты заказа", deliveryParams.DeliveryCost.ToString("N2"), deliveryParams.TransportLeg.ToString());
                deliveryTypeItem.ModeItems.Add(deliveryModeItem);
            }

            if (deliveryTypeItem.ModeItems.Count == 0)
                deliveryTypeItem.Message = "К сожалению, по выбранным условиям доставка не возможна :-(";
            else
            {
                deliveryTypeItem.SelectedDeliveryModeId = deliveryTypeItem.ModeItems[0].DeliveryModeId;
            }
            */
            #endregion устарело

            var deliveryTypeItem = DeliveryTypeStatic.GetCDEKDeliveryTypeItemByParams(salesId, CDEKCityId, CityName);
            //формируем
            ViewData.TemplateInfo.HtmlFieldPrefix = "DeliveryType.TypeItems[1]";
            return PartialView("CDEKDeliveryModesPartial", deliveryTypeItem);
        }

        public ActionResult GetSummaryPartialForIndex()
        {
            string userId;
            if (Request.IsAuthenticated)
                userId = User.Identity.GetUserId();
            else
                userId = Request.AnonymousID;

            double basketSum = 0;
            int basketQty = 0;

            DataService.GetBasketSumAndQty(userId, out basketQty, out basketSum);

            var summary = new OrderSummaryViewModel();

            summary.ItemsQty = basketQty;
            summary.OrderAmount = basketSum;

            return PartialView("OrderSummaryPartial", summary);
        }

        //получение партиала для страницы с выбором условий доставки
        public ActionResult GetSummaryPartialForOrder(string orderId, string selectedDeliveryTypeId, string selfDeliveryPointId = "", int CDEKCityDeliveryId = 0)
        {
            //Mike 2017-03-15
            //пока не работает
            OrderSummaryViewModel summary = new OrderSummaryViewModel();
            //вот здесь надо по заданным параметрам формировать 
            if (selectedDeliveryTypeId == "SelfDelivery")
            {
                //TODO здесь нужна другая функция
                summary = new OrderSummaryViewModel(); //DataService.GetOrderSummaryForOrder(orderId, selectedDeliveryTypeId);
                var deliveryItem = DataService.GetSelfDeliveryParams(selectedDeliveryTypeId);
                summary.DeliveryCost = deliveryItem.DeliveryCost;
                summary.TotalAmount = summary.DeliveryCost + summary.TotalAmount;
            }

            if (selectedDeliveryTypeId == "CDEK")
            {
                //тут пока непонятно, што писать
            }

            return PartialView("OrderSummaryPartial", summary);
        }

        //Увеличение/уменьшение количества товаров в корзине поштучно
        //для добавления вызываем с qty>0
        //для уменьшения вызваем с qty<0
        public ContentResult AddToBasket(string itemId, int qty, double price = 0, int overwriteQty = 0)
        {
            bool ret = false;
            string userId = string.Empty;
            try
            {

                if (Request.IsAuthenticated)
                    userId = User.Identity.GetUserId();
                else
                    userId = Request.AnonymousID;

                ret = DataService.AddToBasket(userId, itemId, qty, overwriteQty);
            }
            catch (Exception ex)
            {
                ret = false;
                DataService.ReportAboutCrash("Ошибка добавления в корзину, пользователь " + userId + " " + itemId, ex.Message);
            }
            if (!ret)
                return Content("Ошибка добавления в корзину");

            return Content("Товар добавлен в корзину.");
        }

        public ContentResult CheckPromoCode(string promoCode)
        {
            int ret = 0;
            string userId = string.Empty;
            try
            {
                if (Request.IsAuthenticated)
                    userId = User.Identity.GetUserId();
                else
                    userId = Request.AnonymousID;

                ret = DataService.CheckPromoCode(promoCode, userId);
            }
            catch (Exception ex)
            {
                ret = -3;
                DataService.ReportAboutCrash("Ошибка применения промо-кода " + promoCode + " пользователь " + userId, ex.Message);
            }

            return Content(ret.ToString());
        }

        public ContentResult GetPromoCodeResultMsg(int checkResult)
        {
            var ret = string.Empty;
            switch (checkResult)
            {
                case -1:
                    {
                        ret = "К сожалению, Вы уже использовали этот промо-код";
                        break;
                    }
                case -2:
                    {
                        ret = "К сожалению, для держателей карт Профессионалов промо-коды не работают :-(";
                        break;
                    }
                case 1:
                    {
                        ret = "Активирую...";
                        break;
                    }
                case 2:
                    {
                        ret = "Промо-код успешно активирован";
                        break;
                    }
                default:
                    {
                        ret = "Неверный промо-код";
                        break;
                    }

            }
            return Content(ret.ToString());
        }
        [HttpPost]
        public ActionResult SavePromoCode(BasketViewModel model)
        {

            if (string.IsNullOrEmpty(model.PromoCode))
            {
                return RedirectToAction("Index", "Basket");
            }

            string userId;

            if (Request.IsAuthenticated)
                userId = User.Identity.GetUserId();
            else
                userId = Request.AnonymousID;

            var promoCodeResult = DataService.ApplyPromoCode(model.PromoCode, userId);

            var ret = string.Empty;

            switch (promoCodeResult)
            {
                case -1:
                    {
                        ret = "К сожалению, Вы уже использовали этот промо-код";
                        break;
                    }
                case -2:
                    {
                        ret = "К сожалению, для держателей карт Профессионалов промо-коды не работают :-(";
                        break;
                    }
                case 1:
                    {
                        ret = "Активирую...";
                        break;
                    }
                case 2:
                    {
                        ret = "Промо-код успешно активирован";
                        break;
                    }
                default:
                    {
                        ret = "Неверный промо-код";
                        break;
                    }

            }

            return RedirectToAction("Index", "Basket");
        }
        public ContentResult ApplyPromoCode(string promoCode)
        {
            int ret = 0;
            string userId = string.Empty;
            try
            {
                if (Request.IsAuthenticated)
                    userId = User.Identity.GetUserId();
                else
                    userId = Request.AnonymousID;

                ret = DataService.ApplyPromoCode(promoCode, userId);
            }
            catch (Exception ex)
            {
                ret = -3;
                DataService.ReportAboutCrash("Ошибка применения промо-кода " + promoCode + " пользователь " + userId, ex.Message);
            }

            return Content(ret.ToString());
        }


        //Удаление товара из корзины полностью
        public ContentResult RemoveFromBasket(string itemId)
        {
            bool ret = false;
            try
            {
                string userId;
                if (Request.IsAuthenticated)
                    userId = User.Identity.GetUserId();
                else
                    userId = Request.AnonymousID;

                ret = DataService.RemoveFromBasket(userId, itemId);
            }
            catch (Exception ex)
            {
                ret = false;
                string error = ex.Message;
            }
            if (!ret)
                return Content("Ошибка удаления из корзины");

            return Content("Товар успешно удалён.");
        }

        //Перенести в список желаний из корзины
        public ContentResult MoveBasketToWishList(string itemId)
        {
            bool ret = false;
            try
            {
                string userId;
                if (Request.IsAuthenticated)
                    userId = User.Identity.GetUserId();
                else
                    userId = Request.AnonymousID;

                ret = DataService.MoveToWishList(userId, itemId);
            }
            catch (Exception ex)
            {
                ret = false;
                string error = ex.Message;
            }
            if (!ret)
                return Content("Ошибка удаления из корзины");

            return Content("Товар успешно удалён.");
        }

        public ContentResult GetBasketParams()
        {
            string ret = string.Empty;
            double basketSum = 0;
            int basketQty = 0;

            string userId;
            if (Request.IsAuthenticated)
                userId = User.Identity.GetUserId();
            else
                userId = Request.AnonymousID;


            DataService.GetBasketSumAndQty(userId, out basketQty, out basketSum);

            ret = basketQty.ToString() + basketOutputDelimiter + basketSum.ToString();

            return Content(ret);
        }

        public ContentResult GetMessageAboutCostAndPeriodIKSelfDeliveryPoint(string selfDeliveryPointId)
        {
            string ret = string.Empty;

            var deliveryCalculator = DeliveryCalculator.Init(string.Empty, DeliveryTypeStatic.SelfDeliveryTypeId,
                string.Empty, DeliveryTypeStatic.SelfDeliveryModeId,
                selfDeliveryPointId);

            var deliveryParams = deliveryCalculator.GetDeliveryPrice();

            string MessageAboutCostAndPeriod = string.Empty;

            if (deliveryParams.DeliveryCost == -1)
            {
                //ошибка!!
                DataService.ReportAboutCrash("Ошибка расчёта стоимости доставки для пункта самовыввоза " + selfDeliveryPointId, string.Empty);
                ret = "Ошибка расчёта стоимости доставки! Наши программисты уже работают над этим!";
            }
            else
            {
                ret = deliveryParams.Description+"<br />"+deliveryParams.Message;
                ///var cost = deliveryParams.DeliveryCost == 0 ? "Бесплатно" : string.Format("Стоимость {0} руб.", deliveryParams.DeliveryCost.ToString("N2"));
                ///ret = string.Format("{0}, срок доставки {1} дн. после оплаты заказа", cost, deliveryParams.TransportLeg.ToString());
            }

            return Content(ret);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult CreateOrder()
        {
            if (!Request.IsAuthenticated)
            {

                var returnUrl = Url.Action("Index");
                return RedirectToAction("Login", "Account", new { returnUrl = returnUrl });
            }

            var userId = User.Identity.GetUserId();

            //заказ у нас уже создан
            var saleInfo = SaleInfo.InitInOpenStatusByUserId(userId);
            if (!saleInfo.OK)
            {
                //нету подходящего заказа. Редирект на BasketIndex
                return RedirectToAction("Index");
            }

            var orderModel = saleInfo.ConvertToOrderViewModel();

            //Mike 2017-11-01
            //и вот теперь у нас час расплаты
            //нам нужно, по типу оплаты определить пункты выдачи

            if (orderModel.DeliveryType.SelectedDeliveryTypeId == DeliveryTypeStatic.SelfDeliveryTypeId)
            {
                var currDeliveryType = orderModel.DeliveryType.TypeItems.FirstOrDefault(x => x.DeliveryTypeId == DeliveryTypeStatic.SelfDeliveryTypeId);
                var currDeliveryTypeIndex = orderModel.DeliveryType.TypeItems.IndexOf(currDeliveryType);
                //здесь всё просто:
                //смотрим, актуален ли ещё наш город
                //если не актуален, ставим на первый из списка
                //если актуален, проверяем пункт св
                //если пункт актуален = ок
                //если пункт закрылся = ставим на первый из города
                var deliveryModeItem = currDeliveryType.ModeItems.FirstOrDefault(x => x.DeliveryModeId == currDeliveryType.SelectedDeliveryModeId);

                if (deliveryModeItem != null)
                {
                    var deliveryModeItemIndex = currDeliveryType.ModeItems.IndexOf(deliveryModeItem);
                    var cityItem = currDeliveryType.CityItems.FirstOrDefault(x => x.CityId == currDeliveryType.SelectedCityId);
                    //проверить работоспособность!!!!                    
                    //если город не нулл, вытаскиваем, а есть ли в нём живые пункты св? (актуально для нал. платежа)
                    //если нет, ищем город, в котором
                    if (cityItem == null)
                    {
                        //Mike 2017-10-31 Вот здесь надо проверять, есть ли в первом городе живые пункты самовывоза (актуально для наложенного платежа). 
                        //Хотя бы один. Если нет ни одного, считаем, что город мёртвый.
                        if (currDeliveryType.CityItems.Count > 0)
                        {
                            currDeliveryType.SelectedCityId = currDeliveryType.CityItems[0].CityId;
                            currDeliveryType.SelectedCityName = currDeliveryType.CityItems[0].CityName;
                            deliveryModeItem.SelfDeliveryItems = DataService.GetSelfDeliveryPointsByCityId(orderModel.OrderId, currDeliveryType.SelectedCityId);
                            if (deliveryModeItem.SelfDeliveryItems.Count > 0)
                            {
                                deliveryModeItem.SelectedSelfDeliveryPointId = deliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointId;
                                deliveryModeItem.SelectedSelfDeliveryPointName = deliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointName;
                                deliveryModeItem.SelectedSelfDeliveryPointDescription = deliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointDescription;
                            }
                            else
                            {
                                DataService.ReportAboutCrash("В CreateOrder пустой список адресов самовывоза по заказу " + orderModel.OrderId, userId);
                            }
                        }
                        else
                        {
                            currDeliveryType.SelectedCityId = string.Empty;
                            currDeliveryType.SelectedCityName = string.Empty;
                        }
                    }
                    else
                    {
                        var selfDeliveryPoint = deliveryModeItem.SelfDeliveryItems.FirstOrDefault(x => x.SelfDeliveryPointId == deliveryModeItem.SelectedSelfDeliveryPointId);
                        if (selfDeliveryPoint == null)
                        {
                            if (deliveryModeItem.SelfDeliveryItems.Count > 0)
                            {
                                deliveryModeItem.SelectedSelfDeliveryPointId = deliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointId;
                                deliveryModeItem.SelectedSelfDeliveryPointName = deliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointName;
                                deliveryModeItem.SelectedSelfDeliveryPointDescription = deliveryModeItem.SelfDeliveryItems[0].SelfDeliveryPointDescription;
                            }
                        }
                    }
                    currDeliveryType.ModeItems[deliveryModeItemIndex] = deliveryModeItem;
                }
                orderModel.DeliveryType.TypeItems[currDeliveryTypeIndex] = currDeliveryType;
            }

            if (orderModel.DeliveryType.SelectedDeliveryTypeId == DeliveryTypeStatic.CDEKDeliveryTypeId)
            {
                //если выбран СДЭК
                //надо
                var currCDEKDeliveryType = orderModel.DeliveryType.TypeItems.FirstOrDefault(x => x.DeliveryTypeId == DeliveryTypeStatic.CDEKDeliveryTypeId);
                var cdekDeliveryTypeIndex = orderModel.DeliveryType.TypeItems.IndexOf(currCDEKDeliveryType);
                var exCityName = orderModel.DeliveryType.TypeItems.FirstOrDefault(x => x.DeliveryTypeId == DeliveryTypeStatic.CDEKDeliveryTypeId).SelectedCityName;
                var exCityId = orderModel.DeliveryType.TypeItems.FirstOrDefault(x => x.DeliveryTypeId == DeliveryTypeStatic.CDEKDeliveryTypeId).SelectedCityId;

                var newCDEKDeliveryType = DeliveryTypeStatic.InitCDEKDeliveryTypeItemByExItem(orderModel.OrderId, currCDEKDeliveryType);
                if (newCDEKDeliveryType == null)
                {
                    //значит, надо делать выбранным тип доставки "Самовывоз"
                    orderModel.DeliveryType.SelectedDeliveryTypeId = DeliveryTypeStatic.SelfDeliveryTypeId;
                    orderModel.DeliveryType.SelectedDeliveryTypeDescription = DeliveryTypeStatic.SelfDeliveryTypeName;
                    orderModel.DeliveryType.TypeItems[cdekDeliveryTypeIndex] = new DeliveryTypeRadioItem();
                    orderModel.DeliveryType.TypeItems[cdekDeliveryTypeIndex].DeliveryTypeId = DeliveryTypeStatic.CDEKDeliveryTypeId;
                    orderModel.DeliveryType.TypeItems[cdekDeliveryTypeIndex].Name = DeliveryTypeStatic.CDEKDeliveryTypeName;
                    orderModel.DeliveryType.TypeItems[cdekDeliveryTypeIndex].SelectedCityId = exCityId;
                    orderModel.DeliveryType.TypeItems[cdekDeliveryTypeIndex].SelectedCityName = exCityName;
                    orderModel.DeliveryType.TypeItems[cdekDeliveryTypeIndex].ParentOrderId = orderModel.OrderId;
                }
                else
                    orderModel.DeliveryType.TypeItems[cdekDeliveryTypeIndex] = newCDEKDeliveryType;
            }

            ViewBag.Title = "Ваша корзина - способ оплаты";

            var summary = DataService.GetOrderSummaryForOrder(orderModel.OrderId);
            //Mike 2017-03-15
            //Заглушка, чтобы не вводить пользователей в заблуждение
            summary.DeliveryCost = 0;
            summary.TotalAmount = 0;
            summary.DeliveryTypeName = string.Empty;
            summary.DeliveryModeName = string.Empty;
            //Mike 2017-03-15

            //Mike 2017-03-07
            #region Старый код           
            //if (orderModel.DeliveryType.SelectedDeliveryTypeId == "SelfDelivery")
            //{ 
            //    //summary = DataService.GetOrderSummaryForOrder(orderModel.OrderId, orderModel.DeliveryType.SelectedDeliveryTypeId, orderModel.SelfDeliveryPoint.SelectedSelfDeliveryPointId);
            //}

            //if (orderModel.DeliveryType.SelectedDeliveryTypeId == "CDEKPVZ")
            //{
            //    //вытащим, что у нас в базе
            //    summary = DataService.GetOrderSummaryForOrder(orderModel.OrderId);
            //    //подсовываем код тарифа
            //    /*
            //    orderModel.DeliveryType.SelectedSdekTariffId = DeliveryTypeStatic.FindDeliveryTypeById(orderModel.DeliveryType.SelectedDeliveryTypeId).SdekTariffId;
            //    //вытаскиваем код города-получателя: получаем по названию города его код в базе сдэк
            //    //orderModel.CityIdSdek = Function(orderModel.CityName);

            //    var SdekDelivPointIdKIN = orderModel.SelfDeliveryPointSDEKPVZ.SelectedSelfDeliveryPointId;
            //    //нужно получить код пвз по базе СДЭК из нашей базы.
            //    var PVZ = new CDEKPVZ(SdekDelivPointIdKIN);
            //    if (!PVZ.OK)
            //    {
            //        if (orderModel.SelfDeliveryPointSDEKPVZ.Items.Any())
            //        {
            //            orderModel.SelfDeliveryPointSDEKPVZ.SelectedSelfDeliveryPointId = orderModel.SelfDeliveryPointSDEKPVZ.Items[0].SelfDeliveryPointId;
            //            orderModel.SelfDeliveryPointSDEKPVZ.SelectedSelfDeliveryPointDescription = orderModel.SelfDeliveryPointSDEKPVZ.Items[0].SelfDeliveryPointDescription;
            //            //вытаскиваем стоимость
            //        }
            //        else
            //        {
            //            //ну если вообще ничего, то что поделать
            //            orderModel.SelfDeliveryPointSDEKPVZ.SelectedSelfDeliveryPointId = string.Empty;
            //            orderModel.SelfDeliveryPointSDEKPVZ.SelectedSelfDeliveryPointDescription = string.Empty;

            //            orderModel.Summary.TotalAmount = -1;
            //            orderModel.Summary.DeliveryCost = -1;
            //        }
            //    }
            //    else
            //    {
            //        //проверяем, актуален ли наш ПВЗ

            //        //if (пункт актуален) 
            //        {
            //            //orderModel.SelfDeliveryPointSDEK.SelectedSelfDeliveryPointId = 
            //            //orderModel.SelfDeliveryPointSDEK.SelectedSelfDeliveryPointDescription = 

            //            //вытаскиваем стоимость доставки
            //            //и общую стоимость
            //        }
            //        //else
            //        {
            //            orderModel.SelfDeliveryPointSDEKPVZ.SelectedSelfDeliveryPointId = string.Empty;
            //            orderModel.SelfDeliveryPointSDEKPVZ.SelectedSelfDeliveryPointDescription = string.Empty;
            //            orderModel.Summary.TotalAmount = -1;
            //            orderModel.Summary.DeliveryCost = -1;
            //        }
            //    }
            //    */
            //}

            //if (orderModel.DeliveryType.SelectedDeliveryTypeId == "CDEKCourier")
            //{
            //    /*
            //    //подсовываем код тарифа
            //    orderModel.DeliveryType.SelectedSdekTariffId = DeliveryTypeStatic.FindDeliveryTypeById(orderModel.DeliveryType.SelectedDeliveryTypeId).SdekTariffId;
            //    //вытаскиваем код города и получаем по названию города его код в базе сдэк
            //    //orderModel.CityIdSdek = 

            //    var SdekAddressIdKIN = orderModel.SelfDeliveryPointSDEKCourier.SelectedSelfDeliveryPointId;

            //    orderModel.SelfDeliveryPointSDEKCourier.SelectedSelfDeliveryPointId = string.Empty; //потому что выбирать мы ничего не будем, дропдауна на сдэк адррес нет
            //    orderModel.SelfDeliveryPointSDEKCourier.SelectedSelfDeliveryPointDescription = string.Empty; //потому что выбирать мы ничего не будем, дропдауна на сдэк адррес нет

            //    //нужно получить код пвз по базе СДЭК из нашей базы.
            //    var Address = new CDEKCourier(SdekAddressIdKIN);

            //    if (Address.OK)
            //    {
            //        orderModel.SdekAddress = Address.CDEKAddress;
            //        //вытаскиваем стоимость доставки
            //        //и общую стоимость

            //    }
            //    else
            //    {
            //        //orderModel.SdekAddress = "";
            //        orderModel.Summary.TotalAmount = -1;
            //        orderModel.Summary.DeliveryCost = -1;
            //    }
            //    */
            //}
            #endregion Старый код

            orderModel.Summary = summary;

            ViewBag.Title = "Ваша корзина - способ доставки";

            return View(orderModel);
        }
        public ActionResult PublicityActions()
        {
            if (!Request.IsAuthenticated)
            {
                var returnUrl = Url.Action("Index");
                return RedirectToAction("Login", "Account", new { returnUrl = returnUrl });
            }

            //тут по пользователю мы вытаскиваем его заказ и акции

            var userId = User.Identity.GetUserId();

            //заказ у нас уже создан
            var saleInfo = SaleInfo.InitInOpenStatusByUserId(userId);
            if (!saleInfo.OK)
            {
                //нету подходящего заказа. Редирект на BasketIndex
                return RedirectToAction("Index");
            }

            //TODO: вот здесь - checkCalculatedActions
            //если акция уже не играет, убиваем её

            //TODO: вот здесь - проверка, есть акции или нет?
            var orderModel = saleInfo.ConvertToOrderViewModel();

            var strUrl = DataService.GetUrl(Request);

            orderModel.Items = DataService.GetSalesTrans(orderModel.OrderId, strUrl);

            //надо заполнить вьюмоделы и показать вьюмодел с акциями
            orderModel.Actions = DataService.GetActionsAndItems(orderModel.OrderId, strUrl);

            if (orderModel.Actions.Count == 0)
            {
                //перебрасываем дальше на выбор условий оплаты оформление параметров доставки
                return RedirectToAction("SelectPayment");
                //return RedirectToAction("CreateOrder");
            }

            var summary = DataService.GetOrderSummaryForOrder(orderModel.OrderId);
            //Mike 2017-03-15
            //Заглушка, чтобы не вводить пользователей в заблуждение
            summary.DeliveryCost = 0;
            summary.TotalAmount = 0;
            summary.DeliveryTypeName = string.Empty;
            summary.DeliveryModeName = string.Empty;

            orderModel.Summary = summary;

            ViewBag.Title = "Ваша корзина - подарки по акциям";
            return View(orderModel);
        }

        public ActionResult GivePresentsByAction(string orderId, string actionId)
        {
            //если пользователь каким-то чудом разлогинился, перебрасываем его на корзину
            if (!Request.IsAuthenticated)
            {
                var returnUrl = Url.Action("Index");
                return RedirectToAction("Login", "Account", new { returnUrl = returnUrl });
            }

            int SortNum2Start = 0;
            List<QualityGoodItem> parsedFilterString = new List<QualityGoodItem>();

            parsedFilterString = DataService.ParseFilterString(DataService.ParseUrlParms(Request), out SortNum2Start, out actionId, out orderId);

            var userId = User.Identity.GetUserId();
            var saleInfo = new SaleInfo(orderId);

            if (!saleInfo.OK || saleInfo.Status != SaleStatus.Edit)
            {
                //с заказом что-то случилось или он отменён
                return RedirectToAction("Index", "Basket");
            }

            if (userId != saleInfo.AspId)
            {
                DataService.ReportAboutCrash("Кто-то попытался влезть в акции для чужого заказа", userId + " " + saleInfo.AspId);
                return RedirectToAction("Index", "Basket");
            }

            //теперь надо вытащить подарки по акции
            var strUrl = DataService.GetUrl(Request);

            //поначалу ничего не фильтруем
            var filters = new List<QualityGoodItem>();

            var action = DataService.GetAvailPresentsForAction(orderId, actionId, strUrl, itemCnt2Display, parsedFilterString, SortNum2Start);

            if ((action == null) || (action.JournalId == null))
            {
                return RedirectToAction("PublicityActions", "Basket");
            }

            List<string> items = new List<string>();

            foreach (var pItem in action.PublicityItems)
            {
                items.Add(pItem.ItemId);
            }

            action.Filter = DataService.GetFilterListForItemList(items);

            ViewBag.Title = "Ваша корзина - выбор подарков по акции " + actionId;
            return View(action);
        }

        [HttpPost]
        public ActionResult GivePresentsByAction(PublicityActionViewModel action)
        {
            return RedirectToAction("PublicityActions");
        }

        [HttpPost]
        public ActionResult PublicityActions(OrderViewModel order)
        {
            //Mike 2017-10-31
            return RedirectToAction("SelectPayment");
            //return RedirectToAction("CreateOrder");
        }

        public ContentResult GetFilteredItemsQty()
        {
            var actionId = string.Empty;
            var orderId = string.Empty;
            int SortNum2Start = 0;

            int i = DataService.GetItemsCountByPublicity(DataService.ParseFilterString(DataService.ParseUrlParms(Request), out SortNum2Start, out actionId, out orderId), actionId);

            return Content(i.ToString());
        }

        public ActionResult GetItemsPartial()
        {
            //пока без сессий
            string userId;

            if (Request.IsAuthenticated)
                userId = User.Identity.GetUserId();
            else
                userId = Request.AnonymousID;


            int SortNum2Start = 0;

            var strUrl = DataService.GetUrl(Request);

            List<string> selectedItems = new List<string>();

            List<QualityGoodItem> parsedFilterString = new List<QualityGoodItem>();

            var actionId = string.Empty;
            var orderId = string.Empty;

            parsedFilterString = DataService.ParseFilterString(DataService.ParseUrlParms(Request), out SortNum2Start, out actionId, out orderId);

            var action = DataService.GetAvailPresentsForAction(orderId, actionId, strUrl, itemCnt2Display, parsedFilterString, SortNum2Start);

            return PartialView("ItemsPartial4Action", action);
        }

        public ContentResult GetNewFilter()
        {
            string ret = String.Empty;

            List<QualityGoodItem> parsedFilterString = new List<QualityGoodItem>();

            var actionId = string.Empty;
            var orderId = string.Empty;

            int SortNum2Start = 0;

            parsedFilterString = DataService.ParseFilterString(DataService.ParseUrlParms(Request), out SortNum2Start, out actionId, out orderId);

            var filterList = DataService.GetFilterListForPublicity(parsedFilterString, actionId);

            foreach (var filter in filterList)
            {
                foreach (var property in filter.TreeNodeModel)
                {
                    if (property.Enabled)
                        ret += filter.Id + "=" + property.Id + ',';
                }
            }
            return Content(ret);
        }


        public ActionResult SelectPayment()
        {
            if (!Request.IsAuthenticated)
            {
                var returnUrl = Url.Action("Index");
                return RedirectToAction("Login", "Account", new { returnUrl = returnUrl });
            }

            //вот тут - дискотека.
            //во-первых, надо вообще найти незавершённый заказ в статусе "редактируется" от этого клиента. Если нет такого, кидаем в корзину
            //во-вторых, если не заполнены поля, вернуть надо на CreateOrder

            var userId = User.Identity.GetUserId();

            //заказ у нас уже создан
            var saleInfo = SaleInfo.InitInOpenStatusByUserId(userId);
            if (!saleInfo.OK)
            {
                //нету подходящего заказа. Редирект на BasketIndex
                return RedirectToAction("Index");
            }            
            /*
            if (!saleInfo.ValidateDeliveryParams())
            {
                //не заполнены параметры доставки
                return RedirectToAction("Index");
            }
            */
            var order = saleInfo.ConvertToOrderViewModel();
            ViewBag.Title = "Ваша корзина - способ оплаты";

            order.Summary = DataService.GetOrderSummaryForOrder(order.OrderId);
            //Заглушка, чтобы не вводить пользователей в заблуждение
            order.Summary.DeliveryCost = 0;
            order.Summary.TotalAmount = 0;
            order.Summary.DeliveryTypeName = string.Empty;
            order.Summary.DeliveryModeName = string.Empty;
            order.Summary.PaymentTypeId = string.Empty;
            order.Summary.PaymentTypeName = string.Empty;

            //возвращаем следующий View
            return View("Payment", order); // Payment(order);///RedirectToAction("Payment", "Basket");
        }

        [HttpPost]
        public ActionResult SelectPayment(OrderViewModel order)
        {
            var sss = false;
            ///Mike 2017-11-01
            /// Здесь будет сохранение параметров оплаты

            var userId = User.Identity.GetUserId();

            //заказ у нас уже создан
            var saleInfo = new SaleInfo(order.OrderId);

            if (!saleInfo.OK)
            {
                return RedirectToAction("Index");
            }

            if (order.PaymentType.SelectedPaymentTypeId == null)
            {
                return RedirectToAction("SelectPayment", "Basket");
            }

            //вот здесь, мы присваиваем

            if (!DataService.SetSalePaymentType(saleInfo, order.PaymentType.SelectedPaymentTypeId))
            {
                return RedirectToAction("SelectPayment");
            }

            return RedirectToAction("CreateOrder");
        }

        public string GivePresent(string orderId, string journalId, string itemId, int qty = 1)
        {
            bool ret = false;
            try
            {
                ret = DataService.GivePublicityPresent(orderId, journalId, itemId, qty);
            }
            catch (Exception ex)
            {
                ret = false;
                DataService.ReportAboutCrash("Ошибка выдачи подарка по заказу " + orderId + " акция " + journalId + " товар " + itemId, ex.Message);
            }
            if (!ret)
                return "0";

            return "1";
        }

        #region Устарело 2017-11-02
        //[HttpPost, ValidateAntiForgeryToken]
        //public ActionResult CreateOrder(OrderViewModel order)
        //{
        //    //Вот это должен быть пост CreateOrder - он же Confirmation
        //    //старая ветка
        //    try
        //    {
        //        if (!Request.IsAuthenticated)
        //        {
        //            var returnUrl = Url.Action("Index");
        //            return RedirectToAction("Login", "Account", new { returnUrl = returnUrl });
        //        }

        //        if (!ModelState.IsValid)
        //            return RedirectToAction("CreateOrder");


        //        var userId = User.Identity.GetUserId();

        //        //заказ у нас уже создан
        //        var saleInfo = new SaleInfo(order.OrderId);

        //        if (!saleInfo.OK)
        //        {
        //            return RedirectToAction("Index");
        //        }

        //        //cтавим параметры
        //        saleInfo.UserFirstName = order.ReceiverFirstName;
        //        saleInfo.UserLastName = order.ReceiverLastName;
        //        saleInfo.UserComment = order.Comment;

        //        if (order.DeliveryType.SelectedDeliveryTypeId == null)
        //        {
        //            return RedirectToAction("CreateOrder", "Basket");
        //        }

        //        saleInfo.DeliveryTypeId = order.DeliveryType.SelectedDeliveryTypeId;

        //        var deliveryTypeItem = order.DeliveryType.TypeItems.FirstOrDefault(x => x.DeliveryTypeId == saleInfo.DeliveryTypeId);

        //        if (deliveryTypeItem == null)
        //        {
        //            throw new Exception("Ошибка поиска типа доставки " + saleInfo.DeliveryTypeId);
        //        }
        //        var selectedCityId = deliveryTypeItem.SelectedCityId;

        //        var selectedDeliveryModeId = deliveryTypeItem.SelectedDeliveryModeId;

        //        if (selectedDeliveryModeId == null)
        //        {
        //            return RedirectToAction("CreateOrder", "Basket");
        //        }

        //        if (selectedCityId == null)
        //        {
        //            return RedirectToAction("CreateOrder", "Basket");
        //        }

        //        saleInfo.CityId = selectedCityId;

        //        saleInfo.DeliveryModeId = selectedDeliveryModeId;

        //        var deliveryModeItem = deliveryTypeItem.ModeItems.FirstOrDefault(x => x.DeliveryModeId == saleInfo.DeliveryModeId);

        //        //здесь нужна вилка: в зависимости от типа и способа доставки считать стоимость доставки
        //        //если самовывоз - вытаскивать из таблицы
        //        //если сдэк - запрашивать
        //        //если почта россии - тоже запрашивать

        //        if (saleInfo.DeliveryModeId == DeliveryTypeStatic.SelfDeliveryModeId)
        //        {
        //            saleInfo.SelfDeliveryPointId = deliveryModeItem.SelectedSelfDeliveryPointId;
        //            var deliveryItem = DataService.GetSelfDeliveryParams(saleInfo.SelfDeliveryPointId);
        //            //здесь надо вытаскивать название города из базы
        //            saleInfo.DeliveryCost = deliveryItem.DeliveryCost;
        //            saleInfo.TransportLeg = deliveryItem.TransportLeg;
        //            saleInfo.SelfDeliveryPointName = deliveryItem.SelfDeliveryPointName;
        //            saleInfo.SelfDeliveryPointDescription = deliveryItem.SelfDeliveryPointDescription;
        //        }

        //        bool returnToPrevStep = false;

        //        if (saleInfo.DeliveryModeId == DeliveryTypeStatic.CDEKPVZ10ModeId
        //            || saleInfo.DeliveryModeId == DeliveryTypeStatic.CDEKPVZ136ModeId)
        //        {
        //            int cityIdInt = 0;

        //            Int32.TryParse(saleInfo.CityId, out cityIdInt);

        //            //saleInfo.CityName = deliveryTypeItem.SelectedCityName;
        //            saleInfo.CityName = DataService.GetCDEKDeliveryCityName(saleInfo.CityId);

        //            var selectedSelfDeliveryPointId = deliveryModeItem.SelectedSelfDeliveryPointId;

        //            var CDEKDeliveryPoints = DataService.GetCDEKDeliveryPointsInCity(saleInfo.SalesId, cityIdInt);

        //            var selectedDeliveryPoint = CDEKDeliveryPoints.FirstOrDefault(x => x.SelfDeliveryPointId == selectedSelfDeliveryPointId);

        //            saleInfo.SelfDeliveryPointId = deliveryModeItem.SelectedSelfDeliveryPointId;

        //            //вытаскиваем здесь стоимость доставки
        //            var deliveryCalculator = DeliveryCalculator.Init(saleInfo.SalesId, saleInfo.DeliveryTypeId,
        //                saleInfo.CityId, saleInfo.DeliveryModeId, selectedSelfDeliveryPointId);

        //            var deliveryParams = deliveryCalculator.GetDeliveryPrice();
        //            deliveryParams.CODLimit = 4700;
        //            if (saleInfo.InvoiceAmount + deliveryParams.DeliveryCost > deliveryParams.CODLimit)
        //                returnToPrevStep = true;

        //            saleInfo.DeliveryCost = deliveryParams.DeliveryCost;
        //            saleInfo.TransportLeg = deliveryParams.TransportLeg;
        //            saleInfo.SelfDeliveryPointName = selectedDeliveryPoint.SelfDeliveryPointName;
        //            saleInfo.SelfDeliveryPointDescription = selectedDeliveryPoint.SelfDeliveryPointDescription;
        //        }


        //        if (saleInfo.DeliveryModeId == DeliveryTypeStatic.CDEKCourier11ModeId
        //            || saleInfo.DeliveryModeId == DeliveryTypeStatic.CDEKCourier137ModeId)
        //        {
        //            if (string.IsNullOrWhiteSpace(deliveryModeItem.DeliveryAddressStreet)
        //                || string.IsNullOrWhiteSpace(deliveryModeItem.DeliveryAddressBuilding))
        //            {
        //                returnToPrevStep = true;
        //            }

        //            int cityIdInt = 0;

        //            Int32.TryParse(saleInfo.CityId, out cityIdInt);
        //            //saleInfo.CityName = deliveryTypeItem.SelectedCityName;
        //            saleInfo.CityName = DataService.GetCDEKDeliveryCityName(saleInfo.CityId);
        //            //вытаскиваем здесь стоимость доставки
        //            var deliveryCalculator = DeliveryCalculator.Init(saleInfo.SalesId, saleInfo.DeliveryTypeId,
        //                saleInfo.CityId, saleInfo.DeliveryModeId, "");

        //            var deliveryParams = deliveryCalculator.GetDeliveryPrice();
        //            deliveryParams.CODLimit = 4700;
        //            if (saleInfo.InvoiceAmount + deliveryParams.DeliveryCost > deliveryParams.CODLimit)
        //                returnToPrevStep = true;

        //            saleInfo.DeliveryCost = deliveryParams.DeliveryCost;
        //            saleInfo.TransportLeg = deliveryParams.TransportLeg;

        //            saleInfo.DeliveryAddressStreet = deliveryModeItem.DeliveryAddressStreet;
        //            saleInfo.DeliveryAddressBuilding = deliveryModeItem.DeliveryAddressBuilding;
        //            saleInfo.DeliveryAddressGate = deliveryModeItem.DeliveryAddressGate;
        //            saleInfo.DeliveryAddressAppt = deliveryModeItem.DeliveryAddressAppt;
        //            saleInfo.DeliveryAddressIntercomCode = deliveryModeItem.DeliveryAddressIntercomCode;
        //            saleInfo.DeliveryAddressLevel = deliveryModeItem.DeliveryAddressLevel;
        //        }

        //        saleInfo.Phone = order.ReceiverPhone;

        //        //вот здесь проверяем: если СДЭК и наложенный платёж, то надо вытащить порог НП для города СДЭК
        //        //и проверить сумму с учётом доставки, если больше лимита, возвращать на предыдущий шаг

        //        if (saleInfo.DeliveryCost == -1)
        //        {
        //            throw new Exception("Ошибка расчёта стоимости доставки " + saleInfo.DeliveryTypeId + saleInfo.DeliveryModeId + saleInfo.CityId.ToString() + saleInfo.CityName.ToString() + saleInfo.SelfDeliveryPointId);
        //        }

        //        //устанавливаем параметры заказа
        //        if (!DataService.SetSaleOrderParamsBySaleInfo(saleInfo))
        //        {
        //            return RedirectToAction("CreateOrder", "Basket");
        //        }

        //        //а теперь проверяем, на всякий случай
        //        if (returnToPrevStep)
        //            return RedirectToAction("CreateOrder", "Basket");
        //    }
        //    catch (Exception ex)
        //    {
        //        DataService.ReportAboutCrash("Ошибка сохранения параметров заказа " + order.OrderId, ex.Message);
        //        return RedirectToAction("CreateOrder", "Basket");
        //    }


        //    order.Summary = DataService.GetOrderSummaryForOrder(order.OrderId);

        //    ViewBag.Title = "Ваша корзина - способ оплаты";
        //    //возвращаем следующий View
        //    return View("Payment", order); // Payment(order);///RedirectToAction("Payment", "Basket");
        //}
        #endregion Устарело 2017-11-02
        public ActionResult Confirmation()
        {
            return RedirectToAction("Index", "Home");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Confirmation(OrderViewModel order)
        {
            if (!Request.IsAuthenticated)
            {
                var returnUrl = Url.Action("Index");
                return RedirectToAction("Login", "Account", new { returnUrl = returnUrl });
            }

            if (!ModelState.IsValid)
                return RedirectToAction("CreateOrder");

            var saleInfo = new SaleInfo(order.OrderId);
            if (!saleInfo.OK)
            {
                //заказ отменили
                return RedirectToAction("Index");
            }

            if (saleInfo.Status != SaleStatus.Edit)
            {
                return RedirectToAction("Index", "Home");
            }
            //Вот сюда переносим потроха из сабмит CreateOrder
            try
            {

                var userId = User.Identity.GetUserId();


                //cтавим параметры
                saleInfo.UserFirstName = order.ReceiverFirstName;
                saleInfo.UserLastName = order.ReceiverLastName;
                saleInfo.UserComment = order.Comment;

                if (order.DeliveryType.SelectedDeliveryTypeId == null)
                {
                    return RedirectToAction("CreateOrder", "Basket");
                }

                saleInfo.DeliveryTypeId = order.DeliveryType.SelectedDeliveryTypeId;

                var deliveryTypeItem = order.DeliveryType.TypeItems.FirstOrDefault(x => x.DeliveryTypeId == saleInfo.DeliveryTypeId);

                if (deliveryTypeItem == null)
                {
                    throw new Exception("Ошибка поиска типа доставки " + saleInfo.DeliveryTypeId);
                }
                var selectedCityId = deliveryTypeItem.SelectedCityId;

                var selectedDeliveryModeId = deliveryTypeItem.SelectedDeliveryModeId;

                if (selectedDeliveryModeId == null)
                {
                    return RedirectToAction("CreateOrder", "Basket");
                }

                if (selectedCityId == null)
                {
                    return RedirectToAction("CreateOrder", "Basket");
                }

                saleInfo.CityId = selectedCityId;

                saleInfo.DeliveryModeId = selectedDeliveryModeId;

                var deliveryModeItem = deliveryTypeItem.ModeItems.FirstOrDefault(x => x.DeliveryModeId == saleInfo.DeliveryModeId);

                //здесь нужна вилка: в зависимости от типа и способа доставки считать стоимость доставки
                //если самовывоз - вытаскивать из таблицы
                //если сдэк - запрашивать
                //если почта россии - тоже запрашивать

                if (saleInfo.DeliveryModeId == DeliveryTypeStatic.SelfDeliveryModeId)
                {
                    saleInfo.SelfDeliveryPointId = deliveryModeItem.SelectedSelfDeliveryPointId;
                    var deliveryItem = DataService.GetSelfDeliveryParams(saleInfo.SelfDeliveryPointId);
                    //здесь надо вытаскивать название города из базы
                    saleInfo.DeliveryCost = deliveryItem.DeliveryCost;
                    saleInfo.TransportLeg = deliveryItem.TransportLeg;
                    saleInfo.SelfDeliveryPointName = deliveryItem.SelfDeliveryPointName;
                    saleInfo.SelfDeliveryPointDescription = deliveryItem.SelfDeliveryPointDescription;
                }

                bool returnToPrevStep = false;

                if (saleInfo.DeliveryModeId == DeliveryTypeStatic.CDEKPVZ10ModeId
                    || saleInfo.DeliveryModeId == DeliveryTypeStatic.CDEKPVZ136ModeId)
                {
                    int cityIdInt = 0;

                    Int32.TryParse(saleInfo.CityId, out cityIdInt);

                    //saleInfo.CityName = deliveryTypeItem.SelectedCityName;
                    saleInfo.CityName = DataService.GetCDEKDeliveryCityName(saleInfo.CityId);

                    var selectedSelfDeliveryPointId = deliveryModeItem.SelectedSelfDeliveryPointId;

                    var CDEKDeliveryPoints = DataService.GetCDEKDeliveryPointsInCity(saleInfo.SalesId, cityIdInt);

                    var selectedDeliveryPoint = CDEKDeliveryPoints.FirstOrDefault(x => x.SelfDeliveryPointId == selectedSelfDeliveryPointId);

                    saleInfo.SelfDeliveryPointId = deliveryModeItem.SelectedSelfDeliveryPointId;

                    //вытаскиваем здесь стоимость доставки
                    var deliveryCalculator = DeliveryCalculator.Init(saleInfo.SalesId, saleInfo.DeliveryTypeId,
                        saleInfo.CityId, saleInfo.DeliveryModeId, selectedSelfDeliveryPointId);

                    var deliveryParams = deliveryCalculator.GetDeliveryPrice();

                    //deliveryParams.CODLimit = 4700;
                    if (saleInfo.InvoiceAmount + deliveryParams.DeliveryCost > deliveryParams.CODLimit)
                        returnToPrevStep = true;


                    saleInfo.DeliveryCost = deliveryParams.DeliveryCost;
                    saleInfo.TransportLeg = deliveryParams.TransportLeg;
                    saleInfo.SelfDeliveryPointName = selectedDeliveryPoint.SelfDeliveryPointName;
                    saleInfo.SelfDeliveryPointDescription = selectedDeliveryPoint.SelfDeliveryPointDescription;
                }



                if (saleInfo.DeliveryModeId == DeliveryTypeStatic.CDEKCourier11ModeId
                    || saleInfo.DeliveryModeId == DeliveryTypeStatic.CDEKCourier137ModeId)
                {
                    if (string.IsNullOrWhiteSpace(deliveryModeItem.DeliveryAddressStreet)
                        || string.IsNullOrWhiteSpace(deliveryModeItem.DeliveryAddressBuilding))
                    {
                        returnToPrevStep = true;
                    }

                    int cityIdInt = 0;

                    Int32.TryParse(saleInfo.CityId, out cityIdInt);
                    //saleInfo.CityName = deliveryTypeItem.SelectedCityName;
                    saleInfo.CityName = DataService.GetCDEKDeliveryCityName(saleInfo.CityId);
                    //вытаскиваем здесь стоимость доставки
                    var deliveryCalculator = DeliveryCalculator.Init(saleInfo.SalesId, saleInfo.DeliveryTypeId,
                        saleInfo.CityId, saleInfo.DeliveryModeId, "");

                    var deliveryParams = deliveryCalculator.GetDeliveryPrice();

                    //deliveryParams.CODLimit = 4700;

                    if (saleInfo.InvoiceAmount + deliveryParams.DeliveryCost > deliveryParams.CODLimit)
                        returnToPrevStep = true;


                    saleInfo.DeliveryCost = deliveryParams.DeliveryCost;
                    saleInfo.TransportLeg = deliveryParams.TransportLeg;

                    saleInfo.DeliveryAddressStreet = deliveryModeItem.DeliveryAddressStreet;
                    saleInfo.DeliveryAddressBuilding = deliveryModeItem.DeliveryAddressBuilding;
                    saleInfo.DeliveryAddressGate = deliveryModeItem.DeliveryAddressGate;
                    saleInfo.DeliveryAddressAppt = deliveryModeItem.DeliveryAddressAppt;
                    saleInfo.DeliveryAddressIntercomCode = deliveryModeItem.DeliveryAddressIntercomCode;
                    saleInfo.DeliveryAddressLevel = deliveryModeItem.DeliveryAddressLevel;
                }

                saleInfo.Phone = order.ReceiverPhone;

                if (saleInfo.DeliveryCost == -1)
                {
                    throw new Exception("Ошибка расчёта стоимости доставки " + saleInfo.DeliveryTypeId + saleInfo.DeliveryModeId + saleInfo.CityId.ToString() + saleInfo.CityName.ToString() + saleInfo.SelfDeliveryPointId);
                }


                //устанавливаем параметры заказа
                if (!DataService.SetSaleOrderParamsBySaleInfo(saleInfo))
                {
                    return RedirectToAction("CreateOrder", "Basket");
                }

                //Mike 2017-11-07
                /*
                var paymentTypeId = PaymentTypeStatic.SalesPaymentTypeToOrderPaymentType(saleInfo.PaymentTypeId);
                var city = new CDEKDeliveryCity(saleInfo.CityId);

                if (paymentTypeId == PaymentTypeStatic.CashOnDeliveryPaymentTypeId)
                {
                    if (city.IsCODAllowed == false)
                    {
                        //                        deliveryTypeItem.Message = "К сожалению, в выбранном городе невозможна доставка СДЭК с оплатой при получении :-(";
                        returnToPrevStep = true;
                    }

                }
                */

                //а теперь проверяем, на всякий случай
                if (returnToPrevStep)
                    return RedirectToAction("CreateOrder", "Basket");
            }
            catch (Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка сохранения параметров заказа " + order.OrderId, ex.Message);
                return RedirectToAction("CreateOrder", "Basket");
            }
            //конец потрохов

            var orderViewModel = saleInfo.ConvertToOrderViewModel();
            var strUrl = DataService.GetUrl(Request);
            orderViewModel.Items = DataService.GetSalesTrans(order.OrderId, strUrl);
            orderViewModel.PresentItems = DataService.GetGivenPresents(order.OrderId, strUrl);

            orderViewModel.Summary = DataService.GetOrderSummaryForOrder(order.OrderId);

            ViewBag.Title = "Ваша корзина - подтверждение заказа";
            //тут прописываем в шапку заказа параметры оплаты и выводим вьюху для подтверждения
            return View("Confirmation", orderViewModel);
        }

        public ActionResult Success()
        {
            return RedirectToAction("Index", "Home");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Success(OrderViewModel order)
        {
            if (!Request.IsAuthenticated)
                return RedirectToAction("Login", "Account");

            //подтверждаем
            //вот здесь делаем submit
            var saleInfo = new SaleInfo(order.OrderId);

            ApplicationUser user = UserManager.FindByEmail(User.Identity.Name);

            var submitResult = false;

            switch (saleInfo.PaymentTypeId)
            {
                case PaymentTypeId.Card:
                    { 
                        submitResult = DataService.SubmitSalesOrder(saleInfo, SaleStatus.Aside, User.Identity.GetUserId());
                        break;
                    }
                case PaymentTypeId.Cash:
                    {
                        submitResult = DataService.SubmitSalesOrder(saleInfo, SaleStatus.Post, User.Identity.GetUserId());
                        break;
                    }
            }


            if (submitResult)
            {
                var mailerParams = DataService.GetMailRobotParams();
                switch (saleInfo.PaymentTypeId)
                {
                    case PaymentTypeId.Card:
                        {
                            DataService.SendSingleSMS(saleInfo.Phone.Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty), "Ваш заказ " + saleInfo.SalesId + " в интернет-магазине krasivo.pro принят. Вы можете оплатить его на сайте");
                            break;
                        }
                    case PaymentTypeId.Cash:
                        {
                            DataService.SendSingleSMS(saleInfo.Phone.Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty), "Ваш заказ " + saleInfo.SalesId + " в интернет-магазине krasivo.pro принят. Мы оповестим Вас, когда заказ прибудет в пункт назначения");
                            if (saleInfo.DeliveryTypeId != DeliveryTypeStatic.CDEKDeliveryTypeId)
                            { 
                                var shopMessage = DataService.GeneratePaymentMailShop(saleInfo);
                                DataService.SendMail(mailerParams, saleInfo.SelfDeliveryPointMail, shopMessage.Subject, shopMessage.Body, null, true);
                            }
                            break;
                        }
                }


                var pickupMessage = DataService.GeneratePickupMail(saleInfo, user);
                DataService.SendMail(mailerParams, user.Email, pickupMessage.Subject, pickupMessage.Body, null, true);

                var supportMessage = DataService.GenerateSubmitMailSupport(saleInfo, mailerParams.AdminConsole);

                DataService.SendMail(mailerParams, mailerParams.Pickup, supportMessage.Subject, supportMessage.Body, null, true);
            }
            else
            {
                DataService.ReportAboutCrash("Некорректный результат сабмита " + order.OrderId, "сабж");
            }
            order = saleInfo.ConvertToOrderViewModel();
            return View("Success", order);
        }

        public JsonResult GetCDEKCityList(string name_startsWith)
        {
            var ret = new CDEKCityArray();
            try
            {
                ret.geonames = DataService.GetCDEKDeliveryCitys(name_startsWith);

            }
            catch (Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка конвертации в JSON списка городов СДЭК " + name_startsWith, ex.Message);
                ret.geonames = new List<CityDropDownItem>();
            }
            return Json(ret, JsonRequestBehavior.AllowGet);
        }
    }
}