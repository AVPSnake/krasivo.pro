﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Data;
using System.Data.SqlClient;
using WebApplication1.Classes;
using WebApplication1.Models.ViewModels;
using Microsoft.AspNet.Identity.Owin;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Authorize(Roles = "Admin")]
    public class OrderController : Controller
    {
        private ApplicationUserManager _userManager;

        public OrderController()
        {

        }

        public OrderController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //List
        public ActionResult Index(SaleStatus status = SaleStatus.None)
        {
            if (!Request.IsAuthenticated)
                return RedirectToAction("Login", "Account", new { returnUrl = Request.Url.PathAndQuery });
            //просто список заказов
            var model = new List<OrderViewModel>();
            model = DataService.GetOrdersListBySaleStatus(status);
            return View(model);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Pickup(string id)
        {
            var saleInfo = new SaleInfo(id);
            var order = new OrderViewModel();
            if (saleInfo.OK)
            {
                order = saleInfo.ConvertToOrderViewModel();
            }
            else
            {
                return RedirectToAction("Error");
            }
            //вытаскиваем наш заказ            

            var strUrl = DataService.GetUrl(Request);
            order.Items = DataService.GetSalesTrans(order.OrderId, strUrl);

            return View(order);
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Pickup(OrderViewModel OrderViewModel)
        {
            //проставляем количества в заказе
            //ставим статус "собрано"
            var dtXmlTable = new DataTable();
            DataColumn dc = new DataColumn();
            dc.DataType = typeof(string);
            dc.ColumnName = "ItemId";

            dtXmlTable.Columns.Add(dc);

            dc = new DataColumn();
            dc.DataType = typeof(int);
            dc.ColumnName = "Qty";

            dtXmlTable.Columns.Add(dc);

            foreach (var item in OrderViewModel.Items)
            {
                DataRow dr = dtXmlTable.NewRow();
                dr["ItemId"] = item.ItemId;
                dr["Qty"] = item.Qty;
                dtXmlTable.Rows.Add(dr);
            }
            //заказ-то у нас уже есть
            var saleInfo = new SaleInfo(OrderViewModel.OrderId);

            var pickupResult = DataService.PickupSalesOrder(saleInfo, SaleStatus.Picked, dtXmlTable, User.Identity.GetUserId());

            if (pickupResult)
            {
                var user = UserManager.FindById(saleInfo.AspId);

                //DataService.SendMail(DataService.GetMailRobotParams(), user.Email, "Заказ " + saleInfo.SalesId + " в интернет - магазине krasivo.pro готов к оплате", "Здравствуйте, "+user.FirstName+ "! \r\n Ваш заказ " + saleInfo.SalesId + " в интернет - магазине krasivo.pro успешно собран.Вы можете оплатить его на сайте");                
                var pickupMessage = DataService.GeneratePickupMail(saleInfo, user);
                DataService.SendMail(DataService.GetMailRobotParams(), user.Email, pickupMessage.Subject, pickupMessage.Body, null, true);
                DataService.SendSingleSMS(user.PhoneNumber.Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty), "Ваш заказ " + saleInfo.SalesId + " в интернет-магазине krasivo.pro успешно собран. Вы можете оплатить его на сайте");
            }

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public JsonResult Cancel(string id)
        {
            var ret = false;
            //вытаскиваем наш заказ
            var order = new OrderViewModel();
            order.OrderId = id;
            var saleInfo = new SaleInfo(order.OrderId);

            ret = DataService.RejectSalesOrder(saleInfo, User.Identity.GetUserId());

            return Json(ret);
        }


        public ActionResult Details(string id)
        {
            //вытаскиваем наш заказ
            var saleInfo = new SaleInfo(id);
            var order = new OrderViewModel();
            if (saleInfo.OK)
            {
                order = saleInfo.ConvertToOrderViewModel();
            }
            else
            {
                return RedirectToAction("Error");
            }

            /*
            var order = new OrderViewModel();
            order.OrderId = id;
            var saleInfo = new SaleInfo(order.OrderId);

            order.CustId = saleInfo.CustId;
            order.CustFirstName = saleInfo.CustFirstName;
            order.CustLastName = saleInfo.CustLastName;
            order.TransDate = saleInfo.TransDate;
            order.Status = saleInfo.Status;
            order.StatusString = saleInfo.StatusDescription;
            order.InvoiceAmount = saleInfo.InvoiceAmount;
            order.ReceiverFirstName = saleInfo.UserFirstName;
            order.ReceiverLastName = saleInfo.UserLastName;
            order.Comment = saleInfo.UserComment;
            */
            var strUrl = DataService.GetUrl(Request);
            order.Items = DataService.GetSalesTrans(order.OrderId, strUrl);
            return View(order);
        }

        //[Authorize]
        //public ActionResult MyOrdersList()
        //{
        //    if (!Request.IsAuthenticated)
        //        return RedirectToAction("Login", "Account", new { returnUrl = Request.Url.PathAndQuery });

        //    var userId = User.Identity.GetUserId();
        //    //просто список заказов
        //    var model = new List<OrderViewModel>();
        //    model = DataService.GetOrdersListByUserId(userId);
        //    return View(model);
        //}
    }
}