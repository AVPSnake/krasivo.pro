﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using WebApplication1.Models;
using System.Data.SqlClient;
using WebApplication1.Classes;
using WebApplication1.Models.ViewModels;
using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;

using System.Net;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.IO;

using System.Security.Cryptography;
using System.Text;

using System.Collections.Specialized;


namespace WebApplication1.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        //krasivo.pro
        
        public static string fbAppId = "307024933114653";
        public static string fbAppSecret = "6ed31295ba395926b15b65578fe0587f";
        public static string fbRedirectUrl = "https://krasivo.pro/Account/FBCallback";

        public static string vkAppId = "6260213";
        public static string vkAppSecret = "ZrUIl1Wpk57v4ToBPjVe";
        public static string vkRedirectUrl = "https://krasivo.pro/Account/VKCallback";

        public static string okAppId = "1258857728";
        public static string okAppPublic = "CBAGKJAMEBABABABA";
        public static string okAppSecret = "90705E966BC4EF9BE5DCAC38";
        public static string okRedirectUrl = "https://krasivo.pro/Account/OKCallback";

        public static string yaAppId = "422e63c576914ce3be4114260d78e345";
        public static string yaAppSecret = "187da376a0a74708a5b701f394189dba";
        public static string yaRedirectUrl = "https://krasivo.pro/Account/YaCallback";

        public static string gpAppId = "151770473728-bmgjmms486q5rbdhnn0sj67i3ode2hae.apps.googleusercontent.com";
        public static string gpAppSecret = "KMzUeN7DobxSclHt5cTxTmn5";
        public static string gpRedirectUrl = "https://krasivo.pro/Account/GpCallback";

        public static string mailAppId = "757366";
        public static string mailAppSecret = "974ee0a908bab3ebac23a5f32f7ef725";
        public static string mailRedirectUrl = "https://krasivo.pro/Account/MailCallback";


        //test
        /*
        public static string fbAppId = "1993125770933142";
        public static string fbAppSecret = "a2bb278044afe0e1792836ced6f7df3f";
        public static string fbRedirectUrl = "https://avp.ru/Account/FBCallback";

        public static string vkAppId = "6260286";
        public static string vkAppSecret = "vSGMif7dVdXRrbrfSRfc";
        public static string vkRedirectUrl = "https://avp.ru/Account/VKCallback";
        
        public static string okAppId = "1258857728";
        public static string okAppPublic = "CBAGKJAMEBABABABA";
        public static string okAppSecret = "90705E966BC4EF9BE5DCAC38";
        public static string okRedirectUrl = "https://avp.ru/Account/OKCallback";

        public static string yaAppId = "422e63c576914ce3be4114260d78e345";
        public static string yaAppSecret = "187da376a0a74708a5b701f394189dba";
        public static string yaRedirectUrl = "https://avp.ru/Account/YaCallback";

        public static string gpAppId = "190035927765-t82porcqhardu40am5n81gffu9ljq1jc.apps.googleusercontent.com";
        public static string gpAppSecret = "M5NNjTOqdj-5zT5yZwVud6wh";
        public static string gpRedirectUrl = "https://avp.ru/Account/GpCallback";
        */

        #region templates

        public AccountController()
        {
            
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }
        #endregion

        public ApplicationUser FindOrCreateUser(string id, string email, string ln, string fn)
        {
            ApplicationUser user = null;

            user = UserManager.FindByEmail(email);

            if (user != null)
                return user;

            user = new ApplicationUser {
                UserName = email,
                Email = email,
                LastName = ln,
                FirstName = fn,
                PhoneNumber = "",
                AgreeToMaintainPersonalData = true,
                AgreeToReceivePromoInfo = true };

            var result = UserManager.Create(user);
            if (!result.Succeeded)
                return null;

            user = UserManager.FindByEmail(email);

            return user;
        }

        public ActionResult ExternalLogin(string id, string email, string ln, string fn)
        {
            string returnUrl = Session["ReturnUrl"].ToString();
            var anonimousId = Request.AnonymousID;

            try
            {
                var user = FindOrCreateUser(id, email, ln, fn);

                SignInManager.SignIn(user, isPersistent: false, rememberBrowser: false);

                DataService.ChangeAnonimousIdInBasket(user.Id, anonimousId);
                DataService.ChangeAnonimousIdInWishList(user.Id, anonimousId);

            }
            catch (Exception ex)
            {
                return View("CustomError", new CustomErrorModel() { ErrorTitle = "Ошибка авторизации", ErrorDescription = ex.Message });
            }
            if (returnUrl != null)
                return RedirectToLocal(returnUrl);

            return RedirectToAction("Index", "Home");
        }

        #region FB

        [AllowAnonymous]
        public ActionResult FBCallback()
        {
            string rs = Request.QueryString.ToString();

            string code = Request.QueryString["code"];
            //string token = Request.QueryString["token"];
            if (code == null)
                return View("Error");

            var fbToken = FBHelper.getToken(code);

            var fbUser = FBHelper.getInfo(fbToken.access_token);


            if (fbUser.email == null || fbUser.email == "")
                return View("CustomError", new CustomErrorModel() { ErrorTitle = "Ошибка авторизации Facebook", ErrorDescription = "Для авторизации через Facebook заполните в профиле VK адрес элетронной почты" });
            
            return ExternalLogin(fbUser.id, fbUser.email, fbUser.last_name, fbUser.first_name);
        }

        public class FBHelper
        {
            public static string clientId = fbAppId;
            public static string clientSecret = fbAppSecret;
            public static string redirectUrl = fbRedirectUrl;


            public static string userFields = "bdate";
            public static string ver = "5.69";

            /*получение токена*/
            public static FBToken getToken(string _code)
            {
                var url = string.Format("https://graph.facebook.com/v2.11/oauth/access_token?client_id={0}&client_secret={1}&redirect_uri={2}&code={3}", clientId, clientSecret, redirectUrl, _code);
                var getRequest = WebRequest.Create(url);

                using (var resp = getRequest.GetResponse())
                {
                    using (var stream = resp.GetResponseStream())
                    {
                        if (stream != null)
                        {
                            using (var sr = new StreamReader(stream))
                            {
                                var data = sr.ReadToEnd();

                                return new JavaScriptSerializer().Deserialize<FBToken>(data);
                            }
                        }
                    }
                }

                return null;
            }

            public static FBUser getInfo(string _token)
            {
                var url = string.Format("https://graph.facebook.com/me?access_token={0}&fields=id,first_name,last_name,email", _token);
                var getRequest = WebRequest.Create(url);

                using (var resp = getRequest.GetResponse())
                {
                    using (var stream = resp.GetResponseStream())
                    {
                        if (stream != null)
                        {
                            using (var sr = new StreamReader(stream))
                            {
                                var data = sr.ReadToEnd();

                                return new JavaScriptSerializer().Deserialize<FBUser>(data);

                                //return JsonConvert.DeserializeObject(data);
                            }
                        }
                    }
                }

                return null;
            }


            public class FBToken
            {
                public string access_token { get; set; }

                public string expires_in { get; set; }

                public string token_type { get; set; }

            }

            public class FBUser
            {
                public string first_name { get; set; }
                public string last_name { get; set; }
                public string email { get; set; }
                public string id { get; set; }
            }

        }


        #endregion

        #region VK
        public class VkHelper
        {
            public static string clientId = vkAppId;
            public static string clientSecret = vkAppSecret;
            public static string redirectUri = vkRedirectUrl;


            public static string userFields = "bdate";
            public static string ver = "5.69";

            /*получение токена*/
            public static VkToken getToken(string _code)
            {
                var url = string.Format("https://oauth.vk.com/access_token?client_id={0}&client_secret={1}&redirect_uri={2}&code={3}", clientId, clientSecret, redirectUri, _code);
                var getRequest = WebRequest.Create(url);

                using (var resp = getRequest.GetResponse())
                {
                    using (var stream = resp.GetResponseStream())
                    {
                        if (stream != null)
                        {
                            using (var sr = new StreamReader(stream))
                            {
                                return new JavaScriptSerializer().Deserialize<VkToken>(sr.ReadToEnd());
                            }
                        }
                    }
                }

                return null;
            }

            /*получение инфы юзера*/
            public static dynamic getInfo(string _token, string _userId)
            {
                var url = string.Format("https://api.vk.com/method/users.get?user_ids={0}&fields={1}&v={2}&access_token={3}", _userId, userFields, ver, _token);
                var getRequest = WebRequest.Create(url);

                using (var resp = getRequest.GetResponse())
                {
                    using (var stream = resp.GetResponseStream())
                    {
                        if (stream != null)
                        {
                            using (var sr = new StreamReader(stream))
                            {
                                //return new JavaScriptSerializer().Deserialize<List<VkInfo>>(sr.ReadToEnd());
                                dynamic jsonDe = JsonConvert.DeserializeObject(sr.ReadToEnd());
                                return jsonDe;
                            }
                        }
                    }
                }

                return null;
            }

            public class VkToken
            {
                public string access_token { get; set; }

                public string expires_in { get; set; }

                public string user_id { get; set; }

                public string error { get; set; }

                public string error_description { get; set; }

                public string email { get; set; }

            }

        }

        [AllowAnonymous]
        public ActionResult VKCallback()
        {

            string code = Request.QueryString["code"];
            if (code == null)
                return View("Error");

            var vkToken = VkHelper.getToken(code);

            //если при получении токена нет ошибок
            if (vkToken.error != null)
                return View("Error");

            var userInfo = VkHelper.getInfo(vkToken.access_token, vkToken.user_id);

            string fn = userInfo.response[0].first_name;
            string ln = userInfo.response[0].last_name;

            string email = vkToken.email;

            //email = null;

            if (email == null || email == "")
                return View("CustomError", new CustomErrorModel() { ErrorTitle = "Ошибка авторизации VK", ErrorDescription = "Для авторизации через VK заполните в профиле VK адрес элетронной почты" });

            return ExternalLogin(vkToken.user_id, email, ln, fn);
        }

        #endregion

        #region OK
        public class OkHelper
        {
            public static string clientId = okAppId;
            public static string clientSecret = okAppSecret;
            public static string redirectUri = okRedirectUrl;

            public static string userFields = "bdate";
            public static string ver = "5.69";

            /*получение токена*/
            public static OkToken getToken(string _code)
            {
                var url = string.Format("https://api.ok.ru/oauth/token.do?code={0}&client_id={1}&client_secret={2}&redirect_uri={3}&grant_type=authorization_code", _code, clientId, clientSecret, redirectUri);

                var getRequest = WebRequest.Create(url);

                getRequest.Method = "POST";

                using (var resp = getRequest.GetResponse())
                {
                    using (var stream = resp.GetResponseStream())
                    {
                        if (stream != null)
                        {
                            using (var sr = new StreamReader(stream))
                            {
                                return new JavaScriptSerializer().Deserialize<OkToken>(sr.ReadToEnd());
                            }
                        }
                    }
                }

                return null;
            }

            /*получение инфы юзера*/
            public static dynamic getInfo(string _okAppPublic, string _sig, string _token)
            {
                var url = string.Format("https://api.ok.ru/fb.do?method=users.getCurrentUser&access_token={0}&application_key={1}&fields=email,first_name,last_name&sig={2}", _token, _okAppPublic, _sig);

                var getRequest = WebRequest.Create(url);

                using (var resp = getRequest.GetResponse())
                {
                    using (var stream = resp.GetResponseStream())
                    {
                        if (stream != null)
                        {
                            using (var sr = new StreamReader(stream))
                            {
                                //return new JavaScriptSerializer().Deserialize<List<VkInfo>>(sr.ReadToEnd());
                                dynamic jsonDe = JsonConvert.DeserializeObject(sr.ReadToEnd());
                                return jsonDe;
                            }
                        }
                    }
                }

                return null;
            }

            public class OkToken
            {
                public string access_token { get; set; }

                public string expires_in { get; set; }

                public string error { get; set; }

                public string error_description { get; set; }

                public string refresh_token { get; set; }
            }

        }

        [AllowAnonymous]
        public ActionResult OKCallback()
        {

            string code = Request.QueryString["code"];
            if (code == null)
                return View("Error");

            var okToken = OkHelper.getToken(code);

            //если при получении токена нет ошибок
            if (okToken.error != null)
                return View("Error");

            /*Расчет подписи запроса
            при отсутствии значения session_secret_key:
            для вызова без сессии считаем session_secret_key = application_secret_key;
            для вызова в сессии session_secret_key = MD5(access_token + application_secret_key), переводим значение в нижний регистр;
            убираем из списка параметров session_key / access_token при наличии;
            параметры сортируются лексикографически по ключам;
            параметры соединяются в формате ключ = значение;
            sig = MD5(значения_параметров + session_secret_key);
            значение sig переводится в нижний регистр.*/
            MD5 md5Hash = MD5.Create();
            //1. сначала заворачиваем в md5 полученый ранее токен + секретный ключ приложения
            string session_secret_key = GetMd5Hash(md5Hash, (okToken.access_token + okAppSecret)).ToLower();
            //2. формируем строку параметров запроса(также как и в УРЛ, но только без разделителей, и по алфавиту!!!) + публичный ключ приложения
            string left_part = string.Format("application_key={0}fields=email,first_name,last_namemethod=users.getCurrentUser", okAppPublic);
            //3. соединяем п.1 и п.2 и еще раз заворачиваем в md5
            string sig = GetMd5Hash(md5Hash, left_part + session_secret_key);

            var userInfo = OkHelper.getInfo(okAppPublic, sig.ToLower(), okToken.access_token);

            string fn = userInfo["first_name"];
            string ln = userInfo["last_name"];
            string email = userInfo["email"];
            string uid = userInfo["uid"];

            //email = null;

            if (email == null || email == "")
                return View("CustomError", new CustomErrorModel() { ErrorTitle = "Ошибка авторизации OK", ErrorDescription = "Для авторизации через OK заполните в профиле OK адрес элетронной почты" });

            return ExternalLogin(uid, email, ln, fn);
            //return null;
        }

       

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }


        #endregion

        #region Yandex
        public class YaHelper
        {
            public static string clientId = yaAppId;
            public static string clientSecret = yaAppSecret;
            public static string redirectUri = yaRedirectUrl;

            /*получение токена*/
            public static YaToken getToken(string _code)
            {
                var url = "https://oauth.yandex.ru/token";

                using (var client = new WebClient())
                {
                    var values = new NameValueCollection();
                    values["grant_type"] = "authorization_code";
                    values["code"] = _code;
                    values["client_id"] = clientId;
                    values["client_secret"] = clientSecret;

                    var response = client.UploadValues(url, values);

                    var responseString = Encoding.Default.GetString(response);

                    return new JavaScriptSerializer().Deserialize<YaToken>(responseString);
                }

                return null;
            }

            /*получение инфы юзера*/
            public static /*dynamic*/ YaUser getInfo(string _token)
            {
                var url = "https://login.yandex.ru/info";
                var getRequest = WebRequest.Create(url);
                getRequest.Headers.Add("Authorization", "OAuth " + _token);

                using (var resp = getRequest.GetResponse())
                {
                    using (var stream = resp.GetResponseStream())
                    {
                        if (stream != null)
                        {
                            using (var sr = new StreamReader(stream))
                            {
                                //dynamic jsonDe = JsonConvert.DeserializeObject(sr.ReadToEnd());
                                //return jsonDe;
                                var data = sr.ReadToEnd();

                                return new JavaScriptSerializer().Deserialize<YaUser>(data);
                            }
                        }
                    }
                }

                return null;
            }

            public class YaToken
            {
                public string access_token { get; set; }

                public string expires_in { get; set; }

                public string user_id { get; set; }

                public string error { get; set; }

                public string error_description { get; set; }

                public string email { get; set; }

            }

            public class YaUser
            {
                public string first_name { get; set; }
                public string last_name { get; set; }
                public string default_email { get; set; }
                public string id { get; set; }
            }

        }

        [AllowAnonymous]
        public ActionResult YaCallback()
        {
            string code = Request.QueryString["code"];
            if (code == null)
                return View("Error");

            var yaToken = YaHelper.getToken(code);

            //если при получении токена нет ошибок
            if (yaToken.error != null)
                return View("Error");

            var userInfo = YaHelper.getInfo(yaToken.access_token);

            string fn = userInfo.first_name;
            string ln = userInfo.last_name;
            string email = userInfo.default_email;

            if (email == null || email == "")
                return View("CustomError", new CustomErrorModel() { ErrorTitle = "Ошибка авторизации Яндекс", ErrorDescription = "Для авторизации через Яндекс заполните в профиле Яндекс адрес элетронной почты" });

            return ExternalLogin(yaToken.user_id, email, ln, fn);
            //return null;
        }

        #endregion

        #region Google
        public class GpHelper
        {
            public static string clientId = gpAppId;
            public static string clientSecret = gpAppSecret;
            public static string redirectUri = gpRedirectUrl;

            /*получение токена*/
            public static GpToken getToken(string _code)
            {
                var url = "https://www.googleapis.com/oauth2/v4/token";

                using (var client = new WebClient())
                {
                    var values = new NameValueCollection();                    
                    values["code"] = _code;
                    values["client_id"] = clientId;
                    values["client_secret"] = clientSecret;
                    values["grant_type"] = "authorization_code";
                    values["redirect_uri"] = redirectUri;

                    var response = client.UploadValues(url, values);

                    var responseString = Encoding.Default.GetString(response);

                    return new JavaScriptSerializer().Deserialize<GpToken>(responseString);
                }

                return null;


            }

            /*получение инфы юзера*/
            public static dynamic getInfo(string _token)
            {
                var url = string.Format("https://www.googleapis.com/plus/v1/people/me?access_token={0}", _token);
                var getRequest = WebRequest.Create(url);

                using (var resp = getRequest.GetResponse())
                {
                    using (var stream = resp.GetResponseStream())
                    {
                        if (stream != null)
                        {
                            using (var sr = new StreamReader(stream))
                            {
                                dynamic jsonDe = JsonConvert.DeserializeObject(sr.ReadToEnd());
                                return jsonDe;
                                /*var data = sr.ReadToEnd();

                                return new JavaScriptSerializer().Deserialize<GpUser>(data);*/
                            }
                        }
                    }
                }

                return null;
            }

            public class GpToken
            {
                public string access_token { get; set; }

                public string refresh_token { get; set; }

                public string expires_in { get; set; }

                public string token_type { get; set; }

                public string error { get; set; }

                public string error_description { get; set; }

            }

            public class GpUser
            {
                public string givenName { get; set; }

                public string familyName { get; set; }

                public string id { get; set; }
            }

        }

        [AllowAnonymous]
        public ActionResult GpCallback()
        {
            string code = Request.QueryString["code"];
            if (code == null)
                return View("Error");

            var gpToken = GpHelper.getToken(code);

            //если при получении токена нет ошибок
            if (gpToken.error != null)
                return View("Error");

            var userInfo = GpHelper.getInfo(gpToken.access_token);

            string fn = userInfo.name.givenName;
            string ln = userInfo.name.familyName;
            string email = userInfo.emails[0].value;
            string user_id = userInfo.id;

            if (email == null || email == "")
                return View("CustomError", new CustomErrorModel() { ErrorTitle = "Ошибка авторизации Google", ErrorDescription = "Для авторизации через Google заполните в профиле Google+ адрес элетронной почты" });

            return ExternalLogin(user_id, email, ln, fn);
            //return null;
        }

        #endregion

        #region Mail.ru
        public class MailHelper
        {
            public static string clientId = mailAppId;
            public static string clientSecret = mailAppSecret;
            public static string redirectUri = mailRedirectUrl;

            /*получение токена*/
            public static MailToken getToken(string _code)
            {
                var url = string.Format("https://connect.mail.ru/oauth/token?client_id={1}&client_secret={2}&grant_type=authorization_code&code={0}&redirect_uri={3}", _code, clientId, clientSecret, redirectUri);

                var getRequest = WebRequest.Create(url);

                getRequest.Method = "POST";

                using (var resp = getRequest.GetResponse())
                {
                    using (var stream = resp.GetResponseStream())
                    {
                        if (stream != null)
                        {
                            using (var sr = new StreamReader(stream))
                            {
                                return new JavaScriptSerializer().Deserialize<MailToken>(sr.ReadToEnd());
                            }
                        }
                    }
                }

                return null;
            }

            /*получение инфы юзера*/
            public static dynamic getInfo(string _mailAppId, string _sig, string _token, string _userId)
            {
                var url = string.Format("https://www.appsmail.ru/platform/api?method=users.getInfo&app_id={1}&session_key={0}&secure=1&sig={2}&uids={3}", _token, _mailAppId, _sig, _userId);

                var getRequest = WebRequest.Create(url);

                using (var resp = getRequest.GetResponse())
                {
                    using (var stream = resp.GetResponseStream())
                    {
                        if (stream != null)
                        {
                            using (var sr = new StreamReader(stream))
                            {
                                //return new JavaScriptSerializer().Deserialize<List<VkInfo>>(sr.ReadToEnd());
                                dynamic jsonDe = JsonConvert.DeserializeObject(sr.ReadToEnd());
                                return jsonDe;
                            }
                        }
                    }
                }

                return null;
            }

            public class MailToken
            {
                public string access_token { get; set; }

                public string expires_in { get; set; }

                public string error { get; set; }

                public string error_description { get; set; }

                public string refresh_token { get; set; }

                public string x_mailru_vid { get; set; }
            }

        }

        [AllowAnonymous]
        public ActionResult MailCallback()
        {

            string code = Request.QueryString["code"];
            if (code == null)
                return View("Error");

            var mailToken = MailHelper.getToken(code);

            //если при получении токена нет ошибок
            if (mailToken.error != null)
                return View("Error");


            //1. формируем строку параметров запроса(также как и в УРЛ, но только без разделителей, и по алфавиту!!!) + публичный ключ приложения
            string parms = string.Format("app_id={0}method=users.getInfosecure=1session_key={1}uids={2}", mailAppId, mailToken.access_token, mailToken.x_mailru_vid);

            MD5 md5Hash = MD5.Create();
            
            //2. соединяем параметры запроса с секретным ключом и заворачиваем в md5
            string sig = GetMd5Hash(md5Hash, parms + mailAppSecret).ToLower();

            var userInfo = MailHelper.getInfo(mailAppId, sig.ToLower(), mailToken.access_token, mailToken.x_mailru_vid);

            string fn = userInfo[0].first_name;
            string ln = userInfo[0].last_name;
            string email = userInfo[0].email;
            string uid = userInfo[0].uid;

            //email = null;

            if (email == null || email == "")
                return View("CustomError", new CustomErrorModel() { ErrorTitle = "Ошибка авторизации Mail.ru", ErrorDescription = "Для авторизации через Mail.ru заполните в профиле Mail.ru адрес элетронной почты" });

            return ExternalLogin(uid, email, ln, fn);
            //return null;
        }


        #endregion



        #region login
        //
        // GET: /Account/Login
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Login(string returnUrl)
        {
            Session["ReturnUrl"] = returnUrl;
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            try
            { 
                if (ModelState.IsValid)
                {
                    var anonimousId = Request.AnonymousID;
                    var user = await UserManager.FindAsync(model.Email, model.Password);
                    if (user != null)
                    {
                        if (user.EmailConfirmed == true)
                        {
                            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                            var userId = user.Id;
                            DataService.ChangeAnonimousIdInBasket(userId, anonimousId);
                            DataService.ChangeAnonimousIdInWishList(userId, anonimousId);
                            if (!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl) && !returnUrl.Contains("Account"))
                            {
                                return RedirectToLocal(returnUrl);
                            }
                            else
                            {
                                return RedirectToAction("Index", "InventTable");
                            }
                            //return RedirectToLocal(returnUrl);
                        }
                        else
                        {
                            //

                            ModelState.AddModelError("", "Email не подтверждён. При регистрации вам было отправлен письмо для подтверждения. <a href=\"https://krasivo.pro/Account/ResendConfirmationEmail/"+user.Id+"\">Отправить ещё раз</a>");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Неверный логин или пароль");
                    }
                }
            }
            catch(Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка входа у пользователя " + model.Email, ex.Message);
                RedirectToAction("Index", "Home");
            }
            return View(model);
        }
        #endregion

        #region verify codes
        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Требовать предварительный вход пользователя с помощью имени пользователя и пароля или внешнего имени входа
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // Приведенный ниже код защищает от атак методом подбора, направленных на двухфакторные коды. 
            // Если пользователь введет неправильные коды за указанное время, его учетная запись 
            // будет заблокирована на заданный период. 
            // Параметры блокирования учетных записей можно настроить в IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent:  model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Неправильный код.");
                    return View(model);
            }
        }
        #endregion

        #region register
        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, LastName = model.LastName, FirstName = model.FirstName, PhoneNumber = model.PhoneNumber, AgreeToMaintainPersonalData = model.AgreeToMaintainPersonalData, AgreeToReceivePromoInfo = true };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);
                    
                    // Дополнительные сведения о том, как включить подтверждение учетной записи и сброс пароля, см. по адресу: http://go.microsoft.com/fwlink/?LinkID=320771
                    // Отправка сообщения электронной почты с этой ссылкой
                    string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);

                    var message = DataService.GenerateConfirmMailMessage(model.FirstName, callbackUrl);

                    await UserManager.SendEmailAsync(user.Id, message.Subject, message.Body);

                    //await UserManager.SendEmailAsync(user.Id, "Подтверждение учетной записи в интернет-магазине Krasivo.pro", "Подтвердите вашу учетную запись, щелкнув <a href=\"" + callbackUrl + "\">здесь</a>");

                    //var mailerParams = DataService.GetMailRobotParams();
                    //DataService.SendMail(mailerParams, user.Email, "Подтверждение учетной записи в интернет-магазине Krasivo.pro", "Подтвердите вашу учетную запись, щелкнув <a href=\"" + callbackUrl + "\">здесь</a>");
                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                    return RedirectToAction("PleaseConfirmMail", "Account");
                    //return RedirectToAction("Index", "Home");
                }
                AddErrors(result);
            }

            // Появление этого сообщения означает наличие ошибки; повторное отображение формы
            return View(model);
        }

        #endregion

        #region confirm email
        [AllowAnonymous]
        public ActionResult ResendConfirmationEmail(string id)
        {
            //алгоритм следующий: пришёл к нам id
            //мы ищем пользователя
            //не нашли? -> возвращаем вьюху, где говорим - сорян
            //нашли, смотрим, email действительно неподтверждён?
            //подтверждён? -> редиректим на глагне
            //теперь генерируем токен, шлём на почту
            var user = UserManager.FindById(id);

            if (user == null)
            {
                DataService.ReportAboutCrash("Кто-то попытался перевыслать подтверждение email для несуществующего пользователя", id); 
                return View("UserNotFound");
            }
            if (user.EmailConfirmed)
                return RedirectToAction("Index", "Home");

            string code = UserManager.GenerateEmailConfirmationToken(user.Id);
            var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);

            var message = DataService.GenerateConfirmMailMessage(user.FirstName, callbackUrl);

            //UserManager.SendEmailAsync(user.Id, message.Subject, message.Body);
            DataService.SendMail(DataService.GetMailRobotParams(), user.Email, message.Subject, message.Body, null, true);

            return RedirectToAction("PleaseConfirmMail", "Account");
        }
        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }

            var user = await UserManager.FindByIdAsync(userId);

            if (user != null)
            {
                if (user.EmailConfirmed == true)
                {
                    if (!Request.IsAuthenticated)
                        return View("ConfirmEmail");
                    else
                        return RedirectToAction("Index", "Home");
                }
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [Authorize]
        public ActionResult SorryMailUnconfirmed()
        {
            //TODO: эту вьюху нужно показывать только зарегистрированным пользователям
            //у которых неподтверждён email
            //если у пользователя подтверждён email - перенаправлять на Action ConfirmEmail
            //если email не подтверждён, нужна кнопка для повторной отправки кода

            return View();
        }

        [AllowAnonymous]
        public ActionResult PleaseConfirmMail()
        {
            //если не залогинен - отправляем на логин
            if (Request.IsAuthenticated)
            {
                RedirectToAction("Index", "Home");
            }
            //эта вьюха используется для повторной отправки кода
            return View();
        }

        #endregion

        #region password
        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null)// || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Не показывать, что пользователь не существует или не подтвержден
                    return View("ForgotPasswordConfirmation");
                }

                //Дополнительные сведения о том, как включить подтверждение учетной записи и сброс пароля, см. по адресу: http://go.microsoft.com/fwlink/?LinkID=320771
                //Отправка сообщения электронной почты с этой ссылкой

                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);

                var message = DataService.GenerateForgotMailMessage(user.FirstName, user.Email, callbackUrl);

                await UserManager.SendEmailAsync(user.Id, message.Subject, message.Body);
                //await UserManager.SendEmailAsync(user.Id, "Сброс пароля в интернет-магазине Krasivo.pro", "Сбросьте ваш пароль, щелкнув <a href=\"" + callbackUrl + "\">здесь</a>");
                //выйдем из-под учётки
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie); 
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // Появление этого сообщения означает наличие ошибки; повторное отображение формы
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Не показывать, что пользователь не существует
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }
        #endregion

        #region send code
        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Создание и отправка маркера
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        #endregion
        
        #region admin

        [Authorize(Roles = "Admin")]
        public ActionResult List()
        {
            var model = new List<IshopUserViewModel>();
            try
            {
                model = DataService.GetUserList();
            }
            catch
            {

            }
            return View(model);
        }

        public void MakeAdmin(string id)
        {
            var s = UserManager.FindById(id);
            UserManager.AddToRole(s.Id, "Admin");
        }

        #endregion

        #region Вспомогательные приложения

        [Authorize]
        public ContentResult GetUserName()
        {
            ApplicationUser user = UserManager.FindByEmail(User.Identity.Name);
            string name = user.FirstName.ToString();
            return Content(name);
        }

        // Используется для защиты от XSRF-атак при добавлении внешних имен входа
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                //context.RequestContext.HttpContext.Response.SuppressFormsAuthenticationRedirect = true;

                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

    }
}