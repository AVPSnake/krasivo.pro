﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Classes;

namespace WebApplication1.Controllers
{
    public class MagazineController : Controller
    {
        // GET: Journal
        public ActionResult Index()
        {
            var strUrl = "//krasivo.pro/"; // DataService.GetUrl(Request);

            var model = DataService.GetMagazineCatalog(strUrl);

            ViewBag.Title = "Гид по профессиональной косметике";
            ViewBag.Description = "Архив журнала Индустрия красоты";
            ViewBag.Keywords = "Интернет-магазин Индустрия красоты. Профессиональная косметика. Интернет-магазин профессиональной косметики, купить профессиональную косметику в Ростове, журнал Индустрия красоты, гид по профессиональной косметике, красиво про, krasivo.pro, красиво.про";

            return View(model);
        }
    }
}