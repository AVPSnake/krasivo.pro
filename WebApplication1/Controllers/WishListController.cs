﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Classes;

namespace WebApplication1.Controllers
{
    public class WishListController : Controller
    {
        private ApplicationUserManager _userManager;
        
        public WishListController()
        {

        }
        public WishListController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: WishList
        public ActionResult Index()
        {
            string userId;
            if (Request.IsAuthenticated)
                userId = User.Identity.GetUserId();
            else
                userId = Request.AnonymousID;

            var strUrl = DataService.GetUrl(Request);
            var models = DataService.GetWishListItems(userId, strUrl);

            ViewBag.Title = "Список желаний";
            return View(models);
        }

        //добавить в список желаний
        public ContentResult AddToWishList(string itemId)
        {
            bool ret = false;
            try
            {
                string userId;
                if (Request.IsAuthenticated)
                    userId = User.Identity.GetUserId();
                else
                    userId = Request.AnonymousID;

                ret = DataService.AddToWishlist(userId, itemId);
            }
            catch (Exception ex)
            {
                ret = false;
                string error = ex.Message;
            }
            if (!ret)
                return Content("Ошибка добавления в список желаний");

            return Content("Товар добавлен в список желаний.");
        }

        //Перенос из списка желаний в корзину
        public ContentResult MoveToBasket(string itemId, double price = 0)
        {
            bool ret = false;
            try
            {
                string userId;
                if (Request.IsAuthenticated)
                    userId = User.Identity.GetUserId();
                else
                    userId = Request.AnonymousID;

                ret = DataService.MoveToBasket(userId, itemId, 1);

            }
            catch (Exception ex)
            {
                ret = false;
                string error = ex.Message;
            }
            if (!ret)
                return Content("Ошибка перемещения в корзину");

            return Content("Товар перемещён в корзину");
        }
        //Удаление из списка желаний: 
        public ContentResult RemoveFromWishList(string itemId, double price = 0)
        {
            bool ret = false;
            try
            {
                string userId;
                if (Request.IsAuthenticated)
                    userId = User.Identity.GetUserId();
                else
                    userId = Request.AnonymousID;

                ret = DataService.RemoveFromWishList(userId, itemId);

            }
            catch (Exception ex)
            {
                ret = false;
                string error = ex.Message;
            }
            if (!ret)
                return Content("Ошибка удаления товара из списка желаний");

            return Content("Товар удален из списка желаний");
        }
    }
}