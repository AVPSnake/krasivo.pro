﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Classes;
using WebApplication1.Models.ViewModels;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationUserManager _userManager;
        public HomeController()
        {

        }

        public HomeController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        const int itemCnt2Display = 10;
        const int actionCnt2Display = 3;

        public ActionResult Index()
        {
            var home = new HomeViewModel();
            try
            {
                string userId;

                if (Request.IsAuthenticated)
                    userId = User.Identity.GetUserId();
                else
                    userId = Request.AnonymousID;

                List<string> selectedItems = new List<string>();
                List<string> hitItems = new List<string>();

                var strUrl = "https://krasivo.pro/"; //DataService.GetUrl(Request);
                //
                hitItems = DataService.GetHitItems(strUrl, itemCnt2Display);

                if (hitItems.Count() > 0)
                    home.InventTable = DataService.GetCatalogByItemList(hitItems, true, strUrl, itemCnt2Display, true, userId, 0, true, true);
                else
                    home.InventTable = DataService.GetCatalogByItemList(selectedItems, false, strUrl, itemCnt2Display, true, userId, 0, false, true);

                home.RecommendedItems = DataService.GetRecommendedItems(home.InventTable, strUrl, itemCnt2Display, userId);

                home.Brands = DataService.GetBrandsCatalog(strUrl);

                home.Banners = DataService.GetBannerCatalog4HomePage(strUrl);
                //home.Actions = DataService.GetActionsCatalog4HomePage(strUrl, 1, actionCnt2Display);

                ViewBag.Title = string.Format("Интернет-магазин Krasivo.pro");
                ViewBag.Description = "Интернет-магазин Индустрия красоты. Профессиональная косметика Elgon, Tigi, Aravia, Wella, Inebrya, Selective, Barex, техника Parlux, Dewal, Tayo, Babyliss и многое другое!";
                ViewBag.Keywords = "Интернет-магазин Индустрия красоты. Профессиональная косметика. Интернет-магазин профессиональной косметики, купить профессиональную косметику в Ростове, купить профессиональную косметику, krasivo.pro, красиво.про, интернет-магазин Индустрия красоты Ростов, красиво про, купить Elgon в Ростове, магазин Elgon в Ростове, магазин Inebrya в Ростове, интернет-магазин Elgon, Moser, интернет-магазин Moser, купить Moser в Ростове, купить Moser, доставка, журнал Индустрия красоты, гид по профессиональной косметике, Wella, selective, barex, купить selective в Ростове-на-Дону, купить Barex в Ростове-на-Дону, купить Selective, купить Barex, красиво про";

            }
            catch (Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка загрузки главной страницы", ex.Message);
                //отправлять сообщение
            }
            return View(home);
        }

        public ActionResult License()
        {
            ViewBag.Title = string.Format("Публичная оферта");
            ViewBag.Description = "Интернет-магазин Индустрия красоты. Профессиональная косметика Elgon, Tigi, Aravia, Wella, Inebrya, Selective, Barex, техника Parlux, Dewal, Tayo, Babyliss и многое другое!";
            ViewBag.Keywords = "Интернет-магазин Индустрия красоты. Профессиональная косметика. Интернет-магазин профессиональной косметики, купить профессиональную косметику в Ростове, купить профессиональную косметику, krasivo.pro, красиво.про, интернет-магазин Индустрия красоты Ростов, красиво про, купить Elgon в Ростове, магазин Elgon в Ростове, магазин Inebrya в Ростове, интернет-магазин Elgon, Moser, интернет-магазин Moser, купить Moser в Ростове, купить Moser, доставка, журнал Индустрия красоты, гид по профессиональной косметике, Wella, selective, barex, купить selective в Ростове-на-Дону, купить Barex в Ростове-на-Дону, купить Selective, купить Barex, красиво про";

            return View();
        }

        public ActionResult Delivery()
        {
            ViewBag.Title = string.Format("Доставка заказов");
            ViewBag.Description = "Интернет-магазин Индустрия красоты. Профессиональная косметика Elgon, Tigi, Aravia, Wella, Inebrya, Selective, Barex, техника Parlux, Dewal, Tayo, Babyliss и многое другое!";
            ViewBag.Keywords = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";

            return View();
        }

        public ActionResult FreeDelivery()
        {
            ViewBag.Title = string.Format("Акция недели!");
            ViewBag.Description = "Интернет-магазин Индустрия красоты. Профессиональная косметика Elgon, Tigi, Aravia, Wella, Inebrya, Selective, Barex, техника Parlux, Dewal, Tayo, Babyliss и многое другое!";
            ViewBag.Keywords = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";

            return View();
        }


        public ActionResult PaymentTerms()
        {
            ViewBag.Title = string.Format("Оплата заказов");
            ViewBag.Description = "Интернет-магазин Индустрия красоты. Профессиональная косметика Elgon, Tigi, Aravia, Wella, Inebrya, Selective, Barex, техника Parlux, Dewal, Tayo, Babyliss и многое другое!";
            ViewBag.Keywords = "Интернет-магазин Индустрия красоты. Профессиональная косметика. Интернет-магазин профессиональной косметики, купить профессиональную косметику в Ростове, купить профессиональную косметику, krasivo.pro, красиво.про, интернет-магазин Индустрия красоты Ростов, красиво про, купить Elgon в Ростове, магазин Elgon в Ростове, магазин Inebrya в Ростове, интернет-магазин Elgon, Moser, интернет-магазин Moser, купить Moser в Ростове, купить Moser, доставка, журнал Индустрия красоты, гид по профессиональной косметике, Wella, selective, barex, купить selective в Ростове-на-Дону, купить Barex в Ростове-на-Дону, купить Selective, купить Barex, красиво про";

            return View();
        }
        public ActionResult Contacts()
        {
            ViewBag.Title = string.Format("Контакты");
            ViewBag.Description = "Интернет-магазин Индустрия красоты. Профессиональная косметика Elgon, Tigi, Aravia, Wella, Inebrya, Selective, Barex, техника Parlux, Dewal, Tayo, Babyliss и многое другое!";
            ViewBag.Keywords = "Интернет-магазин Индустрия красоты. Профессиональная косметика. Интернет-магазин профессиональной косметики, купить профессиональную косметику в Ростове, купить профессиональную косметику, krasivo.pro, красиво.про, интернет-магазин Индустрия красоты Ростов, красиво про, купить Elgon в Ростове, магазин Elgon в Ростове, магазин Inebrya в Ростове, интернет-магазин Elgon, Moser, интернет-магазин Moser, купить Moser в Ростове, купить Moser, доставка, журнал Индустрия красоты, гид по профессиональной косметике, Wella, selective, barex, купить selective в Ростове-на-Дону, купить Barex в Ростове-на-Дону, купить Selective, купить Barex, красиво про";

            return View();
        }

        public ActionResult Shops()
        {
            ViewBag.Title = string.Format("Адреса магазинов");
            ViewBag.Description = "Интернет-магазин Индустрия красоты. Профессиональная косметика Elgon, Tigi, Aravia, Wella, Inebrya, Selective, Barex, техника Parlux, Dewal, Tayo, Babyliss и многое другое!";
            ViewBag.Keywords = "Интернет-магазин Индустрия красоты. Профессиональная косметика. Интернет-магазин профессиональной косметики, купить профессиональную косметику в Ростове, купить профессиональную косметику, krasivo.pro, красиво.про, интернет-магазин Индустрия красоты Ростов, красиво про, купить Elgon в Ростове, магазин Elgon в Ростове, магазин Inebrya в Ростове, интернет-магазин Elgon, Moser, интернет-магазин Moser, купить Moser в Ростове, купить Moser, доставка, журнал Индустрия красоты, гид по профессиональной косметике, Wella, selective, barex, купить selective в Ростове-на-Дону, купить Barex в Ростове-на-Дону, купить Selective, купить Barex, красиво про";

            return View();
        }


        //Отправка сообщений от пользователя
        public ContentResult CreateCustomerFeedback(int messageType, string name, string message = "", string phoneNumber = "", string email = "")
        { 
            var res = DataService.CreateCustomerFeedback(messageType, name, phoneNumber, email, message);

            if (res)
                return Content("Сообщение отправлено");
            else
                return Content("Ошибка отправки сообщения");
        }
    }
}