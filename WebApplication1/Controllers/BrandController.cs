﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Classes;
using WebApplication1.Models.ViewModels;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace WebApplication1.Controllers
{
    public class BrandController : Controller
    {
        private ApplicationUserManager _userManager;
        public BrandController()
        {

        }

        public BrandController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Brand
        public ActionResult Index()
        {
            var model = new List<BrandViewModel>();
            try
            {
                var strUrl = "//krasivo.pro/";// DataService.GetUrl(Request);
                model = DataService.GetBrandsCatalog(strUrl);
                ViewBag.Title = "Бренды в интернет-магазине Индустрия Красоты";
                ViewBag.Keywords = "Интернет-магазин Индустрия красоты, Krasivo.pro, красиво.про, красиво про, профессиональная косметика известных брендов. Купить Elgon, купить elgon в Ростове-на-Дону, купить Tigi, купить tigi в Ростове-на-Дону, купить Moser, купить Moser в Ростове-на-Дону, купить selective, купить selective в ростове-на-дону, купить Barex, купить Barex в Ростове-на-Дону, barex, elgon, selective, inebrya, elgon, moser, parlux, dewal, интернет-магазин";
                ViewBag.Description = "Интернет-магазин Индустрия красоты. Профессиональная косметика Elgon, Tigi, Aravia, Wella, Inebrya, Selective, Barex, техника Parlux, Dewal, Tayo, Babyliss и многое другое!";
            }
            catch
            {
                model = new List<BrandViewModel>();
            }
            return View(model);
        }

        public ActionResult Details(string Id)
        {
            //id здесь slug на самом деле, чтобы было красивенько
            var model = new BrandViewModel();

            try
            {
                string userId;
                if (Request.IsAuthenticated)
                    userId = User.Identity.GetUserId();
                else
                    userId = Request.AnonymousID;

                var strUrl = "//krasivo.pro/"; //DataService.GetUrl(Request);
                model = DataService.GetBrandInfoBySlug(Id, strUrl);
                //
                if (!string.IsNullOrWhiteSpace(model.FilterString))
                { 
                    var parsedFilterString = new List<QualityGoodItem>();
                    var SortNum2Start = 0;
                    var forceSelectedItems = true;
                    var selectedItems = new List<string>();
                    var actionId = string.Empty;
                    var orderId = string.Empty;

                    if (!String.IsNullOrEmpty(model.FilterString))
                    {
                        parsedFilterString = DataService.ParseFilterString(model.FilterString, out SortNum2Start, out actionId, out orderId);
                        selectedItems = DataService.GetItemsList(parsedFilterString);
                    }

                    //filterList = DataService.GetFilterList(parsedFilterString); //вот сюда помеченные надо добавлять

                    model.FeaturedItems= DataService.GetCatalogByItemList(selectedItems, forceSelectedItems, strUrl, 20, true, userId, 0, true);
                }

                ViewBag.Title = model.BrandName;
                ViewBag.Description = DataService.StripHTML(model.BrandDescription);
                ViewBag.Keywords = model.BrandName;

            }
            catch
            {
                RedirectToAction("Index");
            }            
            return View(model);
        }
    }
}