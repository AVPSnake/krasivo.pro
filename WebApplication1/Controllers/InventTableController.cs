﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Data.SqlClient;
using WebApplication1.Models;
using WebApplication1.Classes;
using WebApplication1.Models.ViewModels;
using System.Data;
using System.IO;
using System.Net.Mime;
using Microsoft.AspNet.Identity;

namespace WebApplication1.Controllers
{
    public class InventTableController : Controller
    {
        public int SortNum2Start = 0;
        public int itemCnt2Display = 30;

        List<InventTableModel> itemsList = new List<InventTableModel>();
        List<FilterTreeModel> filterList = new List<FilterTreeModel>();
        CatalogViewModel catalog = new CatalogViewModel();

        #region AJAX-запросы

        // GET: InventTable
        //такая штука отключает кэш [OutputCache(NoStore = true, Location = System.Web.UI.OutputCacheLocation.None)] 
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index(/*string currentFilter, string searchString =""*/)
        {
            try
            {
                string userId;

                if (Request.IsAuthenticated)
                    userId = User.Identity.GetUserId();
                else
                    userId = Request.AnonymousID;

                bool forceSelectedItems = false;
                List<string> selectedItems = new List<string>();
                List<QualityGoodItem> parsedFilterString = new List<QualityGoodItem>();
                var strUrl = DataService.GetUrl(Request);

                string rawFilter = DataService.ParseUrlParms(Request);
                var actionId = string.Empty;
                var orderId = string.Empty;

                //распарсили фильтр

                if (!String.IsNullOrEmpty(rawFilter))
                {
                    parsedFilterString = DataService.ParseFilterString(rawFilter, out SortNum2Start, out actionId, out orderId);
                }

                bool takeFromSession = false;
                var itemCntMultiplier = 1;

                try
                {

                    if (Session["parsedFilterString"] != null && Session["filterList"]!=null && Session["selectedItems"] != null && Session["partialCnt"] != null) //если хоть один равен нулл, все, надо тянуть из базы
                    {
                        var parsedFilterStringSession = (List<QualityGoodItem>)Session["parsedFilterString"];

                        if (DataService.CompareLists(parsedFilterString, parsedFilterStringSession))
                        {
                            //набор фильтров совпал, вытягиваем список номенклатур
                            selectedItems = (List<string>)Session["selectedItems"];
                            filterList = (List<FilterTreeModel>)Session["filterList"];
                            itemCntMultiplier = (int)Session["partialCnt"];
                            takeFromSession = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    DataService.ReportAboutCrash("Ошибка получения данных для каталога из сессии", ex.Message);
                    takeFromSession = false;
                }

                if (!takeFromSession)
                {

                    //тянем из базы
                    filterList = DataService.GetFilterList(parsedFilterString); //вот сюда помеченные надо добавлять

                    if (!String.IsNullOrEmpty(rawFilter))
                    {
                        selectedItems = DataService.GetItemsList(parsedFilterString);
                    }

                    //кладём в сессию
                    Session["parsedFilterString"] = parsedFilterString;
                    Session["selectedItems"] = selectedItems;
                    Session["filterList"] = filterList;
                    Session["partialCnt"] = itemCntMultiplier;
                }
                else
                {

                }

                if (parsedFilterString.Any())
                    forceSelectedItems = true;


                itemsList = DataService.GetCatalogByItemList(selectedItems, forceSelectedItems, "https://krasivo.pro/", itemCnt2Display * itemCntMultiplier, true, userId);

            }
            catch (Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка вывода в каталог!!", ex.Message);
                itemsList = new List<InventTableModel>();
                filterList = new List<FilterTreeModel>();
            }

            finally
            {
                catalog.inventTable = itemsList;
                catalog.Filter = filterList;
            }

            ViewBag.Title = "Каталог товаров";
            ViewBag.Description = "Интернет магазин Индустрия красоты. Krasivo.pro - профессиональная косметика известных брендов";
            ViewBag.Keywords = "Интернет-магазин Индустрия красоты. Профессиональная косметика. Интернет-магазин профессиональной косметики, купить профессиональную косметику в Ростове, купить профессиональную косметику, krasivo.pro, красиво.про, интернет-магазин Индустрия красоты Ростов, красиво про, купить Elgon в Ростове, магазин Elgon в Ростове, магазин Inebrya в Ростове, интернет-магазин Elgon, Moser, интернет-магазин Moser, купить Moser в Ростове, купить Moser, доставка, журнал Индустрия красоты, гид по профессиональной косметике, Wella, selective, barex, купить selective в Ростове-на-Дону, купить Barex в Ростове-на-Дону, купить Selective, купить Barex, красиво про";            

            return View(catalog);
        }

        public ActionResult GetItemsPartial(/*string searchString = ""*/)
        {
            string userId;

            if (Request.IsAuthenticated)
                userId = User.Identity.GetUserId();
            else
                userId = Request.AnonymousID;


            bool forceSelectedItems = false;

            var strUrl = DataService.GetUrl(Request);

            List<string> selectedItems = new List<string>();

            List<QualityGoodItem> parsedFilterString = new List<QualityGoodItem>();

            string actionId = string.Empty;
            string orderId = string.Empty;

            parsedFilterString = DataService.ParseFilterString(DataService.ParseUrlParms(Request), out SortNum2Start, out actionId, out orderId);

            bool takeFromSession = false;

            try
            {
                if (Session["parsedFilterString"] != null && Session["filterList"] != null && Session["selectedItems"] != null) //если хоть один равен нулл, все, надо тянуть из базы
                {
                    var parsedFilterStringSession = (List<QualityGoodItem>)Session["parsedFilterString"];

                    if (DataService.CompareLists(parsedFilterString, parsedFilterStringSession))
                    {
                        //набор фильтров совпал, вытягиваем список номенклатур
                        selectedItems = (List<string>)Session["selectedItems"];
                        takeFromSession = true;

                        if (Session["partialCnt"] != null)
                        {
                            Session["partialCnt"] = (int)Session["partialCnt"] + 1;
                        }
                        else
                        {                            
                            Session["partialCnt"] = 1;
                        }
                    }
                    else
                    {
                        Session["partialCnt"] = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка получения данных для каталога из сессии", ex.Message);
                takeFromSession = false;
            }

            if (!takeFromSession)
            {
                selectedItems = DataService.GetItemsList(parsedFilterString);

                //кладём в сессию
                Session["parsedFilterString"] = parsedFilterString;
                Session["selectedItems"] = selectedItems;
                Session["partialCnt"] = 1;
            }

            if (parsedFilterString.Any())
                forceSelectedItems = true;

            itemsList = DataService.GetCatalogByItemList(selectedItems, forceSelectedItems, strUrl, itemCnt2Display, true, userId, SortNum2Start);

            #region OldVersion
            /*
            bool forceSelectedItems = false;

            var strUrl = DataService.GetUrl(Request);

            List<string> selectedItems = new List<string>();

            List<QualityGoodItem> parsedFilterString = new List<QualityGoodItem>();

            parsedFilterString = DataService.ParseFilterString(DataService.ParseUrlParms(Request), out SortNum2Start);



            selectedItems = DataService.GetItemsList(parsedFilterString);

            if (parsedFilterString.Any())
                forceSelectedItems = true;

            itemsList = DataService.GetCatalogByItemList(selectedItems, forceSelectedItems, strUrl, itemCnt2Display, true, SortNum2Start);
            */
            #endregion OldVersion
            return PartialView("ItemsPartial", itemsList);
        }

        public ActionResult Search(string search_string)
        {
            //вот тт парсим строку поиска и превращаем её в строку фильтар
            var strUrl = DataService.GetUrl(Request);

            search_string = "%" + search_string.Replace(" ", "%") + "%";

            //var filterString = DataService.GetFilterStringBySearch(search_string);

            //return Redirect(strUrl+"/catalog?" + filterString);           
            return RedirectToAction("Index", new { searchString = search_string } );
            
        }

        public ContentResult GetFilteredItemsQty()
        {
            var actionId = string.Empty;
            var orderId = string.Empty;

            int i = DataService.GetItemsCount(DataService.ParseFilterString(DataService.ParseUrlParms(Request), out SortNum2Start, out actionId, out orderId));

            return Content(i.ToString());
        }

        public ContentResult GetNewFilter()
        {
            string ret = String.Empty;

            List<QualityGoodItem> parsedFilterString = new List<QualityGoodItem>();

            var actionId = string.Empty;
            var orderId = string.Empty;

            parsedFilterString = DataService.ParseFilterString(DataService.ParseUrlParms(Request), out SortNum2Start, out actionId, out orderId);

            filterList = DataService.GetFilterList(parsedFilterString);

            foreach (var filter in filterList)
            {
                foreach (var property in filter.TreeNodeModel)
                {
                    if (property.Enabled)
                        ret += filter.Id + "=" + property.Id + ',';
                }
            }
            return Content(ret);
        }

        #endregion

        #region images
        public ActionResult GetImagePath(string filePath)
        {
            var extension = Path.GetExtension(filePath);

            var s = File(filePath, MimeTypes.GetMimeType(extension), Path.GetFileName(filePath));

            return File(filePath, MimeTypes.GetMimeType(extension), Path.GetFileName(filePath));
        }

        // Карточка товара
        public ActionResult Details(string Id) //параметр называется id, на самом деле это urlSlug
        {
            string userId = string.Empty;

            if (Request.IsAuthenticated)
                userId = User.Identity.GetUserId();
            else
                userId = Request.AnonymousID;

            InventTableModel model = null;

            ItemInfo ii = new ItemInfo(Id, ItemInitType.UrlSlug, userId);
            if (!ii.OK)
                throw new HttpException(404, "Товар не существует");

            try
            {

                var strUrl = "//krasivo.pro/"; //DataService.GetUrl(Request);

                //var physicalApplicationPath = Request.PhysicalApplicationPath;
                var defaultImgPath = new string[] { strUrl + "pic/product/default.png", strUrl + "pic/product/default.png" };

                //надо вытащить картинки

                var properies = DataService.GetItemProperties(ii.ItemId);

                model = new InventTableModel(){
                                                ItemId = ii.ItemId,
                                                ItemName = ii.Name,
                                                Price = ii.Price,
                                                AvailQty = ii.RemainQty - ii.ReservQty,
                                                Article = ii.Article,
                                                Description = ii.Description,
                                                ShortDescription = ii.ShortDescription,
                                                UrlSlug = ii.UrlSlug,
                                                Properties = properies,
                                                OldPrice = ii.OldPrice,
                                                PriceExpDate = ii.PriceExpDate

                };

                if (model.OldPrice > model.Price)
                    model.isAction = true;

                DataService.GetItemImages(model, strUrl);

                if (model.MainImagePath == null || string.IsNullOrEmpty(model.MainImagePath[1]))
                {
                    model.AllImages.Remove(model.MainImagePath);
                    model.MainImagePath = defaultImgPath;
                    model.AllImages.Add(model.MainImagePath);
                }

                var relatedItemList = DataService.GetRelatedItems(ii.ItemId, strUrl, 5);

                model.RelatedItems = DataService.GetCatalogByItemList(relatedItemList, true, strUrl, 5, true, userId, 0, true, true);

                ViewBag.Title = model.ItemName;
                ViewBag.Description = DataService.StripHTML(model.Description);
                ViewBag.Keywords = model.ItemName+", "+model.ShortDescription + ", " +model.Article + ", купить" +model.ItemName;

            }
            catch (Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка получения данных о номенклатуре "+Id, ex.Message);                
            }
            return View(model);
        }



        #endregion
        //[NonAction]
        //private List<QualityGoodItem> ParseFilterString(string filterString)
        //{
        //    List<QualityGoodItem> ret = new List<QualityGoodItem>();

        //    try
        //    {
        //        filterString = HttpUtility.UrlDecode(filterString, System.Text.Encoding.UTF8);

        //        string[] filters = filterString.Split(new string[] { "&" }, StringSplitOptions.RemoveEmptyEntries);

        //        foreach (string element in filters)
        //        {
        //            string[] item = element.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);

        //            if (item.Length != 2)
        //                continue;

        //            if (item[0] == "num2start")
        //            {
        //                string n = item[1] == "" ? "0" : item[1];

        //                int.TryParse(n, out SortNum2Start);

        //                continue;
        //            }

        //            int value = 0;

        //            Int32.TryParse(item[1], out value);

        //            if (value == 0)
        //                continue;

        //            ret.Add(new QualityGoodItem() { Id = item[0], Value = value });
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //TODO Redirect
        //    }
        //    return ret;
        //}

        //[NonAction]
        //private string ParseUrlParms()
        //{
        //    string url = Request.Url.ToString();

        //    string[] p = url.Split('?');

        //    if (p.Length >= 2)
        //        return p[1];

        //    return "";
        //}

        //[NonAction]
        //private string GetUrl()
        //{
        //    var strUrl = string.Empty;
        //    try
        //    { 
        //        var strPathAndQuery = Request.Url.PathAndQuery;
        //        var absolute = Request.Url.AbsoluteUri;
        //        strUrl = absolute.Replace(strPathAndQuery, Request.ApplicationPath);
        //        strUrl += "/";
        //    }
        //    catch
        //    {
        //        strUrl = string.Empty;
        //    }
        //    return strUrl;
        //}

    }
}
