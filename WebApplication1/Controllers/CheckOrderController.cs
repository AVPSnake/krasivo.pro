﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Classes;

namespace WebApplication1.Controllers
{
    public class CheckOrderController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        public CheckOrderController()
        {

        }

        public CheckOrderController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: CheckOrder
        public ActionResult Index(string orderId = "")
        {
            try
            {
                if (string.IsNullOrEmpty(orderId))
                    RedirectToAction("Index", "Home");

                string paymServAlias = string.Empty;
                //у нас есть orderId
                //делаем запрос
                /*
                var answer = DataService.GetOrderStatus(orderId);
                //если всё хорошо, тут делаем проверку
                //присылаем сообщение клиенту: спасибо говорим            
                
                switch (answer.OrderStatus)
                {
                    case 2:
                        {
                            paymServAlias = answer.OrderNumber;
                            break;
                        }
                    default:
                        {
                            string errMessage = string.Empty;
                            if (!string.IsNullOrEmpty(answer.errorMessage))
                                errMessage = answer.errorMessage;
                            else
                                errMessage = "Текста нет";

                            DataService.ReportAboutCrash("Сбербанк прислал странный ответ", orderId + " " + errMessage.ToString());
                            //не получилось
                            return RedirectToAction("Index", "Home");
                        }
                }
                */
                var check = DataService.CheckIsOrderPayed(orderId, out paymServAlias);

                if (check)
                {
                    var saleInfo = SaleInfo.InitByPaymAlias(paymServAlias);

                    if (saleInfo.Status == SaleStatus.Aside) // SaleStatus.Aside) Mike 2017-08-10
                    {
                        var posted = DataService.PostSalesOrder(saleInfo);

                        if (!posted)
                            DataService.ReportAboutCrash("Ошибка разноски заказа", orderId + " " + saleInfo.SalesId);
                        else
                        {
                            var user = UserManager.FindById(saleInfo.AspId);

                            //DataService.SendMail(DataService.GetMailRobotParams(), user.Email, "Заказ " + saleInfo.SalesId + " в интернет - магазине krasivo.pro готов к оплате", "Здравствуйте, "+user.FirstName+ "! \r\n Ваш заказ " + saleInfo.SalesId + " в интернет - магазине krasivo.pro успешно собран.Вы можете оплатить его на сайте");                
                            var mailRobotParams = DataService.GetMailRobotParams();
                            var paymentMessage = DataService.GeneratePaymentMail(saleInfo, user);
                            DataService.SendMail(mailRobotParams, user.Email, paymentMessage.Subject, paymentMessage.Body, null, true);

                            var paymentMessageSupport = DataService.GeneratePaymentMailSupport(saleInfo, user, mailRobotParams.AdminConsole);
                            DataService.SendMail(mailRobotParams, mailRobotParams.Pickup, paymentMessageSupport.Subject, paymentMessageSupport.Body, null, true);

                            DataService.SendSingleSMS(user.PhoneNumber.Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty), "Поступила оплата по заказу " + saleInfo.SalesId + " в интернет-магазине krasivo.pro. Спасибо!");

                            if (saleInfo.DeliveryTypeId == DeliveryTypeStatic.SelfDeliveryTypeId && !string.IsNullOrWhiteSpace(saleInfo.SelfDeliveryPointMail))
                            {
                                var shopMessage = DataService.GeneratePaymentMailShop(saleInfo);
                                DataService.SendMail(mailRobotParams, saleInfo.SelfDeliveryPointMail, shopMessage.Subject, shopMessage.Body, null, true);
                            }

                        }
                    }
                }
                //если залогинен - кидаем его в мои заказы
                return RedirectToAction("OrderList", "Cabinet");
            }
            catch (Exception ex)
            {
                DataService.ReportAboutCrash("Ошибка проверки статуса", ex.Message);
            }
            return View();
        }
    }
}