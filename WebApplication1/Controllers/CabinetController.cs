﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Classes;
using WebApplication1.Models;
using WebApplication1.Models.ViewModels;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class CabinetController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public CabinetController()
        {

        }

        public CabinetController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Cabinet
        [Authorize]
        public ActionResult Index(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Ваш пароль изменен."
                : message == ManageMessageId.SetPasswordSuccess ? "Пароль задан."
                : message == ManageMessageId.SetTwoFactorSuccess ? "Настроен поставщик двухфакторной проверки подлинности."
                : message == ManageMessageId.Error ? "Произошла ошибка."
                : message == ManageMessageId.AddPhoneSuccess ? "Ваш номер телефона добавлен."
                : message == ManageMessageId.RemovePhoneSuccess ? "Ваш номер телефона удален."
                : "";

            var userId = User.Identity.GetUserId();
            var model = new PersonalInfoViewModel();

            ApplicationUser user = UserManager.FindByEmail(User.Identity.Name);

            if (!string.IsNullOrWhiteSpace(user.FirstName))
                model.FirstName = user.FirstName.ToString();
            else
                model.FirstName = string.Empty;
            if (!string.IsNullOrWhiteSpace(user.LastName))
                model.LastName = user.LastName.ToString();
            else
                model.LastName = string.Empty;
            if (!string.IsNullOrWhiteSpace(user.Email))
                model.Email = user.Email.ToString();
            else
                model.Email = string.Empty;
            if (!string.IsNullOrWhiteSpace(user.PhoneNumber))
                model.PhoneNumber = user.PhoneNumber.ToString();
            else
                model.PhoneNumber = string.Empty;

            model.AgreeToReceivePromoInfo = user.AgreeToReceivePromoInfo;

            ViewBag.Title = "Krasivo.pro - Личный кабинет";
            ViewBag.Description = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";
            ViewBag.Keywords = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";

            return View(model);
        }

        [Authorize]
        public ActionResult EditProfile()
        {
            var userId = User.Identity.GetUserId();
            var model = new PersonalInfoViewModel();

            ApplicationUser user = UserManager.FindByEmail(User.Identity.Name);

            if (!string.IsNullOrWhiteSpace(user.FirstName))
                model.FirstName = user.FirstName.ToString();
            else
                model.FirstName = string.Empty;
            if (!string.IsNullOrWhiteSpace(user.LastName))
                model.LastName = user.LastName.ToString();
            else
                model.LastName = string.Empty;
            if (!string.IsNullOrWhiteSpace(user.PhoneNumber))
                model.PhoneNumber = user.PhoneNumber.ToString();
            else
                model.PhoneNumber = string.Empty;

            model.AgreeToReceivePromoInfo = user.AgreeToReceivePromoInfo;

            ViewBag.Title = "Krasivo.pro - Редактирование личных данных";
            ViewBag.Description = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";
            ViewBag.Keywords = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";

            return View(model);
        }
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfile(PersonalInfoViewModel PersonalInfoViewModel)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId();
                var currentUser = UserManager.FindById(userId);


                currentUser.FirstName = PersonalInfoViewModel.FirstName;
                currentUser.LastName = PersonalInfoViewModel.LastName;

                currentUser.AgreeToReceivePromoInfo = PersonalInfoViewModel.AgreeToReceivePromoInfo;

                if (currentUser.PhoneNumber != PersonalInfoViewModel.PhoneNumber)
                {
                    currentUser.PhoneNumber = PersonalInfoViewModel.PhoneNumber;
                    currentUser.PhoneNumberConfirmed = false;
                }

                var ret = UserManager.Update(currentUser);

                /*
                model.FirstName = user.FirstName.ToString();
                model.LastName = user.LastName.ToString();
                model.Email = user.Email.ToString();
                model.PhoneNumber = user.PhoneNumber.ToString();
                */

                ViewBag.Title = "Krasivo.pro - Личный кабинет";
                ViewBag.Description = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";
                ViewBag.Keywords = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";

            }
            else
            {
                ViewBag.Title = "Krasivo.pro - Редактирование данных";
                ViewBag.Description = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";
                ViewBag.Keywords = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";

                return View(PersonalInfoViewModel);
            }
            return RedirectToAction("Index");
        }

        // GET: /Manage/ChangePassword

        public ActionResult ChangePassword()
        {
            ViewBag.Title = "Krasivo.pro - смена пароля";
            return View();
        }

        public ActionResult PayOrder(string id)
        {
            var saleInfo = new SaleInfo(id);

            if (saleInfo.OK)
            {

                var userId = User.Identity.GetUserId();

                if (saleInfo.PaymentTypeId == PaymentTypeId.Cash)
                {
                    var order = new OrderViewModel();
                    order.OrderId = id;
                    //TODO редирект на страницу, где говорится, что заказ ещё не может быть оплачен
                    return View("YouCantPayCODOnline");
                }

                /*
                if (userId != saleInfo.AspId)
                {
                    DataService.ReportAboutCrash("Кто-то попытался оплатить чужой заказ", userId + " " + saleInfo.AspId);
                    return RedirectToAction("Index","Home");
                }
                */

                switch (saleInfo.Status)
                {
                    //case SaleStatus.Picked:
                    case SaleStatus.Aside:
                        {
                            //всё хорошо, едем дальше
                            break;
                        }
                    case SaleStatus.Edit:
                        {
                            var order = new OrderViewModel();
                            order.OrderId = id;
                            //TODO редирект на страницу, где говорится, что заказ ещё не может быть оплачен
                            return View("PleaseWait");
                        }
                    default:
                        {
                            return View("PaymentError");
                            //throw new HttpException(string.Format("Кажется, что-то пошло не так"));
                        }
                }

                //var sberbankParams = DataService.GetSberbankParams();
                //ApplicationUser user = UserManager.FindByEmail(User.Identity.Name);
                ApplicationUser user = UserManager.FindById(saleInfo.AspId);
                var email = user.Email.ToString();

                if (saleInfo.PaymLinkExpDatetime == null || saleInfo.PaymLinkExpDatetime == DateTime.MinValue) //точно не оплачивали
                {
                    saleInfo.PaymServAlias = saleInfo.SalesId;
                    //вызываем регистрацию заказа на сервере
                    var answer = DataService.RegisterOrder(saleInfo, email);
                    //
                    if (answer.errorCode == null)
                        answer.errorCode = 0;
                    switch (answer.errorCode)
                    {
                        case 0:
                            {
                                var paymLinkExpDatetime = DateTime.Now.AddMinutes(19.98);
                                var paymServerOrderId = answer.orderId;
                                var paymServLink = answer.formUrl;
                                var paymServAlias = saleInfo.SalesId;
                                DataService.SetSaleOrderPaymParams(saleInfo.SalesId, paymServLink, paymLinkExpDatetime, paymServAlias, paymServerOrderId);
                                return Redirect(paymServLink);
                            }
                        default:
                            {
                                //пишем в почту
                                DataService.ReportAboutCrash("Ошибка оплаты заказа " + saleInfo.SalesId, answer.errorMessage);
                                //редирект на страницу с ошибкой
                                return RedirectToAction("PaymentError");
                            }
                    }
                    //типа всё прошло норм
                }
                else
                {
                    var currentTime = DateTime.Now;
                    var delta = saleInfo.PaymLinkExpDatetime.Subtract(currentTime).TotalMinutes;
                    if (delta < 0)
                    {
                        //генерим префикс
                        saleInfo.PaymServAlias = saleInfo.SalesId + "|" + saleInfo.PaymAttempt.ToString();

                        var answer = DataService.RegisterOrder(saleInfo, email);
                        if (answer.errorCode == null)
                            answer.errorCode = 0;

                        //
                        switch (answer.errorCode)
                        {
                            case 0:
                                {
                                    var paymLinkExpDatetime = DateTime.Now.AddMinutes(19.98);
                                    var paymServLink = answer.formUrl;
                                    var paymServAlias = saleInfo.PaymServAlias;
                                    var paymServOrderId = answer.orderId;
                                    DataService.SetSaleOrderPaymParams(saleInfo.SalesId, paymServLink, paymLinkExpDatetime, paymServAlias, paymServOrderId);
                                    return Redirect(paymServLink);
                                }
                            default:
                                {
                                    DataService.ReportAboutCrash("Ошибка оплаты заказа " + saleInfo.SalesId, answer.errorMessage);
                                    //редирект на страницу с ошибкой
                                    return RedirectToAction("PaymentError");
                                }
                        }
                    }
                    else
                    {
                        return Redirect(saleInfo.PaymServLink);
                    }
                }
            }
            else
            {
                DataService.ReportAboutCrash("Ошибка оплаты заказа", string.Format("Заказ {0} не найден", id));
                return View("PaymentError");
            }
            ViewBag.Title = string.Format("Krasivo.pro - оплата заказа {0}", id);
            ViewBag.Description = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";
            ViewBag.Keywords = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";

            //если мы дошли каким-то чудом сюда, это явный косяк
            return View("PaymentError");
        }

        public ActionResult CheckOrderPayment(string id)
        {
            var saleInfo = new SaleInfo(id);

            if (saleInfo.OK)
            {
                if (string.IsNullOrEmpty(saleInfo.PaymServOrderId))
                    return RedirectToAction("SorryNotPayedYet");

                string alias = string.Empty;

                var check = DataService.CheckIsOrderPayed(saleInfo.PaymServOrderId, out alias);

                if (check && saleInfo.Status == SaleStatus.Aside) // Mike 2017-08-10
                {
                    var posted = DataService.PostSalesOrder(saleInfo);

                    if (!posted)
                    {
                        DataService.ReportAboutCrash("Ошибка разноски заказа", saleInfo.PaymServOrderId + " " + saleInfo.SalesId);
                        return RedirectToAction("PaymentError");
                    }
                    else
                    {
                        var user = UserManager.FindById(saleInfo.AspId);

                        //DataService.SendMail(DataService.GetMailRobotParams(), user.Email, "Заказ " + saleInfo.SalesId + " в интернет - магазине krasivo.pro готов к оплате", "Здравствуйте, "+user.FirstName+ "! \r\n Ваш заказ " + saleInfo.SalesId + " в интернет - магазине krasivo.pro успешно собран.Вы можете оплатить его на сайте");                
                        var mailRobotParams = DataService.GetMailRobotParams();
                        var paymentMessage = DataService.GeneratePaymentMail(saleInfo, user);
                        DataService.SendMail(mailRobotParams, user.Email, paymentMessage.Subject, paymentMessage.Body, null, true);
                        DataService.SendSingleSMS(user.PhoneNumber.Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty), "Поступила оплата по заказу " + saleInfo.SalesId + " в интернет-магазине krasivo.pro. Спасибо!");

                        var paymentMessageSupport = DataService.GeneratePaymentMailSupport(saleInfo, user, mailRobotParams.AdminConsole);
                        DataService.SendMail(mailRobotParams, mailRobotParams.Pickup, paymentMessageSupport.Subject, paymentMessageSupport.Body, null, true);

                        if (saleInfo.DeliveryTypeId == DeliveryTypeStatic.SelfDeliveryTypeId && !string.IsNullOrWhiteSpace(saleInfo.SelfDeliveryPointMail))
                        {
                            var shopMessage = DataService.GeneratePaymentMailShop(saleInfo);
                            DataService.SendMail(mailRobotParams, saleInfo.SelfDeliveryPointMail, shopMessage.Subject, shopMessage.Body, null, true);
                        }
                    }
                }
                else
                {
                    //пишем, что нет, заказ не оплачен
                    return RedirectToAction("SorryNotPayedYet");
                }
                return RedirectToAction("OrderList");
            }
            else
            {
                return RedirectToAction("PaymentError");
            }
        }
        public ActionResult PaymentError()
        {
            return View();
        }

        public ActionResult SorryNotPayedYet()
        {
            return View();
        }


        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            ViewBag.Title = "Krasivo.pro - смена пароля";
            ViewBag.Description = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";
            ViewBag.Keywords = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";

            return View(model);
        }


        [Authorize]
        public ActionResult OrderList()
        {
            var userId = User.Identity.GetUserId();
            var model = new List<OrderViewModel>();
            var strUrl = DataService.GetUrl(Request);
            model = DataService.GetOrdersListByUserId(userId, strUrl);

            ViewBag.Title = "Krasivo.pro - мои заказы";
            ViewBag.Description = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";
            ViewBag.Keywords = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";

            return View(model);
        }

        public ActionResult ReturnToBasket(string id)
        {
            var saleInfo = new SaleInfo(id);
            var order = saleInfo.ConvertToOrderViewModel();
            if (!saleInfo.OK)
            {
                throw new HttpException(404, "Вам сюда нельзя");
            }
            //проверяем наш ли этот заказ
            var userId = User.Identity.GetUserId();

            if (userId != saleInfo.AspId)
            {
                DataService.ReportAboutCrash("Кто-то попытался отредактировать чужой заказа", userId + " " + saleInfo.SalesId);
                return RedirectToAction("Index", "Home");
            }

            if (order.Status != SaleStatus.Aside) // && order.Status!= SaleStatus.Picked) 2017-08-10
            {
                throw new HttpException(404, "Вам сюда нельзя");
            }

            ViewBag.Title = "Krasivo.pro - вернуть в корзину";
            ViewBag.Description = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";
            ViewBag.Keywords = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";

            return View(order);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ReturnToBasket(OrderViewModel _order)
        {
            var saleInfo = new SaleInfo(_order.OrderId);
            var order = saleInfo.ConvertToOrderViewModel();
            if (!saleInfo.OK)
            {
                throw new HttpException(404, "Вам сюда нельзя");
            }
            //проверяем наш ли этот заказ
            var userId = User.Identity.GetUserId();

            if (userId != saleInfo.AspId)
            {
                DataService.ReportAboutCrash("Кто-то попытался отредактировать чужой заказ", userId + " " + saleInfo.SalesId);
                return RedirectToAction("Index", "Home");
            }

            if (order.Status != SaleStatus.Aside) /// && order.Status != SaleStatus.Picked) 2017-08-10
            {
                throw new HttpException(404, "Вам сюда нельзя");
            }

            //отменяем заказ
            var ret = DataService.CancelSalesOrder(saleInfo, saleInfo.AspId);

            if (!ret)
                return View("ReturnError");
            else
            {
                //отправляем письмо
                var mailerParams = DataService.GetMailRobotParams();
                var supportMessage = DataService.GenerateCancelMailSupport(saleInfo);
                DataService.SendMail(mailerParams, mailerParams.Pickup, supportMessage.Subject, supportMessage.Body, null, true);
            }

            DataService.Order2Basket(saleInfo.SalesId, userId);

            if (!ret)
            {
                ViewBag.Title = "Krasivo.pro - ошибка";
                ViewBag.Description = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";
                ViewBag.Keywords = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";

                return View("ReturnError");
            }
            return RedirectToAction("Index", "Basket");
        }

        public ActionResult RejectOrder(string id)
        {
            var saleInfo = new SaleInfo(id);
            var order = saleInfo.ConvertToOrderViewModel();
            if (!saleInfo.OK)
            {
                throw new HttpException(404, "Вам сюда нельзя");
            }
            //проверяем наш ли этот заказ
            var userId = User.Identity.GetUserId();

            if (userId != saleInfo.AspId)
            {
                DataService.ReportAboutCrash("Кто-то попытался отредактировать чужой заказа", userId + " " + saleInfo.SalesId);
                return RedirectToAction("Index", "Home");
            }

            ViewBag.Title = "Krasivo.pro - Отмена заказа";
            ViewBag.Description = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";
            ViewBag.Keywords = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";

            return View(order);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult RejectOrder(OrderViewModel order)
        {
            var saleInfo = new SaleInfo(order.OrderId);

            if (!saleInfo.OK)
            {
                throw new HttpException(404, "Вам сюда нельзя");
            }
            //проверяем наш ли этот заказ
            var userId = User.Identity.GetUserId();

            if (userId != saleInfo.AspId)
            {
                DataService.ReportAboutCrash("Кто-то попытался вернуть чужой заказ", userId + " " + saleInfo.SalesId);
                return RedirectToAction("Index", "Home");
            }

            if (saleInfo.Status == SaleStatus.Cancel || saleInfo.Status == SaleStatus.Received || saleInfo.Status == SaleStatus.Storno || saleInfo.Status == SaleStatus.Delivered)
            {
                DataService.ReportAboutCrash("Кто-то попытался отменить заказ не в том статусе", userId + " " + saleInfo.SalesId);
                return RedirectToAction("Index", "Home");
            }

            var ret = DataService.RejectSalesOrder(saleInfo, saleInfo.AspId);

            if (!ret)
            {
                ViewBag.Title = "Krasivo.pro - ошибка";
                ViewBag.Description = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";
                ViewBag.Keywords = "Интернет магазин Krasivo.pro. Профессиональная косметика известных брендов";

                return View("ReturnError");
            }
            else
            {
                //отправляем письмо
                var mailerParams = DataService.GetMailRobotParams();
                var supportMessage = DataService.GenerateCancelMailSupport(saleInfo);
                DataService.SendMail(mailerParams, mailerParams.Pickup, supportMessage.Subject, supportMessage.Body, null, true);

                if ((saleInfo.PaymentTypeId == PaymentTypeId.Card && saleInfo.Status != SaleStatus.Aside) || (saleInfo.PaymentTypeId == PaymentTypeId.Cash))
                {
                    //
                    if (saleInfo.DeliveryTypeId == DeliveryTypeStatic.SelfDeliveryTypeId && !string.IsNullOrWhiteSpace(saleInfo.SelfDeliveryPointMail))
                    {
                        var shopMessage = DataService.GenerateCancelMailShop(saleInfo);
                        DataService.SendMail(mailerParams, saleInfo.SelfDeliveryPointMail, shopMessage.Subject, shopMessage.Body, null, true);
                    }

                }
            }
            return RedirectToAction("OrderList", "Cabinet");
        }



        public ActionResult RepeatOrder(string id)
        {
            var saleInfo = new SaleInfo(id);
            var order = saleInfo.ConvertToOrderViewModel();
            if (!saleInfo.OK)
            {
                throw new HttpException(404, "Вам сюда нельзя");
            }
            //проверяем наш ли этот заказ
            var userId = User.Identity.GetUserId();

            if (userId != saleInfo.AspId)
            {
                DataService.ReportAboutCrash("Кто-то попытался отредактировать чужой заказа", userId + " " + saleInfo.SalesId);
                return RedirectToAction("Index", "Home");
            }

            DataService.Order2Basket(saleInfo.SalesId, userId);

            return RedirectToAction("Index", "Basket");
        }

        public ActionResult CancelOrder(string id)
        {
            var saleInfo = new SaleInfo(id);
            var order = saleInfo.ConvertToOrderViewModel();
            if (!saleInfo.OK)
            {
                throw new HttpException(404, "Вам сюда нельзя");
            }
            //проверяем наш ли этот заказ
            var userId = User.Identity.GetUserId();

            if (userId != saleInfo.AspId)
            {
                DataService.ReportAboutCrash("Кто-то попытался отменить чужой заказ", userId + " " + saleInfo.SalesId);
                return RedirectToAction("Index", "Home");
            }

            if (order.Status == SaleStatus.None || order.Status == SaleStatus.Cancel)
            {
                DataService.ReportAboutCrash("Кто-то попытался отменить отменённый заказ", userId + " " + saleInfo.SalesId);
                throw new HttpException(404, "Вам сюда нельзя");
            }

            if (order.Status == SaleStatus.Aside || order.Status == SaleStatus.Picked)
            {
                //просто отменяем
                DataService.CancelSalesOrder(saleInfo, saleInfo.AspId);
                return RedirectToAction("OrderList", "Cabinet");
            }
            if (order.Status == SaleStatus.Post || order.Status == SaleStatus.SentToDelivery || order.Status == SaleStatus.Delivered)
            {
                //делаем возврат
            }

            return RedirectToAction("Index", "Basket");
        }

        [NonAction]
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }
    }
}