﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Classes;
using WebApplication1.Models.ViewModels;

namespace WebApplication1.Controllers
{
    public class SaleActionController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
            var model = new List<SaleActionViewModel>();
            try
            {
                var strUrl = DataService.GetUrl(Request);
                model = DataService.GetActionsCatalog4HomePage(strUrl, 0); //вытаскиваем вообще все акции
                ViewBag.Title = "Рекламные акции и специальные предложения";
            }
            catch
            {
                model = new List<SaleActionViewModel>();
            }
            return View(model);
        }

        public ActionResult Details(string Id)
        {
            //id здесь slug на самом деле, чтобы было красивенько
            var model = new SaleActionViewModel();
            try
            {
                string userId;

                if (Request.IsAuthenticated)
                    userId = User.Identity.GetUserId();
                else
                    userId = Request.AnonymousID;


                var strUrl = DataService.GetUrl(Request);
                model = DataService.GetSaleActionInfoBySlug(Id, strUrl);

                var selectedItems = new List<string>();

                selectedItems = DataService.GetPublicityItemsList(model.PublicityJournalId);

                model.PublicityItems = DataService.GetCatalogByItemList(selectedItems, true, strUrl, selectedItems.Count, true, userId, 0, true);

                ViewBag.Title = model.Title;

                ViewBag.Description = "Интернет-магазин профессиональной косметики Индустрия красоты. ";

                if (!string.IsNullOrWhiteSpace(model.ShortDescription))
                    ViewBag.Description = model.ShortDescription;

                ViewBag.Keywords = "Интернет-магазин Индустрия красоты. Профессиональная косметика. Интернет-магазин профессиональной косметики, купить профессиональную косметику в Ростове, купить профессиональную косметику, krasivo.pro, красиво.про, интернет-магазин Индустрия красоты Ростов, красиво про, купить в Ростове-на-Дону, купить в Ростове, ";

                if (!string.IsNullOrWhiteSpace(model.Meta))
                    ViewBag.Keywords += model.Meta;
            }
            catch
            {
                RedirectToAction("Index");
            }

            return View(model);
        }
    }
}