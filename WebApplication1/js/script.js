var baseurl;
//var globalcounter = 0;
$(document).ready(function () {

    //console.log(++globalcounter);
    baseurl = $("#baseurl").attr("base-url");

    /*screen*/
    width = screen.width; // ширина  
    height = screen.height; // высота

    widthW = document.body.clientWidth; // ширина браузера  
    heightW = document.body.clientHeight; // высота браузера

    //alert(widthW + " " + heightW);
    console.log(widthW + " " + heightW);

    $('select#sort_type').styler();
    $('select#DeliveryType_TypeItems_0__SelectedCityId').styler();
    $('select#DeliveryType_TypeItems_0__ModeItems_0__SelectedSelfDeliveryPointId').styler();
    $('select.cdek_pickup_select').styler();
    $('select.m_addr').styler();
    $('.prop_value').styler();
    $('.radio').styler();
    $('.select_date').styler();
    $('.agree').styler();

    $(".value_list").mCustomScrollbar();

    $(".m_phone").mask("+7(999) 999-99-99");

    /* popups*/
    $('.call_order').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#name',

        callbacks: {
            beforeOpen: function() {
                if($(window).width() < 700) {
                    this.st.focus = false;
                } else {
                    this.st.focus = '#name';
                }
            }
        }
    });
    $('.feedback').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#fio',

        callbacks: {
            beforeOpen: function () {
                if ($(window).width() < 700) {
                    this.st.focus = false;
                } else {
                    this.st.focus = '#fio';
                }
            }
        }
    });

    $('.menu_link').magnificPopup({
        type: 'inline',
        preloader: false
    });

    var tmpAddr = $('#DeliveryType_TypeItems_1__ModeItems_0__SelectedSelfDeliveryPointId').val();
    $('#' + tmpAddr).css('display', 'block');

    $('body').on('change', '#DeliveryType_TypeItems_1__ModeItems_0__SelectedSelfDeliveryPointId', function () {
        var tmpValue = $(this).val();
        $('.cdek_pickup_addr').css('display', 'none');
        $('#' + tmpValue).css('display', 'block');
        $('#DeliveryType_TypeItems_1__ModeItems_0__SelectedSelfDeliveryPointId').trigger('refresh');
    });


    $('.delivery_list').find('.r_item').removeClass('active');
    var tmp_dtype = $('.delivery_list').find('.jq-radio.checked');
    tmp_dtype.parent('.r_item').addClass('active');

    $('.shops_block').on('click', '.region_title', function (e) {
        e.preventDefault();
        $(this).closest('.shops_block').toggleClass('active');
    });

    toggleModes();

    /* send forms */
    $('#call_order').on('click', 'input[type="submit"]', function (e) {
       
        $('#call_order').find('div.agree').each(function (i, elem) {
            if (!$(this).hasClass('checked')) {
                $(this).parent('label').addClass('err');
                $(this).focus(function () { });
            }
            else $(this).parent('label').removeClass('err');
        });

        $('#call_order').find('.field').each(function (i, elem) {
            if ($(this).val().trim() == "") {
                $(this).parent('label').addClass('err');
                $(this).focus(function () { });
            }
            else $(this).parent('label').removeClass('err');
        });

        if (!$('#call_order label').hasClass("err")) {

            var msgType = $('#call_order input[name = "frm_type"]').val();
            var msgName = $('#call_order input[name = "name"]').val();
            var msgPhone = $('#call_order input[name = "phone"]').val();

            loaderOn();

            //отправляем запрос       
            var action = baseurl + $('#call_order').attr('action');

            $.ajax({
                url: action,
                data: ({ messageType: msgType, name: msgName, phoneNumber: msgPhone }),
                global: false,
                type: "POST",
                dataType: "text",
                async: false,
               
                success: function (data) {
                    //console.log(data);
                    $('#call_order').children('.error').hide();
                    
                    $('#call_order').children('.fields').hide();

                    $('#call_order').children('.success').show();
                    
                    loaderOff();
                },
                fail: function (data) {
                    $('#call_order').children('.success').hide();
                    $('#call_order').children('.error').show();

                    loaderOff();
                }
            });
        }
        
        return false;
    });

    $('#feedback').on('click', 'input[type="submit"]', function (e) {

        $('#feedback').find('div.agree').each(function (i, elem) {
            if (!$(this).hasClass('checked')) {
                $(this).parent('label').addClass('err');
                $(this).focus(function () { });
            }
            else $(this).parent('label').removeClass('err');
        });

        $('#feedback').find('.field').each(function (i, elem) {
            if ($(this).val().trim() == "") {
                $(this).parent('label').addClass('err');
                $(this).focus(function () { });
            }
            else $(this).parent('label').removeClass('err');
        });

        if (!$('#feedback label').hasClass("err")) {

            var msgType = $('#feedback input[name = "frm_type"]').val();
            var msgName = $('#feedback input[name = "name"]').val();
            var msgPhone = $('#feedback input[name = "phone"]').val();
            var msgEmail = $('#feedback input[name = "email"]').val();
            var msgMess = $('#feedback textarea[name = "message"]').val();

            loaderOn();
       
            var action = baseurl + $('#feedback').attr('action');

            $.ajax({
                url: action,
                data: ({ messageType: msgType, name: msgName, phoneNumber: msgPhone, email: msgEmail, message: msgMess }),
                global: false,
                type: "POST",
                dataType: "text",
                async: false,

                success: function (data) {
                    //console.log(data);
                    $('#feedback').children('.error').hide();

                    $('#feedback').children('.fields').hide();

                    $('#feedback').children('.success').show();

                    loaderOff();
                },
                fail: function (data) {
                    $('#feedback').children('.success').hide();
                    $('#feedback').children('.error').show();

                    loaderOff();
                }
            });
        }

        return false;
    });


    /*actions slider*/
    if ($(".special_slider > .item").length > 1) {
        $(".special_slider").find(".item").css("display", "block");
        $('.special_slider').bxSlider({
            auto: true,
            adaptiveHeight: true,
            speed: 2000,
            pause: 6000,
            controls: false,
            /*onSliderLoad: function () {
                $(".special_slider").find(".item").css("display", "block"); // Показываем все слайды
            }*/
        });
    }
    


    /* brand slider */
    $('.brands_slider').bxSlider({
        slideWidth: 201,
        minSlides: 1,
        maxSlides: 4,
        moveSlides: 4,
        slideMargin: 75,
        speed: 2000,
        controls: false
    });

    $('.bxslider').bxSlider({
        pagerCustom: '#bx-pager',
        controls: false,
        slideWidth:800
    });

    resetQuantity();

    if ($('.orders_list').length) {
        $('.orders_item').on('click', '.toggle_details', function (e) {
            e.preventDefault();
            $(this).toggleClass('active');
            $(this).closest('.orders_item').find('.item_details').toggleClass('active');
        });
    }

    if ($('.with_filter').length)
    {
        getCurrCount("0", "-9999px");
        currFilter(getFiltStr());
    }
        
    
    if ($('.search_page').length) getCurrSearchCount("-9999px", "0");

    toggleFilterGroup();

    //Mike 2017-11-13
    //basketUpdate();

    if ($('.co_attr').length) {
        getCurrPoints();
    }


    /* questions */
    $('.question_list').on('click', '.title a', function(e) {
        e.preventDefault();
        $(this).parents('.title').siblings('.answer').toggleClass('active');
    });



    /* loading products part */
    $('.with_filter').on('click', '.show_more', function (e) {
        e.preventDefault();

        var currPos = $(".item_num").last().val();

        loaderOn();

        var fs = getFiltStr();

        //console.log(currPos.trim());
        $(".more_block").load(baseurl + $(".tmpFilter").attr("data-products-url") + '?' + fs + '&num2start=' + currPos.trim(), function () {

            $(".more_block").replaceWith($(".more_block").html());

            if ($(".count_text").children("span").text().trim() > $("body").find(".product_item").length) {
                $(".more_block").addClass("vis");
            }
            else $(".more_block").removeClass("vis");

            loaderOff();

        });

    });

    /* loading products part on search_page */
    $('.search_page').on('click', '.show_more', function (e) {
        e.preventDefault();

        var currPos = $(".item_num").last().val();

        loaderOn();

        var qstr = $('#QueryStr').val().trim();

        //console.log(currPos.trim());
        $(".more_block").load(baseurl + $(".tmpFilter").attr("data-search-products-url") + '?text=' + qstr + '&num2start=' + currPos.trim(), function () {

            $(".more_block").replaceWith($(".more_block").html());

            if ($(".count_text").children("span").text().trim() > $("body").find(".product_item").length) {
                $(".more_block").addClass("vis");
            }
            else $(".more_block").removeClass("vis");

            loaderOff();

            //console.log("onPage: " + $("body").find(".product_item").length);
        });

    });


    /* filter */
    $('.filter_list').on('click', 'span', function(e) {
        e.preventDefault();
        $(this).parents('.item').toggleClass('active');
    });

    var tmpCount = $('.checked').length;

    $('.filter_list').on('click', 'label', function(e) {
        e.preventDefault();

        if($(this).children('.prop_value').hasClass('checked')) tmpCount++;
        else tmpCount--;
        $('.count_value').text(tmpCount);

        hideCountBlock();

        getCurrCount($(this).offset().left + $(this).width() + 5, $(this).offset().top - 6);

    });


    //////////
    //show btn
    //////////
    $("body").on('click', ".do_filt", function(e) {
        e.preventDefault();

        var fs = getFiltStr();

        loaderOn();


        $(".products_list").load(baseurl + $(".tmpFilter").attr("data-products-url") + '?' + fs, function () {

            if ($(".count_text").children("span").text().trim() > $("body").find(".product_item").length) {
                $(".more_block").addClass("vis");
            }
            //2017-10-25
            $(".count_block").removeClass("active").css("top", "-9999px");
            currFilter(fs); //актуальный фильтр  

            setLocation(baseurl + $(".tmpFilter").attr("data-index-url") + "/?" + fs);

            loaderOff();
        });

    });


    //crear btn
    $(".clear_filt").click(function(e) {
        e.preventDefault();
        //$.cookie('currentFilter', null);
        document.location.href = baseurl + $(".tmpFilter").attr("data-index-url");

    });



    //////////////////
    // add2wishlist //
    //////////////////
    $("body").on("click", ".add2wishlist", function (e) {
        e.preventDefault();
        var currId = $(this).attr("data-item-id");
        add2WishList(currId);
    });

    //////////////////
    // move2basket ///
    //////////////////
    $(".wishlist_block").on("click", ".move2basket", function (e) {
        var currId = $(this).attr("data-id").trim();
        move2basket(currId);
    });
    
    /////////////////////
    // delFromWishList // 
    /////////////////////
    $(".wishlist_block").on("click", ".delFromWishList", function (e) {
        var currId = $(this).attr("data-id").trim();
        delFromWishList(currId);
    });


    ////////////////
    // add2basket //
    ////////////////
    $("body").on("click", ".add2basket", function (e) {
        e.preventDefault();
        console.log(this);
        var currId = $(this).attr("data-item-id").trim();
        var currPrice = $(this).parent(".adds").siblings("a").find(".price").text().trim();
        add2basket(currId, 1, currPrice, 0);
    });

    $("body").on("click", ".add2basket2", function (e) {
        e.preventDefault();
        var currId = $(".product_page_code > span").text().trim();
        var currCount = $(this).siblings(".quantity_block").children(".quantity").text().trim();
        var currPrice = $(this).siblings(".price_block").children(".price").text().trim();
        add2basket(currId, currCount, currPrice, 0);
    });

    ///////////////////
    // delFromBasket //
    ///////////////////
    $("body").on("click", ".delFromBasket", function (e) {
        e.preventDefault();
        var currId = $(this).attr("data-id").trim();
        delFromBasket(currId);
    });

    ////////////////////
    // moveToWishlist //
    ////////////////////
    $("body").on("click", ".moveToWishlist", function (e) {
        e.preventDefault();
        var currId = $(this).attr("data-id").trim();
        moveToWishlist(currId);
    });

    /////////////////
    // changeCount in basket //
    /////////////////
    $(".order_list").on("click", ".counter-minus:not(.dis)", function (e) {
        var currId = $(this).attr("data-id").trim();
        var count = "-1";
        editBasket(currId, count);
        
    });
    $(".order_list").on("click", ".counter-plus:not(.dis)", function (e) {
        var currId = $(this).attr("data-id").trim();
        var count = "1";
        editBasket(currId, count);
        
    });



    /////////////////////////////
    // changeCount in presents //
    /////////////////////////////
    if ($("#GivePresentsByAction").length) {
        var currCount;
        $("#GivePresentsByAction").on("click", ".counter:not(.dis)", function (e) {
            var currOrderId = $(this).attr("data-order-id").trim();
            var currJournalId = $(this).attr("data-action-id").trim();
            var currItemId = $(this).attr("data-id").trim();
            var count;
            if ($(this).hasClass("counter-minus"))
                count = "-1";
            else count = "1";

            //console.log(currOrderId + " " + currJournalId + " " + currItemId + " " + count);

            givePresent(currOrderId, currJournalId, currItemId, count, $(this));
            //console.log(tmpResult);
            /*if (parseInt(givePresent(currOrderId, currJournalId, currItemId, count)) > 0) {
                
                currCount = $(this).siblings(".quantity").text();
                currCount = currCount + count;
                $(this).siblings(".quantity").text(currCount);
                alert("Подарок добавлен к заказу!");
            }
            else {
                $(this).addClass("dis");
                alert("Ошибка! Подорок не может быть добавлен.");
            }*/
        });
    }
    

    $(function () {
        
        //выбор города ИК
        $('.city_line').on('change', 'select', function (e) {
            e.preventDefault();
            //получаем пункты выдачи

            if ($(this).val() == 0) return false;

            loaderOn();

            var _salesId = $('#OrderId').val();
            var _cityId = $(this).val();

            var action = baseurl + '/Basket/GetSelfDeliveryPointsPartial';

            $.ajax({
                url: action,
                data: ({ salesId: _salesId, cityId: _cityId }),
                global: false,
                async: false,
                type: "POST",
                dataType: "text",
                complete: function (data) {
                    //console.log("Запрос отправлен" + " Ответ:");
                    //console.log(data);
                },
                success: function (data) {
                    currValues = data.split(";");
                    if (data.length > 0) {
                        $('.pickup_line').children('.cdek_pickup_select').remove();
                        /*$('.delivery_ammount .value').html(currValues[0].toString() + '<i class="fa fa-rub" aria-hidden="true"></i>');
                        $('.all_ammount .value').html(currValues[1].toString() + '<i class="fa fa-rub" aria-hidden="true"></i>');

                        $('select.m_addr').trigger('refresh');*/

                        $('.pickup_line').children('#IK_CostMessage').before(data);
                        $('.pickup_line').children('.cdek_pickup_select').styler();

                        var tmpPointId = $('body').find('#DeliveryType_TypeItems_0__ModeItems_0__SelectedSelfDeliveryPointId').val();

                        getCurrPointComment(tmpPointId);
                    }

                    loaderOff();
                },
                fail: function (data) {

                }
            });

        });

        $('.pickup_line').on('change', 'select', function (e) {
            e.preventDefault();
            //получаем пункты выдачи
            if ($(this).val() == 0) return false;

            getCurrPointComment($(this).val());

            //запишем выбраную точку
            //$.cookie('currPoint', $(this).val());

            document.cookie = "currPoint=" + $(this).val();
        });


        $('body').on('change', 'input.dm_radio', function (e) {
            e.preventDefault();
            
            if($(this).prop('checked')) {

                toggleModes();
            }
        });

        $('input[type="radio"]').on('change', function(e){
            e.preventDefault();
            if($(this).prop('checked')) {
                $('.r_item').removeClass('active');
                $(this).parents('.r_item').addClass('active');
                $('.select_date').trigger('refresh');

                toggleModes();
            }
        });

        $('.addr_list').on('change', 'input[type="radio"]', function(e){
            e.preventDefault();
            if($(this).prop('checked')) {
                $('.addr_item').removeClass('active');
                $(this).parents('.addr_item').addClass('active');
                $('.select_date').trigger('refresh');
            }
        });

        $('.payment_block').on('change', 'input[type="radio"]', function(e){
            e.preventDefault();
            if($(this).prop('checked')) {
                $('.payment_item').removeClass('active');
                $(this).parents('.payment_item').addClass('active');
            }
        });

        //addr_change
        $("select.m_addr").change(function () {
            if ($(this).val() == 0) return false;

            $('#selfDeliveryPointId').val($(this).val());

            loaderOn();
            
            var currValues = 0;
            var _salesId = $('#OrderId').val();
            var _deliveryTypeId = $('#deliveryTypeId').val();
            var _selfDeliveryPointId = $('#selfDeliveryPointId').val();

            var action = baseurl + '/Basket/GetOrderAndDeliveryAmount';

            $.ajax({
                url: action,
                data: ({ salesId: _salesId, deliveryTypeId: _deliveryTypeId, selfDeliveryPointId: _selfDeliveryPointId }),
                global: false,
                async: false,
                type: "POST",
                dataType: "text",
                complete: function (data) {
                    //console.log("Запрос отправлен" + " Ответ:");
                    //console.log(data);
                },
                success: function (data) {

                    currValues = data.split(";");
                    if (currValues[0] > 0) {
                        $('.delivery_ammount .value').html(currValues[0].toString() + '<i class="fa fa-rub" aria-hidden="true"></i>');
                        $('.all_ammount .value').html(currValues[1].toString() + '<i class="fa fa-rub" aria-hidden="true"></i>');

                        $('select.m_addr').trigger('refresh');
                    }

                    loaderOff();
                },
                fail: function (data) {

                }
            });
            //alert($(this).val());
        });
    });

    
    /* скролбар для группы свойств в фильтре */
    $('.value_list').mCustomScrollbar();


    /* установка количества для товара */
    $('.product').on('click', '.counter', function () {

        var minValue = 1;
        var maxValue = parseInt($(this).parent('.quantity_block').attr('data-max-value'));
        
        var currValue = parseInt($(this).siblings('.quantity').text());
        var tmpParent = $(this).parent('.quantity_block');

        if($(this).hasClass('dis')) return;

        if($(this).hasClass('counter-minus') && currValue > minValue)
        {
            currValue--;
            $(this).siblings('.quantity').text(currValue);
        }
        if ($(this).hasClass('counter-plus') && currValue < maxValue)
        {
            currValue++;
            $(this).siblings('.quantity').text(currValue);
        }

        if(currValue == minValue) $(tmpParent).children('.counter-minus').addClass('dis');
        else $(tmpParent).children('.counter-minus').removeClass('dis');

        if(currValue == maxValue) $(tmpParent).children('.counter-plus').addClass('dis');
        else $(tmpParent).children('.counter-plus').removeClass('dis');

    });


    /**
    * СДЭК
	* автокомплит
	* подтягиваем список городов ajax`ом, данные jsonp в зависмости от введённых символов
	*/
    $("#DeliveryType_TypeItems_1__SelectedCityName").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: baseurl + "/Basket/GetCDEKCityList",
                dataType: "json",
                async: false,
                data: {
                    name_startsWith: function () {
                        return $("#DeliveryType_TypeItems_1__SelectedCityName").val()
                    }
                },
                success: function (data) {
                    console.log(data);
                    response($.map(data.geonames, function (item) {
                        return {
                            label: item.CityName,
                            value: item.CityName,
                            id: item.CityId
                        }
                    }));
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            //$('#receiverCityId').val(ui.item.id);
            $('.currCityId').val(ui.item.id);
            $('.currCityName').val(ui.item.label);
            //console.log(ui.item);
        },
        focus: function (event, ui) { return false;}

    });

    $('.delivery_block').on('click', '.cdek_calc', function (e) {
        e.preventDefault();

        loaderOn();

        //отправляем запрос       
        var action = baseurl + '/Basket/GetCDEKPartial';
        var _salesId = $(this).siblings('.currOrderId').val();
        var _CDEKCityId = $(this).siblings('.currCityId').val();
        //var _CityName = $(this).siblings('.currCityName').val();

        $.ajax({
            url: action,
            data: ({ salesId: _salesId, CDEKCityId: _CDEKCityId/*, CityName: _CityName*/ }),
            global: false,
            async: true,
            type: "POST",
            dataType: "text",
            complete: function (data) {
                //console.log("Запрос отправлен" + " Ответ:");
                //console.log(data);
            },
            success: function (data) {
                $('.CDEK_DeliveryModes').html(data);
                $('select.cdek_pickup_select').styler();

                var tmpAddr = $('#DeliveryType_TypeItems_1__ModeItems_0__SelectedSelfDeliveryPointId').val();
                $('#' + tmpAddr).css('display', 'block');
                $('.radio').styler();

                toggleModes();

                loaderOff();
            },
            fail: function (data) {

            }
        });
    });

});

function getCurrPoints() {
    //получаем пункты выдачи

    loaderOn();

    var _salesId = $('#OrderId').val();
    var _cityId = $('.city_line').find('select').val();

    var action = baseurl + '/Basket/GetSelfDeliveryPointsPartial';

    $.ajax({
        url: action,
        data: ({ salesId: _salesId, cityId: _cityId }),
        global: false,
        async: false,
        type: "POST",
        dataType: "text",
        complete: function (data) {
            //console.log("Запрос отправлен" + " Ответ:");
            //console.log(data);
        },
        success: function (data) {
            if (data.length > 0) {
                $('.pickup_line').find('.cdek_pickup_select').remove();

                $('.pickup_line').children('#IK_CostMessage').before(data);
                
                var ckPointId = get_cookie("currPoint");

                $("select#DeliveryType_TypeItems_0__ModeItems_0__SelectedSelfDeliveryPointId [value='" + ckPointId + "']").attr("selected", "selected");
                var tmpPointId = $('body').find('#DeliveryType_TypeItems_0__ModeItems_0__SelectedSelfDeliveryPointId').val();

                $('.pickup_line').find('.cdek_pickup_select').styler();

                getCurrPointComment(tmpPointId);
            }

            loaderOff();
        },
        fail: function (data) {

        }
    });
}
function getCurrPointComment(pointId) {
    if (pointId.length < 2) return false;
    
    loaderOn();

    var _selfDeliveryPointId = pointId;

    
    var action = baseurl + '/Basket/GetMessageAboutCostAndPeriodIKSelfDeliveryPoint';

    $.ajax({
        url: action,
        data: ({ selfDeliveryPointId: _selfDeliveryPointId }),
        global: false,
        async: false,
        type: "POST",
        dataType: "text",
        complete: function (data) {
            //console.log("Запрос отправлен" + " Ответ:");
            //console.log(data);
        },
        success: function (data) {

            if (data.length > 0) {
                $('.pickup_line').children('#IK_CostMessage').html(data);
            }

            loaderOff();
        },
        fail: function (data) {

        }
    });
}

function toggleModes() {
    $('.CDEK_DeliveryModes').find('.CDEK_DeliveryModes_item').removeClass('active');
    var tmp_dtype = $('.CDEK_DeliveryModes').find('.jq-radio.checked');
    tmp_dtype.parent('.CDEK_DeliveryModes_item').addClass('active');

    $('.CDEK_DeliveryModes_item').children('.sub_info').css('display', 'none');
    $('.CDEK_DeliveryModes_item.active').children('.sub_info').css('display', 'block');
}
function loaderOn() {
    $("#status").fadeIn();
    $("#preloader").fadeIn();
}
function loaderOff() {
    setTimeout(function() {
        $("#status").fadeOut();
        $("#preloader").fadeOut();
    }, 1000);
}

function getCurrCount(_left, _top) {

    if ($("div").is(".count_text")) {
        var fs = getFiltStr();
        loaderOn();

        $(".count_text").children("span").load(baseurl + $(".tmpFilter").attr("data-filter-url") + '?' + fs, function () {

            //console.log("currFilter: " + $(".count_text").children("span").text().trim());
            

            if ($(".count_text").children("span").text().trim() > $("body").find(".product_item").length) {
                $(".more_block").addClass("vis");
            }
            loaderOff();
        });
    }
    

    setTimeout(function () {
        $('.count_block').css("left", _left).css("top", _top).addClass('active');
    }, 1000);
}


function getCurrSearchCount(_left, _top) {

    if ($("div").is(".count_text")) {
        loaderOn();

        var qstr = $('#QueryStr').val().trim();

        $(".count_text").children("span").load(baseurl + $(".tmpFilter").attr("data-search-url") + '?text=' + qstr, function () {

            //console.log("currFilter: " + $(".count_text").children("span").text().trim());


            if ($(".count_text").children("span").text().trim() > $("body").find(".product_item").length) {
                $(".more_block").addClass("vis");
            }
            loaderOff();
        });
    }


    setTimeout(function () {
        $('.count_block').css("left", _left).css("top", _top).addClass('active');
    }, 1000);
}

function hideCountBlock() {
    $('.count_block').removeClass('active');
}

function toggleFilterGroup() {
    $('.value_item.check').closest('.item').addClass('active');
}


/*wishlist*/
function add2WishList(_itemId) {
    if (_itemId != "") {

        loaderOn();

        //отправляем запрос       
        var url = $(this).attr('action');

        var action = baseurl + '/WishList/AddToWishList';

        $.ajax({
            url: action,
            data: ({ itemId: _itemId}),
            global: false,
            async: false,
            type: "POST",
            dataType: "text",
            complete: function (data) {
                //console.log("Запрос отправлен" + " Ответ:");
                //console.log(data);
            },
            success: function (data) {
                loaderOff();
            },
            fail: function (data) {

            }
        });
    }
}

function move2basket(_itemId) {
    if (_itemId != "") {

        //отправляем запрос       
        var url = $(this).attr('action');

        var action = baseurl + '/WishList/MoveToBasket';

        $.ajax({
            url: action,
            data: ({ itemId: _itemId }),
            global: false,
            async: false,
            type: "POST",
            dataType: "text",
            complete: function (data) {
                //console.log("Запрос отправлен" + " Ответ:");
                //console.log(data);

                location.href = baseurl +  "/cart";
            }
        });
    }   
}

function delFromWishList(_itemId) {
    if (_itemId != "") {
        
        //отправляем запрос       
        var url = $(this).attr('action');

        var action = baseurl + '/WishList/RemoveFromWishList';

        $.ajax({
            url: action,
            data: ({ itemId: _itemId }),
            global: false,
            async: false,
            type: "POST",
            dataType: "text",
            complete: function (data) {
                //console.log("Запрос отправлен" + " Ответ:");
                //console.log(data);

                location.href = baseurl + "/WishList";
            }
        });
    }
}


/*presents*/
function givePresent(_currOrderId, _currJournalId, _currItemId, _count, _currCounter) {
    var currCounter = _currCounter;
    var tmpParent = currCounter.parents(".quantity_block_p");
    var currCount = currCounter.siblings(".quantity");  //текущее кол-во товара
    var allSumm = parseInt($("#allPoints").text());     //всего баллов по акции
    var currPrice = tmpParent.attr("data-price");      //стоимость товара в баллах
    var currSumm = parseInt($("#currPoints").text());    //уже использовано баллов

    if (_currOrderId != "" && _currJournalId != "" && _currItemId != "" && _count != "") {

        loaderOn();

        var action = baseurl + '/Basket/GivePresent';

        $.ajax({
            url: action,
            data: ({ orderId: _currOrderId, journalId: _currJournalId, itemId: _currItemId, qty: _count }),
            global: false,
            async: false,
            type: "POST",
            dataType: "text",
            complete: function (data) {
                //console.log("Запрос отправлен" + " Ответ:");
                //console.log(data);
            },
            success: function (data) {
                //текущее кол-во для товара
                var tmpCount = parseInt(currCounter.siblings(".quantity").text()) + parseInt(_count);

                //если получилось изменить
                if (data > 0) {
                    currCount.text(tmpCount); //обновим число в количестве

                    //теперь обновим поле "использовано баллов"
                    if (currCounter.hasClass("counter-minus")) $("#currPoints").text(currSumm - parseInt(currPrice));
                    else $("#currPoints").text(currSumm + parseInt(currPrice));

                    if (allSumm == parseInt($("#currPoints").text())) $(".counter-plus").addClass("dis");
                    else $(".counter-plus").removeClass("dis");

                    if (tmpCount > 0) {
                        if (currCounter.hasClass("counter-minus"))
                            currCounter.removeClass("dis");
                        else currCounter.siblings(".counter-minus").removeClass("dis");
                    }
                    else {
                        //отключаем кнопку "-"
                        if (currCounter.hasClass("counter-minus")) {
                            currCounter.addClass("dis");
                            currCounter.siblings(".counter-plus").removeClass("dis");
                        }
                        else {
                            currCounter.siblings(".counter-minus").addClass("dis");
                            currCounter.removeClass("dis");
                        }                        
                    }

                }
                else {
                    currCounter.addClass("dis");
                }

                

                loaderOff();
                
            },
            fail: function (data) {

            }
        });

    }
}


/*basket*/
function add2basket(_itemId, _count, _price, _overwriteQty) {

    if (_itemId != "" && _price != "") {

        loaderOn();

        //отправляем запрос       
        var url = $(this).attr('action');

        var action = baseurl + '/Basket/AddToBasket';

        $.ajax({
            url: action,
            data: ({ itemId: _itemId, qty: _count, price: _price, overwriteQty: _overwriteQty }),
            global: false,
            async: false,
            type: "POST",
            dataType: "text",
            complete: function (data) {
                //console.log("Запрос отправлен" + " Ответ:");
                //console.log(data);
            },
            success: function (data) {
                //пересчет малой корзины
                basketUpdate();
                loaderOff();
            },
            fail: function (data) {

            }
        });
    }
}

function editBasket(_itemId, _count, _price, _overwriteQty) {

    if (_itemId != "") {

        //отправляем запрос       
        var url = $(this).attr('action');

        var action = baseurl + '/Basket/AddToBasket';

        $.ajax({
            url: action,
            data: ({ itemId: _itemId, qty: _count, price: _price, overwriteQty: _overwriteQty }),
            global: false,
            async: false,
            type: "POST",
            dataType: "text",
            complete: function (data) {
                //console.log("Запрос отправлен" + " Ответ:");
                //console.log(data);
            },
            success: function (data) {
                //пересчет малой корзины
                //basketUpdate();

                location.href = baseurl +  "/cart";
                
            },
            fail: function (data) {

            }
        });
    }
}

function delFromBasket(_itemId) {

    if (_itemId != "") {

        //отправляем запрос       
        var url = $(this).attr('action');

        var action = baseurl + '/Basket/RemoveFromBasket';

        $.ajax({
            url: action,
            data: ({ itemId: _itemId }),
            global: false,
            async: false,
            type: "POST",
            dataType: "text",
            complete: function (data) {
                //console.log("Запрос отправлен" + " Ответ:");
                //console.log(data);
            },
            success: function (data) {
                location.href = baseurl + "/cart";
            },
            fail: function (data) {

            }
        });
    }
}


function moveToWishlist(_itemId) {

    if (_itemId != "") {

        //отправляем запрос       
        var url = $(this).attr('action');

        var action = baseurl + '/Basket/MoveBasketToWishList';

        $.ajax({
            url: action,
            data: ({ itemId: _itemId }),
            global: false,
            async: false,
            type: "POST",
            dataType: "text",
            complete: function (data) {
                //console.log("Запрос отправлен" + " Ответ:");
                //console.log(data);
            },
            success: function (data) {
                location.href = baseurl + "/cart";
            },
            fail: function (data) {

            }
        });
    }
}


function basketUpdate() {

    var currValues = 0;
    var url = $(this).attr('action');

    var action = baseurl + '/Basket/GetBasketParams';

    $.ajax({
        url: action,
        global: false,
        async: false,
        type: "POST",
        dataType: "text",
        complete: function (data) {
            //console.log("Запрос отправлен" + " Ответ:");
            //console.log(data);
        },
        success: function (data) {
            //пересчет малой корзины
            currValues = data.split(";");
            //console.log(currValues[0].toString());
            if (currValues[0] > 0) {
                //$(".basket_content").html('<a href="/Krasivo.local/cart">Товаров: ' + currValues[0].toString() + ' на сумму: ' + currValues[1].toString() + "</a>");
                //$(".cart_summ").html("Итого: " + currValues[1].toString() + "руб.");

                $(".sm_cart_count").html(currValues[0].toString());
            }
            else $(".sm_cart_count").html("0");

        },
        fail: function (data) {

        }
    });
}


function getFiltStr() {
    var fstr = "";

    //тут надо парсить фильтр и передавать его в контроллер
    $('.prop_value').each(function (i, elem) {
        if ($(this).prop('checked')) {

            if (fstr == "")
                fstr += $(this).attr("name") + "=" + $(this).attr("value");
            else
                fstr += "&" + $(this).attr("name") + "=" + $(this).attr("value");

        }
    });

    return encodeURI(fstr);
}


function currFilter(_currentFilterString) {

    $(".tmpFilter").load(baseurl + $(".tmpFilter").attr("data-filter-items") + '?' + _currentFilterString, function () {
        var tmpData = $(".tmpFilter").text().split(',');

        $.each($(".item_group"), function (index, value) {
            var currGroup = $(this);
            var tmpCount = $(currGroup).find(".value_item").length;
            
            
            $.each($(currGroup).find("input.prop_value"), function (index, value) {
                var filtItem = $(this);
                var tmpStr = filtItem.attr("name") + "=" + filtItem.attr("value");

                if ($.inArray(tmpStr, tmpData) == -1) {
                    //console.log("tmpStr - " + tmpStr.toString() + ", tmpData - " + tmpData.toString());
                    filtItem.closest(".value_item").addClass("dis");
                }
                else {
                    filtItem.closest(".value_item").removeClass("dis");
                }
            });

            if ($(currGroup).find(".value_item.dis").length == tmpCount) {
                $(this).parent("ul").addClass("dis");
            }
            else {
                $(this).parent("ul").removeClass("dis");
            }
        });

    });
}


function setLocation(curLoc) {
    try {
        history.pushState(null, null, curLoc);
        return;
    } catch (e) { }
    location.hash = '#' + curLoc;
}


(function ($) {
    /*tabs*/
    $(function() {

        $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
            $(this)
                .addClass('active').siblings().removeClass('active')
                .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
        });

    });

})(jQuery);


/* reset активности кнопок +- */
function resetQuantity() {
    
    $.each($(".quantity_block"), function (index, value) {

        var minValue = 1;
        var maxValue = parseInt($(this).attr('data-max-value'));
        
        var currValue = parseInt($(this).children('.quantity').text());
        var tmpParent = $(this);

        if ($(this).hasClass('dis')) return;

        if (currValue == minValue) $(tmpParent).children('.counter-minus').addClass('dis');
        else $(tmpParent).children('.counter-minus').removeClass('dis');

        if (currValue == maxValue || currValue > maxValue) $(tmpParent).children('.counter-plus').addClass('dis');
        else $(tmpParent).children('.counter-plus').removeClass('dis');
    });
}

function get_cookie(cookie_name) {
    var results = document.cookie.match('(^|;) ?' + cookie_name + '=([^;]*)(;|$)');

    if (results)
        return (unescape(results[2]));
    else
        return null;
}

$(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() != 0) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });

    $('#toTop').click(function () {
        $('body,html').animate({ scrollTop: 0 }, 800);
    });
});